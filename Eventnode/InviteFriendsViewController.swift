//
//  InviteFriendsViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class InviteFriendsViewController: UIViewController {

    @IBOutlet var evenNameText : UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addSubview(wakeUpImageView)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation


    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        self.navigationController?.popViewControllerAnimated(false)
    }
    
    @IBAction func viewTapped(sender : AnyObject) {
        evenNameText.resignFirstResponder()
    }
}
