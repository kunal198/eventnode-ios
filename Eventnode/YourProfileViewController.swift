//
//  YourProfileViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class YourProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate,UITextFieldDelegate  {

    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var saveOrEditBtn: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var fullNametextfield: UITextField!
    @IBOutlet weak var buttonName: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
  
    @IBOutlet weak var subView: UIView!

    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var loaderSubView: UIView!
    
    var isToBeSaved = false
    let imagePicker = UIImagePickerController()
   

    var resultDict = NSDictionary()
    var name = NSString()
    var email = NSString()
    var last_Name = NSString()
    var currentUserId: String!
    override func viewDidLoad() {
        self.view.addSubview(wakeUpImageView)
        
        fullNametextfield.delegate = self
        emailTextField.delegate = self
        
        fullNametextfield.enabled = false
        emailTextField.enabled = false
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Connecting..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        loaderSubView.layer.cornerRadius = 10
        
        self.loaderView.hidden = true
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
       
        super.viewDidLoad()
        imagePicker.delegate = self
        self.navigationController?.navigationBarHidden = true
        profileImageView.layer.cornerRadius = profileImageView.frame.size.width/2
        profileImageView.layer.masksToBounds = true
       
        fullNametextfield.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
        fullNametextfield.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        emailTextField.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
        emailTextField.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        

        downloadProfileImage()
        
profileData()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        emailTextField.adjustsFontSizeToFitWidth = true
        fullNametextfield.resignFirstResponder()
        emailTextField.resignFirstResponder()
        
        
        let fileManager = NSFileManager.defaultManager()
        let imagePath = "\(documentDirectory)/\(currentUserId).png"
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
            
            
           // self.profileImageView.image = UIImage(data: fileManager.contentsAtPath(imagePath)!)
            buttonName.setTitle("Replace Image", forState: UIControlState.Normal)
        }
        else
        {
            //self.profileImageView.image = UIImage(named: "default.png")
            buttonName.setTitle("Add Image", forState: UIControlState.Normal)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        
    }
    
    func keyboardWillShow(sender: NSNotification) {
        self.contentView.frame.origin.y = -80
    }

    func keyboardWillHide(sender: NSNotification) {
        self.contentView.frame.origin.y = 70
    }
    
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        fullNametextfield.resignFirstResponder()
        emailTextField.resignFirstResponder()
        return true
    }
    
    @IBAction func saveOrEditClicked(sender: AnyObject) {
        if isToBeSaved
        {
            self.loaderView.hidden = false
            saveOrEditBtn.setTitle("Edit", forState: UIControlState.Normal)
            fullNametextfield.enabled = false
            emailTextField.enabled = false
            
            fullNametextfield.resignFirstResponder()

            isToBeSaved = false
            
            let user = PFUser()
            user.username = emailTextField.text!
            user.email = emailTextField.text!
            user["fullUserName"] = fullNametextfield.text!
            user.objectId = currentUserId
            
            
            let query = PFQuery(className: "LinkedAccounts")
            query.whereKey("emailId", equalTo: user.email!)
            query.findObjectsInBackgroundWithBlock {
                (users: [AnyObject]?, error: NSError?) -> Void in
                if let users = users as? [PFObject]
                {
                    
                    if users.count == 0
                    {
                        self.updateUserInfo(user)
                    }
                    else
                    {
                        if users[0]["isEmailVerified"] as? Bool == true
                        {
                            self.loaderView.hidden = true
                            let alert1 = UIAlertView()
                            alert1.title = "Error"
                            alert1.delegate = self
                            alert1.message = "This email address is already linked with a different Eventnode account. If you want to use it, please unlink it from the other account."
                            alert1.addButtonWithTitle("Ok")
                            alert1.show()
                           
                        }
                        else
                        {
                            users[0].deleteInBackgroundWithBlock{
                                (success: Bool, error: NSError?) -> Void in
                                if (success)
                                {
                                    self.updateUserInfo(user)
                                }
                                else
                                {
                                    self.loaderView.hidden = true
                                    let alert1 = UIAlertView()
                                    alert1.title = "Error"
                                    alert1.delegate = self
                                    alert1.message = "Unknown error occured"
                                    alert1.addButtonWithTitle("Ok")
                                    alert1.show()
                                }
                            }
                        }
                    }
                }
                else
                {
                    self.loaderView.hidden = true
                    let alert1 = UIAlertView()
                    alert1.title = "Error"
                    alert1.delegate = self
                    alert1.message = "Unknown error occured"
                    alert1.addButtonWithTitle("Ok")
                    alert1.show()

                }
            }
        }
        else
        {
            saveOrEditBtn.setTitle("Save", forState: UIControlState.Normal)
            isToBeSaved = true
            
            fullNametextfield.enabled = true
            emailTextField.enabled = true
        }
    }

    func updateUserInfo(user: PFUser) {
        user.saveInBackgroundWithBlock {
            (succeeded: Bool, error: NSError?) -> Void in
            if let error = error
            {
                self.loaderView.hidden = true
                let alert1 = UIAlertView()
                alert1.title = "Error"
                if error.code == 202
                {
                    alert1.message = "Email id \(self.emailTextField.text!) is already taken. Please try with a different email id."
                }
                else
                {
                    alert1.message = "Something went wrong. Please try again later."
                }
                alert1.addButtonWithTitle("Ok")
                alert1.show()
            } else {
                
                AnalyticsModel.instance.updateExistingUserForAnalytics(self.emailTextField.text!)
                    
                self.loaderView.hidden = true
                
                NSUserDefaults.standardUserDefaults().setObject(user.objectId!, forKey: "currentUserId")
                
                NSUserDefaults.standardUserDefaults().setObject(self.emailTextField.text!, forKey: "email")
                NSUserDefaults.standardUserDefaults().setObject("Yes", forKey: "isLoggedIn")
                
                NSUserDefaults.standardUserDefaults().setObject(self.fullNametextfield.text!, forKey: "fullUserName")

                
            }
        }
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addimageButton(sender: AnyObject)
    {
        imagePicker.allowsEditing = true
        presentViewController(imagePicker, animated: true, completion: nil)
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
    }
    
    
    func downloadProfileImage()
    {
//        let s3BucketName = "eventnodepublicpics"
//        
//        let downloadRequest = AWSS3TransferManagerDownloadRequest()
//        
//        downloadRequest.bucket = s3BucketName
//        
//        let downloadFilePath = "\(documentDirectory)/\(currentUserId).png"
//        
//        let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
//        
//        downloadRequest.key  = "\(self.currentUserId)/profilePic/profilePic.png"
//        
//        downloadRequest.downloadingFileURL = downloadingFileURL
//        
//        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//        
////        let tempstr = String("http://d513o5zfkznc5.cloudfront.net/\(self.currentUserId)/profilePic/profilePic.png")
////  //  //
////        let tempurl = NSURL(string: tempstr)
////        
////        
////        var placeHolderImage = UIImage(named:"default.png")
////        
////        profileImageView.sd_setImageWithURL(tempurl, placeholderImage: placeHolderImage)
//        transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
//            
//            if (task.error != nil){
//                if (task.error.domain == AWSS3TransferManagerErrorDomain) {
//                    switch (task.error.code) {
//                    case AWSS3TransferManagerErrorType.Cancelled.rawValue:
//                        self.profileImageView.image = UIImage(named: "default.png")
//                        break;
//                    case AWSS3TransferManagerErrorType.Paused.rawValue:
//                        self.profileImageView.image = UIImage(named: "default.png")
//                        break;
//                        
//                    default:
//                        
//                        self.profileImageView.image = UIImage(named: "default.png")
//                        break;
//                    }
//                } else {
//                    // Unknown error.
//                    
//                    self.profileImageView.image = UIImage(named: "default.png")
//                }
//            }
//            
//            if (task.result != nil) {
//                
//                
//                self.profileImageView.image = UIImage(named: downloadFilePath)
//            }
//            
//            return nil
//            
//        })

        
        let s3BucketName = "eventnodepublicpics"
        let fileName = "profilePic.png"
        
        let downloadFilePath = (documentDirectory as NSString).stringByAppendingPathComponent(fileName)
        let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
        
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        downloadRequest.bucket = s3BucketName
        //println("\(user.objectId!)/profilePic/profilePic.png")
        downloadRequest.key  = "\(currentUserId)/profilePic/profilePic.png"
        downloadRequest.downloadingFileURL = downloadingFileURL
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        
        transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
            
            if (task.error != nil){
                if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                    switch (task.error.code) {
                    case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                        break;
                    case AWSS3TransferManagerErrorType.Paused.rawValue:
                        break;
                        
                    default:
                        //println("error downloading")
                        break;
                    }
                } else {
                    // Unknown error.
                    //println("error downloading")
                }
            }
            
            if (task.result != nil) {
                
                //println("downloading successfull")
               // self.profileImageView.image = UIImage(named: downloadFilePath)
                
            }
            
            return nil
            
        })
        

        
    }

    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        let profilePic = "profilePic.png"
        
        
        
        
        let profilePicUrl = NSURL(fileURLWithPath: "\(documentDirectory)/\(profilePic)")
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        
        profileImageView.image = pickedImage
        
        let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(pickedImage!), 0.5)
        
        let error:NSErrorPointer = NSErrorPointer()
        
        if NSFileManager.defaultManager().fileExistsAtPath("\(documentDirectory)/\(profilePic)") {
            do {
                try NSFileManager.defaultManager().removeItemAtPath("\(documentDirectory)/\(profilePic)")
                print("old image has been removed")
            } catch {
                print("an error during a removing")
            }
        }
        
        
//        do {
//            
//            let profilePic = "profilePic.png"
//            
//           // try NSFileManager.defaultManager().removeItemAtPath("\(documentDirectory)/\(profilePic)")
//            
//        } catch var error1 as NSError
//        {
//           
//        }
        if error != nil {
            print(error.debugDescription)
        }
        else
        {
            originaldata!.writeToURL(profilePicUrl, atomically: true)
            
            
            uploadRequest.bucket = "eventnodepublicpics"
            uploadRequest.key =  "\(self.currentUserId)/profilePic/profilePic.png"
            uploadRequest.body = profilePicUrl
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
            self.uploadProfilePic(uploadRequest)
            
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }

    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func profileData()
    {
        self.emailTextField.text = NSUserDefaults.standardUserDefaults().valueForKey("email") as? String
        self.fullNametextfield.text = NSUserDefaults.standardUserDefaults().valueForKey("fullUserName") as? String
        let imagePath = "\(documentDirectory)/profilePic.png"
        
        
        
        let fileManager = NSFileManager.defaultManager()
        
        if (fileManager.fileExistsAtPath(imagePath))
        {
           self.profileImageView.image = UIImage(named: "")
            self.profileImageView.image = UIImage(contentsOfFile: imagePath)
            buttonName.setTitle("Replace Image", forState: UIControlState.Normal)
        }
        else
        {
            self.profileImageView.image = UIImage(named: "default.png")
            buttonName.setTitle("Add Image", forState: UIControlState.Normal)
        }
    }
    
    func uploadProfilePic(uploadRequest:AWSS3TransferManagerUploadRequest )
    {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error
            {
                if error.domain == AWSS3TransferManagerErrorDomain as String
                {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code)
                    {
                        switch (errorCode)
                        {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden = true
                                self.internetError(uploadRequest)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden = true
                            self.internetError(uploadRequest)
                            
                            break;
                        }
                    }
                    else
                    {
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest)
                        
                    }
                }
                else
                {
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest)
                    
                }
            }
            
            if let exception = task.exception
            {
                self.loaderView.hidden=true
                self.internetError(uploadRequest)
                
            }
            
            if task.result != nil
            {
                
            }
            return nil
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        fullNametextfield.resignFirstResponder()
        emailTextField.resignFirstResponder()
    }
    
    func correctlyOrientedImage(image: UIImage) -> UIImage
    {
        if image.imageOrientation == UIImageOrientation.Up
        {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest)
    {
        let refreshAlert = UIAlertController(title: "Error", message: "Can't upload profile pic to the cloud.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction) in
            self.uploadProfilePic(uploadRequest)
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
}
