//
//  NotificationViewController.swift
//  eventnode
//
//  Created by brst on 7/6/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import AudioToolbox

class NotificationViewController: UIViewController {

    
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textView2: UITextView!
    
    @IBOutlet weak var guestActivityOutlet: UITextView!
    @IBOutlet weak var activityAttendingOutlet: UITextView!
    @IBOutlet weak var invitedOutlet: UITextView!

    
    @IBOutlet weak var someoneInvitedMeAlertCheckBox: UIButton!
    
    @IBOutlet weak var someoneInvitedMeEmailcheckBox: UIButton!
    
    @IBOutlet weak var newactivityImattendingAlertcheckBox: UIButton!
    
    @IBOutlet weak var newactivityImattendingemailcheckBox: UIButton!
    
    @IBOutlet weak var allActivityimhostingAlertCheckBox: UIButton!
    
    @IBOutlet weak var allActivityImhostingEmailCheckBox: UIButton!
    
    var currentUserId: String!
    
    var status = true

    //var  isChecked:Bool = false
    var  isVibrate:Bool!
    var isSoundAlert:Bool!
    
    var notificationStatuses: Array<Bool>!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*@IBOutlet weak var guestActivityOutlet: UITextView!
        @IBOutlet weak var activityAttendingOutlet: UITextView!
        @IBOutlet weak var invitedOutlet: UITextView!
        
        
        @IBOutlet weak var someoneInvitedMeEmailcheckBox: UIButton!
        @IBOutlet weak var someoneInvitedMeAlertCheckBox: UIButton!
        
        @IBOutlet weak var newactivityImattendingAlertcheckBox: UIButton!
        
        @IBOutlet weak var newactivityImattendingemailcheckBox: UIButton!
        
        @IBOutlet weak var allActivityimhostingAlertCheckBox: UIButton!
        
        @IBOutlet weak var allActivityImhostingEmailCheckBox: UIButton!*/
        
        invitedOutlet.frame.size.height = invitedOutlet.sizeThatFits(invitedOutlet.bounds.size).height
        activityAttendingOutlet.frame.size.height = activityAttendingOutlet.sizeThatFits(activityAttendingOutlet.bounds.size).height
        guestActivityOutlet.frame.size.height = guestActivityOutlet.sizeThatFits(guestActivityOutlet.bounds.size).height
        
        
        invitedOutlet.frame.origin.y = someoneInvitedMeAlertCheckBox.frame.origin.y + ((someoneInvitedMeAlertCheckBox.frame.height/2)-(invitedOutlet.frame.height/2))
        
        activityAttendingOutlet.frame.origin.y = newactivityImattendingAlertcheckBox.frame.origin.y + ((newactivityImattendingAlertcheckBox.frame.height/2)-(activityAttendingOutlet.frame.height/2))
        
        guestActivityOutlet.frame.origin.y = allActivityimhostingAlertCheckBox.frame.origin.y + ((allActivityimhostingAlertCheckBox.frame.height/2)-(guestActivityOutlet.frame.height/2))
        
        
        someoneInvitedMeAlertCheckBox.selected = false;
        someoneInvitedMeEmailcheckBox.selected = false;
        newactivityImattendingAlertcheckBox.selected = false
        newactivityImattendingemailcheckBox.selected = false
        allActivityimhostingAlertCheckBox.selected = false
        allActivityImhostingEmailCheckBox.selected = false
      
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 3
        let attributes = [NSParagraphStyleAttributeName : style]
        
        textView.attributedText = NSAttributedString(string: textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

        
        let style2 = NSMutableParagraphStyle()
        style.lineSpacing = 3
        let attributes2 = [NSParagraphStyleAttributeName : style2]
        
        //var sharedApplication = UIApplication()
        //sharedApplication.registerForRemoteNotificationTypes(UIRemoteNotificationType.Sound)
        
        
//        textView2.attributedText = NSAttributedString(string:textView2.text, attributes:attributes2)
//        textView2.font = UIFont(name: "AvenirNext-Medium", size: 10.0)
//        textView2.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)

        
        let user: PFUser = PFUser.currentUser()!
        
        notificationStatuses = []
        
        if user["inviteNotification"] as! Bool == true
        {
            (self.view.viewWithTag(51) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(51) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["inviteEmail"] as! Bool == true
        {
            (self.view.viewWithTag(53) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(53) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["hostActivityNotification"] as! Bool == true
        {
            (self.view.viewWithTag(55) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(55) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["hostActivityEmail"] as! Bool == true
        {
            (self.view.viewWithTag(57) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(57) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["guestActivityNotification"] as! Bool == true
        {
            (self.view.viewWithTag(59) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(59) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["guestActivityEmail"] as! Bool == true
        {
            (self.view.viewWithTag(61) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(61) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }
        
        if user["allowSound"] as! Bool == true
        {
            (self.view.viewWithTag(63) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            notificationStatuses.append(true)
        }
        else
        {
            (self.view.viewWithTag(63) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            notificationStatuses.append(false)
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func notificationToggle(sender : UIButton)
    {
        let index = ((sender.tag-50)/2)-1
        
        
        let installation = PFInstallation.currentInstallation()
        
        installation["userObjectId"] = currentUserId
        
        let user: PFUser = PFUser.currentUser()!
        
        
//        let invitationObject : PFObject = PFq
        
        var tagg :Int = Int()
        
        
       let predicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
//
        let query = PFQuery(className:"Invitations", predicate: predicate)

//        let inviteObject = PFObject(className:"Invitations", predicate: predicate)
        
        
        if notificationStatuses[index] == true
        {
            status = false
            (self.view.viewWithTag(sender.tag-1) as! UIButton).setImage(UIImage(named: "checkbox.png"), forState: .Normal)
        }
        else
        {
            status = true
            (self.view.viewWithTag(sender.tag-1) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
        }
        
        notificationStatuses[index] = status
        
        if sender.tag == 52
        {
            
            
            user["inviteNotification"] = status
            installation["inviteNotification"] = status
        }
        
        if sender.tag == 54
        {
            user["inviteEmail"] = status
            
            tagg = 54
//            invitationObject["inviteEmail"] = status
        }
        
        if sender.tag == 56
        {
            user["hostActivityNotification"] = status
            installation["hostActivityNotification"] = status
            
        }

        if sender.tag == 58
        {
            tagg = 58

            user["hostActivityEmail"] = status
//            invitationObject["hostActivityEmail"] = status
        }
        
        if sender.tag == 60
        {
            user["guestActivityNotification"] = status
            installation["guestActivityNotification"] = status
        }
        
        if sender.tag == 62
        {            tagg = 62

            user["guestActivityEmail"] = status
//            invitationObject["guestActivityEmail"] = status
        }

        
        if sender.tag == 64
        {
            installation["allowSound"] = status
            user["allowSound"] = status
            //(self.view.viewWithTag(63) as! UIButton).setImage(UIImage(named: "check-box.png"), forState: .Normal)
            
            //NSUserDefaults.standardUserDefaults().setBool(true, forKey: "isSoundAlert")
        }
        
        
        
        installation.saveInBackground()
        user.saveInBackground()
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "invitationobjectSuccess:", successSelectorParameters:[tagg,status], errorSelector: "invitationobjectError:", errorSelectorParameters: nil)
        
        
    
        
        
        
        
       /* let refreshAlert = UIAlertController(title: "Coming Soon!", message: "This feature is not available right now.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)*/
        
    }
    
    func invitationobjectSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let tag = timer.userInfo?.valueForKey("external") as! [AnyObject]
        print(objects)
       // var tmpButton = self.view.viewWithTag(tag[0]) as? UIButton

        if let invitationObjects = objects
        {
            for inviteObjects in invitationObjects
            
            {
                print(inviteObjects)
                print(tag[1])
                
                
                                   if tag[0] as! Int == 54
                                    {
                                        inviteObjects["inviteEmail"] = tag[1]
                                    }
            
            
                                    if tag[0] as! Int == 58
                                    {
                                        inviteObjects["hostActivityEmail"] = tag[1]
                                    }
            
            
                                    if tag[0] as! Int == 62
                                    {
                                        inviteObjects["guestActivityEmail"] = tag[1]
                                    }
                                       inviteObjects.saveInBackground()
                                }
            
                            }

        
        
    }
    
    func invitationobjectError(timer:NSTimer)
    {
        
    }
    
    
    @IBAction func backButton(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addReplybutton(sender: AnyObject)
    {
        
    }
    
   
}
