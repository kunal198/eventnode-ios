//
//  UserEmailSettings.swift
//  Eventnode
//
//  Created by brst on 4/6/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//

import UIKit

class invitedEmail: NSObject
{
    
    func getInviteMailStatus(email:String)->Bool{
        var BoolValue = Bool()
        
        let emailPredicate = NSPredicate(format: "email = '\(email)'")
        
        let emailQuery = PFUser.queryWithPredicate(emailPredicate)
        
        //   let currentStatusTemp = self.currentStatus
        
        emailQuery!.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            ////println(users!.count)
            if let users = users as? [PFUser]
            {
                for user in users
                {
                    
                    print(user["inviteEmail"])
                    BoolValue = user["inviteEmail"] as! Bool
                    
                }
            }
            else
            {
                print(error?.description)
            }
        }
        
        print(BoolValue)
        return BoolValue
    }
    
}
