//
//  DynamicTableViewCell.swift
//  DynamicControlsTableView
//
//  Created by mrinal khullar on 5/15/15.
//  Copyright (c) 2015 mrinal khullar. All rights reserved.
//

import UIKit

class MyEventTableViewCell: UITableViewCell
{
        
    
    @IBOutlet weak var dynamicLabel: UILabel!
    
    @IBOutlet weak var dynamicImage: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
