//
//  ChannelEventsViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 2/11/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//

import UIKit

class ChannelEventsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var onboardingchannelScreen: UIView!
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var wakeUpImage: UIImageView!
    @IBOutlet weak var myChannelTableView: UITableView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var invitedChannelsLbl: UILabel!
    @IBOutlet weak var MyChannelsLbl: UILabel!
    
    
    
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x:105, y: 250, width: 100, height: 150),
        type: .BallScaleMultiple, color: UIColor(red: 220/255.0, green: 203/255.0, blue: 85/255.0, alpha: 1.0), size: CGSize(width: 100, height: 100))
    var loadingMessage = UILabel()
    var currentUserId = ""
    var fullUserName: String!
    var timer = NSTimer()
    var Deletetimer = NSTimer()
    var inPersonEventCount = Int()
    var invitedEventsCounts = Int()
    
    var redirect: Bool! = false
    var redirectFromInviteView = Bool()
    var isRedirected: Bool! = true
    var inPersonCount = Int()
    var messagesRef: Firebase!
    var badgesArray = NSMutableArray()
    var badgeschatArray = NSMutableArray()
    var eventCreatorId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

       // wakeUpImage.clipsToBounds = true
        
        
        showInviteCodePopup = false
        getInPersonCount()
        getInvitedCount()
        invitedChannelsLbl.text = "Invited Channels"
     //   MyEventsHighlightedView.hidden = false
        
       // myChannelTableView.backgroundColor = UIColor.clearColor()
        
        activityIndicatorView.center = CGPointMake(view.frame.width/2, view.frame.height/2)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimation()
        
        activityIndicatorView.hidesWhenStopped = true
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        indicator.startAnimating()
        
        //https://bnc.lt/PYyc/7GJIhWwIPn
        /*
        var data = [
        "deepLinkType": "createEvent"
        ]
        
        Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
        //
        if error == nil
        {
        var eventCreationURL = url! as String
        
        
        }
        
        })
        */
        
        
        /*var isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "1", whereFields: [])
        if isDeleted {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        
        
        var isDeleted1 = ModelManager.instance.deleteTableData("EventImages", whereString: "1", whereFields: [])
        
        if isDeleted1 {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        
        
        var isDeleted2 = ModelManager.instance.deleteTableData("Invitations", whereString: "1", whereFields: [])
        
        if isDeleted2 {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        
        
        var isDeleted3 = ModelManager.instance.deleteTableData("Notifications", whereString: "1", whereFields: [])
        
        if isDeleted3 {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }*/
        
        loaderSubView.addSubview(loadingMessage)
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
            var isExtraEventDataDeleted = ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId IN (SELECT objectId FROM Events WHERE eventCreatorObjectId != '\(self.currentUserId)' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)')) ", whereFields: [])
            if isExtraEventDataDeleted
            {
                isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Events", whereString: "eventCreatorObjectId != '\(self.currentUserId)' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)')", whereFields: [])
                
                if isExtraEventDataDeleted
                {
                    
                    
                    isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Invitations", whereString: "userObjectId != '\(self.currentUserId)' AND eventObjectId NOT IN (SELECT objectId FROM Events WHERE eventCreatorObjectId = '\(self.currentUserId)')", whereFields: [])
                    
                    if isExtraEventDataDeleted
                    {
                        
                        
                        isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Notifications", whereString: "receiverId != '\(self.currentUserId)'", whereFields: [])
                        
                    }
                    
                }
            }
        }
        //myChannelTableView.separatorStyle = .None
        //myChannelTableView.separatorColor = UIColor.redColor()
        
        //        sadSmily.frame.size.height = 40*self.view.frame.width/320
        //        sadSmily.frame.size.width = 40*self.view.frame.width/320
        
        //        sadSmily.frame.origin.x = (self.view.frame.width/2)-(20*self.view.frame.width/320)
        
        if let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as? String
        {
            self.fullUserName = fullUserName
            
        }
        
        // self.view.addSubview(wakeUpImageView)
        
        
        
//        let date = NSDate()
//        let calendar = NSCalendar.currentCalendar()
       // let components = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: date)
//        let hour = components.hour
//        let minute = components.minute
//        let day = components.day
//        let month = components.month
//        let year = components.year
        
        
        // Do any additional setup after loading the view.
        if(redirect == true)
        {
            redirect = false
            let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
            eventPhototsVC.redirectFromLink = redirectFromInviteView
            eventPhototsVC.isRSVP = "false"
            eventPhototsVC.eventCreatorId = eventCreatorId
            self.navigationController?.pushViewController(eventPhototsVC, animated: false)
        }
        
        
    }
    
    
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "starttimer", name:"eventviewtimer", object: nil)
        starttimer()
        scrollToLoadMore()
        
        
        
        // Save the above plist somewhere in your project. Then use it like this:
        
        /*NSString *activeRecordTimeZoneName = @"American Samoa";
        NSDictionary *timeZoneMap = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ActiveRecord2iOS_Timezones" ofType:@"plist"]] objectForKey:@"TimeZones"];
        return [NSTimeZone timeZoneWithName:[timeZoneMap valueForKey:activeRecordTimeZoneName]];*/
        
        /*if UIApplication.sharedApplication().applicationState == UIApplicationState.Active
        {
        msg = "active"
        }
        
        if UIApplication.sharedApplication().applicationState == UIApplicationState.Background
        {
        msg = "background"
        }
        
        if UIApplication.sharedApplication().applicationState == UIApplicationState.Inactive
        {
        msg = "inactive"
        }
        
        
        var refreshAlert = UIAlertController(title: "Alert", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
        
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)*/
        
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            if(redirect == true)
            {
                redirect = false
                let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelEventsViewController") as! ChannelEventsViewController
                self.navigationController?.pushViewController(eventPhototsVC, animated: false)
            }
            else
            {
                
                if(isUpdated == true)
                {
                    isUpdated = false
                    
                    isRedirected = false
                    myChannelTableView.estimatedRowHeight=240
                }
                
                deleteData()
                downloadData()
                refreshList()
                
                if (myEvents.count > 0)
                {
                    updateData()
                }
                
            }
        }
    }

    
    
    func getInvitedCount()
    {
        var resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["COUNT(*) as count"], whereString:"userObjectId = '\(currentUserId)'", whereFields: [])
        
        resultSet.next()
        
        inPersonEventCount = Int(resultSet.intForColumn("count"))
        
     //   invitedEventsLbl.text = "Invited Events (\(invitedEventsCounts))"
        resultSet.close()
    }
    
    func getInPersonCount()
    {
        let userID = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
       
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["COUNT(*) as count"], whereString: "isRSVP = '0' AND eventCreatorObjectId = '\(userID!)' ", whereFields: [])

        
        resultSet.next()
        
        inPersonEventCount = Int(resultSet.intForColumn("count"))
        
      //  MyEventsLbl.text = "My Events (\(inPersonEventCount))"
        resultSet.close()
    }
    
    
    
    
    
    override func viewDidDisappear(animated: Bool) {
        
        activityIndicatorView.stopAnimation()
        timer.invalidate()
        
//        messagesRef.removeAllObservers()

        Deletetimer.invalidate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    func starttimer()
    {
       // timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("scrollToLoadMore"), userInfo: nil, repeats: true)
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        badgesArray.removeAllObjects()
        downloadData()
        refreshList()
        
    }
    
    // MARK: - loader
    func showLoader(message: String)
    {
      
        
        loadingMessage.text = "\(message)"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
//
        loaderView.hidden = false
    }
    
    func scrollViewDidScroll(_scrollView: UIScrollView){
        let newScroll = CGFloat(_scrollView.contentOffset.y)
        
        if newScroll > _scrollView.contentSize.height - _scrollView.frame.height
        {
            //refreshList()
            scrollToLoadMore()
        }
        
    }
    
    func reloadbadge()
    {
        badgesArray = []
        isEventDataUpDated = false
              let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "isRSVP = '0' AND eventCreatorObjectId = '\(self.currentUserId)' GROUP BY objectId ORDER BY createdAt DESC LIMIT 0, \(myEvents.count)", whereFields: [])
        
        
        
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let userevent = PFObject(className: "Events")
                
                userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                let isRSVP = resultSet.stringForColumn("isRSVP")
                
                if isRSVP == "0"
                {
              
             
                
               
                
                
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                   
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                    
                   
                    self.myChannelTableView.reloadData()
                    
                    // self.finishReceivingMessage()
                })
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
              
                
               
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                   
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray  .insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                   
                    
                    
                    self.myChannelTableView.reloadData()
                    
                    // self.finishReceivingMessage()
                    
                })
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
               
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray.insertObject(dictemp, atIndex: 0)
                    self.badgeschatArray.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                    
                    
                   
                    self.myChannelTableView.reloadData()
                    
                    // self.finishReceivingMessage()
                    
                    
                })
                
                
            }
        }
        
        resultSet.close()
        
           }
    }
    
    func refreshList()
    {
        
        isEventDataUpDated = false
        
        /*var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "eventCreatorObjectId = '\(self.currentUserId)'", whereFields: [])
        
        resultSetCount.next()
        
        var eventCount = resultSetCount.intForColumn("count")
        
        resultSetCount.close()
        
        
        if(eventCount>0)
        {*/
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "isRSVP = '0' AND eventCreatorObjectId = '\(self.currentUserId)' GROUP BY objectId ORDER BY createdAt DESC LIMIT 0, \(myEvents.count)", whereFields: [])
        
        myEvents = []
        
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let userevent = PFObject(className: "Events")
                
                userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                let isRSVP = resultSet.stringForColumn("isRSVP")
                
                if isRSVP == "0"
                {
                    userevent["isRSVP"] = false
                    
                    if let description = resultSet.stringForColumn("eventDescription")
                    {
                        userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                    }
                    else
                    {
                        userevent["eventDescription"] = ""
                    }
                }
                else
                {
                    userevent["isRSVP"] = true
                    
                    userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                    userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                    userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                    userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
                    userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                    
                    userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                }
                userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                
                userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
                
                userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                userevent["frameX"] = resultSet.doubleForColumn("frameX")
                userevent["frameY"] = resultSet.doubleForColumn("frameY")
                userevent["senderName"] = resultSet.stringForColumn("senderName")
                userevent["timezoneName"] = resultSet.stringForColumn("timezoneName")
                //rahuluserevent["socialSharingURL"] = resultSet.stringForColumn("socialSharingURL")
                userevent["socialSharingURL"] = ""
                
                //userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                //userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                
                if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil && resultSet.stringForColumn("objectId") != "hi")
                {
                    userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                    userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                    userevent.objectId = resultSet.stringForColumn("objectId")
                }
                
                let isPosted = resultSet.stringForColumn("isPosted")
                
                if isPosted == "0"
                {
                    userevent["isPosted"] = false
                }
                else
                {
                    userevent["isPosted"] = true
                }
                
                userevent["isUploading"] = false
                
                myEvents.append(userevent)
                
               
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                    
                    self.myChannelTableView.reloadData()
                    // self.finishReceivingMessage()
                })
                                     messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                                    messagesRef.queryLimitedToLast(2000).observeEventType(.ChildRemoved, withBlock: { (snapshot) in
                
                                        self.refreshList()
                                        // self.finishReceivingMessage()
                                    })
                                    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                                    messagesRef.queryLimitedToLast(2000).observeEventType(.ChildRemoved, withBlock: { (snapshot) in
                
                                        self.refreshList()
                                        // self.finishReceivingMessage()
                                    })
                
                
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in

            
                    
                    
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                    
                    self.myChannelTableView.reloadData()
                    
                    // self.finishReceivingMessage()
                    
                    
                })
                //                    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                //                    messagesRef.queryLimitedToLast(1).observeEventType(.ChildRemoved, withBlock: { (snapshot) in
                //
                //                        self.refreshList()
                //                        // self.finishReceivingMessage()
                //                    })
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray.insertObject(dictemp, atIndex: 0)
                    self.badgeschatArray.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                    
                    self.myChannelTableView.reloadData()
                    
                    // self.finishReceivingMessage()
                    
                    
                })
                
            }
        }
        
        resultSet.close()

        self.myChannelTableView.reloadData()
        inPersonEventCount = myEvents.count
        inPersonCount = myEvents.count
        if myEvents.count == 0
        {
             onboardingchannelScreen.hidden = false
             MyChannelsLbl.text = "My Channels"
        }
        else
        {
            
            MyChannelsLbl.text = "My Channels"
            onboardingchannelScreen.hidden = true
        }
        
        /*}
        else
        {
        var query = PFQuery(className:"Events")
        query.whereKey("eventCreatorObjectId", equalTo:"\(currentUserId)")
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllEventsError:", errorSelectorParameters:nil)
        
        }*/
    }
    
    
    func scrollToLoadMore()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "isRSVP = '0' AND eventCreatorObjectId = '\(self.currentUserId)' GROUP BY objectId ORDER BY createdAt DESC LIMIT \(myEvents.count), \(myEvents.count + 10)", whereFields: [])
        
        //myEvents = []
        
        if (resultSet != nil)
        {
            while resultSet.next() {
                
                let userevent = PFObject(className: "Events")
                
                userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                
              

                let isRSVP = resultSet.stringForColumn("isRSVP")
                
                if isRSVP == "0"
                {
                    userevent["isRSVP"] = false
                    
                    if let desc = resultSet.stringForColumn("eventDescription")
                    {
                        userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                    }
                    else
                    {
                        userevent["eventDescription"] = ""
                    }
                    
                    
                }
                else
                {
                    userevent["isRSVP"] = true
                    
                    userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                    userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                    userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                    userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
                    userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                    
                    userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                }
                
                
                userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                
                userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
                
                userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                userevent["frameX"] = resultSet.doubleForColumn("frameX")
                userevent["frameY"] = resultSet.doubleForColumn("frameY")
                userevent["senderName"] = resultSet.stringForColumn("senderName")
                userevent["timezoneName"] = resultSet.stringForColumn("timezoneName")
                //userevent["socialSharingURL"] = resultSet.stringForColumn("socialSharingURL")
                userevent["socialSharingURL"] = ""
                
               // userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
//                userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                
                if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil && resultSet.stringForColumn("objectId") != "hi")
                {
                    userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                    userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                    userevent.objectId = resultSet.stringForColumn("objectId")
                }
                
                let isPosted = resultSet.stringForColumn("isPosted")
                
                if isPosted == "0"
                {
                    userevent["isPosted"] = false
                }
                else
                {
                    userevent["isPosted"] = true
                }
                
                userevent["isUploading"] = false
                
                myEvents.append(userevent)
                
                
            }
            
            resultSet.close()
            //rahul
            //self.tableView.reloadData()
            
        }
    }
    
    
    func downloadData()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "isRSVP = 0 AND eventCreatorObjectId = '\(self.currentUserId)' ORDER BY eventId DESC", whereFields: [])
        
        var eventObjectIds: Array<String>
        
        eventObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                eventObjectIds.append(resultSet.stringForColumn("objectId"))
            }
        }
        
        resultSet.close()
        
        let eventObjectIdsString = eventObjectIds.joinWithSeparator("','")
        
        var predicate = NSPredicate()
        
        var eventObjectIdsStringPredicate = ""
        
        if eventObjectIdsString != ""
        {
            eventObjectIdsStringPredicate = "NOT (objectId IN {'\(eventObjectIdsString)'}) AND eventCreatorObjectId = '\(self.currentUserId)'"
        }
        else
        {
            eventObjectIdsStringPredicate = "eventCreatorObjectId = '\(self.currentUserId)'"
        }
        
        
        
        predicate = NSPredicate(format: eventObjectIdsStringPredicate)
        
        let query = PFQuery(className:"Events", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllEventsError:", errorSelectorParameters:nil)
    }
    
    
    func updateData()
    {
        var predicateString = "eventCreatorObjectId = '\(currentUserId)' AND isUpdated = true"
        
        if let timeZoneName = NSUserDefaults.standardUserDefaults().objectForKey("timeZoneName") as? String
        {
            if NSTimeZone.localTimeZone().name != timeZoneName
            {
                
                
                let localTimeZoneName = NSTimeZone.localTimeZone().name
                
                NSUserDefaults.standardUserDefaults().setObject("\(localTimeZoneName)", forKey: "timeZoneName")
                
                var invitedEventsIds = [String]()
                let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["objectId"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(currentUserId)' )", whereFields: [])
                
                if (resultSet != nil) {
                    while resultSet.next()
                    {
                        invitedEventsIds.append(resultSet.stringForColumn("objectId"))
                    }
                }
                resultSet.close()
                
                if invitedEventsIds.count > 0
                {
                    var invitedEventsIdsString = ""
                    
                    invitedEventsIdsString = invitedEventsIds.joinWithSeparator("','")
                    
                    predicateString = "eventCreatorObjectId = '\(currentUserId)' OR objectId IN {'\(invitedEventsIdsString)'}"
                    
                }
                else
                {
                    predicateString = "eventCreatorObjectId = '\(currentUserId)'"
                }
                
                showLoader("Updating Timezone....")
                
            }
        }
        else
        {
            let localTimeZoneName = NSTimeZone.localTimeZone().name
            NSUserDefaults.standardUserDefaults().setObject("\(localTimeZoneName)", forKey: "timeZoneName")
        }
        
        let updatePredicate = NSPredicate(format: predicateString)
        
        let updateQuery = PFQuery(className:"Events", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchEventUpdatesSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventUpdatesError:", errorSelectorParameters:nil)
    }
    
    
    func deleteData()
    {
        let updatePredicate = NSPredicate(format: "eventCreatorObjectId = '\(currentUserId)'")
        
        let updateQuery = PFQuery(className:"Events", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchExistingEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchExistingEventsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchExistingEventsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        
        if let fetchedobjects = objects
        {
            
            var i = 0
            
            var existingEventObjectIds: Array<String>
            existingEventObjectIds = []
            
            for eventObject in fetchedobjects
            {
                existingEventObjectIds.append(eventObject.objectId!)
            }
            
            let existingEventObjectIdsString = existingEventObjectIds.joinWithSeparator("','")
            
            
            var whereQuery = ""
            
            if existingEventObjectIdsString != ""
            {
                whereQuery = "eventCreatorObjectId = '\(currentUserId)' AND objectId NOT IN ('\(existingEventObjectIdsString)') AND objectId != '' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(currentUserId)')"
            }
            else
            {
                whereQuery = "eventCreatorObjectId = '\(currentUserId)' AND objectId != '' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(currentUserId)')"
            }
            
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["objectId"], whereString: whereQuery, whereFields: [])
            
            
            var nonExistingEventObjectIds: Array<String>
            nonExistingEventObjectIds = []
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    nonExistingEventObjectIds.append(resultSet.stringForColumn("objectId"))
                }
            }
           activityIndicatorView.stopAnimation()
            
            resultSet.close()
            
            let nonExistingEventObjectIdsString = nonExistingEventObjectIds.joinWithSeparator("','")
            
            if nonExistingEventObjectIdsString != ""
            {
                ModelManager.instance.deleteTableData("Events", whereString: "objectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("Invitations", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("PostLikes", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventComments", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                activityIndicatorView.stopAnimation()
                
                refreshList()
            }
        }
    }
    
    
    func fetchExistingEventsError(timer:NSTimer)
    {
        activityIndicatorView.stopAnimation()
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
    }
    
    
    func fetchEventUpdatesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        
        
        
        
       loaderView.hidden = true
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            for eventObject in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                tblFields["eventImage"] = eventObject["eventImage"] as? String
                tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                
                let frameX = eventObject["frameX"] as! CGFloat
                let frameY = eventObject["frameY"] as! CGFloat
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                tblFields["senderName"] = eventObject["senderName"] as? String
                
                tblFields["eventFolder"] = eventObject["eventFolder"] as? String
                
                if(eventObject["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if eventObject["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                       
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if eventObject["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                        
                        tblFields["eventEndDateTime"] = date
                        
                    }
                    
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    
                    let eventLatitude = eventObject["eventLatitude"] as! Double
                    let eventLongitude = eventObject["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                }
                
                tblFields["isPosted"] = "1"
                
                tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                tblFields["timezoneName"] = eventObject["timezoneName"] as? String
                
                var date = ""
                
                if eventObject.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                    
                    tblFields["createdAt"] = date
                }
                
                if eventObject.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                    
                    tblFields["updatedAt"] = date
                }
                
                let eventTimezoneOffset = eventObject["eventTimezoneOffset"] as! Int
                
                tblFields["eventTimezoneOffset"] = "\(eventTimezoneOffset)"
                
                var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [eventObject.objectId!])
                
                fetchedobjects[i]["isUpdated"] = false
                i++
                
            }
            
            refreshList()
            
            PFObject.saveAllInBackground(fetchedobjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    
                }
                else
                {
                    
                }
            }
            
        }
    }
    
    func fetchEventUpdatesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
            }
    
    func refreshContent()
    {
        //        if isEventDataUpDated
        //        {
        refreshList()
        // }
    }
    
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return myEvents.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        
        return tableView.frame.width*(3/4)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("channelCell", forIndexPath: indexPath) as! ChannelViewCell        
        let row = indexPath.row
        
        
     

        
//        myChannelTableView.setSeparatorInset = UIEdgeInsetsMake(0,0,0,0)
        
        for view in cell.contentView.subviews
        {
            //view.removeFromSuperview()
        }
        
        var eventTitleText: String = (myEvents[row]["eventTitle"] as? String)!
        
        eventTitleText = String(eventTitleText.characters.prefix(1)).capitalizedString + String(eventTitleText.characters.suffix(eventTitleText.characters.count - 1))
        
        let eventImageView = UIImageView()
        
        eventImageView.frame = CGRectMake(0, 0, cell.contentView.frame.width + 2, cell.contentView.frame.height )
        
        cell.contentView.addSubview(eventImageView)
        
        let eventImageOverlayView = UIView()
        
        
        eventImageView.contentMode = .ScaleToFill
        
        eventImageOverlayView.frame = CGRectMake(0, 0, cell.contentView.frame.width + 2, cell.contentView.frame.height + 0.1)
        
        eventImageOverlayView.backgroundColor = UIColor.blackColor()
        eventImageOverlayView.alpha = 0.5
        
        cell.contentView.addSubview(eventImageOverlayView)
        
        
        let eventTitleView = UITextView()
        
        eventTitleView.frame = CGRectMake(0.053125*cell.contentView.frame.width,0.5333*cell.contentView.frame.height, 0.665625*cell.contentView.frame.width, 0.126*cell.contentView.frame.height)
        
        
        
        eventTitleView.text = eventTitleText
        
        eventTitleView.frame.size.height = eventTitleView.sizeThatFits(eventTitleView.bounds.size).height
        
        eventTitleView.textColor = UIColor.whiteColor()
        eventTitleView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        eventTitleView.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)
        eventTitleView.editable = false
        
        cell.contentView.addSubview(eventTitleView)
        
        if eventTitleView.contentSize.height > eventTitleView.frame.height {
            eventTitleView.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)
        }
        var pdate: NSDate!
        
        if myEvents[row]["isRSVP"] as! Bool == true
        {
            var eventStartDate = myEvents[row]["eventStartDateTime"] as! NSDate
            
            /*var startTimeStamp = Int64(eventStartDate.timeIntervalSince1970)
            var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
            var eventTimezoneOffset = myEvents[row]["eventTimezoneOffset"] as! Int
            
            var timeStampToBeShown = Int64(startTimeStamp-timezoneOffset+eventTimezoneOffset)
            
            pdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))*/
            
            pdate = myEvents[row]["eventStartDateTime"] as! NSDate
            
           
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: pdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour >= 12)
            {
                if(scomponents.hour > 12)
                {
                    shour = scomponents.hour-12
                }
                else
                {
                    shour = 12
                }
                
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            let startDate = "\(monthsArray[smonth-1]) \(sday), \(shour):\(sminute) \(sam)"
            
            let eventDateView = UITextView()
            
            
            
            eventDateView.frame = CGRectMake(0.053125*cell.contentView.frame.width, eventTitleView.frame.origin.y+eventTitleView.frame.height , 0.665625*cell.contentView.frame.width, 0.11*cell.contentView.frame.height)
            
            eventDateView.text = startDate
            eventDateView.textColor = UIColor.whiteColor()
            eventDateView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
            eventDateView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            eventDateView.editable = false
            
            cell.contentView.addSubview(eventDateView)
        }
        
        
        let eventShareButton = UIButton()
        eventShareButton.frame = CGRectMake(0.78875*cell.contentView.frame.width, 0.5650*cell.contentView.frame.height,0.06*cell.contentView.frame.width+0.15*cell.contentView.frame.width,0.075*cell.contentView.frame.height+0.08333*cell.contentView.frame.height)
        
        eventShareButton.tag = indexPath.row
        
        let eventShareImageView = UIImageView()
        
        eventShareImageView.frame = CGRectMake(0.78875*cell.contentView.frame.width, 0.5650*cell.contentView.frame.height,0.06*cell.contentView.frame.width, 0.075*cell.contentView.frame.height)
        
        eventShareButton.addTarget(self, action: "eventShareButton:", forControlEvents:.TouchUpInside)
        
        
        eventShareImageView.image = UIImage(named:"group_manage.png")
        
        cell.contentView.addSubview(eventShareImageView)
        
        
        var totalCount = 0
        
        var typeofevent = myEvents[row]["isRSVP"] as! Bool
        
        
        
        if myEvents[row]["isRSVP"] as! Bool == true
        {
            if myEvents[row]["isPosted"] as! Bool == true
            {
                let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["SUM(noOfChilds) as childs, SUM(noOfAdults) as adults"], whereString: "attendingStatus = 'yes' AND eventObjectId = ? ", whereFields: [myEvents[row].objectId!])
                
                resultSetCount.next()
                
                totalCount = Int(resultSetCount.intForColumn("childs")) + Int(resultSetCount.intForColumn("adults"))
                
                resultSetCount.close()
            }
        }
        else
        {
            
            if myEvents[row]["isPosted"] as! Bool == true
            {
                let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["count(*) as count"], whereString: "userObjectId != '' AND eventObjectId = ? ", whereFields: [myEvents[row].objectId!])
                
                resultSetCount.next()
                
                totalCount = Int(resultSetCount.intForColumn("count"))
                
                resultSetCount.close()
            }
        }
        
        let eventShareCount = UILabel()
        
        eventShareCount.frame = CGRectMake(0.8625*cell.contentView.frame.width, 0.5650*cell.contentView.frame.height, 0.15*cell.contentView.frame.width, 0.08333*cell.contentView.frame.height)
        
        
        eventShareCount.text = totalCount.description
        eventShareCount.textColor = UIColor.lightGrayColor()
        
        eventShareCount.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        
        cell.contentView.addSubview(eventShareCount)
        
        let eventCellOverlayView = UIView()
        
        eventCellOverlayView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height)
        
        eventCellOverlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        cell.contentView.addSubview(eventCellOverlayView)
        
        let loaderCellView = UIView()
        
        let cellIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderCellView.addSubview(cellIndicator)
        
        loaderCellView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height)
        
        loaderCellView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        
        cellIndicator.frame = CGRectMake((cell.contentView.frame.width/2)-(cell.contentView.frame.width*(25/320)/2), (cell.contentView.frame.height/2)-(cell.contentView.frame.width*(25/320)/2), cell.contentView.frame.width*(25/320), cell.contentView.frame.width*(25/320))
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Downloading..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name:"AvenirNext-Regular", size: 9.0)
        loadingMessage.textAlignment = .Center
        loaderCellView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , cellIndicator.frame.origin.y+(cell.contentView.frame.width*(25/320)+10), cell.contentView.frame.width, 20)
        cellIndicator.startAnimating()
        
        cell.contentView.addSubview(loaderCellView)
        
        let eventImageFile = myEvents[row]["eventImage"] as! String
        
        let eventFolder = myEvents[row]["eventFolder"] as! String
        
        let eventImagePath = "\(documentDirectory)/\(eventImageFile)"
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath(eventImagePath)) {
            let image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
            eventImageView.image = image
            
            loaderCellView.hidden = true
        }
        else
        {
                        let s3BucketName = "eventnodepublicpics"
            let fileName = eventImageFile
            
            let downloadFilePath = (documentDirectory as NSString).stringByAppendingPathComponent(fileName)
            let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
            
            let downloadRequest = AWSS3TransferManagerDownloadRequest()
            downloadRequest.bucket = s3BucketName
            downloadRequest.key  = "\(eventFolder)\(fileName)"
            downloadRequest.downloadingFileURL = downloadingFileURL
            
           
            
            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                
            }
            
            
            eventImageView.backgroundColor = UIColor.lightGrayColor()
            
//            let tempstr = String("http://d513o5zfkznc5.cloudfront.net/\(eventFolder)\(fileName)")
//            let tempurl = NSURL(string: tempstr)
//            eventImageView.sd_setImageWithURL(tempurl, completed: block)
//            
//            loaderCellView.hidden = true
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            
            
            //transferManager.download(downloadRequest)
            
            /*transferManager.download(downloadRequest).continueWithSuccessBlock({
            (task: AWSTask!) -> AWSTask! in
            dispatch_async(dispatch_get_main_queue(), {
            
            var image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
            eventImageView.image = image
            loaderCellView.hidden = true
            })
            return nil
            })*/
            
            
            
            transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                
                if (task.error != nil){
                    if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                        switch (task.error.code) {
                        case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                            break;
                        case AWSS3TransferManagerErrorType.Paused.rawValue:
                            break;
                            
                        default:
                            
                            break;
                        }
                    } else {
                        // Unknown error.
                       
                    }
                }
                
                if (task.result != nil) {
                    
                    
                    
                    let image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
                    eventImageView.image = image
                    loaderCellView.hidden = true
                    
                }
                
                return nil
                
            })
            
        }
        
        let eventOriginalImageFile = myEvents[row]["originalEventImage"] as! String
        
        let eventOriginalImagePath = "\(documentDirectory)/\(eventOriginalImageFile)"
        let managerOriginal = NSFileManager.defaultManager()
        
        if (managerOriginal.fileExistsAtPath(eventOriginalImagePath) != true)
        {
            let s3OriginalBucketName = "eventnodepublicpics"
            let fileOriginalName = eventOriginalImageFile
            
            let downloadOriginalFilePath = (documentDirectory as NSString).stringByAppendingPathComponent(fileOriginalName)
            let downloadingOriginalFileURL = NSURL.fileURLWithPath(downloadOriginalFilePath)
            
            let downloadOriginalRequest = AWSS3TransferManagerDownloadRequest()
            downloadOriginalRequest.bucket = s3OriginalBucketName
            downloadOriginalRequest.key  = "\(eventFolder)\(fileOriginalName)"
            downloadOriginalRequest.downloadingFileURL = downloadingOriginalFileURL
            
            let transferOriginalManager = AWSS3TransferManager.defaultS3TransferManager()
            
            
            transferOriginalManager.download(downloadOriginalRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                
                if (task.error != nil){
                    if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                        switch (task.error.code) {
                        case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                            break;
                        case AWSS3TransferManagerErrorType.Paused.rawValue:
                            break;
                            
                        default:
                            
                            break;
                        }
                    } else {
                        // Unknown error.
                        
                    }
                }
                
                if (task.result != nil) {
                   
                    
                }
                
                return nil
                
            })
            
            
        }
        
        if( myEvents[indexPath.row]["isPosted"] as! Bool == false)
        {
            let uploadButton = UIButton()
            
            uploadButton.frame = CGRectMake(30, cell.contentView.frame.height-45, 30, 30)
            
            uploadButton.setImage(UIImage(named: "upload.png"), forState: UIControlState.Normal)
            
            uploadButton.tag = indexPath.row
            uploadButton.addTarget(self, action:"uploadEvent:",forControlEvents: UIControlEvents.TouchUpInside)
            
            let notUploadedButton = UIButton()
            
            notUploadedButton.frame = CGRectMake(75, cell.contentView.frame.height-38, 23, 23)
            
            notUploadedButton.setImage(UIImage(named: "not_uploaded.png"), forState: UIControlState.Normal)
            
            notUploadedButton.tag = indexPath.row
            notUploadedButton.addTarget(self, action:"notUploaded:",forControlEvents: UIControlEvents.TouchUpInside)
            
            
            cell.contentView.addSubview(uploadButton)
            cell.contentView.addSubview(notUploadedButton)
        }
        
        cell.contentView.addSubview(eventShareButton)
        
        
        //rahul
        
        var totalChatCount = 0
        var totalLikeCount = 0

        
        
        
        let eventShareCountt = UILabel()
        
        eventShareCountt.frame = CGRectMake(0.70875*cell.contentView.frame.width, 0.5650*cell.contentView.frame.height+25, 0.30*cell.contentView.frame.width, 0.087*cell.contentView.frame.height + 0.087*cell.contentView.frame.height)
        eventShareCountt.lineBreakMode = NSLineBreakMode.ByWordWrapping
        eventShareCountt.numberOfLines = 2
        
        
        
        
        
        
        if myEvents[row]["isPosted"] as! Bool == true
        {
            
            let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [myEvents[row].objectId!])
            
            resultSetCount.next()
            
            //totalChatCount = Int(resultSetCount.intForColumn("count"))
            let tempp = badgesArray.valueForKey("eventid") as! NSArray
            let bag = NSCountedSet()
            bag.addObjectsFromArray(tempp as [AnyObject])
            
             totalLikeCount = bag.countForObject(myEvents[row].objectId!)
           
            resultSetCount.close()
        }
        else
        {
            //eventShareCount.text = "Pending Approval"
            eventShareCountt.text = ""
            eventShareCountt.textColor = UIColor.lightGrayColor()
            eventShareCountt.frame.size.height = 2*CGFloat(0.08333*cell.contentView.frame.height)
            eventShareCountt.numberOfLines = 2
        }
        
        
        
        
        
        
        if totalLikeCount > 0
        {
            eventShareCountt.layer.masksToBounds = true
            eventShareCountt.layer.cornerRadius = 3.0
            eventShareCountt.text = "\(totalLikeCount)"
            eventShareCountt.backgroundColor = UIColor.redColor()
            eventShareCountt.textColor = UIColor.whiteColor()
            
            if totalLikeCount < 10
            {
                eventShareCountt.frame.size.width = 20
                eventShareCountt.layer.cornerRadius = eventShareCountt.frame.size.width/2
            }
            else if totalLikeCount < 100
            {
                let labelWidth = eventShareCountt.sizeThatFits(eventShareCountt.bounds.size).width
                eventShareCountt.frame.size.width = labelWidth+14
                eventShareCountt.layer.cornerRadius = 10
            }
            else
            {
                let labelWidth = eventShareCountt.sizeThatFits(eventShareCountt.bounds.size).width
                eventShareCountt.frame.size.width = labelWidth+24
                eventShareCountt.layer.cornerRadius = 10
            }
            
            eventShareCountt.frame.origin.x = (0.70875*cell.contentView.frame.width)+((0.20*cell.contentView.frame.width/2)-(eventShareCountt.frame.width/2))
            
            //eventShareCountt.frame.size.height = CGFloat(0.08333*cell.contentView.frame.height)
            eventShareCountt.frame.size.height = CGFloat(20)
        }
        else
        {
            eventShareCountt.text = ""
        }
        eventShareCountt.textAlignment = .Center
        
        eventShareCountt.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        //attendingStatusImageView.frame.origin.x = eventShareCount.frame.origin.x + ((eventShareCount.frame.width/2)-(attendingStatusImageView.frame.width/2))
        
        
        //        eventShareCountt.frame.origin.x = attendingStatusImageView.frame.origin.x + ((attendingStatusImageView.frame.width/2)-(eventShareCountt.frame.width/2))
        //
        
        cell.contentView.addSubview(eventShareCountt)
        
        eventShareCountt.frame = CGRectMake(eventShareImageView.frame.origin.x, 15, eventShareCountt.frame.size.width, eventShareCountt.frame.size.height)
        
        
        cell.backgroundColor = UIColor.lightGrayColor()
        tableView.separatorStyle = .None
        return cell
    }
    
    
    func eventShareButton(sender:UIButton)
    {
//        if myEvents[sender.tag]["isRSVP"] as! Bool == true
//            
//        {
//            let manageVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageGuestViewController") as! ManageGuestViewController
//            
//            currentEvent = myEvents[sender.tag]
//            
//            self.navigationController?.pushViewController(manageVC, animated: false)
//        }
//        else
//        {
            let friendsVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageFreindsViewController") as! ManageFreindsViewController
            
            currentEvent = myEvents[sender.tag]
            
            self.navigationController?.pushViewController(friendsVC, animated: false)
//        }
    }
    
        func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    
            if (editingStyle == UITableViewCellEditingStyle.Delete) {
    
                let refreshAlert = UIAlertController(title: "Delete Channel", message: "This Channel will be deleted now. This cannot be undone.", preferredStyle: UIAlertControllerStyle.Alert)
    
                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
    
                }))
    
                refreshAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction) in
    
                    self.showLoader("Deleting Channel...")
    
                    let eventToBeDeleted = myEvents[indexPath.row]
    
    
                    if( myEvents[indexPath.row]["isPosted"] as! Bool == false && myEvents[indexPath.row].objectId == nil)
                    {
                        let isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "eventId=?", whereFields: [eventToBeDeleted["eventId"]!])
                        if isDeleted
                        {
                            if eventToBeDeleted.objectId != nil
                            {
                                ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId=?", whereFields: [eventToBeDeleted.objectId!])
                            }
                        }
    
                        myEvents.removeAtIndex(indexPath.row)
    
                        self.loaderView.hidden = true
    
    
                        tableView.reloadData()
                    }
                    else
                    {
                        self.Deletetimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("deleteEventFromParse:"), userInfo: eventToBeDeleted, repeats: false)
                    }
                }))
    
    
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        
        if( myEvents[indexPath.row]["isPosted"] as! Bool == true && myEvents[indexPath.row].objectId != nil)
        {
            let manager = NSFileManager.defaultManager()
            
            let eventImageFile = myEvents[indexPath.row]["eventImage"] as! String
            
            let eventImagePath = "\(documentDirectory)/\(eventImageFile)"
            
            let eventOriginalImageFile = myEvents[indexPath.row]["originalEventImage"] as! String
            
            let eventOriginalImagePath = "\(documentDirectory)/\(eventOriginalImageFile)"
            
//            if !(manager.fileExistsAtPath(eventOriginalImagePath) && manager.fileExistsAtPath(eventImagePath))
//            {
            
                isPostUpdated = true
                
                myEventData.removeAll()
                
            
                
                currentEvent = myEvents[indexPath.row]
                
                let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
                self.navigationController?.pushViewController(eventPhototsVC, animated: true)
//            }
//            else
//            {
                //                var refreshAlert = UIAlertController(title: "Error", message: "Please wait while the images for this event are downloading.", preferredStyle: UIAlertControllerStyle.Alert)
                //
                //                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction!) in
                //
                //                }))
                //
                //                self.presentViewController(refreshAlert, animated: true, completion: nil)
           // }
            
        }
        else
        {
            
        }
    }
    
    func uploadEvent(sender: UIButton)
    {
        self.loaderView.hidden = false
        
        
        if myEvents[sender.tag].objectId != nil
        {
            myEvents[sender.tag]["isNew"] = false
        }
        else
        {
            myEvents[sender.tag]["isNew"] = true
        }
        
        sender.enabled = false
        
        var originalEventLogoFile = myEvents[sender.tag]["originalEventImage"] as! String
        
        let eventLogoFile = myEvents[sender.tag]["eventImage"] as! String
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        let eventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(eventLogoFile))
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
        
        uploadRequest.bucket = "eventnodepublicpics"
        uploadRequest.key =  "\(currentUserId)/eventProfileImages/\(eventLogoFile)"
        uploadRequest.body = eventLogoFileUrl
        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
        upload(uploadRequest, isOriginal: false, insertedId: myEvents[sender.tag]["eventId"] as! Int, eventToBeUploaded: myEvents[sender.tag])
        
    }
    
    func notUploaded(sender: UIButton)
    {
     self.loaderView.hidden = true
        
        let refreshAlert = UIAlertController(title: "Not Uploaded to Cloud", message: "If you just created this and there’s internet connection, then it’s uploading in the background. If not, try uploading manually.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
            
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    func uploadFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int, postToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                               self.loaderView.hidden=true
                                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                       
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                                    }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.createFirstPost(postToBeUploaded, insertedId: insertedId)
                    
                })
            }
            return nil
        }
    }
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int, eventToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                            
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                        
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                    
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    
                    if(isOriginal == true){
                        
                        self.createEvent(eventToBeUploaded, insertedId: insertedId)
                        
                    }
                    else
                    {
                        
                        
                        
                        let originalEventFile = eventToBeUploaded["originalEventImage"] as! String
                        
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        let originalEventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(originalEventFile))
                        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        
                        
                        uploadRequest.bucket = "eventnodepublicpics"
                        uploadRequest.key =  "\(self.currentUserId)/eventProfileImages/\(originalEventFile)"
                        uploadRequest.body = originalEventLogoFileUrl
                        
                        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                        
                        
                        self.upload(uploadRequest, isOriginal: true, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
                    }
                    
                })
            }
            return nil
        }
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int, eventToBeUploaded: PFObject){
        let refreshAlert = UIAlertController(title: "Error", message: "There’s bad or no internet connection. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            self.loaderView.hidden=true
            self.myChannelTableView.reloadData()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction) in
            self.loaderView.hidden=false
            self.upload(uploadRequest, isOriginal: isOriginal, insertedId: insertedId, eventToBeUploaded: eventToBeUploaded)
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    
    func internetErrorForFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int){
        
    }
    
    func createFirstPost(eventObject:PFObject, insertedId: Int){
        
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createFirstPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createFirstPostError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    
    func createEvent(eventObject:PFObject, insertedId: Int)
    {
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createEventSuccess:", successSelectorParameters: insertedId, errorSelector: "createEventError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    func createFirstPostSuccess(timer:NSTimer)
    {
         self.loaderView.hidden=true
        isPostUpdated = true
        
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        let postId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            
            tblFields["updatedAt"] = date
        }
        
        
        
        let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            
            
        } else {
            
            
        }
        
    }
    
    
    func createFirstPostError(timer:NSTimer)
    {
        self.loaderView.hidden = true
    }
    
    
    func createEventSuccess(timer:NSTimer)
    {
        var eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var eventId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            
            tblFields["updatedAt"] = date
        }
        
        
        var data = [
            "eventObjectId": eventObject.objectId!,
            "emailId": "noemail",
            "eventCreatorId": "\(currentUserId)"
        ]
        
        let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS9 = iosVersion >= 9
        let iOS7 = iosVersion >= 8 && iosVersion < 9
        
        if iOS9
        {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            branchUniversalObject.title = "\(eventTitle)"
            branchUniversalObject.contentDescription = "My Content Description"
            
            branchUniversalObject.addMetadataKey("eventObjectId", value:eventObject.objectId!)
            branchUniversalObject.addMetadataKey("emailId", value: "noemail")
            branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
            
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")
            linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")
            linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")
            
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        tblFields["socialSharingURL"] = url! as String
                        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
                        
                        eventObject["socialSharingURL"] = url! as String
                        
                        eventObject.saveInBackground()
                        
                    }
            })
        }
        else
        {
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    
                    tblFields["socialSharingURL"] = url! as String
                    var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
                    
                    eventObject["socialSharingURL"] = url! as String
                    
                    eventObject.saveInBackground()
                }
                
            })
        }
        
        
        
        
        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
        if isUpdated {
            
            
            var localNotification = UILocalNotification()
            
            localNotification.fireDate = NSDate()
            
            localNotification.alertBody = "Congrats you just created an event. Don’t forget to invite your friends & guests."
            
            // localNotification.applicationIconBadgeNumber = 100
            
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            
            
            
            refreshList()
            
            if eventObject["isNew"] as! Bool == true
            {
                
                var tblFieldsPost: Dictionary! = [String: String]()
                
                let originalFileName = eventObject["originalEventImage"] as? String
                
                let originalImageData = UIImage(named: (documentDirectory as NSString).stringByAppendingPathComponent(originalFileName!))
                
                tblFieldsPost["postData"] = originalFileName
                tblFieldsPost["isApproved"] = "0"
                tblFieldsPost["postHeight"] = "\(originalImageData!.size.height)"
                tblFieldsPost["postWidth"] = "\(originalImageData!.size.width)"
                tblFieldsPost["postType"] = "image"
                tblFieldsPost["eventObjectId"] = "\(eventObject.objectId!)"
                tblFieldsPost["eventFolder"] = "\(self.currentUserId)/\(eventObject.objectId!)/"
                
                let insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFieldsPost)
                if insertedId>0
                {
                    _ = AWSS3TransferManager.defaultS3TransferManager()
                    let originalEventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(originalFileName!))
                    let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                    
                    let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(originalImageData!), 0.5)
                    
                    originaldata!.writeToURL(originalEventLogoFileUrl, atomically: true)
                    uploadRequest.bucket = "eventnode1"
                    uploadRequest.key =  "\(self.currentUserId)/\(eventObject.objectId)/\(originalFileName)"
                    uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead

                    uploadRequest.body = originalEventLogoFileUrl

                    var myFirstPost = PFObject(className:"EventImages")
                    myFirstPost["postData"] = originalFileName
                    myFirstPost["postHeight"] = originalImageData!.size.height
                    myFirstPost["postWidth"] = originalImageData!.size.height
                    myFirstPost["postType"] = "image"
                    myFirstPost["eventObjectId"] = eventObject.objectId!
                    myFirstPost["eventFolder"] = "\(self.currentUserId)/\(eventObject.objectId!)/"
                    
                    
                    self.uploadFirstPost(uploadRequest, insertedId: insertedId, postToBeUploaded: myFirstPost)
                }
                else
                {
                    
                }
            }
            
        } else {
            
        }
        
        isUpdated = true
        
        
        
        currentEvent = eventObject;
    }
    
    func createEventError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        let errorObject: NSArray = timer.userInfo?.valueForKey("external") as! NSArray
        let eventObject: PFObject = errorObject[0] as! PFObject
        
        let insertedId: Int = errorObject[1] as! Int
        
        
        
        
        self.loaderView.hidden=true
        
        let refreshAlert = UIAlertController(title: "Error", message: "There’s bad or no internet connection. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            self.refreshList()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction) in
            self.loaderView.hidden=false
            self.createEvent(eventObject, insertedId: insertedId)
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
    
    func fetchAllEventsSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for eventObject in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                tblFields["eventImage"] = eventObject["eventImage"] as? String
                tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                
                let frameX = eventObject["frameX"] as! CGFloat
                let frameY = eventObject["frameY"] as! CGFloat
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                tblFields["senderName"] = eventObject["senderName"] as? String
                
                tblFields["eventFolder"] = eventObject["eventFolder"] as? String
                
                
                if(eventObject["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if eventObject["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                        
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if eventObject["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                        
                        tblFields["eventEndDateTime"] = date
                        
                    }
                    
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    
                    let eventLatitude = eventObject["eventLatitude"] as! Double
                    let eventLongitude = eventObject["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                    
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                    
                    
                    
                    if let desc = eventObject["eventDescription"] as? String
                    {
                        tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    }
                    else
                    {
                        tblFields["eventDescription"] = ""
                    }
                }
                
                tblFields["objectId"] = eventObject.objectId
                tblFields["isPosted"] = "1"
                
                let eventTimezoneOffset = eventObject["eventTimezoneOffset"] as! Int
                
                tblFields["eventTimezoneOffset"] = "\(eventTimezoneOffset)"
                
                tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                tblFields["timezoneName"] = eventObject["timezoneName"] as? String
                
                var date = ""
                
                if eventObject.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                    
                    tblFields["createdAt"] = date
                }
                
                if eventObject.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                    
                    tblFields["updatedAt"] = date
                }
                
                
                
                let insertedId = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                
                if insertedId>0
                {
                    
                }
                else
                {
                    self.loaderView.hidden = true
                    
                }
                
                fetchedEventObjectIds.append(eventObject.objectId!)
                
                i++
            }
            scrollToLoadMore()
            refreshList()
            
            let eventObjectIdsString = fetchedEventObjectIds.joinWithSeparator("','")
            
            if eventObjectIdsString != ""
            {
                
                
                let predicate = NSPredicate(format: "eventObjectId IN {'\(eventObjectIdsString)'}")
                
                let query = PFQuery(className:"EventImages", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
            }
            
            
        }
    }
    
    func fetchAllEventsError(timer:NSTimer)
    {
        activityIndicatorView.stopAnimation()
        
        self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
    }
    
    
    func fetchAllPostsSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        isPostUpdated = false
        
        
        if let fetchedobjects = objects {
            self.loaderView.hidden=true
            for post in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["postData"] = post["postData"] as? String
                
                tblFields["isApproved"] = "0"
                
                let postHeight = post["postHeight"] as! CGFloat
                let postWidth = post["postWidth"] as! CGFloat
                
                tblFields["postHeight"] = "\(postHeight)"
                tblFields["postWidth"] = "\(postWidth)"
                tblFields["eventObjectId"] = post["eventObjectId"] as? String
                
                tblFields["eventFolder"] = post["eventFolder"] as? String
                
                tblFields["postType"] = post["postType"] as? String
                
                tblFields["objectId"] = post.objectId
                tblFields["isPosted"] = "1"
                tblFields["isRead"] = "0"
                
                var date = ""
                
                if post.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.createdAt)!)
                    
                    tblFields["createdAt"] = date
                }
                
                if post.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.updatedAt)!)
                    
                    tblFields["updatedAt"] = date
                }
                var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
            }
           activityIndicatorView.stopAnimation()
            
            self.refreshList()
        }
    }
    
    func fetchAllPostsError(timer:NSTimer)
    {
        activityIndicatorView.stopAnimation()
        
     self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
    }
    
    
    func deleteEventFromParse(timer: NSTimer)
    {
        if MyReachability.isConnectedToNetwork()
        {
            let eventToBeDeleted: PFObject = timer.userInfo as! PFObject
            ParseOperations.instance.deleteData(eventToBeDeleted, target: self, successSelector: "deleteEventSuccess:", successSelectorParameters: nil, errorSelector: "deleteEventError:", errorSelectorParameters:nil)
        }
        else
        {
            
            self.loaderView.hidden=true
            
            let refreshAlert = UIAlertController(title: "Error", message: "Event cannot be deleted as you seem to be offline. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    func deleteEventSuccess(timer:NSTimer)
    {
        let eventToBeDeleted: PFObject = timer.userInfo?.valueForKey("internal") as! PFObject
        
        let   messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/GroupChat/\(eventToBeDeleted.objectId!)")
        
        messagesRef.removeValue()
        
        let isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "eventId=?", whereFields: [eventToBeDeleted["eventId"]!])
        
        if isDeleted
        {
            if eventToBeDeleted.objectId != nil
            {
                ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId=?", whereFields: [eventToBeDeleted.objectId!])
            }
        }
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(eventToBeDeleted.objectId!)'")
        
        let invitationQuery = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(invitationQuery, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: eventToBeDeleted, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        ModelManager.instance.deleteTableData("Events", whereString: "objectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        ModelManager.instance.deleteTableData("Invitations", whereString: "eventObjectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        ModelManager.instance.deleteTableData("PostLikes", whereString: "eventObjectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        ModelManager.instance.deleteTableData("EventComments", whereString: "eventObjectId = '\(eventToBeDeleted.objectId!)'", whereFields: [])
        
        refreshList()
        
        let eventFolder = eventToBeDeleted["eventFolder"] as! String
        let eventFile = eventToBeDeleted["eventImage"] as! String
        
        self.loaderView.hidden = true
        
        let deleteRequest = AWSS3DeleteObjectRequest()
        
        
        deleteRequest.bucket = "eventnodepublicpics"
        deleteRequest.key = "\(eventFolder)\(eventFile)"
        
        let s3 = AWSS3.defaultS3()
        
        s3.deleteObject(deleteRequest).continueWithBlock {
            (task: AWSTask!) -> AnyObject! in
            
            if(task.error != nil){
                
                
                
            }else{
                
                
                
                let eventOriginalFile = eventToBeDeleted["originalEventImage"] as! String
                
                let deleteRequestOriginal = AWSS3DeleteObjectRequest()
                deleteRequestOriginal.bucket = "eventnodepublicpics"
                deleteRequestOriginal.key = "\(eventFolder)\(eventOriginalFile)"
                
                let s3Original = AWSS3.defaultS3()
                
                s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                    (task: AWSTask!) -> AnyObject! in
                    
                    if(task.error != nil){
                       
                    }else{
                        
                    }
                    return nil
                }
                
            }
            return nil
        }
        
        let query = PFQuery(className:"EventImages")
        query.whereKey("eventObjectId", equalTo:eventToBeDeleted.objectId!)
        query.orderByDescending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventsAfterDeleteOriginalSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventsAfterDeleteOriginalError:", errorSelectorParameters:nil)
        
        self.refreshList()
        
    }
    
    func deleteEventError(timer:NSTimer)
    {
        
       self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        let refreshAlert = UIAlertController(title: "Error", message: "Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
        
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
    func fetchEventsAfterDeleteOriginalSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        if let objects = objects {
           self.loaderView.hidden=true
            
            for post in objects
            {
                let postType = post["postType"] as! String
                
                if(postType == "image" || postType == "video")
                {
                    
                    let postFolder = post["eventFolder"] as! String
                    let postImage = post["postData"] as! String
                    
                    let deleteRequestOriginal = AWSS3DeleteObjectRequest()
                    deleteRequestOriginal.bucket = "eventnode1"
                    deleteRequestOriginal.key = "\(postFolder)\(postImage)"
                    
                    let s3Original = AWSS3.defaultS3()
                    
                    s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                        (task: AWSTask!) -> AnyObject! in
                        
                        if(task.error != nil){
                            
                        }else{
                            
                        }
                        
                        return nil
                    }
                }
                
            }
        }
    }
    
    func fetchEventsAfterDeleteOriginalError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
   
    }
    
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let eventToBeDeleted = timer.userInfo?.valueForKey("external") as! PFObject
        
        var fetchedUserEmailIds: Array<String>
        fetchedUserEmailIds = []
        
        
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        let eventTitle = eventToBeDeleted["eventTitle"] as! String
        
        var notificationObjects = [PFObject]()
        
        var notifMessage = ""
        
        if eventToBeDeleted["isRSVP"] as! Bool == true
        {
            notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
        }
        else
        {
            notifMessage = "\(fullUserName) deleted the Channel, \(eventTitle)"
        }
        
        
        
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            
            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    let notificationObject = PFObject(className: "Notifications")
                    
                    if invitation["hostActivityEmail"] as! Bool == true
                    {
                    fetchedUserEmailIds.append(invitation["emailId"] as! String)
                    }
                    notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                    notificationObject["notificationImage"] = "profilePic.png"
                    notificationObject["senderId"] = currentUserId
                    notificationObject["receiverId"] = invitation["userObjectId"] as! String
                    notificationObject["notificationActivityMessage"] = notifMessage
                    notificationObject["eventObjectId"] = eventToBeDeleted.objectId!
                    notificationObject["notificationType"] = "eventdeleted"
                    
                    notificationObjects.append(notificationObject)
                }
            }
            
            
            PFObject.deleteAllInBackground(fetchedobjects)
            
            let eventCreatorObjectId = currentUserId
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //var eventTitle = currentEvent["eventTitle"] as! String
            let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            
            
            var createdAt = ""
            
            let fetchedUserObjectIdsString = fetchedUserObjectIds.joinWithSeparator("','")
            
            
            if fetchedUserObjectIdsString != ""
            {
                
                PFObject.saveAllInBackground(notificationObjects)
                
                
                let eventCreatorId = eventToBeDeleted["eventCreatorObjectId"] as! String
                var data: Dictionary<String, String!> = [
                    "alert" : "\(notifMessage)",
                    "notifType" :  "eventdeleted",
                    "eventObjectId": eventToBeDeleted.objectId!  ,
                    "eventCreatorId" : "\(eventCreatorId)",
                    "badge": "Increment",
                    "sound": "defaut"
                ]
                
                
                var urlString = String()
                
                
                let Device = UIDevice.currentDevice()
                
                let iosVersion = NSString(string: Device.systemVersion).doubleValue
                
                let iOS9 = iosVersion >= 9
                let iOS7 = iosVersion >= 8 && iosVersion < 9
                
                if iOS9
                {
                    let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
                    branchUniversalObject.title = "\(eventTitle)"
                    branchUniversalObject.contentDescription = "My Content Description"
                    
                    branchUniversalObject.addMetadataKey("alert", value: "\(notifMessage)")
                    branchUniversalObject.addMetadataKey("notifType", value: "eventdeleted")
                    branchUniversalObject.addMetadataKey("eventObjectId", value:eventToBeDeleted.objectId!)
                    branchUniversalObject.addMetadataKey("eventCreatorId", value: "\(eventCreatorId)")
                    
                    
                    
                    
                    
                    let linkProperties: BranchLinkProperties = BranchLinkProperties()
                    linkProperties.feature = "sharing"
                    
                    branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                        { (url: String?, error: NSError?) -> Void in
                            if error == nil
                            {
                                urlString = url!
                            }
                            
                            let sendEmailObject = SendEmail()
                        
                            var emailMessage = ""
                            
                            let deleteEmail = DeleteChannel()
                            
                            emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "online", url: urlString)
                            
                            for email in fetchedUserEmailIds
                            {
                                let emailPredicate = NSPredicate(format: "email = '\(email)'")
                                
                                let emailQuery = PFUser.queryWithPredicate(emailPredicate)
                                var BoolValue = Bool()
                                emailQuery!.findObjectsInBackgroundWithBlock {
                                    (users: [AnyObject]?, error: NSError?) -> Void in
                                    
                                    if let users = users as? [PFUser]
                                    {
                                        for user in users
                                        {
                                            print(user["hostActivityEmail"])
                                            BoolValue = user["hostActivityEmail"] as! Bool
                                            
                                            
                                            if BoolValue == true
                                            {
                                                sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                                            }
                                        }
                                    }
                                }
                            }
                            
                    })
                }
                else
                {
                    Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                        
                        if error == nil
                        {
                            
                            
                            urlString = url!
                            
                        }
                        
                        let sendEmailObject = SendEmail()
                        
                        let deleteEmail = DeleteEvent()
                        var emailMessage = ""
                        
                        if eventToBeDeleted["isRSVP"] as! Bool == true
                        {
                            emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "rsvp",url:urlString)
                        }
                        else
                        {
                            emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "online",url:urlString)
                        }
                        
                        
                        for email in fetchedUserEmailIds
                        {
                            let emailPredicate = NSPredicate(format: "email = '\(email)'")
                            
                            let emailQuery = PFUser.queryWithPredicate(emailPredicate)
                            var BoolValue = Bool()
                            emailQuery!.findObjectsInBackgroundWithBlock {
                                (users: [AnyObject]?, error: NSError?) -> Void in
                                
                                if let users = users as? [PFUser]
                                {
                                    for user in users
                                    {
                                        print(user["hostActivityEmail"])
                                        BoolValue = user["hostActivityEmail"] as! Bool
                                        
                                        
                                        if BoolValue == true
                                        {
                                            sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                    })
                    
                }
                
                
                
                var predicateString: String! = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = true"
                
                sendParsePush(predicateString, data: data)
                
                
                data["sound"] = ""
                
                predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = false"
                sendParsePush(predicateString, data: data)
            }
        }
    }
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        let predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        let query = PFUser.queryWithPredicate(predicate)
        
        let push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                
                if let fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        let userObjectId = object.objectId!
                        
                        let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        let query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                
                                if let fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    let query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }
    
    
    @IBAction func sharedEventButtonClicked(sender : AnyObject)
    {
        let channelVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelEventsViewController") as! ChannelEventsViewController
        self.navigationController?.pushViewController(channelVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
        
        /*let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        
        alertVC.haveData = true
        alertVC.alertEventObjectId = "J2Be4nYQA3"
        alertVC.alertNotificationType = "invitation"
        
        self.navigationController?.pushViewController(alertVC, animated: false)*/
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject){
        
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingViewControllerOne") as! settingViewControllerOne
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }
    
    @IBAction func eventItemButtonClicked(sender : AnyObject){
        
        let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
        self.navigationController?.pushViewController(eventPhototsVC, animated: true)
    }
    
    @IBAction func getStartedButtonClicked(sender : AnyObject){
        // Navigate to EventDetailsViewController to initiate createEvent.
        let eventDetailsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        // TODO(geetikak): What does animated to true do ?
        
        eventDetailsVC.isAfterImage = false
        
        self.navigationController?.pushViewController(eventDetailsVC, animated: true)
    }
    
    @IBAction func MyEventsButton(sender: AnyObject)
    {
        
      //  MyEventsHighlightedView.hidden = false
        
    }
    
    
    @IBAction func invitedEventsButton(sender: AnyObject)
    {
        
       // MyEventsHighlightedView.hidden = true
        
        
        
        let sharedEventVC = self.storyboard!.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
        self.navigationController?.pushViewController(sharedEventVC, animated: false)
    }
    
    

    @IBAction func EventsBtn(sender: AnyObject)
    {
        
        let eventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        
        self.navigationController?.pushViewController(eventsVC, animated: false)
    }

    @IBAction func ChannelsBtn(sender: AnyObject)
    {
    }

    @IBAction func settingsBtn(sender: AnyObject)
    {
        
        let settingVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewControllerOne") as! settingViewControllerOne
        self.navigationController?.pushViewController(settingVC, animated: false)
    }
    
    @IBAction func alertsBtn(sender: AnyObject)
    {
        let alertsVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        self.navigationController?.pushViewController(alertsVC, animated: false)
        
    }
    
    
    @IBAction func invitedChannels(sender: AnyObject)
    {
        let channelEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelInvitedViewController") as! ChannelInvitedViewController
        channelEventsVC.inPersonEventCount = inPersonCount
        
        self.navigationController?.pushViewController(channelEventsVC, animated: false)
    }
    
    
    @IBAction func MyChannelsBtn(sender: AnyObject)
    {
        let channelInvitedVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelEventsViewController") as! ChannelEventsViewController
        channelInvitedVC.inPersonEventCount = invitedEventsCounts
        self.navigationController?.pushViewController(channelInvitedVC, animated: false)
    }
    
    
    @IBAction func createChannel(sender: AnyObject)
    {
        let createChannelVC = self.storyboard!.instantiateViewControllerWithIdentifier("CreateChannelViewController") as! CreateChannelViewController
        self.navigationController?.pushViewController(createChannelVC, animated: false)

    }
}
