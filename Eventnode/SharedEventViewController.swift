//
//  SharedEventViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit


var mySharedEvents = [PFObject]()
var currentSharedEvent:PFObject!


var isOnSharedEvents: Bool = false

var isSharedEventsUpdated: Bool = false

class SharedEventViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var getNotificationButton: UIButton!
    @IBOutlet weak var pushPopUpView: UIView!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var wakeUpImage: UIImageView!
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet var tableView: UITableView!
    var messagesRef: Firebase!
    
    @IBOutlet weak var InvitedEventsLbl: UILabel!
    @IBOutlet weak var myEventsLbl: UILabel!
    @IBOutlet weak var inviteBoardingTextView: UITextView!
    @IBOutlet weak var invitedBoardingScreen: UIView!
    @IBOutlet var inviteCode: UIButton!
    
    var currentUserId: String!
    var deepLinkObjectId = String()
    var deepLinkEmail = ""
    var eventCreatorObjectId = ""
    var isApproved = ""
    var invitationObjectId = ""
    var isUpdateComplete = false
    var isDownloadComplete = false
    var isUserNew : Bool = false
    var timer = NSTimer()
    var badgesDict = NSMutableDictionary()
    var badgesArray = NSMutableArray()
    var badgeschatArray = NSMutableArray()
    var badgeslikeArray = NSMutableArray()
    
    var tempdic = NSMutableDictionary()
    var isFromLogin = ""
    var i : Int = 0
    var inPersonEventCount = Int()
    var invitedEventsCounts = Int()
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x:105, y: 250, width: 100, height: 150),
        type: .BallScaleMultiple, color: UIColor(red: 220/255.0, green: 203/255.0, blue: 85/255.0, alpha: 1.0), size: CGSize(width: 100, height: 100))
    
    override func viewWillAppear(animated: Bool) {
        badgesArray.removeAllObjects()
        refreshList()
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.pushPopUpView.frame.origin.y = -250.0

        pushPopUpView.hidden = true
        blackView.hidden = true
        
        getNotificationButton.layer.borderWidth = 2.0
        getNotificationButton.layer.borderColor = UIColor(red: 68.0/255.0, green: 185.0/255.0, blue: 227.0/255.0, alpha: 1.0).CGColor
        getNotificationButton.layer.cornerRadius = 2.0
        getNotificationButton.layer.masksToBounds = true
        
        
        pushPopUpView.layer.cornerRadius = 5.0
        pushPopUpView.layer.masksToBounds = true
        
        if isFromLogin == "true"
        {
            if let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
            {
                if settings.types.contains([.Alert, .Sound, .Badge])
                {
                    self.pushPopUpView.hidden = true
                    self.blackView.hidden = true
                    print("alerts")
                }
                else
                {
                    self.pushPopUpView.hidden = false
                    self.blackView.hidden = false
                    self.pushPopUp()
                }
                
            }
            
        }

        
        
        activityIndicatorView.center = CGPointMake(view.frame.width/2, view.frame.height/2)
        
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimation()
        
        activityIndicatorView.hidesWhenStopped = true
        
        
        let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        
        //getChannelsCount()
        getInPersonCount()
        
        InvitedEventsLbl.text = "Invited Events"
        myEventsLbl.text = "My Events"
        //createInvitation()
        
        //        self.view.addSubview(wakeUpImageView)
        //        wakeUpImageView.hidden = false
        
        checkdata()
        //  var reloadTimer =  NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("reloadData"), userInfo: nil, repeats: true)
        
    }
    
    /* func getChannelsCount()
    {
    print(currentUserId)
    
    let predicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
    
    let query = PFQuery(className:"Invitations", predicate: predicate)
    
    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitations:", successSelectorParameters:nil, errorSelector: "fetchInvitations:", errorSelectorParameters:nil)
    }
    */
    
    func pushPopUp()
    {
        
        UIView.animateWithDuration(1.0) { () -> Void in
            self.pushPopUpView.frame.origin.y = 150.0
            
        }
        
        
    }

    
    func fetchInvitatedEvents(timer:NSTimer)
    {
        let fetchedEmail = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if let objects = fetchedEmail
        {
            for events in objects
            {
                
            }
        }
        
    }
    
    func fetchInvitatedError(timer:NSTimer)
    {
        
    }
    
    func getInPersonCount()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["COUNT(*) as count"], whereString: " eventCreatorObjectId = '\(self.currentUserId)' AND isRSVP = 1", whereFields: [])
        
        if (resultSet != nil) {
            while resultSet.next() {
                
                
                
            }
        }
        
        inPersonEventCount = Int(resultSet.intForColumn("count"))
        
        // myEventsLbl.text = "My Events (\(inPersonEventCount))"
        resultSet.close()
    }
    
    
    
    func reloadData()
    {
        downloadData()
        deleteData()
        refreshList()
       // tableView.reloadData()
        
    }
    
    @IBAction func getNotificationButton(sender: AnyObject)
    {
        UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
        //
        let userNotificationTypes: UIUserNotificationType = ([UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]);
        
        
        let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        pushPopUpView.hidden = true
        blackView.hidden = true
    }
    @IBAction func closeButton(sender: AnyObject)
    {
        pushPopUpView.hidden = true
        blackView.hidden = true
    }
    
    func refreshContent()
    {
        //refreshList()
        
        ////println("premessage")
        
        //        if isSharedEventsUpdated
        //        {
        ////println("postmessage")
        scrollToLoadMore()
        isSharedEventsUpdated = false
        refreshList()
        //}
    }
    
    func scrollToLoadMore()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "isRSVP = 1 AND objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') GROUP BY objectId ORDER BY createdAt DESC LIMIT \(mySharedEvents.count), \(mySharedEvents.count + 10)", whereFields: [])
        
        //mySharedEvents = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                
                let userevent = PFObject(className: "Events")
                print (userevent)
                userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                
                userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                let isRSVP = resultSet.stringForColumn("isRSVP")
                
                if isRSVP == "0"
                {
                    userevent["isRSVP"] = false
                }
                else
                {
                    userevent["isRSVP"] = true
                    userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                    
                    
                    
//                    if let tes = resultSet.doubleForColumn("eventLatitude") as? String
//                    {
//                        
//                        userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
//                        userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
//                    }
//                    else
//                    {
//                       // userevent["eventLocation"] = ""
//                        userevent["eventLatitude"] = 0.00
//                        userevent["eventLongitude"] = 0.00
//                    
//                    }
                    
                    userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                    userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
                    
                    if resultSet.stringForColumn("eventLocation") == nil
                    {
                        userevent["eventLocation"] = ""
                    }
                    else
                    {
                        userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                    }
                    
                    
                    userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                    userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                }
                
                userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                userevent["frameX"] = resultSet.doubleForColumn("frameX")
                userevent["frameY"] = resultSet.doubleForColumn("frameY")
                
                userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
                
                userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                
                userevent["senderName"] = resultSet.stringForColumn("senderName")
                userevent["timezoneName"] = resultSet.stringForColumn("timezoneName")
              
                
                //rahul  userevent["socialSharingURL"] = resultSet.stringForColumn("socialSharingURL")
                
                
                
                
                
                if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil && resultSet.stringForColumn("objectId") != "hi")
                {
                    userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                    userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                    userevent.objectId = resultSet.stringForColumn("objectId")
                }
                
                let isPosted = resultSet.stringForColumn("isPosted")
                
                if isPosted == "0"
                {
                    userevent["isPosted"] = false
                }
                else
                {
                    userevent["isPosted"] = true
                }
                
                userevent["isUploading"] = false
                
                userevent["isDownloading"] = true
                
                mySharedEvents.append(userevent)
                
            }
        }
        
        resultSet.close()
        
        var i = mySharedEvents.count - (mySharedEvents.count % 10)
        
        if i == 0 && mySharedEvents.count >= 10
        {
            i = 0
        }
        
        
        for var j = i; j < (i + (mySharedEvents.count % 10)); j++
        {
            let isApproved = getApprovedStatus(mySharedEvents[j])
            
            let invitationNote = getInvitationNote(mySharedEvents[j])
            
            let noOfNewPosts = getNoOfPosts(mySharedEvents[j])
            
            let attendingStatus = getAttendingStatus(mySharedEvents[j])
            
            let invitationId = getInvitationId(mySharedEvents[j])
            
            let noOfAdults = getNoOfAdults(mySharedEvents[j])
            
            let noOfChilds = getNoOfChilds(mySharedEvents[j])
            
            mySharedEvents[j]["isApproved"] = isApproved
            mySharedEvents[j]["noOfNewPosts"] = noOfNewPosts
            mySharedEvents[j]["attendingStatus"] = attendingStatus
            mySharedEvents[j]["invitationId"] = invitationId
            mySharedEvents[j]["noOfAdults"] = noOfAdults
            mySharedEvents[j]["noOfChilds"] = noOfChilds
            mySharedEvents[j]["invitationNote"] = invitationNote
        }
        
        self.tableView.reloadData()
    }
    
    override func viewDidDisappear(animated: Bool) {
        activityIndicatorView.stopAnimation()
//        messagesRef.removeAllObservers()

        //  timer.invalidate()
    }
    
    
    
    
    func starttimer()
    {
        // timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("checkdidappeardata"), userInfo: nil, repeats: false)
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "starttimer", name:"sharedtimer", object: nil)
        
        checkdidappeardata()
    }
    func checkdidappeardata()
    {
        //let  timeer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("scrollToLoadMore"), userInfo: nil, repeats: true)
        scrollToLoadMore()
        
        isOnSharedEvents = true
        
        if deepLinkEmail != ""
        {
            
            if currentUserId != eventCreatorObjectId
            {
                if (deepLinkEmail != "noemail")
                {
                    self.loaderView.hidden = false
                    
                    self.wakeUpImage.hidden = true
                    let linkedQuery = PFUser.query()
                    
                    
                    linkedQuery!.whereKey("email", equalTo:self.deepLinkEmail)
                    
                    ParseOperations.instance.fetchData(linkedQuery!, target: self, successSelector: "fetchEmailSuccess:", successSelectorParameters: deepLinkObjectId, errorSelector:"fetchEmailError:", errorSelectorParameters: nil)
                }
                else
                {
                    let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND userObjectId = '\(currentUserId!)'")
                    
                    let query = PFQuery(className:"Invitations", predicate: predicate)
                    
                    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
                }
                
            }
            else
            {
                if fetchHostEventDetails(deepLinkObjectId)
                {
                    isOnSharedEvents = false
                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                    homeVC.eventCreatorId = eventCreatorObjectId
                    homeVC.redirectFromInviteView = false
                    homeVC.redirect = true
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
                else
                {
                    
                    
                    self.loaderView.hidden = true
                    self.wakeUpImage.hidden = true
                    let Alert = UIAlertController(title: "Error", message: "Event does not exist.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    Alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                        
                        
                    }))
                    
                    self.presentViewController(Alert, animated: true, completion: nil)
                }
                
                deleteData()
                downloadData()
                refreshList()
                updateData()
                
            }
            
        }
        else
        {
            deleteData()
            downloadData()
            refreshList()
            
            if (mySharedEvents.count > 0)
            {
                updateData()
            }
            
        }
        
    }
    func checkdata()
    {
        mySharedEvents = []
        self.view.addSubview(wakeUpImageView)
        
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        //
        //indicator.frame = (loaderSubView.frame.width/2) - (indicator.frame.width/2)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Loading..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loaderSubView.layer.cornerRadius = 10.0
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        //self.wakeUpView.hidden = false
        self.loaderView.hidden = true
        self.wakeUpImage.hidden = true
        self.wakeUpImage.clipsToBounds = true
        /*var isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "1", whereFields: [])
        if isDeleted {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        
        
        var isDeleted1 = ModelManager.instance.deleteTableData("EventImages", whereString: "1", whereFields: [])
        
        if isDeleted1 {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }
        
        
        var isDeleted2 = ModelManager.instance.deleteTableData("Invitations", whereString: "1", whereFields: [])
        
        if isDeleted2 {
        Util.invokeAlertMethod("", strBody: "Record deleted successfully.", delegate: nil)
        } else {
        Util.invokeAlertMethod("", strBody: "Error in deleting record.", delegate: nil)
        }*/
        
        
        
        
        // Do any additional setup after loading the view.
        invitedBoardingScreen.hidden = true
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        
        var attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(12.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        
        inviteBoardingTextView.attributedText = NSAttributedString(string:inviteBoardingTextView.text, attributes:attributes)
        inviteBoardingTextView.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        inviteBoardingTextView.textAlignment = .Center
        inviteBoardingTextView.textColor = UIColor(red: 150.0/255, green: 150.0/255, blue: 150.0/255, alpha: 1.0)
        
        tableView.separatorColor = UIColor.clearColor()
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
        }
        
        
        
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
            var isExtraEventDataDeleted = ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId IN (SELECT objectId FROM Events WHERE eventCreatorObjectId != '\(self.currentUserId)' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)')) ", whereFields: [])
            if isExtraEventDataDeleted
            {
                isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Events", whereString: "eventCreatorObjectId != '\(self.currentUserId)' AND objectId NOT IN (SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)')", whereFields: [])
                
                if isExtraEventDataDeleted
                {
                    
                    isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Invitations", whereString: "userObjectId != '\(self.currentUserId)' AND eventObjectId NOT IN (SELECT objectId FROM Events WHERE eventCreatorObjectId = '\(self.currentUserId)')", whereFields: [])
                    
                    if isExtraEventDataDeleted
                    {
                        
                        isExtraEventDataDeleted = ModelManager.instance.deleteTableData("Notifications", whereString: "receiverId != '\(self.currentUserId)'", whereFields: [])
                        
                    }
                    
                }
            }
        }
        
        
        refreshContent()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func fetchHostEventDetails(eventObjectId: String) -> Bool
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId = '\(eventObjectId)' GROUP BY objectId ORDER BY eventId DESC", whereFields: [])
        
        var i = 0
        
        if (resultSet != nil)
        {
            while resultSet.next() {
                i++
                let userevent = PFObject(className: "Events")
                
                userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                let isRSVP = resultSet.stringForColumn("isRSVP")
                
                if isRSVP == "0"
                {
                    userevent["isRSVP"] = false
                }
                else
                {
                    userevent["isRSVP"] = true
                    
                    userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                    if let tes = resultSet.doubleForColumn("eventLatitude") as? String
                    {
                        userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                        userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                        userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
                    }
                    else
                    {
                        userevent["eventLocation"] = ""
                        userevent["eventLatitude"] = 0.00
                        userevent["eventLongitude"] = 0.00
                        
                    }
                    userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                    
                    userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                }
                userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                
                userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
                
                userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                userevent["frameX"] = resultSet.doubleForColumn("frameX")
                userevent["frameY"] = resultSet.doubleForColumn("frameY")
                userevent["senderName"] = resultSet.stringForColumn("senderName")
                userevent["timezoneName"] = resultSet.stringForColumn("timezoneName")
                userevent["socialSharingURL"] = ""
                
                userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                
                if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil && resultSet.stringForColumn("objectId") != "hi")
                {
                    userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                    userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                    userevent.objectId = resultSet.stringForColumn("objectId")
                }
                
                let isPosted = resultSet.stringForColumn("isPosted")
                
                if isPosted == "0"
                {
                    userevent["isPosted"] = false
                }
                else
                {
                    userevent["isPosted"] = true
                }
                
                userevent["isUploading"] = false
                
                currentEvent = userevent
                
            }
        }
        
        resultSet.close()
        
        if i > 0
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func fetchInvitationsDataSuccess(timer:NSTimer)
    {
        let fetchedEmail = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let eventObjectId = timer.userInfo?.valueForKey("external") as! String
        
        var isTrue = false
        
        for emails in fetchedEmail!
        {
            
            //if  emails.objectId! == invitationObjectId && (emails["userObjectId"] as! String != currentUserId || emails["emailId"] as! String == NSUserDefaults.standardUserDefaults().valueForKey("email") as! String)
            
            if  emails.objectId! != invitationObjectId && ( emails["emailId"] as! String == NSUserDefaults.standardUserDefaults().valueForKey("email") as! String || emails["userObjectId"] as! String == currentUserId)
            {
                
              
                isTrue = true
                
                break
                
            }
        }
        
        if isTrue == true
        {
            if invitationObjectId != ""
            {
                self.wakeUpImage.hidden = true
                let Alert = UIAlertController(title: "Error", message: "You already have access to this event. You cannot respond to more than one invitation from the same account.", preferredStyle: UIAlertControllerStyle.Alert)
                
                Alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                    
                    self.loaderView.hidden = true
                    
                }))
                self.presentViewController(Alert, animated: true, completion: nil)
                
            }
            
        }
        else
        {
            
            let linkedQuery = PFQuery(className: "LinkedAccounts")
            linkedQuery.whereKey("emailId", equalTo: deepLinkEmail)
            ParseOperations.instance.fetchData(linkedQuery, target: self, successSelector: "fetchLinkedAccountSuccess:", successSelectorParameters: eventObjectId, errorSelector:"fetchLinkedAccountError:", errorSelectorParameters: nil)
            
        }
        
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
        
        self.wakeUpImage.hidden = true
        self.loaderView.hidden = true
        
    }
    
    func fetchInvitationsDataError(timer:NSTimer)
    {
        print("error")
    }
    
    
    
    func fetchEmailSuccess(timer:NSTimer)
    {
        let fetchedEmail = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let eventObjectId = timer.userInfo?.valueForKey("external") as! String
        
        
        if fetchedEmail?.count == 0
        {
            let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' ")
            //AND userObjectId = '\(currentUserId)'
            let query = PFQuery(className:"Invitations", predicate: predicate)
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsDataSuccess:", successSelectorParameters:eventObjectId, errorSelector: "fetchInvitationsDataError:", errorSelectorParameters:nil)
            
        }
        else
        {
            if deepLinkEmail == NSUserDefaults.standardUserDefaults().valueForKey("email") as! String
            {
                let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)'")
                //AND emailId = '\(deepLinkEmail)'
                let query = PFQuery(className:"Invitations", predicate: predicate)
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
            }
            else
            {
                if eventCreatorObjectId != NSUserDefaults.standardUserDefaults().valueForKey("currentUserId") as! String
                {
                    self.loaderView.hidden = true
                    self.wakeUpImage.hidden = true
                    let Alert = UIAlertController(title: "Error", message: "The invited email address is already associated with a different Eventnode account. Log in from that Eventnode account to continue.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    Alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                        
                        
                    }))
                    self.presentViewController(Alert, animated: true, completion: nil)
                }
                
                deleteData()
                downloadData()
                refreshList()
                updateData()
                
            }
            
        }
        
    }
    
    func fetchEmailError(timer:NSTimer)
    {
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        self.loaderView.hidden = true
        self.wakeUpImage.hidden = true
    }
    
    
    func fetchInvitationsSuccess(timer:NSTimer)
    {
        let invitationObjects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        var isSendPush : Bool = true
        
        if let fetchedobjects = invitationObjects
        {
            if fetchedobjects.count == 0
            {
                let inviteObject: PFObject = PFObject(className: "Invitations")
                
                inviteObject["invitedName"] = NSUserDefaults.standardUserDefaults().valueForKey("fullUserName") as! String
                inviteObject["eventObjectId"] = deepLinkObjectId
                inviteObject["isApproved"] = false
                
                if deepLinkEmail == "noemail"
                {
                    if isApproved == "true"
                    {
                        inviteObject["isApproved"] = true
                        isSendPush = false
                    }
                }
                
                
                inviteObject["userObjectId"] = currentUserId
                inviteObject["emailId"] = NSUserDefaults.standardUserDefaults().valueForKey("email") as! String
                inviteObject["attendingStatus"] = ""
                inviteObject["invitationType"] = "email"
                
                inviteObject["isUpdated"] = false
                inviteObject["noOfChilds"] = 0
                inviteObject["noOfAdults"] = 0
                inviteObject["invitationNote"] = ""
                inviteObject["isEventUpdated"] = false
                
                inviteObject["isEventStreamUpdated"] = true
                inviteObject["isTextUpdated"] = false
                
                ParseOperations.instance.saveData(inviteObject, target: self, successSelector: "saveInvitationSuccess:", successSelectorParameters:"new", errorSelector: "saveInvitationError:", errorSelectorParameters: nil)
            }
            else
            {
                for invitation in fetchedobjects
                {
                    if deepLinkEmail != "" && deepLinkEmail != "noemail"
                    {
                        invitation["isApproved"] = true
                        invitation["isEventUpdated"] = true
                        invitation["isUpdated"] = true
                        invitation["userObjectId"] = currentUserId
                        
                        var new = ""
                        
                        if isUserNew == true
                        {
                            new = "new"
                        }
                        
                        
                        ParseOperations.instance.saveData(invitation, target: self, successSelector: "saveInvitationSuccess:", successSelectorParameters:new, errorSelector: "saveInvitationError:", errorSelectorParameters: nil)
                    }
                    else
                    {
                        //deepLinkEmail = ""
                        //eventCreatorObjectId = ""
                        deleteData()
                        downloadData()
                        updateData()
                        refreshList()
                    }
                }
            }
        }
    }
    
    
    
    func fetchLinkedinfoSuccess(timer:NSTimer)
    {
        let fetchedEmails = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        
        for object in fetchedEmails!
        {
            if object.objectId! != invitationObjectId && (object["userObjectId"] as! String == currentUserId  || deepLinkEmail == NSUserDefaults.standardUserDefaults().valueForKey("email") as! String)
            {
                if i == 0
                {
                    i++
                    
                    if invitationObjectId != ""
                    {
                        self.wakeUpImage.hidden = true
                        let Alert = UIAlertController(title: "Error", message: "You already have access to this event. You cannot respond to more than one invitation from the same account.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        Alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                            
                            self.loaderView.hidden = true
                            
                        }))
                        self.presentViewController(Alert, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }
            else
            {
                //deepLinkEmail = ""
                //eventCreatorObjectId = ""
                deleteData()
                downloadData()
                refreshList()
                updateData()
            }
            
        }
        
        self.wakeUpImage.hidden = true
        
        self.loaderView.hidden = true
    }
    
    func fetchLinkedinfoError(timer:NSTimer)
    {
        
    }
    
    func fetchInvitationsError(timer:NSTimer)
    {
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        self.loaderView.hidden = true
        self.wakeUpImage.hidden = true
    }
    
    func fetchLinkedAccountSuccess(timer:NSTimer)
    {
        let fetchedEmails = timer.userInfo?.valueForKey("internal") as? [PFObject]
        var eventObjectId = timer.userInfo?.valueForKey("external") as! String
        //var userObjectid = fetchedEmail[0][""]
        
        if let fetchedobjects = fetchedEmails
        {
            if fetchedobjects.count == 0
            {
                let linkEmail = PFObject(className: "LinkedAccounts")
                
                linkEmail["emailId"] = deepLinkEmail
                linkEmail["userObjectId"] = currentUserId!
                linkEmail["isEmailVerified"] = true
                linkEmail.saveInBackground()
                
                let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)'")
                
                let query = PFQuery(className:"Invitations", predicate: predicate)
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
                
                fetchUnRespondedInvitations()
                
            }
            else
            {
                for emailObject in fetchedobjects
                {
                    if currentUserId == emailObject["userObjectId"] as! String
                    {
                        
                        let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)' ")
                        //AND emailId = '\(deepLinkEmail)'
                        let query = PFQuery(className:"Invitations", predicate: predicate)
                        
                        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
                        
                        
                    }
                    else
                    {
                        if emailObject["isEmailVerified"] as! Bool == true
                        {
                            
                            self.loaderView.hidden = true
                            self.wakeUpImage.hidden = true
                            
                            let Alert = UIAlertController(title: "Error", message: "The invited email address is already associated with a different Eventnode account. Log in from that Eventnode account to continue.", preferredStyle: UIAlertControllerStyle.Alert)
                            
                            Alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                                
                                
                            }))
                            
                            self.presentViewController(Alert, animated: true, completion: nil)
                            
                            deleteData()
                            downloadData()
                            refreshList()
                            updateData()
                        }
                        else
                        {
                            //deletetion code
                            //emailObject["userObjectId"] as! String
                            ParseOperations.instance.deleteData(emailObject, target: self, successSelector: "deleteEmailSuccess:", successSelectorParameters: [emailObject["userObjectId"] as! String, emailObject["emailId"] as! String], errorSelector: "deleteEmailError:", errorSelectorParameters: nil)
                        }
                    }
                    
                }
            }
            
        }
    }
    
    func fetchLinkedAccountError(timer:NSTimer)
    {
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
        self.loaderView.hidden = true
        self.wakeUpImage.hidden = true
        
    }
    
    func deleteEmailSuccess(timer:NSTimer)
    {
        //rahul  var emailData = timer.userInfo?.valueForKey("internal") as! PFObject
        //rahul var eventObjectId = timer.userInfo?.valueForKey("external") as! [String]
        
        let linkEmail = PFObject(className: "LinkedAccounts")
        
        linkEmail["emailId"] = deepLinkEmail
        linkEmail["userObjectId"] = currentUserId!
        linkEmail["isEmailVerified"] = true
        
        linkEmail.saveInBackground()
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)'")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchInvitationsError:", errorSelectorParameters:nil)
        
        fetchUnRespondedInvitations()
        
    }
    
    
    func deleteEmailError(timer:NSTimer)
    {
        var emailData = timer.userInfo?.valueForKey("internal") as! PFObject
        
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
        self.loaderView.hidden = true
        self.wakeUpImage.hidden = true
        
    }
    
    
    func fetchAllMessagesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        if let fetchedobjects = objects {
            
            //rahul  var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = message.objectId!
                tblFields["messageText"] = message["messageText"] as? String
                tblFields["senderObjectId"] = message["senderObjectId"] as? String
                tblFields["eventObjectId"] = message["eventObjectId"] as? String
                tblFields["senderName"] = message["senderName"] as? String
                //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                
                
                
                var date = ""
                
                if message.createdAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.createdAt)!)
                    tblFields["createdAt"] = date
                }
                
                if message.updatedAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.updatedAt)!)
                    tblFields["updatetAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                
                if isChatMode == true
                {
                    tblFields["isRead"] = "1"
                }
                else
                {
                    tblFields["isRead"] = "0"
                }
                
                
                let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["COUNT(*) as count"], whereString: "objectId = '\(message.objectId!)'", whereFields: [])
                
                resultSet.next()
                
                let messageCount = Int(resultSet.intForColumn("count"))
                
                resultSet.close()
                
                if messageCount == 0
                {
                    var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                }
                
                
            }
            
        }
    }
    
    
    
    func fetchAllMessagesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    func fetchUnRespondedInvitations()
    {
        
        let predicate = NSPredicate(format: "eventObjectId != '\(deepLinkObjectId)' AND emailId = '\(deepLinkEmail)' AND userObjectId = ''")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchUnRespondedInvitationsSuccess:", successSelectorParameters:nil, errorSelector: "fetchUnRespondedInvitationsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchUnRespondedInvitationsSuccess(timer:NSTimer)
    {
        let invitationObjects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if var fetchedobjects = invitationObjects
        {
            if fetchedobjects.count > 0
            {
                for var i = 0; i < fetchedobjects.count; i++
                {
                    fetchedobjects[i]["isApproved"] = true
                    fetchedobjects[i]["isEventUpdated"] = true
                    fetchedobjects[i]["isUpdated"] = true
                    fetchedobjects[i]["userObjectId"] = currentUserId
                    
                    
                    PFObject.saveAllInBackground(fetchedobjects)
                    
                    let predicate = NSPredicate(format: "objectId != '\(deepLinkObjectId)'")
                    
                    let query = PFQuery(className:"Events", predicate: predicate)
                    
                    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventDetailsSuccess:", successSelectorParameters:["\(fetchedobjects[i].objectId!)", "true"], errorSelector: "fetchEventDetailsError:", errorSelectorParameters:nil)
                    
                }
            }
        }
    }
    
    
    func fetchUnRespondedInvitationsError(timer:NSTimer)
    {
        
    }
    
    
    func fetchEventDetailsSuccess(timer:NSTimer)
    {
        
        let fetchedEvents = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        
        //let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject
        
        
        
        for eventObject in fetchedEvents!
        {
            
            if eventObject["isRSVP"] as! Bool == true
            {
                let boolValue = eventObject["isRSVP"] as! Bool
                if boolValue == false
                {
                    
                    var invitationInfo = timer.userInfo?.valueForKey("external") as! [String]
                    
                    let eventTitle = eventObject["eventTitle"] as! String
                    
                    let fullUserName = NSUserDefaults.standardUserDefaults().valueForKey("fullUserName") as! String
                    let currentUserId = NSUserDefaults.standardUserDefaults().valueForKey("currentUserId") as! String
                    
                    let userObjectId = eventObject["eventCreatorObjectId"] as! String
                    
                    var email =  ""
                    
                    if deepLinkEmail == "noemail" || deepLinkEmail == ""
                    {
                        email = NSUserDefaults.standardUserDefaults().valueForKey("email") as! String
                    }
                    else
                    {
                        email = deepLinkEmail
                    }
                    
                    var notifMessage = "\(fullUserName) joined your channel, \(eventTitle), and might need your permission to gain access."
                    
                    if invitationInfo[1] == "true"
                    {
                        notifMessage = "\(fullUserName) joined your channel, \(eventTitle), and might need your permission to gain access."
                    }
                    
                    var data: Dictionary<String, String!> = [
                        "alert" : "\(notifMessage)",
                        "notifType" :  "invitationresponse",
                        "userObjectId": "\(currentUserId)",
                        "eventObjectId": "\(eventObject.objectId!)",
                        "objectId": "\(invitationInfo[0])",
                        "invitedName": "\(fullUserName)",
                        "attendingStatus" : "",
                        "noOfChilds": "0",
                        "noOfAdults": "0",
                        "invitationNote": "0",
                        "createdAt": "",
                        "updatedAt": "",
                        "isUpdated": "",
                        "emailId": "\(email)",
                        "badge": "Increment",
                        "sound" : "default",
                        "eventCreatorId" : "\(userObjectId)"
                    ]
                    
                    
                    
                    let notificationObject = PFObject(className: "Notifications")
                    notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                    notificationObject["notificationImage"] = "profilePic.png"
                    notificationObject["senderId"] = currentUserId
                    notificationObject["receiverId"] = userObjectId
                    notificationObject["notificationActivityMessage"] = notifMessage
                    notificationObject["eventObjectId"] = eventObject.objectId!
                    notificationObject["notificationType"] = "invitationresponse"
                    
                    notificationObject.saveInBackground()
                    
                    
                    var predicateString: String! = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = true"
                    
                    sendParsePush(predicateString, data: data)
                    
                    
                    data["sound"] = ""
                    
                    predicateString = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = false"
                    
                    sendParsePush(predicateString, data: data)
                }
            }
            else
            {
                
                //                if isApproved == "false"
                //                {
                if eventObject["isRSVP"] as! Bool == false
                {
                    var invitationInfo = timer.userInfo?.valueForKey("external") as! [String]
                    
                    let eventTitle = eventObject["eventTitle"] as! String
                    
                    let fullUserName = NSUserDefaults.standardUserDefaults().valueForKey("fullUserName") as! String
                    let currentUserId = NSUserDefaults.standardUserDefaults().valueForKey("currentUserId") as! String
                    
                    let userObjectId = eventObject["eventCreatorObjectId"] as! String
                    
                    var email =  ""
                    
                    if deepLinkEmail == "noemail" || deepLinkEmail == ""
                    {
                        email = NSUserDefaults.standardUserDefaults().valueForKey("email") as! String
                    }
                    else
                    {
                        email = deepLinkEmail
                    }
                    
                    var notifMessage = "\(fullUserName) joined your channel, \(eventTitle), and might need your permission to gain access."
                    
                    if invitationInfo[1] == "true"
                    {
                        notifMessage = "\(fullUserName) joined your channel, \(eventTitle), and might need your permission to gain access."
                    }
                    
                    var data: Dictionary<String, String!> = [
                        "alert" : "\(notifMessage)",
                        "notifType" :  "invitationresponse",
                        "userObjectId": "\(currentUserId)",
                        "eventObjectId": "\(eventObject.objectId!)",
                        "objectId": "\(invitationInfo[0])",
                        "invitedName": "\(fullUserName)",
                        "attendingStatus" : "",
                        "noOfChilds": "0",
                        "noOfAdults": "0",
                        "invitationNote": "0",
                        "createdAt": "",
                        "updatedAt": "",
                        "isUpdated": "",
                        "emailId": "\(email)",
                        "badge": "Increment",
                        "sound" : "default",
                        "eventCreatorId" : "\(userObjectId)"
                    ]
                    
                    
                    
                    let notificationObject = PFObject(className: "Notifications")
                    notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                    notificationObject["notificationImage"] = "profilePic.png"
                    notificationObject["senderId"] = currentUserId
                    notificationObject["receiverId"] = userObjectId
                    notificationObject["notificationActivityMessage"] = notifMessage
                    notificationObject["eventObjectId"] = eventObject.objectId!
                    notificationObject["notificationType"] = "invitationresponse"
                    
                    notificationObject.saveInBackground()
                    
                    
                    var predicateString: String! = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = true"
                    
                    sendParsePush(predicateString, data: data)
                    
                    
                    data["sound"] = ""
                    
                    predicateString = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = false"
                    
                    sendParsePush(predicateString, data: data)
                }
                //                }
                
            }
        }
        
        
    }
    
    
    func fetchEventDetailsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        print(error)
    }
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        let predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        let query = PFUser.queryWithPredicate(predicate)
        
        let push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                if let fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        let userObjectId = object.objectId!
                        
                        let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        let query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                if let fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    let query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }
    
    
    func saveInvitationSuccess(timer:NSTimer)
    {
        
        let invitationObject = timer.userInfo?.valueForKey("internal") as! PFObject
        let isNew = timer.userInfo?.valueForKey("external") as! String
        
        
        
        
        
        if isNew as! String == "new"
        {
            let predicate = NSPredicate(format: "objectId = '\(deepLinkObjectId)'")
            
            let query = PFQuery(className:"Events", predicate: predicate)
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventDetailsSuccess:", successSelectorParameters:["\(invitationObject.objectId!)", isApproved], errorSelector: "fetchEventDetailsError:", errorSelectorParameters:nil)
        }
        
        if isUserNew == true
        {
            
        }
        
        
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        //isDownloadComplete = false
        //isUpdateComplete = false
        
        self.loaderView.hidden = true
        self.wakeUpImage.hidden = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
    }
    
    func saveInvitationError(timer:NSTimer)
    {
        //deepLinkEmail = ""
        //eventCreatorObjectId = ""
        
        isDownloadComplete = true
        isUpdateComplete = true
        
        deleteData()
        downloadData()
        refreshList()
        updateData()
        
        self.loaderView.hidden = true
        self.wakeUpImage.hidden = true
        
        
    }
    
    func downloadData()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "userObjectId = '\(self.currentUserId)' ORDER BY invitationId DESC", whereFields: [])
        
        var invitationObjectIds: Array<String>
        
        invitationObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                invitationObjectIds.append(resultSet.stringForColumn("objectId"))
            }
        }
        
        resultSet.close()
        
        let invitationObjectIdsString = invitationObjectIds.joinWithSeparator("','")
        
        
        
        let resultSetInvitationWithoutEvent: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "objectId IN ('\(invitationObjectIdsString)') AND eventObjectId NOT IN (SELECT objectId FROM Events) ORDER BY invitationId DESC", whereFields: [])
        
        var invitationWithoutEventObjectIds: Array<String>
        
        invitationWithoutEventObjectIds = []
        
        if (resultSetInvitationWithoutEvent != nil) {
            while resultSetInvitationWithoutEvent.next() {
                invitationWithoutEventObjectIds.append(resultSetInvitationWithoutEvent.stringForColumn("objectId"))
            }
        }
        
        resultSetInvitationWithoutEvent.close()
        
        let invitationWithoutEventObjectIdsString = invitationWithoutEventObjectIds.joinWithSeparator("','")
        
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(invitationObjectIdsString)'}) AND userObjectId = '\(self.currentUserId)'")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        
        let updatePredicate = NSPredicate(format: "userObjectId = '\(self.currentUserId)' AND isUpdated = true ")
        
        let updateQuery = PFQuery(className:"Invitations", predicate: updatePredicate)
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchAllUpdatedSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllUpdatedError:", errorSelectorParameters:nil)
        
        
        
        if invitationWithoutEventObjectIdsString != ""
        {
            let invitationWithoutEventPredicate = NSPredicate(format: "objectId IN {'\(invitationWithoutEventObjectIdsString)'} AND userObjectId = '\(self.currentUserId)'")
            
            let invitationWithoutEventPredicateQuery = PFQuery(className:"Invitations", predicate: invitationWithoutEventPredicate)
            
            invitationWithoutEventPredicateQuery.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(invitationWithoutEventPredicateQuery, target: self, successSelector: "fetchEventsForExistingInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventsForExistingInvitationsError:", errorSelectorParameters:nil)
        }
    }
    
    
    func fetchAllUpdatedSuccess(timer:NSTimer)
    {
       let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if let invites = objects
        {
            for invitation in invites
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = invitation.objectId!
                tblFields["userObjectId"] = invitation["userObjectId"] as? String
                tblFields["attendingStatus"] = invitation["attendingStatus"] as? String
                tblFields["invitationType"] = invitation["invitationType"] as? String
                
                tblFields["invitationNote"] = invitation["invitationNote"] as? String
                let noOfAdults = invitation["noOfAdults"] as! Int
                let noOfChilds = invitation["noOfChilds"] as! Int
                
                tblFields["noOfAdults"] = "\(noOfAdults)" 
                tblFields["noOfChilds"] = "\(noOfChilds)"
                
                
                if let needsContentApproval = invitation["needsContentApproval"] as? Bool
                {
                    if needsContentApproval as Bool == true
                    {
                        tblFields["needsContentApprovel"] = "1"
                    }
                        
                    else
                    {
                        tblFields["needsContentApprovel"] = "0"
                    }
                }
                else
                {
                    tblFields["needsContentApprovel"] = "0"
                }
                
                var date = ""
                
                if invitation.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.createdAt)!)
                    //println(date)
                    tblFields["createdAt"] = date
                }
                
                if invitation.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.updatedAt)!)
                    //println(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                tblFields["emailId"] = invitation["emailId"] as? String
                tblFields["eventObjectId"] = invitation["eventObjectId"] as? String
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }
                
                
               var insertedId = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId='\(invitation.objectId!)'", whereFields: [])
                
               

            }
        }
        
        
    }
    
    func fetchAllUpdatedError(timer:NSTimer)
    {
        
    }
    
    func scrollViewDidScroll(_scrollView: UIScrollView){
        let newScroll = CGFloat(_scrollView.contentOffset.y)
        
        if newScroll > _scrollView.contentSize.height - _scrollView.frame.height
        {
            //refreshList()
            scrollToLoadMore()
        }
        
    }
    
    
    func refreshList()
    {
        //isEventDataUpDated = false
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "isRSVP = 1 AND objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') GROUP BY objectId ORDER BY createdAt DESC LIMIT 0, \(mySharedEvents.count)", whereFields: [])
        
        mySharedEvents = []
        
        if (resultSet != nil) {
            activityIndicatorView.stopAnimation()

            while resultSet.next() {
                
                let userevent = PFObject(className: "Events")
                
                userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                
                userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                let isRSVP = resultSet.stringForColumn("isRSVP")
                
                if isRSVP == "0"
                {
                    userevent["isRSVP"] = false
                }
                else
                {
                    userevent["isRSVP"] = true
                    userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
//                    if let tes = resultSet.doubleForColumn("eventLatitude") as? String
//                    {
//                        userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                        userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                        userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
//                    }
//                    else
//                    {
////                        userevent["eventLocation"] = ""
//                        userevent["eventLatitude"] = 0.00
//                        userevent["eventLongitude"] = 0.00
//                        
//                    }
                    
                    if  let location = resultSet.stringForColumn("eventLocation")
                    {
                        userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                    }
                    else
                    {
                        userevent["eventLocation"] = ""
                    }
                    
                    
                    
                    userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                    userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                }
                
                userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                userevent["frameX"] = resultSet.doubleForColumn("frameX")
                userevent["frameY"] = resultSet.doubleForColumn("frameY")
                
                userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
                
                userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                
                userevent["senderName"] = resultSet.stringForColumn("senderName")
                userevent["timezoneName"] = resultSet.stringForColumn("timezoneName")
                //rahul  userevent["socialSharingURL"] = resultSet.stringForColumn("socialSharingURL")
                
                
                if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil && resultSet.stringForColumn("objectId") != "hi")
                {
                    userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                    userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                    userevent.objectId = resultSet.stringForColumn("objectId")
                }
                
                let isPosted = resultSet.stringForColumn("isPosted")
                
                if isPosted == "0"
                {
                    userevent["isPosted"] = false
                }
                else
                {
                    userevent["isPosted"] = true
                }
                
                userevent["isUploading"] = false
                
                userevent["isDownloading"] = true
                
                mySharedEvents.append(userevent)
                
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                    
                    self.tableView.reloadData()
                    // self.finishReceivingMessage()
                })
//                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
//                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildRemoved, withBlock: { (snapshot) in
//                    self.refreshList()
//                    // self.finishReceivingMessage()
//                })
//                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
//                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildRemoved, withBlock: { (snapshot) in
//                    self.refreshList()
//                    // self.finishReceivingMessage()
//                })
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    //print(  snapshot.key )
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray  .insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                    
                    self.tableView.reloadData()
                    // self.finishReceivingMessage()
                    
                })
//                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
//                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildRemoved, withBlock: { (snapshot) in
//                    self.refreshList()
//                    // self.finishReceivingMessage()
//                })
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(resultSet.stringForColumn("objectId"))/\(self.currentUserId)")
                messagesRef.queryLimitedToLast(2000).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.badgesArray.insertObject(dictemp, atIndex: 0)
                    self.badgeschatArray.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.badgesArray
                    self.badgesArray = []
                    
                    for e in arrtemp
                    {
                        if !self.badgesArray.containsObject(e)
                        {
                            self.badgesArray.addObject(e)
                        }
                        
                    }
                   
                    self.tableView.reloadData()
                    
                    // self.finishReceivingMessage()
                    
                    
                })
                
                
            }
        }
        else
        {
            activityIndicatorView.stopAnimation()

        }
        
        resultSet.close()
        
        var i = 0
        
        for userevent in mySharedEvents
        {
            let isApproved = getApprovedStatus(userevent)
            
            let invitationNote = getInvitationNote(userevent)
            
            let noOfNewPosts = getNoOfPosts(userevent)
            
            let attendingStatus = getAttendingStatus(userevent)
            
            let invitationId = getInvitationId(userevent)
            
            let noOfAdults = getNoOfAdults(userevent)
            
            let noOfChilds = getNoOfChilds(userevent)
            
            mySharedEvents[i]["isApproved"] = isApproved
            mySharedEvents[i]["noOfNewPosts"] = noOfNewPosts
            mySharedEvents[i]["attendingStatus"] = attendingStatus
            mySharedEvents[i]["invitationId"] = invitationId
            mySharedEvents[i]["noOfAdults"] = noOfAdults
            mySharedEvents[i]["noOfChilds"] = noOfChilds
            mySharedEvents[i]["invitationNote"] = invitationNote
            
            i++
        }
        
        self.tableView.reloadData()
        
//        activityIndicatorView.stopAnimation()

        if mySharedEvents.count == 0
        {
            invitedBoardingScreen.hidden = false
        }
        else
        {
            invitedBoardingScreen.hidden = true
        }
        
        
       
        
        if isDownloadComplete == true && isUpdateComplete == true && deepLinkEmail != ""
        {
            deepLinkEmail = ""
            eventCreatorObjectId = ""
            
            isDownloadComplete = false
            isUpdateComplete = false
            self.wakeUpImage.hidden = true
            self.loaderView.hidden = true
            
            for event in mySharedEvents
            {
                if deepLinkObjectId == event.objectId!
                {
                    currentSharedEvent = event
                    
                    mySharedEventData = []
                    
                    if((event["isApproved"] as! Bool) == true || (event["isRSVP"] as! Bool) == true)
                    {
                        isOnSharedEvents = false
                        
                        let streamVC = self.storyboard!.instantiateViewControllerWithIdentifier("StreamViewController") as! StreamViewController
                        if emailLinkType == "changeresponse" || emailLinkType == "eventdateupdated" || emailLinkType == "eventlocationupdate" || emailLinkType == "eventtimezoneupdate" || emailLinkType == "invitationreminder"
                        {
                            streamVC.isOpenInvitScreen = "true"
                        }
                        
                        
                        
                        
                        self.navigationController?.pushViewController(streamVC, animated: false)
                    }
                    else
                    {
                        self.wakeUpImage.hidden = true
                        self.loaderView.hidden = true
                        
                        
                        
                        let Alert = UIAlertController(title: "Error", message: "This is a private channel and the host needs to approve before you can get access to it.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        Alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                            
                        }))
                        
                        self.presentViewController(Alert, animated: true, completion: nil)
                        
                        //deleteData()
                        //downloadData()
                        //refreshList()
                        //updateData()
                        
                    }
                    
                }
            }
        }
        
        
    }
    
    func fetchlikecount(event:NSString)
    {
        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(event)/\(self.currentUserId)")
        
        messagesRef .observeEventType(.Value, withBlock: { snapshot in
            self.badgesDict.removeAllObjects()
            
            self.badgesDict.setObject(String(snapshot.childrenCount), forKey: "likecount")
            self.badgesArray.addObject(self.badgesDict)
            
            self.fetchChatcount(event)
            
            }, withCancelBlock: { error in
                print(error.description)
        })
    }
    func fetchChatcount(event:NSString)
    {
        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(event)/\(self.currentUserId)")
        
        messagesRef .observeEventType(.Value, withBlock: { snapshot in
            self.badgesDict.removeAllObjects()
            
            self.badgesDict.setObject(String(snapshot.childrenCount), forKey: "chatcount")
            self.badgesArray.addObject(self.badgesDict)
            }, withCancelBlock: { error in
                print(error.description)
        })
        
    }
    func deleteData()
    {
        let updatePredicate = NSPredicate(format: "userObjectId = '\(currentUserId)'")
        
        let updateQuery = PFQuery(className:"Invitations", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchExistingEventsSuccess:", successSelectorParameters: nil, errorSelector: "fetchExistingEventsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchExistingEventsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        if let fetchedobjects = objects
        {
            var i = 0
            var existingEventObjectIds: Array<String>
            existingEventObjectIds = []
            
            for invitationObject in fetchedobjects
            {
                existingEventObjectIds.append(invitationObject["eventObjectId"] as! String)
            }
            
            let existingEventObjectIdsString = existingEventObjectIds.joinWithSeparator("','")
            
            var whereQuery = ""
            
            if existingEventObjectIdsString != ""
            {
                whereQuery = "eventCreatorObjectId != '\(currentUserId)' AND objectId NOT IN ('\(existingEventObjectIdsString)') AND objectId != ''"
            }
            else
            {
                whereQuery = "eventCreatorObjectId != '\(currentUserId)' AND objectId != ''"
            }
            
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["objectId"], whereString: whereQuery, whereFields: [])
            
            
            var nonExistingEventObjectIds: Array<String>
            nonExistingEventObjectIds = []
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    nonExistingEventObjectIds.append(resultSet.stringForColumn("objectId"))
                }
            }
            
            resultSet.close()
            
            let nonExistingEventObjectIdsString = nonExistingEventObjectIds.joinWithSeparator("','")
            
            if nonExistingEventObjectIdsString != ""
            {
                ModelManager.instance.deleteTableData("Events", whereString: "objectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("Invitations", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("PostLikes", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventComments", whereString: "eventObjectId IN ('\(nonExistingEventObjectIdsString)')", whereFields: [])
                
                refreshList()
            }
            
        }
    }
    
    
    func fetchExistingEventsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
    }
    
    
    
    func updateData()
    {
        let predicateString = "userObjectId = '\(currentUserId)' AND (isEventUpdated = true OR isEventStreamUpdated = true)"
        
        /*if var timeZoneName = NSUserDefaults.standardUserDefaults().objectForKey("timeZoneName") as? String
        {
        if NSTimeZone.localTimeZone().name != timeZoneName
        {
        //println(timeZoneName)
        
        var localTimeZoneName = NSTimeZone.localTimeZone().name
        
        NSUserDefaults.standardUserDefaults().setObject("\(localTimeZoneName)", forKey: "timeZoneName")
        
        predicateString = "userObjectId = '\(currentUserId)'"
        
        showLoader("Updating Timezone....")
        }
        }
        else
        {
        var localTimeZoneName = NSTimeZone.localTimeZone().name
        NSUserDefaults.standardUserDefaults().setObject("\(localTimeZoneName)", forKey: "timeZoneName")
        }*/
        
        let updatePredicate = NSPredicate(format: predicateString)
        
        let updateQuery = PFQuery(className:"Invitations", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchEventUpdatesSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventUpdatesError:", errorSelectorParameters:nil)
    }
    
    
    
    
    func fetchEventUpdatesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        //println("Successfully retrieved \(objects!.count) events.")
        
        loaderView.hidden = true
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            var updateIds = [String]()
            
            var updateStreamEventIds = [String]()
            
            
            var tblFields: Dictionary! = [String: String]()
            
            for invitation in fetchedobjects
            {
                if invitation["isEventUpdated"] as! Bool == true
                {
                    updateIds.append(invitation["eventObjectId"] as! String)
                }
                
                if invitation["isEventStreamUpdated"] as! Bool == true
                {
                    updateStreamEventIds.append(invitation["eventObjectId"] as! String)
                }
                
                fetchedobjects[i]["isEventUpdated"] = false
                fetchedobjects[i]["isEventStreamUpdated"] = false
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }
                
                var isUpdated = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitation.objectId!])
                
                i++
                //updateInvitationIds.append(invitation.objectId!)
            }
            
            if fetchedobjects.count > 0
            {
                PFObject.saveAllInBackground(fetchedobjects)
            }
            
            
            var updateIdsString = ""
            var updateStreamEventIdsString = ""
            
            if updateIds.count > 0
            {
                updateIdsString = updateIds.joinWithSeparator("','")
                
            }
            
            if updateStreamEventIds.count > 0
            {
                updateStreamEventIdsString = updateStreamEventIds.joinWithSeparator("','")
                
            }
            
            if updateIdsString != ""
            {
                let predicate = NSPredicate(format: "objectId IN {'\(updateIdsString)'}")
                
                let query = PFQuery(className:"Events", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchUpdatedEventsSuccess:", successSelectorParameters: fetchedobjects, errorSelector: "fetchUpdatedEventsError:", errorSelectorParameters:nil)
            }
            
            if updateStreamEventIdsString != ""
            {
                
                let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["objectId"], whereString: "eventObjectId IN ('\(updateStreamEventIdsString)')", whereFields: [])
                
                
                var newPostIds = [String]()
                
                if (resultSet != nil) {
                    while resultSet.next()
                    {
                        newPostIds.append(resultSet.stringForColumn("objectId"))
                    }
                }
                
                resultSet.close()
                
                if newPostIds.count > 0
                {
                    var newPostIdsString = ""
                    
                    newPostIdsString = newPostIds.joinWithSeparator("','")
                    
                    let predicate = NSPredicate(format: "NOT (objectId IN {'\(newPostIdsString)'}) AND eventObjectId IN {'\(updateStreamEventIdsString)'}")
                    
                    let query = PFQuery(className:"EventImages", predicate: predicate)
                    
                    query.orderByAscending("createdAt")
                    
                    ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
                }
            }
            
            
        }
    }
    
    
    func fetchEventUpdatesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func fetchUpdatedEventsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        let invitationObjects = timer.userInfo?.valueForKey("external") as? [PFObject]
        
        //println("Successfully retrieved \(objects!.count) events.")
        
        var eventObjectIds = [String]()
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            for eventObject in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                tblFields["eventImage"] = eventObject["eventImage"] as? String
                tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                
                let frameX = eventObject["frameX"] as! CGFloat
                let frameY = eventObject["frameY"] as! CGFloat
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                tblFields["senderName"] = eventObject["senderName"] as? String
                
                tblFields["eventFolder"] = eventObject["eventFolder"] as? String
                
                let eventTimezoneOffset = eventObject["eventTimezoneOffset"] as! Int
                
                tblFields["eventTimezoneOffset"] = "\(eventTimezoneOffset)"
                
                if(eventObject["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if eventObject["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                        //println(date)
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if eventObject["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                        //println(date)
                        tblFields["eventEndDateTime"] = date
                        //println(tblFields["eventEndDateTime"])
                    }
                    
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    
                    let eventLatitude = eventObject["eventLatitude"] as! Double
                    let eventLongitude = eventObject["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                    
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                }
                
                
                tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                tblFields["timezoneName"] = eventObject["timezoneName"] as? String
                
                tblFields["isPosted"] = "1"
                
                var date = ""
                
                if eventObject.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                    //println(date)
                    tblFields["createdAt"] = date
                }
                
                if eventObject.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                    //println(date)
                    tblFields["updatedAt"] = date
                }
                
                var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [eventObject.objectId!])
                
                eventObjectIds.append(eventObject.objectId!)
                
            }
            
            let eventObjectIdsString = eventObjectIds.joinWithSeparator("','")
            
            let messageIdsString = fetchMessageIds()
            
            let predicate = NSPredicate(format: "NOT (objectId IN {'\(messageIdsString)'}) AND eventObjectId IN {'\(eventObjectIdsString)'}")
            
            let query = PFQuery(className:"EventComments", predicate: predicate)
            
            query.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagesError:", errorSelectorParameters:nil)
            
            if deepLinkEmail != ""
            {
                isUpdateComplete = true
            }
            
            
            refreshList()
            
            PFObject.saveAllInBackground(invitationObjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    
                }
                else
                {
                    
                }
            }
        }
    }
    
    
    
    func fetchUpdatedEventsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func getNoOfPosts(userevent: PFObject) -> Int
    {
        let newPostsResultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "eventObjectId = '\(userevent.objectId!)' AND isRead = 0", whereFields: [])
        
        newPostsResultSet.next()
        
        let noOfNewPosts = Int(newPostsResultSet.intForColumn("count"))
        
        newPostsResultSet.close()
        
        let newMessagesResultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId = '\(userevent.objectId!)' AND isRead = 0", whereFields: [])
        
        newMessagesResultSet.next()
        
        let noOfNewMessages = Int(newMessagesResultSet.intForColumn("count"))
        
        newMessagesResultSet.close()
        
        return (noOfNewPosts+noOfNewMessages)
    }
    
    func getApprovedStatus(userevent: PFObject) -> Bool
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let isApproved = statusResultSet.stringForColumn("isApproved")
        
        statusResultSet.close()
        
        
        if isApproved == nil || isApproved == "0"
        {
            return false
        }
        else
        {
            return true
        }
    }
    
    
    func getNoOfChilds(userevent: PFObject) -> Int
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let noOfChilds = statusResultSet.intForColumn("noOfChilds")
        
        statusResultSet.close()
        
        //println("noOfChilds: \(noOfChilds)")
        
        return Int(noOfChilds)
    }
    
    func getNoOfAdults(userevent: PFObject) -> Int
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let noOfAdults = statusResultSet.intForColumn("noOfAdults")
        
        statusResultSet.close()
        
        //println("noOfAdults: \(noOfAdults)")
        
        return Int(noOfAdults)
    }
    
    
    func getAttendingStatus(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let attendingStatus = statusResultSet.stringForColumn("attendingStatus")
        
        
        
        statusResultSet.close()
        
        if attendingStatus != nil
        {
            return attendingStatus
        }
        else
        {
            return " "
        }
        
        
        
    }
    
    
    func getInvitationId(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let invitationId = statusResultSet.stringForColumn("objectId")
        
        statusResultSet.close()
        
        return invitationId
    }
    
    
    func getInvitationNote(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        
        
        guard let invitationNote = statusResultSet.stringForColumn("invitationNote") else {
            return ""
        }
        statusResultSet.close()
        
        return invitationNote
    }
    
    
    
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        //println("Successfully retrieved \(objects!.count) events.")
        if let fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for invitation in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = invitation.objectId!
                tblFields["userObjectId"] = invitation["userObjectId"] as? String
                tblFields["attendingStatus"] = invitation["attendingStatus"] as? String
                tblFields["invitationType"] = invitation["invitationType"] as? String
                
                tblFields["invitationNote"] = invitation["invitationNote"] as? String
                let noOfAdults = invitation["noOfAdults"] as? Int
                let noOfChilds = invitation["noOfChilds"] as? Int
                
                tblFields["noOfAdults"] = "\(noOfAdults)"
                tblFields["noOfChilds"] = "\(noOfChilds)"
                
                
                if let needsContentApproval = invitation["needsContentApproval"] as? Bool
                {
                    if needsContentApproval as Bool == true
                    {
                        tblFields["needsContentApprovel"] = "1"
                    }
                        
                    else
                    {
                        tblFields["needsContentApprovel"] = "0"
                    }
                }
                else
                {
                    tblFields["needsContentApprovel"] = "0"
                }
                
                var date = ""
                
                if invitation.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.createdAt)!)
                    //println(date)
                    tblFields["createdAt"] = date
                }
                
                if invitation.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((invitation.updatedAt)!)
                    //println(date)
                    tblFields["updatedAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                tblFields["emailId"] = invitation["emailId"] as? String
                tblFields["eventObjectId"] = invitation["eventObjectId"] as? String
                
                if invitation["isApproved"] as? Bool == true
                {
                    tblFields["isApproved"] = "1"
                }
                else
                {
                    tblFields["isApproved"] = "0"
                }
                
                var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
                
                fetchedEventObjectIds.append(invitation["eventObjectId"] as! String)
            }
            
            let eventObjectIdsString = fetchedEventObjectIds.joinWithSeparator("','")
            //println(eventObjectIdsString)
            if eventObjectIdsString != ""
            {
                let predicate = NSPredicate(format: "objectId IN {'\(eventObjectIdsString)'} AND isRSVP = true")
                
                let query = PFQuery(className:"Events", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllEventsSuccess:", successSelectorParameters: eventObjectIdsString, errorSelector: "fetchAllEventsError:", errorSelectorParameters:nil)
            }
            else
            {
                if deepLinkEmail != ""
                {
                    isDownloadComplete = true
                }
                
                self.refreshList()
            }
        }
        
        if objects?.count == 0
        {
            
        }
        
    }
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    func fetchEventsForExistingInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        //println("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for invitation in fetchedobjects
            {
                fetchedEventObjectIds.append(invitation["eventObjectId"] as! String)
            }
            
            let eventObjectIdsString = fetchedEventObjectIds.joinWithSeparator("','")
            //println(eventObjectIdsString)
            if eventObjectIdsString != ""
            {
                let predicate = NSPredicate(format: "objectId IN {'\(eventObjectIdsString)'} AND isRSVP = true")
                
                let query = PFQuery(className:"Events", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllEventsSuccess:", successSelectorParameters: eventObjectIdsString, errorSelector: "fetchAllEventsError:", errorSelectorParameters:nil)
            }
        }
    }
    
    func fetchEventsForExistingInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    func fetchAllEventsSuccess(timer:NSTimer)
    {
        activityIndicatorView.stopAnimation()
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let eventObjectIdsString = timer.userInfo?.valueForKey("external") as! String
        print(objects)
        if let fetchedobjects = objects {
            
            var i = 0
            
            
            for eventObject in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventObject["eventTitle"] as? String
                tblFields["eventImage"] = eventObject["eventImage"] as? String
                tblFields["originalEventImage"] = eventObject["originalEventImage"] as? String
                
                let frameX = eventObject["frameX"] as! CGFloat
                let frameY = eventObject["frameY"] as! CGFloat
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = eventObject["eventCreatorObjectId"] as? String
                
                let eventTimezoneOffset = eventObject["eventTimezoneOffset"] as! Int
                
                tblFields["eventTimezoneOffset"] = "\(eventTimezoneOffset)"
                
                
                tblFields["eventFolder"] = eventObject["eventFolder"] as? String
                
                tblFields["senderName"] = eventObject["senderName"] as? String
                
                if(eventObject["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if eventObject["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventStartDateTime"] as? NSDate)!)
                        //println(date)
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if eventObject["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((eventObject["eventEndDateTime"] as? NSDate)!)
                        //println(date)
                        tblFields["eventEndDateTime"] = date
                        //println(tblFields["eventEndDateTime"])
                    }
                    
                    tblFields["eventDescription"] = eventObject["eventDescription"] as? String
                    
                    print(eventObject)
                    
                    let eventLatitude = eventObject["eventLatitude"] as! Double
                    let eventLongitude = eventObject["eventLongitude"] as! Double
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = eventObject["eventLocation"] as? String
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                }
                
                
                tblFields["socialSharingURL"] = eventObject["socialSharingURL"] as? String
                tblFields["timezoneName"] = eventObject["timezoneName"] as? String
                
                tblFields["objectId"] = eventObject.objectId
                tblFields["isPosted"] = "1"
                
                var date = ""
                
                if eventObject.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.createdAt)!)
                    //println(date)
                    tblFields["createdAt"] = date
                }
                
                if eventObject.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
                    //println(date)
                    tblFields["updatedAt"] = date
                }
                
                let insertedId = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                
                if insertedId>0
                {
                    //println("Record inserted at \(insertedId).")
                }
                else
                {
                    //println("Error in inserting record.")
                }
            }
            
            scrollToLoadMore()
            if deepLinkEmail != ""
            {
                isDownloadComplete = true
            }
            
            self.refreshList()
            
            //println(eventObjectIdsString)
            
            let messageIdsString = fetchMessageIds()
            
            var predicate = NSPredicate(format: "NOT (objectId IN {'\(messageIdsString)'}) AND eventObjectId IN {'\(eventObjectIdsString)'}")
            
            var query = PFQuery(className:"EventComments", predicate: predicate)
            
            query.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagesError:", errorSelectorParameters:nil)
            
            predicate = NSPredicate(format: "eventObjectId IN {'\(eventObjectIdsString)'}")
            
            query = PFQuery(className:"EventImages", predicate: predicate)
            
            query.orderByAscending("createdAt")
            
            ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
            
        }
    }
    
    func fetchAllEventsError(timer:NSTimer)
    {
        activityIndicatorView.stopAnimation()
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId NOT IN ( SELECT eventId FROM Events ORDER BY eventId DESC", whereFields: [])
        ModelManager.instance.deleteTableData("Invitations", whereString: "eventObjectId NOT IN (SELECT objectId FROM Events)", whereFields: [])
    }
    
    
    func fetchMessageIds()->String
    {
        var messageIds: [String] = []
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["*"], whereString: "", whereFields: [])
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                messageIds.append(resultSet.stringForColumn("objectId"))
            }
            
        }
        resultSet.close()
        return messageIds.joinWithSeparator("','")
    }
    
    func fetchAllPostsSuccess(timer:NSTimer)
    {
        activityIndicatorView.stopAnimation()
        
        //self.loaderView.hidden = true
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        isPostUpdated = false
        //println("Successfully retrieved \(objects!.count) posts.")
        
        if let fetchedobjects = objects {
            //self.loaderView.hidden=true
            for post in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["postData"] = post["postData"] as? String
                
                tblFields["isApproved"] = "0"
                
                let postHeight = post["postHeight"] as! CGFloat
                let postWidth = post["postWidth"] as! CGFloat
                
                tblFields["postHeight"] = "\(postHeight)"
                tblFields["postWidth"] = "\(postWidth)"
                tblFields["eventObjectId"] = post["eventObjectId"] as? String
                
                tblFields["eventFolder"] = post["eventFolder"] as? String
                
                tblFields["postType"] = post["postType"] as? String
                
                tblFields["objectId"] = post.objectId!
                tblFields["isPosted"] = "1"
                tblFields["isRead"] = "0"
                
                var date = ""
                
                if post.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.createdAt)!)
                    //println(date)
                    tblFields["createdAt"] = date
                }
                
                if post.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.updatedAt)!)
                    //println(date)
                    tblFields["updatedAt"] = date
                }
                
                let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "objectId = '\(post.objectId!)'", whereFields: [])
                
                resultSet.next()
                
                let noOfPosts = Int(resultSet.intForColumn("count"))
                
                resultSet.close()
                
                if noOfPosts > 0
                {
                    var isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "objectId=?", whereFields: [post.objectId!])
                }
                else
                {
                    var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
                }
            }
            
            self.refreshList()
        }
    }
    
    func fetchAllPostsError(timer:NSTimer)
    {
        activityIndicatorView.stopAnimation()
        
        //self.loaderView.hidden = true
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        return mySharedEvents.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        
        return tableView.frame.width*(3.0/4)
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("SharedEventsTableViewCell", forIndexPath: indexPath) as! SharedEventsTableViewCell
        
        let row = indexPath.row
        
        for view in cell.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        //        if mySharedEvents[row]["isRSVP"] as! Bool == true
        //        {
        var eventTitleText: String = (mySharedEvents[row]["eventTitle"] as? String)!
        
        eventTitleText = String(eventTitleText.characters.prefix(1)).capitalizedString + String(eventTitleText.characters.suffix(eventTitleText.characters.count - 1))
        
        let eventImageView = UIImageView()
        
        eventImageView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height+(1*self.view.frame.height/568))
        
        cell.contentView.addSubview(eventImageView)
        
        let eventImageOverlayView = UIView()
        
        eventImageOverlayView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height+(1*self.view.frame.height/568))
        
        eventImageOverlayView.backgroundColor = UIColor.blackColor()
        eventImageOverlayView.alpha = 0.5
        
        cell.contentView.addSubview(eventImageOverlayView)
        
        
        let eventTitleView = UITextView()
        
        eventTitleView.frame = CGRectMake(0.053125*cell.contentView.frame.width, 0.520833*cell.contentView.frame.height, 0.665625*cell.contentView.frame.width, 0.25*cell.contentView.frame.height)
        
        
        
        eventTitleView.text = eventTitleText
        eventTitleView.frame.size.height = eventTitleView.sizeThatFits(eventTitleView.bounds.size).height
        eventTitleView.textColor = UIColor.whiteColor()
        eventTitleView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        eventTitleView.font = UIFont(name: "AvenirNext-DemiBold", size: 18.0)
        eventTitleView.editable = false
        
        cell.contentView.addSubview(eventTitleView)
        
        if eventTitleView.contentSize.height > eventTitleView.frame.height
        {
            eventTitleView.font = UIFont(name: "AvenirNext-Demibold", size: 18.0)
        }
        
        
        let senderNameView = UITextView()
        
        senderNameView.frame = CGRectMake(0.053125*cell.contentView.frame.width, eventTitleView.frame.origin.y+eventTitleView.frame.height, 0.665625*cell.contentView.frame.width, 0.11*cell.contentView.frame.height)
        
        
        //println(eventTitleView.frame.height)
        
        senderNameView.text = mySharedEvents[row]["senderName"] as! String
        senderNameView.textColor = UIColor.whiteColor()
        senderNameView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        senderNameView.font = UIFont(name: "AvenirNext-Medium", size: 14.0)
        senderNameView.editable = false
        
        cell.contentView.addSubview(senderNameView)
        
        
        var pdate: NSDate!
        
        let eventDateView = UITextView()
        
        if mySharedEvents[row]["isRSVP"] as! Bool == true
        {
            
            /*var eventStartDate = mySharedEvents[row]["eventStartDateTime"] as! NSDate
            
            var startTimeStamp = Int64(eventStartDate.timeIntervalSince1970)
            var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
            var eventTimezoneOffset = mySharedEvents[row]["eventTimezoneOffset"] as! Int
            
            var timeStampToBeShown = Int64(startTimeStamp-timezoneOffset+eventTimezoneOffset)
            
            //pdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))*/
            
            pdate = mySharedEvents[row]["eventStartDateTime"] as! NSDate
            
            
            
            //println(pdate)
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: pdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour >= 12)
            {
                if(scomponents.hour > 12)
                {
                    shour = scomponents.hour-12
                }
                else
                {
                    shour = 12
                }
                
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            let startDate = "\(monthsArray[smonth-1]) \(sday), \(shour):\(sminute) \(sam)"
            
            //println("senderName height: \(senderNameView.frame.height)")
            
            eventDateView.frame = CGRectMake(0.053125*cell.contentView.frame.width, senderNameView.frame.origin.y+senderNameView.frame.height - 7 , 0.665625*cell.contentView.frame.width, 0.11*cell.contentView.frame.height)
            
            eventDateView.text = startDate
            eventDateView.textColor = UIColor.whiteColor()
            eventDateView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
            eventDateView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            eventDateView.editable = false
            
            cell.contentView.addSubview(eventDateView)
        }
        else
        {
            //var eventTypeView = UITextView()
            
            //println("senderName height: \(senderNameView.frame.height)")
            
            eventDateView.frame = CGRectMake(0.053125*cell.contentView.frame.width, senderNameView.frame.origin.y+senderNameView.frame.height, 0.665625*cell.contentView.frame.width, 0.11*cell.contentView.frame.height)
            
            eventDateView.text = ""
            eventDateView.textColor = UIColor.whiteColor()
            eventDateView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            
            eventDateView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            eventDateView.editable = false
            
            cell.contentView.addSubview(eventDateView)
            
        }
        
        let loaderCellView = UIView()
        
        let cellIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderCellView.addSubview(cellIndicator)
        
        loaderCellView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height+(1*self.view.frame.height/568))
        
        loaderCellView.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        
        cellIndicator.frame = CGRectMake((cell.contentView.frame.width/2)-(cell.contentView.frame.width*(25/320)/2), (cell.contentView.frame.height/2)-(cell.contentView.frame.width*(25/320)/2), cell.contentView.frame.width*(25/320), cell.contentView.frame.width*(25/320))
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Downloading..."
        loadingMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name:"AvenirNext-Regular", size: 9.0)
        loadingMessage.textAlignment = .Center
        loaderCellView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , cellIndicator.frame.origin.y+(cell.contentView.frame.width*(25/320)+10), cell.contentView.frame.width, 20)
        cellIndicator.startAnimating()
        
        cell.contentView.addSubview(loaderCellView)
        
        let attendingStatusImageView = UIImageView()
        
        attendingStatusImageView.frame = CGRectMake(0.70875*cell.contentView.frame.width+((0.20*cell.contentView.frame.width/2)-(0.0625*cell.contentView.frame.width/2)), (0.520833*cell.contentView.frame.height)+10, 0.0625*cell.contentView.frame.width, 0.0625*cell.contentView.frame.width)
        
        if  mySharedEvents[row]["attendingStatus"] as! String == "yes"
        {
            attendingStatusImageView.image = UIImage(named:"check-green.png")
        }
        
        if  mySharedEvents[row]["attendingStatus"] as! String == "no"
        {
            attendingStatusImageView.image = UIImage(named:"check-red.png")
        }
        
        if  mySharedEvents[row]["attendingStatus"] as! String == "maybe"
        {
            attendingStatusImageView.image = UIImage(named:"questionmark.png")
        }
        
        if  mySharedEvents[row]["attendingStatus"] as! String == "online"
        {
            attendingStatusImageView.image = UIImage(named:"web-globe.png")
        }
        
        if  mySharedEvents[row]["isRSVP"] as! Bool == false
        {
            attendingStatusImageView.image = UIImage(named:"web-globe.png")
        }
        
        cell.contentView.addSubview(attendingStatusImageView)
        
        
        let eventShareCount = UILabel()
        
        //   eventShareCount.frame = CGRectMake(0.70875*cell.contentView.frame.width, attendingStatusImageView.frame.origin.y+attendingStatusImageView.frame.height+20, 0.30*cell.contentView.frame.width, 0.087*cell.contentView.frame.height + 0.087*cell.contentView.frame.height)
        eventShareCount.frame = CGRectMake(cell.contentView.frame.width-50, 15, 0.30*cell.contentView.frame.width, 20)
        
        eventShareCount.lineBreakMode = NSLineBreakMode.ByWordWrapping
        eventShareCount.numberOfLines = 2
        
        //println(0.10*cell.contentView.frame.width)
        
        
        if mySharedEvents[row]["isRSVP"] as! Bool == true
        {
            if  mySharedEvents[row]["isApproved"] as! Bool == true
            {
                
                //let noOfNewPosts = mySharedEvents[row]["noOfNewPosts"] as! Int
                let tempp = badgesArray.valueForKey("eventid") as! NSArray
                let bag = NSCountedSet()
                bag.addObjectsFromArray(tempp as [AnyObject])
                //print (tempp)
                //print (bag.countForObject(arrdata[indexPath.row].valueForKey("keyvalue")!))
                let totalLikeCount = bag.countForObject(mySharedEvents[row].objectId!)
                // print("noOfNewPosts:\(noOfNewPosts)")
                
                if totalLikeCount > 0
                {
                    eventShareCount.layer.masksToBounds = true
                    eventShareCount.layer.cornerRadius = 3.0
                    eventShareCount.text = "\(totalLikeCount)"
                    eventShareCount.backgroundColor = UIColor.redColor()
                    eventShareCount.textColor = UIColor.whiteColor()
                    
                    if totalLikeCount < 10
                    {
                        eventShareCount.frame.size.width = 20
                        eventShareCount.layer.cornerRadius = eventShareCount.frame.size.width/2
                        
                    }
                    else if totalLikeCount < 100
                    {
                        let labelWidth = eventShareCount.sizeThatFits(eventShareCount.bounds.size).width
                        eventShareCount.frame.size.width = labelWidth+14
                        eventShareCount.layer.cornerRadius = 10
                        
                    }
                    else
                    {
                        let labelWidth = eventShareCount.sizeThatFits(eventShareCount.bounds.size).width
                        eventShareCount.frame.size.width = labelWidth+24
                        eventShareCount.layer.cornerRadius = 10
                        
                    }
                    
                    eventShareCount.frame.origin.x = (0.70875*cell.contentView.frame.width)+((0.20*cell.contentView.frame.width/2)-(eventShareCount.frame.width/2))
                    
                    //eventShareCount.frame.size.height = CGFloat(0.08333*cell.contentView.frame.height)
                    eventShareCount.frame.size.height = 15
                }
                else
                {
                    eventShareCount.text = ""
                }
            }
            else
            {
                //eventShareCount.text = "Pending Approval"
                eventShareCount.text = ""
                eventShareCount.textColor = UIColor.lightGrayColor()
                eventShareCount.frame.size.height = 2*CGFloat(0.08333*cell.contentView.frame.height)
                eventShareCount.numberOfLines = 2
            }
            
        }
        else
        {
            if mySharedEvents[row]["isApproved"] as! Bool == false
            {
                eventShareCount.text = "Pending\nApproval"
                eventShareCount.textAlignment = .Center
                eventShareCount.textColor = UIColor.lightGrayColor()
                eventShareCount.frame.size.height = 15
                eventShareCount.numberOfLines = 2
            }
            else
            {
                let noOfNewPosts = mySharedEvents[row]["noOfNewPosts"] as! Int
                
                let tempp = badgesArray.valueForKey("eventid") as! NSArray
                
                let bag = NSCountedSet()
                bag.addObjectsFromArray(tempp as [AnyObject])
                //print (tempp)
                //print (bag.countForObject(arrdata[indexPath.row].valueForKey("keyvalue")!))
                let totalLikeCount = bag.countForObject( mySharedEvents[row].objectId!)
              
                
                
                if noOfNewPosts > 0
                {
                    eventShareCount.layer.masksToBounds = true
                    eventShareCount.layer.cornerRadius = 3.0
                    eventShareCount.text = "\(totalLikeCount)"
                    eventShareCount.backgroundColor = UIColor.redColor()
                    eventShareCount.textColor = UIColor.whiteColor()
                    
                    if noOfNewPosts < 10
                    {
                        eventShareCount.frame.size.width = 20
                        eventShareCount.layer.cornerRadius = eventShareCount.frame.size.width/2
                        
                    }
                    else if noOfNewPosts < 100
                    {
                        let labelWidth = eventShareCount.sizeThatFits(eventShareCount.bounds.size).width
                        eventShareCount.frame.size.width = labelWidth+14
                        eventShareCount.layer.cornerRadius = 10
                        
                    }
                    else
                    {
                        let labelWidth = eventShareCount.sizeThatFits(eventShareCount.bounds.size).width
                        eventShareCount.frame.size.width = labelWidth+24
                        eventShareCount.layer.cornerRadius = 10
                        
                    }
                    
                    eventShareCount.frame.origin.x = (0.70875*cell.contentView.frame.width)+((0.20*cell.contentView.frame.width/2)-(eventShareCount.frame.width/2))
                    
                    //eventShareCount.frame.size.height = CGFloat(0.08333*cell.contentView.frame.height)
                    eventShareCount.frame.size.height = 15
                }
                else
                {
                    eventShareCount.text = ""
                }
            }
        }
        
        eventShareCount.textAlignment = .Center
        
        eventShareCount.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        //attendingStatusImageView.frame.origin.x = eventShareCount.frame.origin.x + ((eventShareCount.frame.width/2)-(attendingStatusImageView.frame.width/2))
        
        eventShareCount.frame.origin.y = eventDateView.frame.origin.y + ((eventDateView.frame.height/2)-(eventShareCount.frame.height/2))
        
        eventShareCount.frame.origin.x = attendingStatusImageView.frame.origin.x + ((attendingStatusImageView.frame.width/2)-(eventShareCount.frame.width/2))
        
        eventShareCount.frame.size.height = 15
        
        cell.contentView.addSubview(eventShareCount)
        
        let eventCellOverlayView = UIView()
        
        eventCellOverlayView.frame = CGRectMake(0, 0, cell.contentView.frame.width, cell.contentView.frame.height+(1*self.view.frame.height/568))
        
        eventCellOverlayView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        
        cell.contentView.addSubview(eventCellOverlayView)
        
        
        let eventImageFile = mySharedEvents[row]["eventImage"] as! String
        
        let eventFolder = mySharedEvents[row]["eventFolder"] as! String
        
        let eventImagePath = "\(documentDirectory)/\(eventImageFile)"
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath(eventImagePath)) {
            let image = UIImage(contentsOfFile:  "\(documentDirectory)/\(eventImageFile)")
            eventImageView.image = image
            loaderCellView.hidden = true
        }
        else
        {
            let fileName = eventImageFile
            //            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
            //                //			println(self)
            //            }
            //
            
            
            
            //            let tempstr = String("http://d513o5zfkznc5.cloudfront.net/\(eventFolder)\(fileName)")
            //            let tempurl = NSURL(string: tempstr)
            //            eventImageView.sd_setImageWithURL(tempurl, completed: block)
            //            loaderCellView.hidden = true
            
            var isDownloading = mySharedEvents[row]["isDownloading"] as! Bool
            //println("isDownloading for \(row) is \(isDownloading)")
            
            if mySharedEvents[row]["isDownloading"] as! Bool == true
            {
                mySharedEvents[row]["isDownloading"] = false
                
                let s3BucketName = "eventnodepublicpics"
                
                let downloadFilePath = (documentDirectory as NSString).stringByAppendingPathComponent(fileName)
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                //println("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            //println("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        //println("downloading successfull")
                        
                        let image = UIImage(named: "\(documentDirectory)/\(eventImageFile)")
                        eventImageView.image = image
                        loaderCellView.hidden = true
                        
                    }
                    
                    return nil
                    
                })
            }
        }
        
        
        /* var eventOriginalImageFile = mySharedEvents[row]["originalEventImage"] as! String
        
        var eventOriginalImagePath = "\(documentDirectory)/\(eventOriginalImageFile)"
        let managerOriginal = NSFileManager.defaultManager()
        
        if (managerOriginal.fileExistsAtPath(eventOriginalImagePath) != true)
        {
        let s3OriginalBucketName = "eventnodepublicpics"
        let fileOriginalName = eventOriginalImageFile
        
        let downloadOriginalFilePath = documentDirectory.stringByAppendingPathComponent(fileOriginalName)
        let downloadingOriginalFileURL = NSURL.fileURLWithPath(downloadOriginalFilePath)
        
        let downloadOriginalRequest = AWSS3TransferManagerDownloadRequest()
        downloadOriginalRequest.bucket = s3OriginalBucketName
        downloadOriginalRequest.key  = "\(eventFolder)\(fileOriginalName)"
        downloadOriginalRequest.downloadingFileURL = downloadingOriginalFileURL
        
        let transferOriginalManager = AWSS3TransferManager.defaultS3TransferManager()
        transferOriginalManager.download(downloadOriginalRequest).continueWithBlock {
        (task: AWSTask!) -> AnyObject! in
        
        if task.error != nil {
        //println("Error downloading")
        //println(task.error.description)
        }
        else{
        
        }
        return nil
        }
        
        }*/
        eventShareCount.frame = CGRectMake(attendingStatusImageView.frame.origin.x, 15, eventShareCount.frame.size.width, 20)
        cell.selectionStyle = .None
        
        // }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        print (mySharedEvents)
        currentSharedEvent = mySharedEvents[indexPath.row]
        
        mySharedEventData = []
        
        if((mySharedEvents[indexPath.row]["isApproved"] as! Bool) == true || (mySharedEvents[indexPath.row]["isRSVP"] as! Bool) == true)
        {
            isOnSharedEvents = false
            
            print(mySharedEvents[indexPath.row])
            
            let streamVC = self.storyboard!.instantiateViewControllerWithIdentifier("StreamViewController") as! StreamViewController
            
            self.navigationController?.pushViewController(streamVC, animated: true)
        }
        else
        {
            if mySharedEvents[indexPath.row]["isApproved"] as! Bool == false
            {
                Util.invokeAlertMethod("", strBody: "This is a private channel and the host needs to approve you before you can get access to it.", delegate: nil)
            }
        }
    }
    
    
    /*func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    
    }*/
    
    
    func createInvitation()
    {
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["isApproved"] = "1"
        tblFields["isUpdated"] = "1"
        tblFields["isEventUpdated"] = "1"
        
        tblFields["objectId"] = "ewrewrer"
        tblFields["invitedName"] = "rewrewrewrer"
        tblFields["userObjectId"] = "retyrgrhyr"
        tblFields["eventObjectId"] = "jkffgregrt"
        tblFields["emailId"] = "ewfergtee@fertre.tewr"
        tblFields["attendingStatus"] = "yes"
        
        tblFields["invitationType"] = "email"
        tblFields["noOfChilds"] = "3"
        tblFields["noOfAdults"] = "2"
        tblFields["invitationNote"] = "ergewgtgergrg"
        
        tblFields["needsContentApprovel"] = "0"
        tblFields["createdAt"] = "2015-12-08 22:23:54"
        
        tblFields["updatetAt"] = "2015-12-08 22:23:54"
        
        tblFields["isPosted"] = "1"
        
        
        
        var insertedId = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
    }
    
    
    @IBAction func invitationCodeButtonClicked(sender: UIButton) {
        
//        isOnSharedEvents = false
//        
//        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteCodeViewController") as! InviteCodeViewController
//        eventVC.backOrPop = false
//        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func streamButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        
        isOnSharedEvents = false
        
        let streamVC = self.storyboard!.instantiateViewControllerWithIdentifier("StreamViewController") as! StreamViewController
        self.navigationController?.pushViewController(streamVC, animated: true)
    }
    
    
    @IBAction func eventButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        
        isOnSharedEvents = false
        
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        
        isOnSharedEvents = false
        
        let settingsVC = self.storyboard!.instantiateViewControllerWithIdentifier("SettingsViewControllerOne") as! settingViewControllerOne
        self.navigationController?.pushViewController(settingsVC, animated: false)
    }
    
    @IBAction func alertButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        
        isOnSharedEvents = false
        
        let alertVC = self.storyboard!.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
        self.navigationController?.pushViewController(alertVC, animated: false)
    }
    
    
    @IBAction func MyEventsBtn(sender: AnyObject)
    {
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: false)
    }
    
    
    @IBAction func invitedEventBtn(sender: AnyObject) {
    }
    
    
    @IBAction func channelView(sender: AnyObject)
    {
        let channelVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelEventsViewController") as! ChannelEventsViewController
        self.navigationController?.pushViewController(channelVC, animated: false)
    }
    
}
