//
//  EventDescriptionViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/24/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class EventDescriptionViewController: UIViewController {

    @IBOutlet var evenNameText : UITextView!
    @IBOutlet weak var headerTitle: UILabel!
    
    var hTitle = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    evenNameText.autocorrectionType = UITextAutocorrectionType.No
        self.view.addSubview(wakeUpImageView)
        
        if hTitle.characters.count > 0
        {
            headerTitle.text = hTitle
        }
        
        print(newEvent)
        
        // Do any additional setup after loading the view.
        evenNameText.text = newEvent["eventDescription"] as! String
        
        evenNameText.becomeFirstResponder()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func keyboardWillShow(sender: NSNotification)
    {
        
        var height : CGFloat! = 0
        
        if self.view.frame.width == 320
        {
            height = 230
        }
        else if self.view.frame.width == 375 && self.view.frame.width == 414
        {
            height = 240
        }
        else
        {
            height = 240
        }
        
        evenNameText.frame = CGRectMake(15 , 90, evenNameText.frame.width, evenNameText.frame.height - height)
    }
    func keyboardWillHide(sender: NSNotification) {
        var height : CGFloat! = 0
        
        if self.view.frame.width == 320
        {
            height = 230
        }
        else if self.view.frame.width == 375 && self.view.frame.width == 414
        {
            height = 240
        }
        else
        {
            height = 240
        }

        
        evenNameText.frame = CGRectMake(15 , 90, evenNameText.frame.width, evenNameText.frame.height + height)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventVC.imageData=imageData
        self.navigationController?.pushViewController(eventVC, animated: true)*/
        newEvent["eventDescription"] = evenNameText.text
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    @IBAction func viewTapped(sender : AnyObject) {
        evenNameText.resignFirstResponder()
    }

    
    @IBAction func saveButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
       /* let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
        eventVC.imageData=imageData
        self.navigationController?.pushViewController(eventVC, animated: true)*/
        newEvent["eventDescription"] = evenNameText.text
        self.navigationController?.popViewControllerAnimated(true)
    }
}
