//
//  EventSettingsViewController.swift
//  Eventnode
//
//  Created by brst on 7/31/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
//import Crashlytics

var isLocationUpdated: Bool!
var isDateUpdated:  Bool!
var isTimeZoneUpdated:  Bool!
//

class EventSettingsViewController: UIViewController,UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var eventImageLbl: UILabel!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var deleteChannelLabel: UILabel!
    @IBOutlet weak var channelDescription: UILabel!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var deleteEvent: UIView!
    
    @IBOutlet weak var showDate: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var editEventTitle: UITextField!
    
    @IBOutlet weak var invitationPreview: UIView!
    @IBOutlet weak var noteFromhost: UIView!
    @IBOutlet weak var location: UIView!
    @IBOutlet weak var dateandTime: UIView!
    @IBOutlet weak var timeZoneView: UIView!
    @IBOutlet weak var timezoneLabel: UILabel!
    
    var timeZonesString: Array<String!> = []
    var timeZoneNames: Array<String!> = []
    var messagesRef: Firebase!
    
    @IBOutlet var loaderView: UIView!
    @IBOutlet var loaderSubView: UIView!
    @IBOutlet weak var eventTitleTextField: UITextField!
    var loadingMessage = UILabel()
    var startDate = NSString()
    var showPicker = false
    var showTimezonePicker = false
    
    let tapRec = UITapGestureRecognizer()
    var isToBeEdit = false
    var isToBeBack = false
    
    var currentUserId: String!
    
    var timeZoneViewY: CGFloat!
    var timeZonePickerY: CGFloat!
    var invitationPreviewY: CGFloat!
    var deleteEventY: CGFloat!
    
    var eventDateTime: NSDate!
    var currentEventTimezone: String!
    var isChannel = ""
    
    @IBOutlet var datePickerView : UIDatePicker! = UIDatePicker()
    @IBOutlet var timezonePickerView: UIPickerView!
    //    var datePickerView  : UIDatePicker! = UIDatePicker(frame(CGRectMake,0, 330,self.scrollView.frame.size.width,230))
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        //var arrayndex = ["1","2","3","4"]
        
        //arrayndex.insert("4", atIndex: 7)
        
        print (currentEvent)
        
        if currentEvent["isRSVP"] as! Bool
        {
            eventDateTime = currentEvent["eventStartDateTime"] as! NSDate
            currentEventTimezone = currentEvent["timezoneName"] as! String
        }
        
        
        print(isChannel)
        
        let isRsvp: Bool = currentEvent["isRSVP"] as! Bool
        
        if isRsvp == true
        {
            isChannel = "false"
        }
        else
        {
            isChannel = "true"
            headerLbl.text = "Channel Settings"
        }
        
        
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        isLocationUpdated = false
        isDateUpdated = false
        isTimeZoneUpdated = false
        
        timeZoneViewY = self.timeZoneView.frame.origin.y
        timeZonePickerY = self.timezonePickerView.frame.origin.y
        invitationPreviewY = self.invitationPreview.frame.origin.y
        deleteEventY = self.deleteEvent.frame.origin.y
        
        
        self.view.addSubview(wakeUpImageView)
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        loadingMessage.text = "Saving..."
        //   printMessage.textColor = UIColor.whiteColor()
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        loadingMessage.textAlignment = .Center
        loaderSubView.addSubview(loadingMessage)
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        indicator.startAnimating()
        loaderSubView.layer.cornerRadius = 10
        self.loaderView.hidden = true
        
        
        isUpdated = false
        
        eventTitleTextField.delegate = self
        
        //println(currentEvent["eventTitle"] as! String)
        eventTitleTextField.text = currentEvent["eventTitle"]  as! String
        
        
        
        if( isRsvp == false )
        {
            
            datePickerView.hidden = true
            //noteFromhost.hidden = true
            eventImageLbl.text = "Channel Image"
            channelDescription.text = "Channel Description"
            deleteChannelLabel.text = "Delete Channel"
            location.hidden = true
            invitationPreview.hidden = true
            dateandTime.hidden = true
            timeZoneView.hidden = true
        }
        else
        {
            
            //println(currentEvent["eventStartDateTime"])
            //println(currentEvent["eventTimezoneOffset"] as! Int)
            
            //var sDate = currentEvent["eventStartDateTime"] as? NSDate
            
            
            currentTimeZone = currentEvent["timezoneName"] as! String
            
            let formatter = NSDateFormatter();
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
            
            formatter.timeZone = NSTimeZone(name: currentTimeZone)
            //formatter.timeZone = NSTimeZone.localTimeZone()
            
            ////println(formatter.dateFromString(formatter.stringFromDate(datePickerView.date)))
            
            
            let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
            
            let localTimezoneOffset = Double(NSTimeZone.localTimeZone().secondsFromGMT)
            
            currentEvent["eventStartDateTime"] = NSDate(timeIntervalSince1970: (currentEvent["eventStartDateTime"] as! NSDate).timeIntervalSince1970 + timezoneOffset - localTimezoneOffset)
            print (currentEvent["eventStartDateTime"])
            datePickerView.datePickerMode = UIDatePickerMode.DateAndTime
            datePickerView.backgroundColor = UIColor(red: 225/255, green: 241/255, blue: 249/255, alpha: 1.0)
            datePickerView.setDate(currentEvent["eventStartDateTime"] as! NSDate, animated: true)
            
            datePickerView.minuteInterval = 15
            
            datePickerView.addTarget(self, action: Selector("dateChanged:"), forControlEvents: UIControlEvents.ValueChanged)
            
            datePickerView.hidden = true
            
            
            datePickerView.minimumDate = NSDate()
            
            if var eventStartDate = currentEvent["eventStartDateTime"] as? NSDate
            {
                /*var startTimeStamp = Int64(eventStartDate.timeIntervalSince1970)
                var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
                var eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
                
                var timeStampToBeShown = Int64(startTimeStamp-timezoneOffset+eventTimezoneOffset)
                
                //october 22 2015, 8:30 pm
                
                var sdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))*/
                
                showDate.text = getFormatedStringFromDate(currentEvent["eventStartDateTime"] as! NSDate)
                
            }
            else
            {
                showDate.text = ""
            }
            
            
            showDate.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
            showDate.textAlignment = .Right
            
            
            let path = NSBundle.mainBundle().pathForResource("timezones", ofType: "plist")
            let dict = NSDictionary(contentsOfFile: path!)
            
            let tzDict = dict!.objectForKey("TimeZones") as! NSDictionary
            
            //println(dict!.objectForKey("TimeZones"))
            timeZonesString = []
            
            timeZoneNames = tzDict.allKeys as! Array<String!>
            
            var timeZonesStringTemp = tzDict.allValues as! Array<String!>
            
            var timeZoneNamesTemp = tzDict.allKeys as! Array<String!>
            
            
            
            
            timeZoneNames.sortInPlace( { $0 < $1 } )
            
            //println(timeZoneNames)
            
            for var i = 0; i < timeZoneNames.count; i++
            {
                for var j = 0; j < timeZonesStringTemp.count; j++
                {
                    if timeZoneNames[i] == timeZoneNamesTemp[j]
                    {
                        timeZonesString.append(timeZonesStringTemp[j])
                    }
                }
            }
            
            for var i = 0; i < timeZonesString.count; i++
            {
                if timeZonesString[i] == currentTimeZone
                {
                    currentTimeZoneName = timeZoneNames[i]
                }
            }
            
            addTimezonePicker()
            
        }
        
        
        
        tapRec.addTarget(self, action: "tapped")
        scrollView.addGestureRecognizer(tapRec)
        scrollView.userInteractionEnabled = true
        
    }
    
    
    override func viewDidAppear(animated: Bool) {
//        messagesRef.removeAllObservers()

        /*if var timeZoneName = NSUserDefaults.standardUserDefaults().objectForKey("timeZoneName") as? String
        {
        if NSTimeZone.localTimeZone().name != timeZoneName
        {
        NSUserDefaults.standardUserDefaults().setObject("\(NSTimeZone.localTimeZone().name)", forKey: "timeZoneName")
        var isDeleted1 = ModelManager.instance.deleteTableData("Events", whereString: "1", whereFields: [])
        
        
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: true)
        }
        }
        else
        {
        NSUserDefaults.standardUserDefaults().setObject("\(NSTimeZone.localTimeZone().name)", forKey: "timeZoneName")
        
        
        let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(eventVC, animated: true)
        
        }*/
        
    }
    
    
    func addTimezonePicker()
    {
        
        
        timezonePickerView.delegate = self
        timezonePickerView.dataSource = self
        
        
        //timeZonesString
        
        //timezonePickerView.select(NSTimeZone.localTimeZone().name)
        
        var timeZones = NSTimeZone.knownTimeZoneNames()
        var i = 0
        var selectedIndex = 0;
        
        for var i = 0; i < timeZonesString.count; i++
        {
            if timeZonesString[i] == currentTimeZone
            {
                selectedIndex = i
            }
        }
        
        timezonePickerView.selectRow(selectedIndex, inComponent: 0, animated: true)
        
        timezonePickerView.backgroundColor = UIColor(red: 225/255, green: 241/255, blue: 249/255, alpha: 1.0)
        
        //timezonePickerView.addTarget(self, action: Selector("dateChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        timezonePickerView.hidden = true
        
        let formatter = NSDateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        
        //formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.timeZone = NSTimeZone(name: currentTimeZone)
        
        
        timezoneLabel.text = currentTimeZoneName
        timezoneLabel.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
        timezoneLabel.textAlignment = .Right
        
        //self.inPersonDetailsView.addSubview(timezonePickerView)
        
        
    }
    
    func showLoader(message: String)
    {
        loadingMessage.text = "\(message)"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        loaderView.hidden = false
    }
    
    
    func dateChanged(sender: UIDatePicker)
    {
        let timeFormatter = NSDateFormatter()
        timeFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        startDate = timeFormatter.stringFromDate(datePickerView.date)
        currentEvent["eventStartDateTime"] = datePickerView.date
        
        
        
        isUpdated = true
        isDateUpdated = true
        
        let formatter = NSDateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        //let defaultTimeZoneStr = formatter.stringFromDate(date);
        // "2014-07-23 11:01:35 -0700" <-- same date, local, but with seconds
        //formatter.timeZone = NSTimeZone(name: currentTimeZone)
        
        ////println(formatter.timeZone.name)
        //NSTimeZone(forSecondsFromGMT: formatter.timeZone.secondsFromGMT)
        
        ////println(formatter.dateFromString(formatter.stringFromDate(datePickerView.date)))
        
        //newEvent["eventStartDateTime"] = datePickerView.date
        
        //currentEvent["eventStartDateTime"] = formatter.dateFromString(formatter.stringFromDate(datePickerView.date))!
        
        currentEvent["eventStartDateTime"] = datePickerView.date
        
        showDate.text = getFormatedStringFromDate(datePickerView.date)
        showDate.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
        showDate.textAlignment = .Right
        
        //timezoneLabel.text = currentTimeZone
        //timezoneLabel.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
        //timezoneLabel.textAlignment = .Right
        
    }
    
    
    func numberOfComponentsInPickerView(colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(currentEvent)
        currentTimeZone = timeZonesString[row]
        currentTimeZoneName = timeZoneNames[row]
        //dateChanged(datePickerView)
        
        timezoneLabel.text = currentTimeZoneName
        timezoneLabel.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
        timezoneLabel.textAlignment = .Right
        
        let formatter = NSDateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        
        formatter.timeZone = NSTimeZone(name: currentTimeZone)
        
        ////println(formatter.timeZone.name)
        //NSTimeZone(forSecondsFromGMT: formatter.timeZone.secondsFromGMT)
        
        //datePickerView.timeZone = NSTimeZone(name: currentTimeZone)
        
        ////println(formatter.dateFromString(formatter.stringFromDate(datePickerView.date)))
        ////println(formatter.dateFromString(formatter.stringFromDate(newEvent["eventStartDateTime"] as! NSDate)))
        
        //newEvent["eventStartDateTime"] = datePickerView.date
        
        isUpdated = true
        isTimeZoneUpdated = true
        if pickerView == timezonePickerView
        {
        }
        
        currentEvent["eventStartDateTime"] = datePickerView.date
        
        
        //println(formatter.dateFromString(formatter.stringFromDate(datePickerView.date)))
        //println(formatter.stringFromDate(NSDate()))
        
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return timeZoneNames[row]
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return timeZonesString.count
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tapped()
    {
        
        eventTitleTextField.resignFirstResponder()
        
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        eventTitleTextField.resignFirstResponder()
        eventTitleTextField.userInteractionEnabled = false
    }
    
    
    @IBAction func editEventTitle(sender: AnyObject)
    {
        //handler(UIDatePicker())
        
        if isToBeEdit
        {
            eventTitleTextField.userInteractionEnabled = false
            eventTitleTextField.resignFirstResponder()
            isToBeEdit = false
        }
        else
        {
            
            eventTitleTextField.userInteractionEnabled = true
            eventTitleTextField.becomeFirstResponder()
            isToBeEdit = true
            
        }
        
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        return true
    }
    
    
    func textFieldDidEndEditing(textField: UITextField)
    {
        currentEvent["eventTitle"] = eventTitleTextField.text!
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool{
        
        //println(string)
        
        //\u200B
        var _char = string.cStringUsingEncoding(NSUTF8StringEncoding)
        
        
        
        if(string=="")
        {
            //println("sdsdfs__\(_char?.count)")
        }
        
        // eventTitleLabel.text = eventTitle.text
        eventTitleTextField.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        // eventTitleTextField.textAlignment =
        //eventTitleTextField.font = UIFont(name: "Monsterrat - Regular", size: 15.0)
        
        if(eventTitleTextField.text! != "")
        {
            if eventTitleTextField.text!.characters.count < 30 {
                //println("eerfre")
                
                // //println("")
                isUpdated = true
                currentEvent["eventTitle"] = eventTitleTextField.text!
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            
            return true
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        eventTitleTextField.resignFirstResponder()
        return true
    }
    
    
    
    @IBAction func eventImages(sender: AnyObject)
    {
        let eventImage = self.storyboard!.instantiateViewControllerWithIdentifier("ChangeEventImageViewController") as! ChangeEventImageViewController
        
        self.navigationController?.pushViewController(eventImage, animated: false)
        
        
    }
    
    
    @IBAction func noteFromHost(sender: AnyObject) {
        
        let updateDescription = self.storyboard!.instantiateViewControllerWithIdentifier("UpdateDescriptionViewController") as! UpdateDescriptionViewController
        
        updateDescription.isChannel = isChannel
        
        self.navigationController?.pushViewController(updateDescription, animated: false)
        
    }
    
    @IBAction func locationbtn(sender: AnyObject)
    {
        let addLocationVC = self.storyboard!.instantiateViewControllerWithIdentifier("UpdateLocationViewController") as! UpdateLocationViewController
        addLocationVC.eventTitle = eventTitleTextField.text
        self.navigationController?.pushViewController(addLocationVC, animated: false)
    }
    
    
    @IBAction func Date(sender: AnyObject)
    {
        
        
        if showPicker
        {
            
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height - 85)
            self.datePickerView.hidden = true
            self.timezonePickerView.hidden = true
            //            showDate.hidden = true
            self.invitationPreview.frame.origin.y = invitationPreviewY
            self.deleteEvent.frame.origin.y = deleteEventY
            
            self.timeZoneView.frame.origin.y = timeZoneViewY
            
            showPicker = false
            showTimezonePicker = false
        }
        else
        {
            //            showDate.hidden = false
            //showDate.text = self.sDate as String
            showPicker = true
            showTimezonePicker = false
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height + self.datePickerView.frame.size.height - 85)
            self.datePickerView.hidden = false
            self.timezonePickerView.hidden = true
            
            self.invitationPreview.frame.origin.y = invitationPreviewY + datePickerView.frame.height
            self.deleteEvent.frame.origin.y = deleteEventY + datePickerView.frame.height
            
            self.timeZoneView.frame.origin.y = timeZoneViewY + datePickerView.frame.height
            
            /*showDate.text = getFormatedStringFromDate(datePickerView.date)
            showDate.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
            showDate.textAlignment = .Right
            
            dateChanged(datePickerView)*/
            
        }
    }
    
    @IBAction func timezoneButtonClicked(sender: UIButton) {
        
        if showTimezonePicker
        {
            
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height - 85)
            self.datePickerView.hidden = true
            self.timezonePickerView.hidden = true
            //            showDate.hidden = true
            self.invitationPreview.frame.origin.y = invitationPreviewY
            self.deleteEvent.frame.origin.y = deleteEventY
            
            self.timeZoneView.frame.origin.y = timeZoneViewY
            
            showPicker = false
            showTimezonePicker = false
        }
        else
        {
            //            showDate.hidden = false
            //showDate.text = self.sDate as String
            showPicker = false
            showTimezonePicker = true
            self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width,self.view.frame.size.height + self.datePickerView.frame.size.height - 85)
            self.datePickerView.hidden = true
            self.timezonePickerView.hidden = false
            
            self.invitationPreview.frame.origin.y = invitationPreviewY + timezonePickerView.frame.height
            self.deleteEvent.frame.origin.y = deleteEventY + timezonePickerView.frame.height
            
            self.timeZoneView.frame.origin.y = timeZoneViewY
            
            /*showDate.text = getFormatedStringFromDate(datePickerView.date)
            showDate.font = UIFont(name: "AvenirNext-Regular", size: 11.0)
            showDate.textAlignment = .Right
            
            dateChanged(datePickerView)*/
            
        }
        
    }
    
    
    @IBAction func previewInviteBtn(sender: AnyObject)
    {
        let eventPreviewVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPreviewViewController") as! EventPreviewViewController
        //eventPreviewVC.imageData=imageData
        eventPreviewVC.eventTitle = currentEvent["eventTitle"] as! String
        eventPreviewVC.eventObject = currentEvent
        print(currentEvent)
        self.navigationController?.pushViewController(eventPreviewVC, animated: true)
    }
    
    @IBAction func deleteEventBtn(sender: AnyObject)
    {
        var strtemp = ""
        var strtemp1 = ""
        
        if self.isChannel == "true"
        {
            strtemp = "Delete Channel"
            strtemp1 = "This Channel will be deleted now. This cannot be undone."
        }
        else
        {
            strtemp = "Delete Event"
            strtemp1 = "This Event will be deleted now. This cannot be undone."
            
        }
        let refreshAlert = UIAlertController(title: strtemp, message:strtemp1 , preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Delete", style: .Default, handler: { (action: UIAlertAction) in
            
            if self.isChannel == "true"
            {
                self.showLoader("Deleting Channel")
            }
            else
            {
                self.showLoader("Deleting Event")
            }
            
            
            
            
            var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("deleteEventFromParse:"), userInfo: currentEvent, repeats: false)
            
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    
    @IBAction func cancelBtn(sender: AnyObject)
    {
        isToBeBack = true
        
        if isUpdated == true
        {
            let refreshAlert = UIAlertController(title: "Discard Changes", message: "You have pending changes. If you do not save these changes they will be discarded. Do you still want to continue?", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action: UIAlertAction) in
                self.saveData()
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Discard", style: .Default, handler: { (action: UIAlertAction) in
                
                if self.isChannel == "false"
                {
                    currentEvent["eventStartDateTime"] = self.eventDateTime
                    currentEvent["timezoneName"] = self.currentEventTimezone
                }
                
                self.navigationController?.popViewControllerAnimated(false)
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
            
        }
        else
        {
            if currentEvent["isRSVP"] as! Bool
            {
                currentEvent["eventStartDateTime"] = eventDateTime
                currentEvent["timezoneName"] = currentEventTimezone
            }
            print(currentEvent)
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    @IBAction func saveButton(sender: UIButton)
    {
        isToBeBack = false
        saveData()
    }
    
    
    func saveData()
    {
        showLoader("Saving Changes...")
        let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
        currentEvent["eventTimezoneOffset"] = timezoneOffset
        
        var title = eventTitleTextField.text!
        title = title.stringByReplacingOccurrencesOfString(" ", withString: "", options: [], range: nil)
        ///Users/brst981/Desktop/eventnode_backup/19 october 2015/eventnode/Eventnode/EventSettingsViewController.swift:94:18: Cannot assign to immutable value of type 'String?'
        eventTitleTextField.resignFirstResponder()
        
        if title != ""
        {
            loaderView.hidden = false
            
            if(isUpdated == true)
            {
                currentEvent["eventTitle"] = eventTitleTextField.text!
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventTitle"] = eventTitleTextField.text!
                
                
                tblFields["isPosted"] = "0"
                
                if(currentEvent["isRSVP"] as? Bool == true)
                {
                    tblFields["isRSVP"] = "1"
                    
                    var date = ""
                    if currentEvent["eventStartDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        
                        let formatter = NSDateFormatter();
                        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
                        
                        formatter.timeZone = NSTimeZone(name: currentTimeZone)
                        
                        let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
                        
                        let localTimezoneOffset = Double(NSTimeZone.localTimeZone().secondsFromGMT)
                        
                        let eventStartDateTime = NSDate(timeIntervalSince1970: (currentEvent["eventStartDateTime"] as! NSDate).timeIntervalSince1970 - timezoneOffset + localTimezoneOffset)
                        
                        date = dateFormatter.stringFromDate((eventStartDateTime))
                        
                        //date = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
                        //println(date)
                        tblFields["eventStartDateTime"] = date
                    }
                    
                    if currentEvent["eventEndDateTime"] != nil {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((currentEvent["eventEndDateTime"] as? NSDate)!)
                        //println(date)
                        tblFields["eventEndDateTime"] = date
                        //println(tblFields["eventEndDateTime"])
                    }
                    
                    tblFields["eventDescription"] = currentEvent["eventDescription"] as? String
                    let eventLatitude = currentEvent["eventLatitude"] as! Double
                    let eventLongitude = currentEvent["eventLongitude"] as! Double
                    
                    
                    tblFields["eventLatitude"] = "\(eventLatitude)"
                    tblFields["eventLongitude"] = "\(eventLongitude)"
                    
                    tblFields["eventLocation"] = currentEvent["eventLocation"] as? String
                    currentEvent["timezonedisplayname"] = currentTimeZoneName
                    
                    //loaderView.hidden = true
                }
                else
                {
                    tblFields["isRSVP"] = "0"
                    
                    //loaderView.hidden = true
                }
                
                let formatter = NSDateFormatter();
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
                
                formatter.timeZone = NSTimeZone(name: currentTimeZone)
                
                let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
                
                //var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
                
                tblFields["eventTimezoneOffset"] = "\(timezoneOffset)"
                tblFields["timezoneName"] = currentTimeZone
                currentEvent["timezoneName"] = currentTimeZone
                let isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [currentEvent.objectId!])
                
                if isUpdated
                {
                    //showLoader("Updating Event")
                    
                    var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("uploadEventWithoutImage:"), userInfo: currentEvent["eventId"] as! Int, repeats: false)
                }
                else
                {
                    self.loaderView.hidden = true
                    Util.invokeAlertMethod("", strBody: "Error in updating record.", delegate: nil)
                    
                }
                //loaderView.hidden = true
            }
            else
            {
                loaderView.hidden = true
            }
        }
        else
        {
            Util.invokeAlertMethod("", strBody: "Please enter event title.", delegate: nil)
        }
    }
    
    
    func updateEvent()
    {
        currentEvent["isUpdated"] = true
        
        currentEvent["timezoneName"] = currentTimeZone
        currentEventTimezone = currentTimeZone
        
        let formatter = NSDateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        
        formatter.timeZone = NSTimeZone(name: currentTimeZone)
        //formatter.timeZone = NSTimeZone.localTimeZone()
        let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
        
        // var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
        
        currentEvent["eventTimezoneOffset"] = timezoneOffset
        
        ParseOperations.instance.saveData(currentEvent, target: self, successSelector: "updateEventSuccess:", successSelectorParameters: nil, errorSelector: "updateEventError:", errorSelectorParameters:currentEvent)
    }
    
    func uploadEventWithoutImage(timer: NSTimer)
    {
        var insertedId = timer.userInfo as! Int
        
        if MyReachability.isConnectedToNetwork()
        {
            updateEvent()
        }
        else
        {
            isPostUpdated = true
        }
    }
    
    func displayLoader(message: String )
    {
        self.loadingMessage.text = "\(message)"
        self.loadingMessage.textColor = UIColor.whiteColor()
        self.loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        self.loadingMessage.numberOfLines = 2
        self.loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        self.loadingMessage.textAlignment = .Center
        self.loaderView.hidden = false
    }
    
    func updateEventSuccess(timer:NSTimer)
    {
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        self.loaderView.hidden=true
        var tblFields: Dictionary! = [String: String]()
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            //println(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            //println(date)
            tblFields["updatedAt"] = date
        }
        
        let data = [
            "eventObjectId": eventObject.objectId!,
            "emailId": "noemail",
            "eventCreatorId": "\(currentUserId)",
            "isRSVP" : isChannel
            
        ]
        
        let formatter = NSDateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        
        formatter.timeZone = NSTimeZone(name: currentTimeZone)
        
        let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
        
        let localTimezoneOffset = Double(NSTimeZone.localTimeZone().secondsFromGMT)
        
        var Cdate : NSDate!
        
        if isChannel == "true"
        {
            Cdate = NSDate()
        }
        else
        {
            Cdate = currentEvent["eventStartDateTime"] as! NSDate
        }
        
        
        eventObject["eventStartDateTime"] = NSDate(timeIntervalSince1970: (Cdate).timeIntervalSince1970 - timezoneOffset + localTimezoneOffset)
        
        let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS9 = iosVersion >= 9
        let iOS7 = iosVersion >= 8 && iosVersion < 9
        
        var rsvp = ""
        
        if currentEvent["isRSVP"] as! Bool == true
        {
            rsvp = "true"
        }
        else
        {
            rsvp = "false"
        }
        
        
        if iOS9
        {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            branchUniversalObject.title = "\(eventTitle)"
            branchUniversalObject.contentDescription = "My Content Description"
            // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
            branchUniversalObject.addMetadataKey("eventObjectId", value: eventObject.objectId!)
            branchUniversalObject.addMetadataKey("emailId", value: "noemail")
            branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
            branchUniversalObject.addMetadataKey("isRSVP", value:rsvp)
            print(rsvp)
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            //linkProperties.channel = "email"
            
            
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        tblFields["socialSharingURL"] = url! as String
                        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [currentEvent["eventId"] as! Int])
                        
                        eventObject["socialSharingURL"] = url! as String
                        
                        eventObject.saveInBackground()
                        
                        
                    }
            })
            
        }
        else
        {
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                //
                if error == nil
                {
                    //println(url!)
                    tblFields["socialSharingURL"] = url! as String
                    var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [currentEvent["eventId"] as! Int])
                    
                    eventObject["socialSharingURL"] = url! as String
                    
                    eventObject.saveInBackground()
                }
                
            })
        }
        
        
        
        
        isUpdated = false
        
        var isUpdated1 = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "objectId=?", whereFields: [currentEvent.objectId!])
        
        //println(currentEvent.objectId)
        
        let predicate = NSPredicate(format: "eventObjectId IN {'\(currentEvent.objectId!)'}")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventInvitationsSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventInvitationsError:", errorSelectorParameters: nil)
        
        if isToBeBack == true
        {
            
            // currentEvent["eventStartDateTime"] = eventDateTime
            currentEvent["timezoneName"] = currentEventTimezone
            
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    func updateEventError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        
        //self.navigationController?.popViewControllerAnimated(false)
        
        //println("error")
    }
    
    
    
    func fetchEventInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        //println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedObjects = objects {
            
            var i = 0
            
            for object in fetchedObjects
            {
                fetchedObjects[i]["isEventUpdated"] = true
                i++
            }
            
            PFObject.saveAllInBackground(fetchedObjects) {
                (success:Bool, error:NSError?) -> Void in
                if success
                {
                    
                }
                else
                {
                    
                }
            }
        }
        
        let predicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)'")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: "update", errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
    }
    
    
    
    
    func fetchEventInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
    }
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let actionType = timer.userInfo?.valueForKey("external") as! String
        
        print(" \(objects)")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            
            var notificationObjects = [PFObject]()
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            var fetchInvitationsIds : Array<String>!
            fetchInvitationsIds = []
            var fetchedUserEmailIds: Array<String!>
            fetchedUserEmailIds = []
            var hostActivityEmails: Array<Bool!>
            hostActivityEmails = []
            var invitationObjects : Array<PFObject>
            invitationObjects = []
            
            
            let eventTitle = currentEvent["eventTitle"] as! String
            
            let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            
            var notifMessage = ""
            
            //var emailMessage = ""
            
            /*if actionType == "update"
            {
            if isDateUpdated == true || isLocationUpdated == true
            {
            if isDateUpdated == true && isLocationUpdated == true
            {
            notifMessage = "\(fullUserName) changed the date & time and location for the event, \(eventTitle)"
            }
            else if isDateUpdated == true
            {
            notifMessage = "\(fullUserName) changed the date & time for the event, \(eventTitle)"
            }
            else
            {
            notifMessage = "\(fullUserName) changed the location for the event, \(eventTitle)"
            }
            }
            }
            else
            {
            
            if currentEvent["isRSVP"] as! Bool == true
            {
            notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
            }
            else
            {
            notifMessage = "\(fullUserName) deleted the event, \(eventTitle)"
            }
            
            }*/
            
            
            if let  objectsids = objects
            {
                for ids in objectsids
                {
                    fetchedUserObjectIds.append(ids["userObjectId"] as! String)
                }
            }
            
            
//            let eventCreatorObjectId = currentUserId
//            
//            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            let createdAt = ""
            let updatedAt = ""
            
            let fetchedUserObjectIdsString = fetchedUserObjectIds.joinWithSeparator("','")
            
            if actionType == "update"
            {
                if isDateUpdated == true || isLocationUpdated == true || isTimeZoneUpdated == true
                {
                    var notifMessage = ""
                    
                    if isDateUpdated == true && isLocationUpdated == true && isTimeZoneUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the date, time, location and timezone of the event, \(eventTitle)"
                    }
                    else if isDateUpdated == true && isTimeZoneUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the date, time and timezone of the event, \(eventTitle)"
                    }
                    else if isLocationUpdated == true && isTimeZoneUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the location and timezone of the event, \(eventTitle)"
                    }
                    else if isLocationUpdated == true && isDateUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the date, time and location of the event, \(eventTitle)"
                    }
                    else if isDateUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the date, time of the event, \(eventTitle)"
                    }
                    else if isTimeZoneUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the timezone of the event, \(eventTitle)"
                    }
                    else if isLocationUpdated == true
                    {
                        notifMessage = "\(fullUserName) changed the location of the event, \(eventTitle)"
                    }
                    
                    var userObjectid = String()
                    var eventCreatorId = ""
                    for invitation in fetchedobjects
                    {
                        if invitation["userObjectId"] as! String != ""
                        {
                            
                            invitationObjects.append(invitation)
                            print(invitation["hostActivityEmail"] as! Bool)
                            fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                            fetchedUserEmailIds.append(invitation["emailId"] as! String)
                            fetchInvitationsIds.append(invitation.objectId!)
                            hostActivityEmails.append(invitation["hostActivityEmail"] as! Bool)
                            let notificationObject = PFObject(className: "Notifications")
                            notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                            notificationObject["notificationImage"] = "profilePic.png"
                            notificationObject["senderId"] = currentUserId
                            userObjectid = invitation["userObjectId"] as! String
                            notificationObject["receiverId"] = invitation["userObjectId"] as! String
                            notificationObject["notificationActivityMessage"] = notifMessage
                            notificationObject["eventObjectId"] = currentEvent.objectId!
                            if actionType == "update"
                            {
                                notificationObject["notificationType"] = "eventdataupdated"
                            }
                            else
                            {
                                notificationObject["notificationType"] = "eventdeleted"
                            }
                            
                            notificationObjects.append(notificationObject)
                        }
                    }
                    
                    PFObject.saveAllInBackground(notificationObjects)
                    
                    let locationString = currentEvent["eventLocation"] as! String
                    
                    var date = currentEvent["eventStartDateTime"] as! NSDate
                    
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    let eventStartDateTime = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
                    
                    let eventDescription = currentEvent["eventDescription"] as! String
                    
                    let eventLatitude = currentEvent["eventLatitude"] as! Double
                    let eventLongitude = currentEvent["eventLongitude"] as! Double
                    
                    let isRSVP = currentEvent["isRSVP"] as! Bool
                    let frameX = currentEvent["frameX"] as! Double
                    let frameY = currentEvent["frameY"] as! Double
                    let eventFolder = currentEvent["eventFolder"] as! String
                    let eventImage = currentEvent["eventImage"] as! String
                    let originalEventImage = currentEvent["originalEventImage"] as! String
                    
                    eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
                    
                    let socialSharingURL = currentEvent["socialSharingURL"] as! String
                    let eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
                    
                    var data: Dictionary<String, String!> = [
                        "alert" : "\(notifMessage)",
                        "notifType" :  "eventdataupdated",
                        "eventTitle": "\(eventTitle)",
                        "eventCreatorObjectId": "\(currentUserId)",
                        "eventImage": "\(eventImage)",
                        "originalEventImage": "\(originalEventImage)",
                        "eventFolder": "\(eventFolder)",
                        "emailId": "",
                        "frameX": "\(frameX)",
                        "frameY": "\(frameY)",
                        "eventLatitude": "\(eventLatitude)",
                        "eventLongitude": "\(eventLongitude)",
                        "senderName": "\(fullUserName)",
                        "isRSVP": "\(isRSVP)",
                        "eventTimezoneOffset": "\(eventTimezoneOffset)",
                        "socialSharingURL": "\(socialSharingURL)",
                        "eventStartDateTime": "\(eventStartDateTime)",
                        "eventDescription": "\(eventDescription)",
                        "eventLocation": "\(locationString)",
                        "eventObjectId": "\(currentEvent.objectId!)",
                        "createdAt": "\(createdAt)",
                        "updatedAt": "\(updatedAt)",
                        "badge": "Increment",
                        "sound" : "default",
                        "eventCreatorId" : "\(eventCreatorId)"
                    ]
                    
                    print(isDateUpdated)
                    print(isLocationUpdated)
                    print(isTimeZoneUpdated)
                    
                    if isDateUpdated == true && isLocationUpdated == true
                    {
                        for (var i = 0; i < fetchedUserEmailIds.count; i++  )
                        {
                            
                            data["emailId"] = fetchedUserEmailIds[i]
                            
                            if hostActivityEmails[i] == true
                            {
                                sendEventLocationUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i],invitationObjectId: fetchInvitationsIds[i],invitation: invitationObjects[i])
                                sendEventDateUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i],invitationObjectId: fetchInvitationsIds[i],invitation: invitationObjects[i])
                            }
                            
                            
                        }
                    }
                    else if isDateUpdated == true
                    {
                        var urlString = ""
                        for (var i = 0; i < fetchedUserEmailIds.count; i++  )
                        {
                            
                            data["emailId"] = fetchedUserEmailIds[i]
                            if hostActivityEmails[i] == true
                            {
                                sendEventDateUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i],invitationObjectId: fetchInvitationsIds[i],invitation: invitationObjects[i])
                            }
                            
                        }
                        
                    }
                    else if isTimeZoneUpdated == true
                    {
                        var urlString = ""
                        for (var i = 0; i < fetchedUserEmailIds.count; i++  )
                        {
                            
                            data["emailId"] = fetchedUserEmailIds[i]
                            if hostActivityEmails[i] == true
                            {
                              sendTimeZoneUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i],invitationObjectId: fetchInvitationsIds[i],invitation: invitationObjects[i])
                            }
                        }
                        
                    }
                        
                    else
                    {
                        var urlString = String()
                        for (var i = 0; i < fetchedUserEmailIds.count; i++  )
                        {
                            data["emailId"] = fetchedUserEmailIds[i]
                            if hostActivityEmails[i] == true
                            {
                                
                                sendEventLocationUpdateEmail(notifMessage, emailId: fetchedUserEmailIds[i],invitationObjectId: fetchInvitationsIds[i],invitation: invitationObjects[i])
                            }
                            
                        }
                    }
                    
                    
                    print(fetchedUserObjectIdsString)
                    var predicateString: String! = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = true "
                    
                    sendParsePush(predicateString, data: data)
                    
                    
                    data["sound"] = ""
                    
                    predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = false  "
                    sendParsePush(predicateString, data: data)
                    
                    isDateUpdated = false
                    isLocationUpdated = false
                    isTimeZoneUpdated = false
                }
            }
            else
            {
                var notifMessage = ""
                
                
                if currentEvent["isRSVP"] as! Bool == true
                {
                    notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
                }
                else
                {
                    notifMessage = "\(fullUserName) deleted the channel, \(eventTitle)"
                }
                
                for invitation in fetchedobjects
                {
                    if invitation["userObjectId"] as! String != ""
                    {
                        fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                        fetchedUserEmailIds.append(invitation["emailId"] as! String)
                        
                        let notificationObject = PFObject(className: "Notifications")
                        notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                        notificationObject["notificationImage"] = "profilePic.png"
                        notificationObject["senderId"] = currentUserId
                        notificationObject["receiverId"] = invitation["userObjectId"] as! String
                        notificationObject["notificationActivityMessage"] = notifMessage
                        notificationObject["eventObjectId"] = currentEvent.objectId!
                        
                        if actionType == "update"
                        {
                            notificationObject["notificationType"] = "eventdataupdated"
                        }
                        else
                        {
                            notificationObject["notificationType"] = "eventdeleted"
                        }
                        
                        notificationObjects.append(notificationObject)
                    }
                }
                
                PFObject.saveAllInBackground(notificationObjects)
                
                let creatorId = currentEvent["eventCreatorObjectId"] as! String
                
                var data: Dictionary<String, String!> = [
                    "alert" : "\(notifMessage)",
                    "notifType" :  "eventdeleted",
                    "eventObjectId" : currentEvent.objectId!  ,
                    "eventCreatorId":"\(creatorId)",
                    "badge": "Increment",
                    "sound" : "default"
                ]
                
                var urlString = String()
                
                let Device = UIDevice.currentDevice()
                
                let iosVersion = NSString(string: Device.systemVersion).doubleValue
                
                let iOS9 = iosVersion >= 9
                
                
                
                var rsvp = ""
                
                if currentEvent["isRSVP"] as! Bool == true
                {
                    rsvp = "true"
                }
                else
                {
                    rsvp = "false"
                }
                
                
                if iOS9
                {
                    
                    let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
                    branchUniversalObject.title = "\(eventTitle)"
                    branchUniversalObject.contentDescription = "My Content Description"
                    // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
                    branchUniversalObject.addMetadataKey("eventObjectId", value: currentEvent.objectId!)
                    branchUniversalObject.addMetadataKey("alert", value: "\(notifMessage)")
                    branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(creatorId)")
                    branchUniversalObject.addMetadataKey("badge", value: "increment")
                    branchUniversalObject.addMetadataKey("sound", value:"default")
                    branchUniversalObject.addMetadataKey("notifType", value:"eventdeleted")
                    branchUniversalObject.addMetadataKey("isRSVP", value:rsvp)
                    
                    
                    let linkProperties: BranchLinkProperties = BranchLinkProperties()
                    linkProperties.feature = "sharing"
                    //linkProperties.channel = "email"
                    
                    
                    branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                        { (url: String?, error: NSError?) -> Void in
                            if error == nil
                            {
                                
                                urlString = url!
                                
                                print(currentEvent)
                                
                                if self.isChannel == "false"
                                {
                                    let sendEmailObject = SendEmail()
                                    
                                    let deleteEmail = DeleteEvent()
                                    
                                    let emailMessage = deleteEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "rsvp",url:urlString)
                                    
                                    for email in fetchedUserEmailIds
                                    {
                                        sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                                    }
                                    
                                }
                                else
                                {
                                    let sendEmailObject = SendEmail()
                                    
                                    let deleteEmail = DeleteChannel()
                                    
                                    let emailMessage = deleteEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "online", url: urlString)
                                    
                                    
                                    
                                    for email in fetchedUserEmailIds
                                    {
                                        sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                                    }
                                    
                                }
                                
                                
                            }
                    })
                }
                else
                {
                    Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                        
                        if error == nil
                        {
                            //println(url!)
                            
                            urlString = url!
                            
                            if self.isChannel == "false"
                            {
                                let sendEmailObject = SendEmail()
                                
                                let deleteEmail = DeleteEvent()
                                
                                let emailMessage = deleteEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "rsvp",url:urlString)
                                
                                for email in fetchedUserEmailIds
                                {
                                    sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                                }
                                
                            }
                            else
                            {
                                let sendEmailObject = SendEmail()
                                
                                let deleteEmail = DeleteChannel()
                                
                                let emailMessage = deleteEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "online", url: urlString)
                                
                                
                                
                                for email in fetchedUserEmailIds
                                {
                                    sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                                }
                            }
                        }
                        
                    })
                    
                }
                
                var predicateString: String! = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = true "
                
                sendParsePush(predicateString, data: data)
                
                
                data["sound"] = ""
                
                predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = false "
                sendParsePush(predicateString, data: data)
                
            }
        }
    }
    
    
    
    func sendEmail ()
    {
        
    }
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        let predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        let query = PFUser.queryWithPredicate(predicate)
        
        let push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                //println(objects?.count)
                if let fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        
                        print(object)
                        let userObjectId = object.objectId!
                        
                        let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        let query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                //println(objects?.count)
                                if let fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    let query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }
    
    
    func sendTimeZoneUpdateEmail(notifMessage: String!, emailId: String!,invitationObjectId: String,invitation:PFObject)
    {
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        let locationString = currentEvent["eventLocation"] as! String
        
        //var date = currentEvent["eventStartDateTime"] as! NSDate
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        let eventStartDateTime = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
        
        let eventDescription = currentEvent["eventDescription"] as! String
        
        let eventLatitude = currentEvent["eventLatitude"] as! Double
        let eventLongitude = currentEvent["eventLongitude"] as! Double
        
        let eventTitle = currentEvent["eventTitle"] as! String
        
        let isRSVP = currentEvent["isRSVP"] as! Bool
        let frameX = currentEvent["frameX"] as! Double
        let frameY = currentEvent["frameY"] as! Double
        let eventFolder = currentEvent["eventFolder"] as! String
        let eventImage = currentEvent["eventImage"] as! String
        let originalEventImage = currentEvent["originalEventImage"] as! String
        
        let eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        
        let socialSharingURL = currentEvent["socialSharingURL"] as! String
        let eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
       
        
        let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        var rsvp = ""
        
        if currentEvent["isRSVP"] as! Bool == true
        {
            rsvp = "true"
        }
        else
        {
            rsvp = "false"
        }
        
        let iOS9 = iosVersion >= 9
        let iOS7 = iosVersion >= 8 && iosVersion < 9
        
        if iOS9
        {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            branchUniversalObject.title = "\(eventTitle)"
            branchUniversalObject.contentDescription = "My Content Description"
            // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
            branchUniversalObject.addMetadataKey("alert", value: "\(notifMessage)")
            branchUniversalObject.addMetadataKey("notifType", value: "eventtimezoneupdate")
            branchUniversalObject.addMetadataKey("eventTitle", value:"\(eventTitle)")
            branchUniversalObject.addMetadataKey("originalEventImage", value: "\(originalEventImage)")
            branchUniversalObject.addMetadataKey("eventFolder", value:"\(eventFolder)")
            branchUniversalObject.addMetadataKey("frameX", value:"\(frameX)")
            branchUniversalObject.addMetadataKey("frameY", value: "\(frameY)")
            branchUniversalObject.addMetadataKey("eventLatitude", value:"\(eventLatitude)")
            branchUniversalObject.addMetadataKey("eventLongitude", value: "\(eventLongitude)")
            branchUniversalObject.addMetadataKey("senderName", value: "\(fullUserName)")
            branchUniversalObject.addMetadataKey("eventTimezoneOffset", value:"\(eventTimezoneOffset)")
            branchUniversalObject.addMetadataKey("socialSharingURL", value: "\(socialSharingURL)")
            branchUniversalObject.addMetadataKey("eventStartDateTime", value:"\(eventStartDateTime)")
            branchUniversalObject.addMetadataKey("eventDescription", value: "\(eventDescription)")
            branchUniversalObject.addMetadataKey("eventLocation", value:"\(locationString)")
            branchUniversalObject.addMetadataKey("eventObjectId", value: currentEvent.objectId!)
            branchUniversalObject.addMetadataKey("createdAt", value: "")
            branchUniversalObject.addMetadataKey("updatedAt", value:" ")
            branchUniversalObject.addMetadataKey("eventCreatorId", value: "\(currentUserId)")
            branchUniversalObject.addMetadataKey("emailId", value:"\(emailId)")
            branchUniversalObject.addMetadataKey("isRSVP", value:rsvp)
            
            
            
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            //linkProperties.channel = "email"
            let attendingStatus = invitation["attendingStatus"] as! String
            if attendingStatus == "yes" || attendingStatus == "online"
            {
                linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
                linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
                linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
            }
            else
            {
                linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
                linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
                linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
            }

            
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        let urlString = url!
                        
                        let sendEmailObject = SendEmail()
                        
                        let locationEmail = TimeZoneChanged()
//                          let locationEmail = LocationChange()
                        let emailMessage = locationEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "rsvp", url: urlString, timeZone: currentTimeZoneName)
                        
//                        let emailMessage = locationEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, locationString: locationString, hostName: fullUserName, type: "rsvp",url:urlString)
                        
                        
                        sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [emailId])
                    }
                    else
                    {
                        print(error?.localizedDescription)
                    }
                    
            })
            
        }
        else
        {
            let data = [
                "alert" : "\(notifMessage)",
                "notifType" :  "eventdataupdated",
                "eventTitle": "\(eventTitle)",
                "eventCreatorObjectId": "\(currentUserId)",
                "eventImage": "\(eventImage)",
                "originalEventImage": "\(originalEventImage)",
                "eventFolder": "\(eventFolder)",
                "emailId": "",
                "frameX": "\(frameX)",
                "frameY": "\(frameY)",
                "eventLatitude": "\(eventLatitude)",
                "eventLongitude": "\(eventLongitude)",
                "senderName": "\(fullUserName)",
                "isRSVP": "\(isRSVP)",
                "eventTimezoneOffset": "\(eventTimezoneOffset)",
                "socialSharingURL": "\(socialSharingURL)",
                "eventStartDateTime": "\(eventStartDateTime)",
                "eventDescription": "\(eventDescription)",
                "eventLocation": "\(locationString)",
                "eventObjectId": "\(currentEvent.objectId!)",
                "createdAt": "",
                "updatedAt": "",
                "eventCreatorId" : "\(eventCreatorId)",
                "isRSVP" : isChannel
            ]
            
            
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    //println(url!)
                    
                    let urlString = url!
                    
                    let sendEmailObject = SendEmail()
                    
                    let locationEmail = LocationChange()
                    
                    let emailMessage = locationEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, locationString: locationString, hostName: fullUserName, type: "rsvp",url:urlString)
                    
                    
                    sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [emailId])
                }
                
            })
            
        }
    }
    
    
    func sendEventLocationUpdateEmail(notifMessage: String!, emailId: String!,invitationObjectId: String,invitation:PFObject)
    {
        
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        let locationString = currentEvent["eventLocation"] as! String
        let attendingStatus : String = invitation["attendingStatus"] as! String
        //var date = currentEvent["eventStartDateTime"] as! NSDate
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        let eventStartDateTime = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
        
        let eventDescription = currentEvent["eventDescription"] as! String
        
        let eventLatitude = currentEvent["eventLatitude"] as! Double
        let eventLongitude = currentEvent["eventLongitude"] as! Double
        
        let eventLatestTitle = currentEvent["eventTitle"] as! String
        
        let isRSVP = currentEvent["isRSVP"] as! Bool
        let frameX = currentEvent["frameX"] as! Double
        let frameY = currentEvent["frameY"] as! Double
        let eventFolder = currentEvent["eventFolder"] as! String
        let eventImage = currentEvent["eventImage"] as! String
        let originalEventImage = currentEvent["originalEventImage"] as! String
        
        let eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        
        let socialSharingURL = currentEvent["socialSharingURL"] as! String
        let eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
        
        
        let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        var rsvp = ""
        
        if currentEvent["isRSVP"] as! Bool == true
        {
            rsvp = "true"
        }
        else
        {
            rsvp = "false"
        }
        
        let iOS9 = iosVersion >= 9
        let iOS7 = iosVersion >= 8 && iosVersion < 9
        
        if iOS9
        {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            branchUniversalObject.title = "\(eventTitle)"
            branchUniversalObject.contentDescription = "My Content Description"
            // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
            branchUniversalObject.addMetadataKey("alert", value: "\(notifMessage)")
            branchUniversalObject.addMetadataKey("notifType", value: "eventlocationupdate")
            branchUniversalObject.addMetadataKey("eventTitle", value:"\(eventTitle)")
            branchUniversalObject.addMetadataKey("eventCreatorObjectId", value:"\(currentUserId)")
            branchUniversalObject.addMetadataKey("originalEventImage", value: "\(originalEventImage)")
            branchUniversalObject.addMetadataKey("eventFolder", value:"\(eventFolder)")
            branchUniversalObject.addMetadataKey("frameX", value:"\(frameX)")
            branchUniversalObject.addMetadataKey("frameY", value: "\(frameY)")
            branchUniversalObject.addMetadataKey("eventLatitude", value:"\(eventLatitude)")
            branchUniversalObject.addMetadataKey("eventLongitude", value: "\(eventLongitude)")
            branchUniversalObject.addMetadataKey("senderName", value: "\(fullUserName)")
            // branchUniversalObject.addMetadataKey("isRSVP", value:"\(isRSVP)")
            branchUniversalObject.addMetadataKey("eventTimezoneOffset", value:"\(eventTimezoneOffset)")
            branchUniversalObject.addMetadataKey("socialSharingURL", value: "\(socialSharingURL)")
            branchUniversalObject.addMetadataKey("eventStartDateTime", value:"\(eventStartDateTime)")
            branchUniversalObject.addMetadataKey("eventDescription", value: "\(eventDescription)")
            branchUniversalObject.addMetadataKey("eventLocation", value:"\(locationString)")
            branchUniversalObject.addMetadataKey("eventObjectId", value: currentEvent.objectId!)
            branchUniversalObject.addMetadataKey("createdAt", value: "")
            branchUniversalObject.addMetadataKey("updatedAt", value:" ")
            branchUniversalObject.addMetadataKey("eventCreatorId", value: "\(eventCreatorId)")
            branchUniversalObject.addMetadataKey("emailId", value:"\(emailId)")
            branchUniversalObject.addMetadataKey("isRSVP", value:rsvp)
            
            
            
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            //linkProperties.channel = "email"
            
            print(attendingStatus)
            
            if attendingStatus == "yes" || attendingStatus == "online"
            {
                linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
                linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
                linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
            }
            else
            {
                linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
                linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
                linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
            }

            
            
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        let urlString = url!
                        
                        let sendEmailObject = SendEmail()
                        
                        let locationEmail = LocationChange()
                        
                        print(locationString)
                        
                        let emailMessage = locationEmail.emailMessage(currentEvent.objectId!, eventTitle: eventLatestTitle, locationString: locationString, hostName: fullUserName, type: "rsvp",url:urlString)
                        
                        
                        sendEmailObject.sendEmail("Update – \(eventLatestTitle)", message: emailMessage, emails: [emailId])
                    }
            })
            
        }
        else
        {
            let data = [
                "alert" : "\(notifMessage)",
                "notifType" :  "eventdataupdated",
                "eventTitle": "\(eventTitle)",
                "eventCreatorObjectId": "\(currentUserId)",
                "eventImage": "\(eventImage)",
                "originalEventImage": "\(originalEventImage)",
                "eventFolder": "\(eventFolder)",
                "emailId": "",
                "frameX": "\(frameX)",
                "frameY": "\(frameY)",
                "eventLatitude": "\(eventLatitude)",
                "eventLongitude": "\(eventLongitude)",
                "senderName": "\(fullUserName)",
                "isRSVP": "\(isRSVP)",
                "eventTimezoneOffset": "\(eventTimezoneOffset)",
                "socialSharingURL": "\(socialSharingURL)",
                "eventStartDateTime": "\(eventStartDateTime)",
                "eventDescription": "\(eventDescription)",
                "eventLocation": "\(locationString)",
                "eventObjectId": "\(currentEvent.objectId!)",
                "createdAt": "",
                "updatedAt": "",
                "eventCreatorId" : "\(eventCreatorId)",
                "isRSVP" : isChannel
            ]
            
            
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    //println(url!)
                    
                    let urlString = url!
                    
                    let sendEmailObject = SendEmail()
                    
                    let locationEmail = LocationChange()
                    
                    let emailMessage = locationEmail.emailMessage(currentEvent.objectId!, eventTitle: eventLatestTitle, locationString: locationString, hostName: fullUserName, type: "rsvp",url:urlString)
                    
                    
                    sendEmailObject.sendEmail("Update – \(eventLatestTitle)", message: emailMessage, emails: [emailId])
                }
                
            })
            
        }
        
    }
    
    
    func sendEventDateUpdateEmail(notifMessage: String!, emailId: String!,invitationObjectId: String,invitation:PFObject)
    {
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        let attendingStatus = invitation["attendingStatus"] as! String
        
        print(invitation)
        
        let locationString = currentEvent["eventLocation"] as! String
        
        let eventLatestTitle = currentEvent["eventTitle"] as! String
        
        let date = currentEvent["eventStartDateTime"] as! NSDate
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        let eventStartDateTime = dateFormatter.stringFromDate((currentEvent["eventStartDateTime"] as? NSDate)!)
        
        let eventDescription = currentEvent["eventDescription"] as! String
        
        let eventLatitude = currentEvent["eventLatitude"] as! Double
        let eventLongitude = currentEvent["eventLongitude"] as! Double
        
        let isRSVP = currentEvent["isRSVP"] as! Bool
        let frameX = currentEvent["frameX"] as! Double
        let frameY = currentEvent["frameY"] as! Double
        let eventFolder = currentEvent["eventFolder"] as! String
        let eventImage = currentEvent["eventImage"] as! String
        let originalEventImage = currentEvent["originalEventImage"] as! String
        let email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        let eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
        
        let socialSharingURL = currentEvent["socialSharingURL"] as! String
        let eventTimezoneOffset = currentEvent["eventTimezoneOffset"] as! Int
        
        let data = [
            "alert" : "\(notifMessage)",
            "notifType" :  "eventdataupdated",
            "eventTitle": "\(eventTitle)",
            "eventCreatorObjectId": "\(currentUserId)",
            "eventImage": "\(eventImage)",
            "originalEventImage": "\(originalEventImage)",
            "eventFolder": "\(eventFolder)",
            "emailId": "",
            "frameX": "\(frameX)",
            "frameY": "\(frameY)",
            "eventLatitude": "\(eventLatitude)",
            "eventLongitude": "\(eventLongitude)",
            "senderName": "\(fullUserName)",
            "isRSVP": "\(isRSVP)",
            "eventTimezoneOffset": "\(eventTimezoneOffset)",
            "socialSharingURL": "\(socialSharingURL)",
            "eventStartDateTime": "\(eventStartDateTime)",
            "eventDescription": "\(eventDescription)",
            "eventLocation": "\(locationString)",
            "eventObjectId": "\(currentEvent.objectId!)",
            "createdAt": "",
            "updatedAt": ""
        ]
        
        
        let formatter = NSDateFormatter();
        var timeZoneName = currentEvent["timezoneName"] as! String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        
        formatter.timeZone = NSTimeZone(name: timeZoneName)
        
        let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
        
        let localTimezoneOffset = Double(NSTimeZone.localTimeZone().secondsFromGMT)
        
        //println(formatter.dateFromString(formatter.stringFromDate(eventObject["eventStartDateTime"] as! NSDate)))
        
        
        let sdate =  NSDate(timeIntervalSince1970: (currentEvent["eventStartDateTime"] as! NSDate).timeIntervalSince1970 + timezoneOffset - localTimezoneOffset)
        
        
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        
        var  timeeString = "\(shour):\(sminute) \(sam)"
        let  dateeString = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        
        
        
        // print(currentEvent)
        //println(sweekday)
        
        let path = NSBundle.mainBundle().pathForResource("timezones", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        
        let tzDict = dict!.objectForKey("TimeZones") as! NSDictionary
        
        let str = tzDict.allKeysForObject(timeZoneName) as NSArray
        let strtime = str.objectAtIndex(0) as! String
        
        
        
        
        timeZoneName = String (format: " (%@ time)",  strtime)
        timeeString = "\(timeeString) \(timeZoneName)"
        
        var rsvp = ""
        
        if currentEvent["isRSVP"] as! Bool == true
        {
            rsvp = "true"
        }
        else
        {
            rsvp = "false"
        }
        let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS9 = iosVersion >= 9
        
        if iOS9
        {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            branchUniversalObject.title = "\(eventTitle)"
            branchUniversalObject.contentDescription = "My Content Description"
            
            branchUniversalObject.addMetadataKey("alert", value:"\(notifMessage)")
            branchUniversalObject.addMetadataKey("notifType", value: "eventdateupdated")
            branchUniversalObject.addMetadataKey("eventTitle", value:"\(eventTitle)")
            branchUniversalObject.addMetadataKey("eventCreatorObjectId", value:"\(currentUserId)")
            branchUniversalObject.addMetadataKey("originalEventImage", value: "\(originalEventImage)")
            
            print(emailId)
            
            branchUniversalObject.addMetadataKey("eventFolder", value:"\(eventFolder)")
            branchUniversalObject.addMetadataKey("emailId", value:"\(emailId)")
            branchUniversalObject.addMetadataKey("frameX", value: "\(frameX)")
            branchUniversalObject.addMetadataKey("frameY", value:"\(frameY)")
            branchUniversalObject.addMetadataKey("eventLatitude", value:"\(eventLatitude)")
            branchUniversalObject.addMetadataKey("eventLongitude", value:"\(eventLongitude)")
            branchUniversalObject.addMetadataKey("senderName", value:"\(fullUserName)")
            // branchUniversalObject.addMetadataKey("isRSVP", value:"\(isRSVP)")
            branchUniversalObject.addMetadataKey("eventTimezoneOffset", value:"\(eventTimezoneOffset)")
            branchUniversalObject.addMetadataKey("socialSharingURL", value:"\(socialSharingURL)")
            branchUniversalObject.addMetadataKey("eventStartDateTime", value:"\(eventStartDateTime)")
            branchUniversalObject.addMetadataKey("eventLocation", value: "\(locationString)")
            branchUniversalObject.addMetadataKey("eventObjectId", value:"\(currentEvent.objectId!)")
            branchUniversalObject.addMetadataKey("createdAt", value:"")
            branchUniversalObject.addMetadataKey("updatedAt", value: "")
            
            branchUniversalObject.addMetadataKey("eventImage", value: "\(eventImage)")
            branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
            branchUniversalObject.addMetadataKey("eventDescription", value:"\(eventDescription)")
            branchUniversalObject.addMetadataKey("isRSVP", value:"true")
            
            
            
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            
            
            if attendingStatus == "yes" || attendingStatus == "online"
            {
                linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
                linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
                linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)?update")
            }
            else
            {
                linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
                linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
                linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/invite/\(invitationObjectId)")
            }
            
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        let urlString = url!
                        
                        let dateEmail = DateChange()
                        
                        let emailMessage = dateEmail.emailMessage(currentEvent.objectId!, eventTitle: eventLatestTitle, dateString: dateeString, timeString: timeeString, hostName: fullUserName, type: "rsvp",url:urlString)
                        
                        let sendEmailObject = SendEmail()
                        
                        sendEmailObject.sendEmail("Update – \(eventLatestTitle)", message: emailMessage, emails: [emailId])
                    }
            })
        }
        else
        {
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    //println(url!)
                    let urlString = url!
                    
                    let dateEmail = DateChange()
                    
                    let emailMessage = dateEmail.emailMessage(currentEvent.objectId!, eventTitle: eventLatestTitle, dateString: dateeString, timeString: timeeString, hostName: fullUserName, type: "rsvp",url:urlString)
                    
                    let sendEmailObject = SendEmail()
                    
                    sendEmailObject.sendEmail("Update – \(eventLatestTitle)", message: emailMessage, emails: [emailId])
                }
                
            })
            
        }
        
    }
    
    
    /*
    
    if actionType == "update"
    {
    if isDateUpdated == true || isLocationUpdated == true
    {
    if isDateUpdated == true && isLocationUpdated == true
    {
    notifMessage = "\(fullUserName) changed the date & time and location for the event, \(eventTitle)"
    }
    else if isDateUpdated == true
    {
    
    
    var dateEmail = DateChange()
    
    emailMessage = dateEmail.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, dateString: dateStringFromNSDate(date), timeString: timeStringFromNSDate(date), hostName: fullUserName, type: "rsvp")
    
    var sendEmailObject = SendEmail()
    
    notifMessage = "\(fullUserName) changed the date & time for the event, \(eventTitle)"
    
    sendEmailObject.sendEmail(notifMessage, message: emailMessage, emails: [String])
    
    
    }
    else
    {
    notifMessage = "\(fullUserName) changed the location for the event, \(eventTitle)"
    }
    }
    }
    else
    {
    
    if currentEvent["isRSVP"] as! Bool == true
    {
    notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
    }
    else
    {
    notifMessage = "\(fullUserName) deleted the event, \(eventTitle)"
    }
    
    }
    
    
    */
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    func dateStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        let sweekday = scomponents.weekday
        
        let dateString = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        
        return dateString
    }
    
    
    func timeStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        let timeString = "\(shour):\(sminute) \(sam)"
        
        return timeString
    }
    
    
    func deleteEventFromParse(timer: NSTimer)
    {
        if MyReachability.isConnectedToNetwork()
        {
            ParseOperations.instance.deleteData(currentEvent, target: self, successSelector: "deleteEventSuccess:", successSelectorParameters: nil, errorSelector: "deleteEventError:", errorSelectorParameters:nil)
        }
        else
        {
            self.loaderView.hidden = true
            
            let refreshAlert = UIAlertController(title: "No Internet Connection", message: "Event cannot be deleted as you seem to be offline. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Go Back", style: .Default, handler: { (action: UIAlertAction) in
                
                let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                self.navigationController?.pushViewController(eventVC, animated: true)
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            presentViewController(refreshAlert, animated: true, completion: nil)
            
            
            
        }
    }
    
    func fetchAllEventInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let eventToBeDeleted = timer.userInfo?.valueForKey("external") as! PFObject
        
        var fetchedUserEmailIds: Array<String>
        fetchedUserEmailIds = []
        
        var fetchedEmail = NSArray()
        fetchedEmail = []
        
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        let eventTitle = eventToBeDeleted["eventTitle"] as! String
        
        var notificationObjects = [PFObject]()
        
        var notifMessage = ""
        
        if eventToBeDeleted["isRSVP"] as! Bool == true
        {
            notifMessage = "\(fullUserName) cancelled the event, \(eventTitle)"
        }
        else
        {
            notifMessage = "\(fullUserName) deleted the channel, \(eventTitle)"
        }
        
        
        //println("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects {
            
            var i = 0
            
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            
            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    
                    
                    
                    let notificationObject = PFObject(className: "Notifications")
                    fetchedUserEmailIds.append(invitation["emailId"] as! String)
                    
                    notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                    notificationObject["notificationImage"] = "profilePic.png"
                    notificationObject["senderId"] = currentUserId
                    notificationObject["receiverId"] = invitation["userObjectId"] as! String
                    notificationObject["notificationActivityMessage"] = notifMessage
                    notificationObject["eventObjectId"] = eventToBeDeleted.objectId!
                    notificationObject["notificationType"] = "eventdeleted"
                    
                    notificationObjects.append(notificationObject)
                }
            }
            
            
            PFObject.deleteAllInBackground(fetchedobjects)
            
//            let eventCreatorObjectId = currentUserId
//            
//            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //var eventTitle = currentEvent["eventTitle"] as! String
            let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            
            var createdAt = ""
            
            print(objects)
            
            
            
            
            if let  objectsids = objects
            {
                fetchedEmail = objectsids
                print(fetchedEmail)
                for ids in objectsids
                {
                    
                    print(ids)
                    
                    fetchedUserObjectIds.append(ids["userObjectId"] as! String)
                    
                }
            }
            
            let fetchedUserObjectIdsString = fetchedUserObjectIds.joinWithSeparator("','")
            
            if fetchedUserObjectIdsString != ""
            {
                
                PFObject.saveAllInBackground(notificationObjects)
                
                
                let eventCreatorId = eventToBeDeleted["eventCreatorObjectId"] as! String
                var data: Dictionary<String, String!> = [
                    "alert" : "\(notifMessage)",
                    "notifType" :  "eventdeleted",
                    "eventObjectId": eventToBeDeleted.objectId!  ,
                    "eventCreatorId" : "\(eventCreatorId)",
                    "isRSVP": isChannel,
                    "badge": "Increment",
                    "sound": "defaut"
                ]
                var rsvp = ""
                
                if currentEvent["isRSVP"] as! Bool == true
                {
                    rsvp = "true"
                }
                else
                {
                    rsvp = "false"
                }
                
                
                var urlString = String()
                
                
                let Device = UIDevice.currentDevice()
                
                let iosVersion = NSString(string: Device.systemVersion).doubleValue
                
                let iOS9 = iosVersion >= 9
                let iOS7 = iosVersion >= 8 && iosVersion < 9
                
                if iOS9
                {
                    let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
                    branchUniversalObject.title = "\(eventTitle)"
                    branchUniversalObject.contentDescription = "My Content Description"
                    
                    branchUniversalObject.addMetadataKey("alert", value: "\(notifMessage)")
                    branchUniversalObject.addMetadataKey("notifType", value: "eventdeleted")
                    branchUniversalObject.addMetadataKey("eventObjectId", value:eventToBeDeleted.objectId!)
                    branchUniversalObject.addMetadataKey("eventCreatorId", value: "\(eventCreatorId)")
                    branchUniversalObject.addMetadataKey("isRSVP", value: rsvp)
                    
                    
                    
                    
                    
                    let linkProperties: BranchLinkProperties = BranchLinkProperties()
                    linkProperties.feature = "sharing"
                    
                    branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                        { (url: String?, error: NSError?) -> Void in
                            if error == nil
                            {
                                urlString = url!
                            }
                            
                            let sendEmailObject = SendEmail()
                            
                            
                            var emailMessage = ""
                            
                            if self.isChannel == "false"
                            {
                                let deleteEmail = DeleteEvent()
                                
                                emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "rsvp",url:urlString)
                                
                            }
                            else
                            {
                                let deleteEmail = DeleteChannel()
                                
                                emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "online", url: urlString)
                                
                            }
                        
                            
                            for email in fetchedUserEmailIds
                            {
                                
                                let emailPredicate = NSPredicate(format: "email = '\(email)'")
                                
                                let emailQuery = PFUser.queryWithPredicate(emailPredicate)
                                var BoolValue = Bool()
                                emailQuery!.findObjectsInBackgroundWithBlock {
                                    (users: [AnyObject]?, error: NSError?) -> Void in
                                    ////println(users!.count)
                                    if let users = users as? [PFUser]
                                    {
                                        for user in users
                                        {
                                            print(user)
                                            
                                            print(user["hostActivityEmail"])
                                            BoolValue = user["hostActivityEmail"] as! Bool
                                            
                                            
                                            if BoolValue == true
                                            {
                                               sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                                            }
                                        }
                                    }
                                }
                                
                                
                                
                                
                            }
                            
                    })
                }
                else
                {
                    Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                        
                        if error == nil
                        {
                            //println(url!)
                            
                            urlString = url!
                            
                        }
                        
                        let sendEmailObject = SendEmail()
                        
                        let deleteEmail = DeleteEvent()
                        var emailMessage = ""
                        
                        if eventToBeDeleted["isRSVP"] as! Bool == true
                        {
                            emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "rsvp",url:urlString)
                        }
                        else
                        {
                            emailMessage = deleteEmail.emailMessage(eventToBeDeleted.objectId!, eventTitle: eventTitle, hostName: fullUserName, type: "online",url:urlString)
                        }
                        
                        
                        for email in fetchedUserEmailIds
                        {
                            let emailPredicate = NSPredicate(format: "email = '\(email)'")
                            
                            let emailQuery = PFUser.queryWithPredicate(emailPredicate)
                            var BoolValue = Bool()
                            emailQuery!.findObjectsInBackgroundWithBlock {
                                (users: [AnyObject]?, error: NSError?) -> Void in
                                ////println(users!.count)
                                if let users = users as? [PFUser]
                                {
                                    for user in users
                                    {
                                        print(user["hostActivityEmail"])
                                        BoolValue = user["hostActivityEmail"] as! Bool
                                        
                                        
                                        if BoolValue == true
                                        {
                                            sendEmailObject.sendEmail("Update – \(eventTitle)", message: emailMessage, emails: [email])
                                        }
                                    }
                                }
                            }
                            
                        }
                        
                        
                    })
                    
                }
                
                
                print(fetchedUserObjectIdsString)
                var predicateString: String! = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = true"
                
                sendParsePush(predicateString, data: data)
                
                
                data["sound"] = ""
                
                predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = false "
                sendParsePush(predicateString, data: data)
            }
        }
        
    }
    func fetchAllEventInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
    }
    func deleteLikesSuccess()
    {
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentEvent.objectId!)")
        print (currentEvent.objectId!)
        messagesRef.removeValue()
    }
    func deleteStreamSuccess()
    {
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
        print (currentEvent.objectId!)
        messagesRef.removeValue()
        deleteLikesSuccess()
    }
    
    func deleteEventSuccess(timer:NSTimer)
    {
        let eventToBeDeleted: PFObject = timer.userInfo?.valueForKey("internal") as! PFObject
        
        self.loaderView.hidden = true
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/GroupChat/\(currentEvent.objectId!)")
        print (currentEvent.objectId!)
        messagesRef.removeValue()
        
     
        self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(currentEvent.objectId!)")
        self.messagesRef.removeValue()
        self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentEvent.objectId!)")
        self.messagesRef.removeValue()
        self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(currentEvent.objectId!)")
        self.messagesRef.removeValue()
    
        

        deleteStreamSuccess()
        
        print ("done delete")
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)'")
        
        let invitationQuery = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(invitationQuery, target: self, successSelector: "fetchAllEventInvitationsSuccess:", successSelectorParameters: eventToBeDeleted, errorSelector: "fetchAllEventInvitationsError:", errorSelectorParameters:nil)
        
        
        let imageFolder = currentEvent["eventFolder"] as! String
        
        let imageName = currentEvent["eventImage"] as! String
        
        let deleteRequest = AWSS3DeleteObjectRequest()
        deleteRequest.bucket = "eventnodepublicpics"
        deleteRequest.key = "\(imageFolder)\(imageName)"
        
        let s3 = AWSS3.defaultS3()
        
        s3.deleteObject(deleteRequest).continueWithBlock {
            (task: AWSTask!) -> AnyObject! in
            
            if(task.error != nil)
            {
                
            }else{
                
                let imageOriginalName = currentEvent["originalEventImage"] as! String
                
                let deleteRequestOriginal = AWSS3DeleteObjectRequest()
                deleteRequestOriginal.bucket = "eventnodepublicpics"
                deleteRequestOriginal.key = "\(imageFolder)\(imageOriginalName)"
                
                let s3Original = AWSS3.defaultS3()
                
                s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                    (task: AWSTask!) -> AnyObject! in
                    
                    if(task.error != nil){
                        
                    }else{
                        
                    }
                    return nil
                }
                
            }
            return nil
        }
        
        
        isUpdated = true
        
        let isDeleted = ModelManager.instance.deleteTableData("Events", whereString: "objectId=?", whereFields: [currentEvent.objectId!])
        
        if isDeleted
        {
            ModelManager.instance.deleteTableData("EventImages", whereString: "eventObjectId=?", whereFields: [currentEvent.objectId!])
        }
        
        
        let query = PFQuery(className:"EventImages")
        query.whereKey("eventObjectId", equalTo:eventToBeDeleted.objectId!)
        query.orderByDescending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchEventsAfterDeleteOriginalSuccess:", successSelectorParameters: nil, errorSelector: "fetchEventsAfterDeleteOriginalError:", errorSelectorParameters:nil)
        if isChannel == "false"
        {
            let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            self.navigationController?.pushViewController(eventVC, animated: true)
        }
        else
        {
            let eventVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelEventsViewController") as! ChannelEventsViewController
            self.navigationController?.pushViewController(eventVC, animated: true)
        }
        
    }
    
    func deleteEventError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        let refreshAlert = UIAlertController(title: "Error", message: "Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
            self.loaderView.hidden=true
        }))
        
        
        
        //println("error occured \(error.description)")
    }
    
    
    func fetchEventsAfterDeleteOriginalSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        //println("Successfully retrieved \(objects!.count) posts.")
        if let objects = objects {
            self.loaderView.hidden=true
            
            for post in objects
            {
                let postType = post["postType"] as! String
                
                if(postType == "image" || postType == "video")
                {
                    
                    let postFolder = post["eventFolder"] as! String
                    let postImage = post["postData"] as! String
                    
                    let deleteRequestOriginal = AWSS3DeleteObjectRequest()
                    deleteRequestOriginal.bucket = "eventnode1"
                    deleteRequestOriginal.key = "\(postFolder)\(postImage)"
                    
                    let s3Original = AWSS3.defaultS3()
                    
                    s3Original.deleteObject(deleteRequestOriginal).continueWithBlock {
                        (task: AWSTask!) -> AnyObject! in
                        
                        if(task.error != nil){
                            //println("not deleted post")
                            
                        }else{
                            //println("deleted post")
                            
                            
                            
                            
                        }
                        return nil
                    }
                }
                
            }
        }
    }
    
    func fetchEventsAfterDeleteOriginalError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        self.loaderView.hidden=true
        //println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func getFormatedStringFromDate(sdate: NSDate) -> String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        
        let startDate = "\(monthsArray[smonth-1]) \(sday) \(syear),\(shour):\(sminute) \(sam)"
        return startDate
    }
    
}
