//
//  OnboardingViewController.swift
//  Eventnode
//
//  Created by Chandra Kilaru on 9/26/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation
import Intercom
//import PermissionScope

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var welcomeMsg: UITextView!
    @IBOutlet weak var pushPopUpView: UIView!
    @IBOutlet weak var getNotificationButton: UIButton!
    
//    let multiPscope = PermissionScope()
//    let noUIPscope = PermissionScope()
//    let pscope = PermissionScope()
    
    override func viewDidLoad() {
        
//        multiPscope.addPermission(NotificationsPermission(notificationCategories: nil),
//            message: " ")
        pushPopUpView.hidden = true
        blackView.hidden = true
        self.pushPopUpView.frame.origin.y = -250.0
        getNotificationButton.layer.borderWidth = 2.0
        getNotificationButton.layer.borderColor = UIColor(red: 68.0/255.0, green: 185.0/255.0, blue: 227.0/255.0, alpha: 1.0).CGColor
        getNotificationButton.layer.cornerRadius = 2.0
        getNotificationButton.layer.masksToBounds = true
        
        
        pushPopUpView.layer.cornerRadius = 5.0
        pushPopUpView.layer.masksToBounds = true
        if let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
        {
            if settings.types.contains([.Alert, .Sound, .Badge])
            {
                self.pushPopUpView.hidden = true
                self.blackView.hidden = true
                print("alerts")
            }
            else
            {
                self.pushPopUpView.hidden = false
                self.blackView.hidden = false
                self.pushPopUp()
            }
            
        }
        
        let fullUserName: String = NSUserDefaults.standardUserDefaults().valueForKey("fullUserName") as! String
        
        welcomeMsg.text = "Welcome \(fullUserName)"
        welcomeMsg.textAlignment = NSTextAlignment.Center
        welcomeMsg.font = UIFont(name: "AvenirNext-Demibold", size: 16.0)
        welcomeMsg.textColor = UIColor(red: 155.0/255, green: 155.0/255, blue: 155.0/255, alpha: 1.0)
        
    }
    
    
    func pushPopUp()
    {
        
        UIView.animateWithDuration(1.0) { () -> Void in
            self.pushPopUpView.frame.origin.y = 150.0
            
        }
        
        
    }
    
    
    @IBAction func createInPersonButtonClicked(sender: AnyObject) {
        print("create In Person Button Clicked", terminator: "")
        let eventDetailsVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
       // eventDetailsVC.isInPersonEvent = true
        self.navigationController?.pushViewController(eventDetailsVC, animated: false)
    }
    
   
    @IBAction func closeButton(sender: AnyObject)
    {
        pushPopUpView.hidden = true
        blackView.hidden = true
    }
    @IBAction func getNotificationButton(sender: AnyObject)
    {
        
        UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
        let userNotificationTypes: UIUserNotificationType = ([UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]);
        let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        pushPopUpView.hidden = true
        blackView.hidden = true
        
    }
    @IBAction func createOnlineButtonClicked(sender: AnyObject)
    {
        
        print("create Online Person Button Clicked", terminator: "")
        let eventDetailsVC = self.storyboard!.instantiateViewControllerWithIdentifier("CreateChannelViewController") as! CreateChannelViewController
        self.navigationController?.pushViewController(eventDetailsVC, animated: false)
        
    }
    
    @IBAction func joinEventButtonClicked(sender: AnyObject) {
        print("Join Event Button Clicked", terminator: "")
        let refreshAlert = UIAlertController(title: "Do you have an invitation?", message: "If you have an invitation email or a link, click on that to access the event or channel.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func supportButtonClicked(sender: AnyObject) {
        Intercom.presentMessageComposer()
    }
    
    
    @IBAction func logoutBtn(sender: AnyObject)
    {
        
        let refreshAlert = UIAlertController(title: "Alert", message: "Do You really want to logout", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Logout", style: .Default, handler: { (action: UIAlertAction) in
            self.logoutcurrentevent()
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
        
        
    }
    
    
    
    func logoutcurrentevent()
    {
        let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: " isPosted=0", whereFields: [])
        
        resultSetCount.next()
        
        let eventCount = resultSetCount.intForColumn("count")
        
        resultSetCount.close()
        
        let resultSetPostCount: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: " isPosted=0", whereFields: [])
        
        resultSetPostCount.next()
        
        let postCount = resultSetPostCount.intForColumn("count")
        
        resultSetPostCount.close()
        
        
        if(eventCount>0 || postCount>0)
        {
            let refreshAlert = UIAlertController(title: "Pending Changes", message: "You have pending changes that need to be uploaded. If you logout now you will lose those changes.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Logout", style: .Default, handler: { (action: UIAlertAction) in
                self.logout()
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
        else
        {
            logout()
        }

    }
    
    func logout()
    {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 1
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        NSUserDefaults.standardUserDefaults().setObject("No", forKey: "isLoggedIn")
        
        self.updateDeviceToken()
        
        PFUser.logOut()
        
        
        
        let isDeleted = ModelManager.instance.deleteTableData("Events", whereString: " isPosted=0", whereFields: [])
        if isDeleted {
            //println("Record deleted successfully.")
        } else {
            //println("Error in deleting record.")
        }
        
        
        let isDeleted1 = ModelManager.instance.deleteTableData("EventImages", whereString: " isPosted=0", whereFields: [])
        
        if isDeleted1 {
            //println("Record deleted successfully.")
        } else {
            //println("Error in deleting record.")
        }
        
        Intercom.reset()
        
        
        let VC = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        self.navigationController?.pushViewController(VC, animated: false)
        
    }
    
    func updateDeviceToken()
    {
        let installation = PFInstallation.currentInstallation()
        installation["userObjectId"] = ""
        installation.saveInBackground()
    }
    
}