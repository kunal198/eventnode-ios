//
//  ChannelInviteFirstViewController.swift
//  Eventnode
//
//  Created by brst on 2/15/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//

import UIKit

class ChannelInviteFirstViewController: UIViewController {

    
    var currentUserId: String!
    
    var isFromCreated = false
    var isFromChannel: Bool!
    var isApproved: Bool!
    var isRSVP = ""
    
//    let multiPscope = PermissionScope()
//    let noUIPscope = PermissionScope()
//    let pscope = PermissionScope()
    
   // @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var getNotificationButton: UIButton!
    @IBOutlet weak var pushPopUpView: UIView!
    @IBOutlet weak var blackView: UIView!
    @IBOutlet weak var channelAccessToggleTextView: UITextView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
          isApproved = true
        self.pushPopUpView.frame.origin.y = -250.0
        getNotificationButton.layer.borderWidth = 2.0
        getNotificationButton.layer.borderColor = UIColor(red: 68.0/255.0, green: 185.0/255.0, blue: 227.0/255.0, alpha: 1.0).CGColor
        getNotificationButton.layer.cornerRadius = 2.0
        getNotificationButton.layer.masksToBounds = true
        
        
        pushPopUpView.layer.cornerRadius = 5.0
        pushPopUpView.layer.masksToBounds = true
        
        
        if let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
        {
            if settings.types.contains([.Alert, .Sound, .Badge])
            {
                pushPopUpView.hidden = true
                blackView.hidden = true
                print("alerts")
            }
            else
            {
                pushPopUpView.hidden = false
                blackView.hidden = false
                self.pushPopUp()
            }
            
        }

        
        
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pushPopUp()
    {
        
        UIView.animateWithDuration(1.0) { () -> Void in
            self.pushPopUpView.frame.origin.y = self.textView.frame.origin.y + self.textView.frame.size.height
        }
        
    }
    
    
    
    @IBAction func getNotification(sender: AnyObject)
    {
//        let notificationSettings = UIUserNotificationSettings(
//        forTypes: [.Badge, .Sound, .Alert], categories: nil)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        pushPopUpView.hidden = true
        blackView.hidden = true
    }
    @IBAction func closeButton(sender: AnyObject)
    {
        pushPopUpView.hidden = true
        blackView.hidden = true
    }

    @IBAction func facebookFriends(sender: AnyObject)
    {
        
        if isFacebookLogin
        {
            let inviteFbVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFacebookFriendsViewController") as! InviteFacebookFriendsViewController
            self.navigationController?.pushViewController(inviteFbVC, animated: false)
        }
        else
        {
            showLinkfacebookView = true
            
            let ConnectFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("connectFacebookAccount") as! facebookLinkedViewController
            
            ConnectFacebook.isInviteView = true
            
            self.navigationController?.pushViewController(ConnectFacebook, animated: false)
            
        }

        
    }
    @IBAction func closButton(sender: AnyObject)
    {
        
        if isFromChannel == true
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelEventsViewController") as! ChannelEventsViewController
            homeVC.redirect = true
            homeVC.redirectFromInviteView = true
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
        else
        {
            
            self.navigationController?.popViewControllerAnimated(false)
        }

    }
    
    @IBAction func emailContacts(sender: AnyObject)
    {
        
        if #available(iOS 9.0, *)
        {
            
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            homeVC.isRSVP = "false"
            if isApproved == true
            {
                homeVC.isApproved = "true"
            }
            else
            {
                homeVC.isApproved = "true"
            }
            
            
            homeVC.channelDescription = currentEvent["eventDescription"] as! String
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
        else
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SerchAndInviteUsingABAddressBookViewController") as! SerchAndInviteUsingABAddressBookViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
            
        }
        
    }
    
    @IBAction func shareButton(sender: AnyObject)
    {
        let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS9 = iosVersion >= 9
        let iOS7 = iosVersion >= 8 && iosVersion < 9
        
        if iOS9
        {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            branchUniversalObject.title = "\(eventTitle)"
            branchUniversalObject.contentDescription = "My Content Description"
            // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
            branchUniversalObject.addMetadataKey("eventObjectId", value: currentEvent.objectId!)
            branchUniversalObject.addMetadataKey("emailId", value: "noemail")
            branchUniversalObject.addMetadataKey("isApproved", value:"true")
            branchUniversalObject.addMetadataKey("isSocialSharing", value:"true")
            branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId!)")
             branchUniversalObject.addMetadataKey("isRSVP", value:isRSVP)
            
            var isAccess = "YWNjZXNzPTE"
            
            if isApproved == false
            {
                branchUniversalObject.addMetadataKey("isApproved", value:"false")
                isAccess = "YWNjZXNzPTA"
            }
            else
            {
                branchUniversalObject.addMetadataKey("isApproved", value:"true")
            }

            
            
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/view_channel/\(currentEvent.objectId!)?\(isAccess)")
            linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/view_channel/\(currentEvent.objectId!)?\(isAccess)")
            linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/view_channel/\(currentEvent.objectId!)?\(isAccess)")
            
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        let urlLink = url!
                        
                        self.generatelink(urlLink)
                    }
            })
        }
        else
        {
            var data = [
                "eventObjectId": currentEvent.objectId!,
                "emailId": "noemail",
                "eventCreatorId": "\(currentUserId!)",
                "isApproved": "true",
                "isSocialSharing": "true",
                "eventCreatorId": "\(currentUserId!)",
                "isRSVP" : "true"
            ]
            
            
            if isApproved == false
            {
                data["isApproved"] = "false"
            }
            
            
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    //println(url!)
                    
                    let urlLink = url!
                    
                    self.generatelink(urlLink)
                    //
                    //                let plainData = (currentEvent.objectId! as NSString).dataUsingEncoding(NSUTF8StringEncoding)
                    //                let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                    //                //println(base64String)
                    //
                    //                var branchUrl = url! as String
                    //
                    //                let plainDataUrl = (branchUrl as NSString).dataUsingEncoding(NSUTF8StringEncoding)
                    //
                    //                let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                    //                //println(base64UrlString)
                    //
                    //                var urlString = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId=\(base64String)&url=\(base64UrlString)"
                    //
                    //                var eventTitle = currentEvent["eventTitle"] as! String
                    //
                    //                var objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(urlString)"
                    //
                    //                let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
                    //                self.navigationController!.presentViewController(activityVC,
                    //                    animated: true,
                    //                    completion: nil)
                    
                }
                
            })

        }
        
        
    }
    
    
    func generatelink(urlLink:String)
    {
        let plainData = (currentEvent.objectId! as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //println(base64String)
        
        let branchUrl = urlLink
        print(branchUrl)
        //        let plainDataUrl = (branchUrl as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        //
        //        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //        //println(base64UrlString)
        //
        //        let urlString = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId=\(base64String)&url=\(branchUrl)"
        
        let eventTitle = currentEvent["eventTitle"] as! String
        
        let objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(branchUrl)"
        
        let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
        self.navigationController!.presentViewController(activityVC,
            animated: true,
            completion: nil)
        
    }
    
    
    @IBAction func accessButton(sender: AnyObject)
    {
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        if isApproved == false
        {
            isApproved = true
            sender.setImage(UIImage(named: "check-box.png"), forState: .Normal)
            
            channelAccessToggleTextView.attributedText = NSAttributedString(string: "Anyone with this link can join the channel and get automatic access to photos & videos", attributes:attributes)
            channelAccessToggleTextView.textAlignment = .Center
            channelAccessToggleTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
            channelAccessToggleTextView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            
        }
        else
        {
            isApproved = false
            sender.setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            
            channelAccessToggleTextView.attributedText = NSAttributedString(string: "Anyone who joins with this link will not get automatic access to the channel until you manually approve them", attributes:attributes)
            channelAccessToggleTextView.textAlignment = .Center
            channelAccessToggleTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
            channelAccessToggleTextView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            
        }

        
    }
    
}
