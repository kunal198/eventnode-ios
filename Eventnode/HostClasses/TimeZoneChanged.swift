//
//  TimeZoneChanged.swift
//  Eventnode
//
//  Created by brst on 2/29/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//

import Foundation


class TimeZoneChanged
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, type: String, url:String, timeZone :String)-> String
    {
        
        let file = "timezone_change.html"
        
        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //println(base64String)
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        message = message.stringByReplacingOccurrencesOfString("Rosy's Baby Shower", withString: "\(eventTitle)", options: [], range: nil)
        
        message = message.stringByReplacingOccurrencesOfString("Sharon Tucker", withString: "\(hostName)", options: [], range: nil)
        message = message.stringByReplacingOccurrencesOfString("Roooooooooosy’s Baby Shower", withString: "\(eventTitle)", options: [], range: nil)
        
        
        message = message.stringByReplacingOccurrencesOfString("Los Angeles Time", withString: "\(timeZone)", options: [], range: nil)
        
        message = message.stringByReplacingOccurrencesOfString("deeeeeeeeeeeeppppppppppppplllllllliiiiiiiinnnnnnnnkkkkkkkk", withString:"\(url)", options: [], range: nil)
        
        return message
    }
}
