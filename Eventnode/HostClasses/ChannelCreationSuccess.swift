//
//  ChannelCreationSuccess.swift
//  Eventnode
//
//  Created by brst on 2/22/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//


import Foundation


class ChannelCreationSuccess
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, imageUrl:String ,type: String , url:String,addToStreamLink: String, InviteMemberlink: String)-> String
    {
        
        let file = "channel_creation_success.html"
        
        
        
        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)
        
        
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        //println(base64String)
        
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("EEEEEEEEEEurope Trip 2014", withString:"\(eventTitle)", options: [], range: nil)
        
        message = message.stringByReplacingOccurrencesOfString("SSSSSSharon Tucker", withString:"\(hostName)", options: [], range: nil)

        message = message.stringByReplacingOccurrencesOfString("demo.html", withString:"\(url)", options: [], range: nil)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("demo1.html", withString:"\(InviteMemberlink)", options: [], range: nil)
        print("InviteMemberlink is  \(InviteMemberlink)")
        message = message.stringByReplacingOccurrencesOfString("demo2.html", withString:"\(addToStreamLink)", options: [], range: nil)
        print("addToStreamLink is  \(addToStreamLink)")

        
        message = message.stringByReplacingOccurrencesOfString("<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"http://dcy86hdr5o800.cloudfront.net/image1-online.jpg\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", withString:"<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"\(imageUrl)\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", options: [], range: nil)
        
        return message
    }
}
