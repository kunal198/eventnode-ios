//
//  Attending.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class Attending
{
    func emailMessage(eventId: String, eventTitle: String,guestName:String, adultsCount:Int,childsCount:Int ,type: String, url:String)-> String
{

   let file = "Attending3.html"



   let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)



    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)

    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

    //println(base64String)

    

    let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)

    

    let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))



   var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)



   message = message.stringByReplacingOccurrencesOfString("Chandra Kilaru", withString:"\(guestName)", options: [], range: nil)

    
    message = message.stringByReplacingOccurrencesOfString("Rosy’s Baby Shower", withString:"\(eventTitle)", options: [], range: nil)


  if type == "rsvp"

  {

     message = message.stringByReplacingOccurrencesOfString("Adults: 2", withString: "Adults: \(adultsCount)", options: [], range: nil)
    
    message = message.stringByReplacingOccurrencesOfString("Children: 2", withString: "Children: \(childsCount)", options: [], range: nil)
    
     message = message.stringByReplacingOccurrencesOfString("deeeeeeeeeepppppppppppllllllllliiiiiiiinnnnnnkkkkk", withString: "\(url)", options: [], range: nil)
    

  }

   return message
 }
}