//
//  GuestListChange.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class GuestListChange
{
    func emailMessage(eventId: String, eventTitle: String, guestName: String, oldAdultCount: Int, oldChildsCount: Int, newAdultCount: Int, newChildCount: Int, type: String, url:String )-> String
{

let file = "guest_list_change2.html"



let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)



    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)

    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

    //println(base64String)

    

    let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)

    

    let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))



var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)



message = message.stringByReplacingOccurrencesOfString("Rosy's Baby Shower", withString:"\(eventTitle)", options: [], range: nil)



if type == "rsvp"

{
    
    
    message = message.stringByReplacingOccurrencesOfString("Chandra Kilaru", withString:"\(guestName)", options: [], range: nil)

message = message.stringByReplacingOccurrencesOfString("<b>Rosy’s Baby Shower</b>", withString:"<b>\(eventTitle)</b>", options: [], range: nil)

    

    message = message.stringByReplacingOccurrencesOfString(" 2 adults, 1 children", withString: " \(oldAdultCount) Adults, \(oldChildsCount) Children", options: [], range: nil)
    
    message = message.stringByReplacingOccurrencesOfString("</b> 1 Adult, 1 Children</td>", withString:"</b> \(newAdultCount) Adults, \(newChildCount) Children</td>", options: [], range: nil)


}



message = message.stringByReplacingOccurrencesOfString("deeeeeeppppppppllllllliiiinnnnnkkk", withString:"\(url)", options: [], range: nil)



return message
}
}