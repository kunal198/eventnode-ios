//
//  ChannelNewMember.swift
//  Eventnode
//
//  Created by brst on 2/22/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//

import Foundation


class ChannelNewMember
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, type: String, url:String)-> String
    {
        
        let file = "channel_new_member.html"
        
        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //println(base64String)
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        
        message = message.stringByReplacingOccurrencesOfString("Chandra Kilaru just joined your channel <b>Rosy’s Baby Shower", withString: "\(hostName) just joined your channel <b> \(eventTitle)", options: [], range: nil)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("deeeeeeeeeeeeppppppppppppplllllllliiiiiiiinnnnnnnnkkkkkkkk", withString:"\(url)", options: [], range: nil)
         
        return message
    }
}
