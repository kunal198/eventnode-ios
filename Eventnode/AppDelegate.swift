    //
//  AppDelegate.swift
//  eventnode
//
//  Created by mrinal khullar on 4/23/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import AudioToolbox
import Intercom
var googleApiKey = "AIzaSyBZgzVWDT6-5zjmBLTnLiDMFI3zJZJRipY"

var isLoginScreen = false

var isEventDataUpDated = false

var isPostDataUpDated = false

var hasNewMessage = false

var eventPageBaseUrl = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId="

var documentDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] 

var wakeUpImageView: UIImageView = UIImageView()

var weekDaysArray: NSArray = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

var monthsArray: NSArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

var isChatMode = false

var isChatUpdated = false

var msg = ""

var currentChatEventObjectId = ""

var emailLinkType = ""
var addToStream = ""
var invitedLink = ""

    var linkForGuest = ""
    
    var currentAttendingStatus = ""
var deviceName = "iPhone"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    //var wakeUpView: UIView?
    
    override init() {
        
        super.init()
        
        Firebase.defaultConfig().persistenceEnabled = true
        
    }
    

    @IBOutlet var window1:UIWindow!
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: "chckupdate")

       
        wakeUpImageView.hidden = true
        
        Util.copyFile("EventNodeDb.sqlite")
//      Util.copyFile("Invite_User.html")
//      Util.copyFile("Invite_NonUser.html")
        
        deviceName = platformString()
        
        
        Util.copyFile("channel_invite_reminder.html")
        Util.copyFile("channel_invite.html")
        Util.copyFile("delete_channel.html")
        Util.copyFile("stream_access_channel.html")
        Util.copyFile("channel_creation_success.html")
        Util.copyFile("channel_new_member.html")
        Util.copyFile("Event_Creation_Success.html")
        Util.copyFile("content_like.html")
        Util.copyFile("guest_list_change.html")
        Util.copyFile("guest_response_change.html")
        Util.copyFile("new_message_host.html")
        Util.copyFile("Attending.html")
        Util.copyFile("Maybe.html")
        Util.copyFile("Not_Attending.html")
        Util.copyFile("Online_Only.html")
        Util.copyFile("Online_Only_Event.html")
        Util.copyFile("date_change.html")
        Util.copyFile("delete_event.html")
        Util.copyFile("location_change.html")
        Util.copyFile("new_content.html")
        Util.copyFile("new_message_guest.html")
        Util.copyFile("stream_access.html")
        Util.copyFile("In_Person.html")
        Util.copyFile("In_Person_Reminder.html")
        Util.copyFile("Online_Only_Invite_Email_Guest.html")
        Util.copyFile("Online_Only_Reminder.html")
        Util.copyFile("Attending_Invite_Response_Guest.html")
        Util.copyFile("Attending_Reminder.html")
        Util.copyFile("Maybe_Invite_Response_Guest.html")
        Util.copyFile("Maybe_Reminder.html")
        Util.copyFile("Not_Attending_Invite_Response_Guest.html")
        Util.copyFile("Online_Only_Invite_Response_Guest.html")
        Util.copyFile("Online_Only_Reminder_Invite_Response_Guest.html")
        Util.copyFile("online_event_creation.html")
        Util.copyFile("event_invite.html")
        Util.copyFile("event_reminder_invite.html")
        Util.copyFile("Online_Only3.html")
        Util.copyFile("Not_Attending3.html")
        Util.copyFile("Maybe3.html")
        Util.copyFile("Attending3.html")
        Util.copyFile("Attending2.html")
        Util.copyFile("timezone_change.html")
        Util.copyFile("stream_access2.html")
        Util.copyFile("location_change2.html")
        Util.copyFile("delete_event2.html")
        Util.copyFile("date_change.html")
        Util.copyFile("Maybe2.html")
        Util.copyFile("Not_Attending2.html")
        Util.copyFile("Online_Only2.html")
        Util.copyFile("guest_response_change2.html")
        Util.copyFile("guest_list_change2.html")
        Util.copyFile("Online_Only3.html")
        Util.copyFile("Not_Attending3.html")
        self.startReachabilityTest();

        
        
        
        
        var isAdded = ModelManager.instance.addColumnToTable("EventComments", columnName: "isRead", colType: "BOOL", defaultValue: "0")

        //println(isAdded)
        
        
        isAdded = ModelManager.instance.addColumnToTable("EventComments", columnName: "timezoneOffset", colType: "INTEGER", defaultValue: "\(NSTimeZone.localTimeZone().secondsFromGMT)")
        
        //println(isAdded)


        isAdded = ModelManager.instance.addColumnToTable("Events", columnName: "timezoneName", colType: "VARCHAR", defaultValue: "'America/Los_Angeles'")
        
        //println(isAdded)
        
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "reachabilityChanged:",
            name: kReachabilityChangedNotification,
            object: nil)
        
        let credentialsProvider = AWSCognitoCredentialsProvider(
            regionType: AWSRegionType.USEast1,
            identityPoolId: "us-east-1:4a7cf640-0065-44af-9e4d-f113afa41009")
        let configuration = AWSServiceConfiguration(
            region: AWSRegionType.USEast1,
            credentialsProvider: credentialsProvider)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration

        //ParseCrashReporting.enable();

        // Productions Keys
        Parse.setApplicationId("O4B1M7zEY4B1SWu43LmH3k9znM6FTMF0Gh8NWjBu", clientKey:"YFYf0oxN7qpZ3yMuafVrAPRQimVy56KfwqqT3IOf")
        
        // Development Keys
//        Parse.setApplicationId("h3HHyRF14DroCfmbt6L9A66UeJ4B3w37kuqC7wvP", clientKey:"shcJL2NzXFTOGwVruLtO2tJObmx4qPBfIZkaQQZc")


        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions)

        // Log App Launch Event.
        AnalyticsModel.instance.logAppLaunchEvent()
        
        // SupportModel currently uses FreshDesk
        SupportModel.instance.initialize()
        
        // Intercom 
        Intercom.setApiKey("ios_sdk-710e4a8486b1af4e4de2c43cb0039d84a48e9379", forAppId: "qaq17pgj")
        
        let width = window?.frame.size.width
        
        let height = window?.frame.size.height
        
        //println("\(height), \(width)")
        
        resetBadge()
        
        //wakeUpView?.frame = CGRectMake(0, 0, width!, height!)
        
        wakeUpImageView.frame = CGRectMake(0, 0, width!, height!)
        
        wakeUpImageView.contentMode = .ScaleAspectFit
        
        wakeUpImageView.image = UIImage(named: "splashimage.png")
        
        wakeUpImageView.hidden = true
        
        //window!.addSubview(wakeUpImageView)
        
        
        //let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("HomeViewController") as! HomeViewController
        
        /*window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let mainViewController: MainViewController = MainViewController(nibName: "WakeupScreen", bundle: nil)
        window!.rootViewController = mainViewController
        window!.makeKeyAndVisible()*/
        
//        application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
////
//        let userNotificationTypes: UIUserNotificationType = ([UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]);
        
        
        
       var soundId = SystemSoundID()
        
//        
//        let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
//        application.registerUserNotificationSettings(settings)
//        application.registerForRemoteNotifications()
        
               
        
        let branch: Branch = Branch.getInstance()
        branch.setDebug()
        branch.initSessionWithLaunchOptions(launchOptions, andRegisterDeepLinkHandler: { data, error in
            if (error == nil)
            {
                if let eventObjectId = data["eventObjectId"] as? String
                {
                    //println(data.description)
                    //println("https://bnc.lt/a/key_live_lmcRw2ZuPudWC0C78p7SUcalBqdFjAaI?abc=123")
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    /*if PFUser.currentUser() != nil
                    {
                        let SharedVC = storyboard.instantiateViewControllerWithIdentifier("SharedEventViewController") as! SharedEventViewController
                        
                        SharedVC.deepLinkEmail = data["emailId"] as! String
                        SharedVC.deepLinkObjectId = data["eventObjectId"] as! String
                        SharedVC.eventCreatorObjectId = data["eventCreatorId"] as! String
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(SharedVC, animated: false)
                    }
                    else
                    {*/
                    let customLoginVC = storyboard.instantiateViewControllerWithIdentifier("CustomLoginViewController") as! CustomLoginViewController
                    
                    
                    
                    
                    
                    if let linkForguest = data["linkForguest"]
                    {
                        linkForGuest = data["linkForguest"] as! String
                        print(linkForGuest)
                    }
                    
                    
                    if let notifType = data["notifType"]
                    {
                        emailLinkType = data["notifType"] as! String
                        print(emailLinkType)
                    }
                    
                    if let attendingStatus = data["attendingStatus"]
                    {
                        currentAttendingStatus = data["attendingStatus"] as! String
                        
                    }

                    if let rsvp = data["isRSVP"]
                    {
                        print(data)
                        customLoginVC.isRSVP = data["isRSVP"] as! String
                    }
                    customLoginVC.deepLinkEmail = data["emailId"] as! String
                    customLoginVC.deepLinkObjectId = data["eventObjectId"] as! String
                    
                    customLoginVC.eventCreatorObjectId = data["eventCreatorId"] as! String
                    
                    if let datasharing = data["isSocialSharing"]
                    {
                        if data["isSocialSharing"] as! String == "true"
                        {
                            customLoginVC.invitationObjectId = ""
                        }
                    }
                        
                        
                    else
                    {
                        if let objId = data["objectId"]
                        {
                            customLoginVC.invitationObjectId = data["objectId"] as! String
                        }
                    }
                    
                    
                    if let isApproved = data["isApproved"] as? String
                    {
                        customLoginVC.isApproved = data["isApproved"] as! String
                    }
                    else
                    {
                        customLoginVC.isApproved = "false"
                    }
                    
                    let navigationController = self.window?.rootViewController as! UINavigationController
                    
                    navigationController.pushViewController(customLoginVC, animated: false)
                    
                    
                    //}
                }
                else
                {
                    if PFUser.currentUser() != nil
                    {
                        if let deepLinkType = data["deepLinkType"] as? String
                        {
                            if deepLinkType == "createEvent"
                            {                                
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                
                                let eventDetailsVC = storyboard.instantiateViewControllerWithIdentifier("EventDetailsViewController") as! EventDetailsViewController
                                
                                eventDetailsVC.isAfterImage = false
                                
                                let navigationController = self.window?.rootViewController as! UINavigationController
                                
                                navigationController.pushViewController(eventDetailsVC, animated: false)
                            }
                        }
                    }
                }

            }
        })
        
        print(NSUserDefaults.standardUserDefaults().valueForKey("firstTimeLogin"))
        if NSUserDefaults.standardUserDefaults().valueForKey("firstTimeLogin") == nil
        {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "firstTimeLogin")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let walkthroughVC = storyboard.instantiateViewControllerWithIdentifier("walkthroughViewController") as! walkthroughViewController
            
            walkthroughVC.isFrom = "appdelegate"
            
            let navigationController = self.window?.rootViewController as! UINavigationController
            
            navigationController.pushViewController(walkthroughVC, animated: false)
        }


        Fabric.with([Crashlytics.self()])
        logUser()
        return true
    }
    
    ///Users/brst981/Desktop/eventnode_backup/16 october 2015/eventnode/eventnode/AppDelegate.swift:220:16: Type 'String' does not conform to protocol 'NilLiteralConvertible'
    
    
    func logUser()
    {
        let email =  NSUserDefaults.standardUserDefaults().objectForKey("email") as? String
        let name = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as? String
        let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        
        
        Crashlytics.sharedInstance().setUserIdentifier(currentUserId)
        Crashlytics.sharedInstance().setUserEmail(email)
        Crashlytics.sharedInstance().setUserName(name)
        
    }
    
    func platform() -> String {
        
        if let key = "hw.machine".cStringUsingEncoding(NSUTF8StringEncoding) {
            var size: Int = 0
            sysctlbyname(key, nil, &size, nil, 0)
            var machine = [CChar](count: Int(size), repeatedValue: 0)
            sysctlbyname(key, &machine, &size, nil, 0)
            return String.fromCString(machine)!
        }
        else
        {
            return "unknown"
        }
    }
    
    
    func platformString() -> String
    {
        var devSpec : String
        
        switch platform()
        {
        case "iPhone1,2": devSpec = "iPhone 3G"
        case "iPhone2,1": devSpec = "iPhone 3GS"
        case "iPhone3,1": devSpec = "iPhone 4"
        case "iPhone3,3": devSpec = "Verizon iPhone 4"
        case "iPhone4,1": devSpec = "iPhone 4S"
        case "iPhone5,1": devSpec = "iPhone 5"
        case "iPhone5,2": devSpec = "iPhone 5"
        case "iPhone5,3": devSpec = "iPhone 5c"
        case "iPhone5,4": devSpec = "iPhone 5c"
        case "iPhone6,1": devSpec = "iPhone 5s"
        case "iPhone6,2": devSpec = "iPhone 5s"
        case "iPhone7,1": devSpec = "iPhone 6 Plus"
        case "iPhone7,2": devSpec = "iPhone 6"
        case "iPhone8,1": devSpec = "iPhone 6s"
        case "iPhone8,2": devSpec = "iPhone 6s Plus"
        case "iPod1,1": devSpec = "iPod Touch"
        case "iPod2,1": devSpec = "iPod Touch"
        case "iPod3,1": devSpec = "iPod Touch"
        case "iPod4,1": devSpec = "iPod Touch"
        case "iPod5,1": devSpec = "iPod Touch"
        case "iPad1,1": devSpec = "iPad"
        case "iPad2,1": devSpec = "iPad 2"
        case "iPad2,2": devSpec = "iPad 2"
        case "iPad2,3": devSpec = "iPad 2"
        case "iPad2,4": devSpec = "iPad 2"
        case "iPad2,5": devSpec = "iPad Mini"
        case "iPad2,6": devSpec = "iPad Mini"
        case "iPad2,7": devSpec = "iPad Mini"
        case "iPad3,1": devSpec = "iPad 3"
        case "iPad3,2": devSpec = "iPad 3"
        case "iPad3,3": devSpec = "iPad 3"
        case "iPad3,4": devSpec = "iPad 4"
        case "iPad3,5": devSpec = "iPad 4"
        case "iPad3,6": devSpec = "iPad 4"
        case "iPad4,1": devSpec = "iPad Air"
        case "iPad4,2": devSpec = "iPad Air"
        case "iPad4,4": devSpec = "iPad mini"
        case "iPad4,5": devSpec = "iPad mini"
            
        case "iPad4,7": devSpec = "iPad mini 3"
        case "iPad4,8": devSpec = "iPad mini 3"
        case "iPad4,9": devSpec = "iPad mini 3"
            
        case "iPad5,3": devSpec = "iPad Air 2"
        case "iPad5,4": devSpec = "iPad Air 2"
            
        case "i386": devSpec = "Simulator"
        case "x86_64": devSpec = "Simulator"
            
        default: devSpec = "unknown"
        }
        
        return devSpec
    }
    
    
    func startReachabilityTest()
    {
        // Allocate a reachability object to test internet access by hostname
        let reach = Reachability(hostName: "www.apple.com")
        
        // Tell the reachability that we DON'T want to be reachable on 3G/EDGE/CDMA
        //reach.reachableOnWWAN = false
        
        reach.startNotifier()
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?,
        annotation: AnyObject) -> Bool
    {
            
            Branch.getInstance().handleDeepLink(url)
            
            return FBSDKApplicationDelegate.sharedInstance().application(application,
                openURL: url,
                sourceApplication: sourceApplication,
                annotation: annotation)
        
    }


    
    func reachabilityChanged(notice: NSNotification)
    {
        //println("reachability changed")
        let reach = notice.object as? Reachability
        if let remoteHostStatus = reach?.currentReachabilityStatus()
        {
            if remoteHostStatus == NetworkStatus.NotReachable
            {
                //println("not reachable")
            }
            else
            {
                //println("reachable")
                
                let postContent = PostContent()
                postContent.postUnpublishedContent()
                
            }
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication)
    {
        
        //wakeUpImageView.hidden = false
        
        
        if MyReachability.isConnectedToNetwork()
        {
            //let postContent = PostContent()
           // postContent.postUnpublishedContent()
        }
        else
        {
            //println("not reachable")
        }
        
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    //
    
    func resetBadge()
    {
        
        let installation = PFInstallation.currentInstallation()
        installation["badgeCount"] = 0
        installation.saveInBackground()
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        let pfinst = PFInstallation .currentInstallation()
        if pfinst.badge != 0
        {
        pfinst.badge = 0
            pfinst.saveEventually()
        }
        if PFUser.currentUser() != nil
        {
            let currentUser: PFUser = PFUser.currentUser()!
            let userObjectId = currentUser.objectId!
            
            
            let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
            
            let query = PFQuery(className: "BadgeRecords", predicate: predicate)
            
            query.findObjectsInBackgroundWithBlock {
                (objects: [AnyObject]?, error: NSError?) -> Void in
                
                if error == nil
                {
                    //println(objects?.count)
                    if let fetchedObjects = objects as? [PFObject]
                    {
                        var badgeObject: PFObject!
                        var badgeCount = 0
                        
                        if fetchedObjects.count > 0
                        {
                            for object in fetchedObjects
                            {
                                badgeObject = object
                                badgeCount = badgeObject["badgeCount"] as! Int
                            }
                        }
                        else
                        {
                            badgeObject = PFObject(className: "BadgeRecords")
                            
                            badgeObject["userObjectId"] = userObjectId
                        }
                        
                        badgeObject["badgeCount"] = 0
                        
                        badgeObject.saveInBackground()
                        
                    }
                }
                else
                {
                    
                }
                
            }
            
        }
    }
    
    
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool
    {
        return Branch.getInstance().continueUserActivity(userActivity)
    }
    
//    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool
//    {
//        // pass the url to the handle deep link call
//        
//        return Branch.getInstance().continueUserActivity(userActivity)
//    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        ////println("came to foreground")
        //window!.makeKeyAndVisible()
        NSNotificationCenter.defaultCenter().postNotificationName("eventviewtimer", object: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("sharedtimer", object: nil)
        NSNotificationCenter.defaultCenter().postNotificationName("updatechatforeground", object: nil)

        resetBadge()
        
        //var pushNotificationService: PushNotificationService! = App42API.BuildPushNotificationService();
        
       // downloadData()
        //downloadInvitation()
        
        if MyReachability.isConnectedToNetwork()
        {
            //println("reachable")
        }
        else
        {
            //println("not reachable")
        }
        
        
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func application(application: UIApplication,  didReceiveRemoteNotification userInfo: [NSObject : AnyObject],  fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        resetBadge()

        
         NSNotificationCenter.defaultCenter().postNotificationName("loadalert", object: nil)
        
        if let notifType: String = userInfo["notifType"] as? String {
            
            if PFUser.currentUser() != nil
            {
                
                resetBadge()
                
                //application.applicationState
                
              //rahul  var msg = ""

                /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let eventPhototsVC = storyboard.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
                
                let navigationController = self.window?.rootViewController as! UINavigationController
                
                    navigationController.pushViewController(eventPhototsVC, animated: false)*/
                
                let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
                
                if notifType == "groupchat"
                {

                    let rsvp = userInfo["isRSVP"] as! String
                    
                 
                    
                    var tblFields: Dictionary! = [String: String]()
                    if currentChatEventObjectId == userInfo["eventObjectId"] as! String
                    {
                        isChatUpdated = true
                        if isChatMode == true
                        {
                            tblFields["isRead"] = "1"
                        }
                        else
                        {
                            tblFields["isRead"] = "0"
                        }
                    }
                    else
                    {
                        tblFields["isRead"] = "0"
                    }

                    if !checkExistance("objectId", objectId: userInfo["objectId"] as! String, tableName: "EventComments")
                    {
                        tblFields["objectId"] = userInfo["objectId"] as! String

                        tblFields["messageText"] = userInfo["messageText"] as! String

                        tblFields["senderObjectId"] = userInfo["senderObjectId"] as! String

                        tblFields["eventObjectId"] = userInfo["eventObjectId"] as! String

                        tblFields["senderName"] = userInfo["senderName"] as! String

                        tblFields["timezoneOffset"] = "\(NSTimeZone.localTimeZone().secondsFromGMT)"

                        let timezoneOffsetString = userInfo["timezoneOffset"] as! String

                        let timezoneOffset = Double(Int("\(timezoneOffsetString)")!)

                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        let createdAt = dateFormatter.dateFromString(userInfo["createdAt"] as! String)
                        let updatedAt = dateFormatter.dateFromString(userInfo["updatedAt"] as! String)
                        
                        let createdAtString = dateFormatter.stringFromDate( NSDate(timeIntervalSince1970: Double(createdAt!.timeIntervalSince1970) + Double(NSTimeZone.localTimeZone().secondsFromGMT) - timezoneOffset ))
                        
                        let updatedAtString = dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: Double(updatedAt!.timeIntervalSince1970) + Double(NSTimeZone.localTimeZone().secondsFromGMT) - timezoneOffset ))
                        
                        tblFields["senderName"] = userInfo["senderName"] as! String
                        
                        tblFields["createdAt"] = createdAtString
                        tblFields["updatetAt"] = updatedAtString
                        
                        tblFields["isPosted"] = "1"
                        var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                        
                        if isOnEventStream == true
                        {
                            if currentSharedEvent.objectId! == userInfo["eventObjectId"] as! String
                            {
                                isEventStreamUpdated = true
                            }
                        }
                        
                        if isOnMyEventStream == true
                        {
                            if currentEvent.objectId! == userInfo["eventObjectId"] as! String
                            {
                                //println("new my event messageeeeeeee")
                                isMyeventStreamUpdated = true
                            }
                        }
                        
                        
                        if isOnSharedEvents == true
                        {
                            //println("new messageeeeeeee")
                            isSharedEventsUpdated = true
                        }
                        
                    }

                    
                    if (application.applicationState != UIApplicationState.Active) {
                        
                        NSNotificationCenter.defaultCenter().postNotificationName("updatechat", object: nil)

                        
                        
                        let eventObjectId = userInfo["eventObjectId"] as! String
                        
                        var isShared = false
                        
                        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId = '\(eventObjectId)' GROUP BY objectId ORDER BY eventId DESC", whereFields: [])
                        
                        if (resultSet != nil)
                        {
                            while resultSet.next() {
                                
                                let userevent = PFObject(className: "Events")
                                
                                userevent["eventId"] = Int(resultSet.intForColumn("eventId"))
                                userevent["eventTitle"] = resultSet.stringForColumn("eventTitle")
                                userevent["eventCreatorObjectId"] = resultSet.stringForColumn("eventCreatorObjectId")
                                let isRSVP = resultSet.stringForColumn("isRSVP")
                                
                                if isRSVP == "0"
                                {
                                    userevent["isRSVP"] = false
                                }
                                else
                                {
                                    userevent["isRSVP"] = true
                                    
                                    userevent["eventDescription"] = resultSet.stringForColumn("eventDescription")
                                    userevent["eventLocation"] = resultSet.stringForColumn("eventLocation")
                                    userevent["eventLatitude"] = resultSet.doubleForColumn("eventLatitude")
                                    userevent["eventLongitude"] = resultSet.doubleForColumn("eventLongitude")
                                    userevent["eventStartDateTime"] = stringToDate(resultSet.stringForColumn("eventStartDateTime"))
                                    
                                    userevent["eventEndDateTime"] = stringToDate(resultSet.stringForColumn("eventEndDateTime"))
                                }
                                userevent["eventImage"] = resultSet.stringForColumn("eventImage")
                                userevent["originalEventImage"] = resultSet.stringForColumn("originalEventImage")
                                
                                userevent["eventTimezoneOffset"] = resultSet.doubleForColumn("eventTimezoneOffset")
                                
                                userevent["eventFolder"] = resultSet.stringForColumn("eventFolder")
                                userevent["frameX"] = resultSet.doubleForColumn("frameX")
                                userevent["frameY"] = resultSet.doubleForColumn("frameY")
                                userevent["senderName"] = resultSet.stringForColumn("senderName")
                                userevent["timezoneName"] = resultSet.stringForColumn("timezoneName")
                                //userevent["socialSharingURL"] = resultSet.stringForColumn("socialSharingURL")
                                userevent["socialSharingURL"] = ""
                                
                                userevent["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                                userevent["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                                
                              if(resultSet.stringForColumn("objectId") != "" && resultSet.stringForColumn("objectId") != nil && resultSet.stringForColumn("objectId") != "hi")
                                {
                                    userevent["createdAt"] = stringToDate(resultSet.stringForColumn("createdAt"))
                                    userevent["updatedAt"] = stringToDate(resultSet.stringForColumn("updatedAt"))
                                    userevent.objectId = resultSet.stringForColumn("objectId")
                                }
                                
                                let isPosted = resultSet.stringForColumn("isPosted")
                                
                                if isPosted == "0"
                                {
                                    userevent["isPosted"] = false
                                }
                                else
                                {
                                    userevent["isPosted"] = true
                                }
                                
                                userevent["isUploading"] = false
                                
                                if currentUserId == userInfo["senderObjectId"] as! String
                                {
                                    isShared = false
                                    currentEvent = userevent
                                }
                                else
                                {
                                    currentSharedEvent = userevent
                                    isShared = true
                                }
                                
                                //println(userevent["isPosted"]!)
                            }
                        }
                        
                        resultSet.close()
                        
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let addCommentVC = storyboard.instantiateViewControllerWithIdentifier("AddCommentsViewController") as! AddCommentsViewController
                        
                        addCommentVC.isShared = isShared
                        
                        if rsvp == "true"
                        {
                            addCommentVC.isRSVP = true
                        }
                        else
                        {
                            addCommentVC.isRSVP = false
                        }
                        
                        
                        
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(addCommentVC, animated: false)
                    }
                    
                }
                
                if notifType == "postnewcontent"
                {
                    if isOnEventStream == true
                    {
                        if currentSharedEvent.objectId! == userInfo["eventObjectId"] as! String
                        {
                            isEventStreamUpdated = true
                            noOfStreamUpdates++
                        }
                    }
                    
                    if !checkExistance("objectId", objectId: userInfo["objectId"] as! String, tableName: "EventImages")
                    {
                        var tblFields: Dictionary! = [String: String]()
                        
                        tblFields["objectId"] = userInfo["objectId"] as? String
                        tblFields["postData"] = userInfo["postData"] as? String
                        tblFields["postHeight"] = userInfo["postHeight"] as? String
                        
                        tblFields["postWidth"] = userInfo["postWidth"] as! String
                        tblFields["eventObjectId"] = userInfo["eventObjectId"] as! String
                        
                        tblFields["eventFolder"] = userInfo["eventFolder"] as! String
                        
                        tblFields["postType"] = userInfo["postType"] as! String
                        tblFields["createdAt"] = userInfo["createdAt"] as! String
                        tblFields["updatedAt"] = userInfo["updatedAt"] as! String
                        
                        tblFields["isPosted"] = "1"
                        
                        tblFields["isRead"] = "0"
                        
                        var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
                    }
                    
                    if (application.applicationState != UIApplicationState.Active) {
                        //userInfo["eventObjectId"] as! String
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let alertVC = storyboard.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
                        
                        alertVC.haveData = true
                        alertVC.alertEventObjectId = userInfo["eventObjectId"] as! String
                        alertVC.alertNotificationType = "postnewcontent"
                        alertVC.senderObjectId = currentUserId
                        
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(alertVC, animated: false)
                        
                        //alertVC
                    }
                    
                }
                
                
                if notifType == "eventdataupdated"
                {
                    
                    if !checkExistance("objectId", objectId: userInfo["objectId"] as! String, tableName: "Events")
                    {
                        var tblFields: Dictionary! = [String: String]()
                        
                        if userInfo["isRSVP"] as! String == "true"
                        {
                            tblFields["isRSVP"] = "1"
                        }
                        else
                        {
                            tblFields["isRSVP"] = "0"
                        }
                        
                        tblFields["eventTitle"] = userInfo["eventTitle"] as? String
                        
                        tblFields["eventCreatorObjectId"] = userInfo["eventCreatorObjectId"] as? String
                        
                        tblFields["eventImage"] = userInfo["eventImage"] as? String
                        
                        tblFields["originalEventImage"] = userInfo["originalEventImage"] as? String
                        
                        tblFields["eventFolder"] = userInfo["eventFolder"] as? String
                        
                        tblFields["frameX"] = userInfo["frameX"] as? String
                        tblFields["frameY"] = userInfo["frameY"] as? String
                        tblFields["eventStartDateTime"] = userInfo["eventStartDateTime"] as? String
                        
                        tblFields["eventTimezoneOffset"] = userInfo["eventTimezoneOffset"] as? String
                        
                        tblFields["eventDescription"] = userInfo["eventDescription"] as? String
                        
                        tblFields["objectId"] = userInfo["eventObjectId"] as? String
                        
                        tblFields["eventLocation"] = userInfo["eventLocation"] as? String
                        
                        tblFields["eventLatitude"] = userInfo["eventLatitude"] as? String
                        
                        tblFields["eventLongitude"] = userInfo["eventLongitude"] as? String
                        
                        tblFields["socialSharingURL"] = userInfo["socialSharingURL"] as? String
                        tblFields["timezoneName"] = userInfo["timezoneName"] as? String
                        
                        tblFields["senderName"] = userInfo["senderName"] as? String
                        
                        
                        tblFields["createdAt"] = userInfo["createdAt"] as? String
                        
                        tblFields["updatedAt"] = userInfo["updatedAt"] as? String
                        
                        tblFields["isPosted"] = "1"
                        
                        _ = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                    }
                    else
                    {
                        if (application.applicationState == UIApplicationState.Active)
                        {
                            var tblFields: Dictionary! = [String: String]()
                            
                            tblFields["eventTitle"] = userInfo["eventTitle"] as? String
                            
                            tblFields["eventStartDateTime"] = userInfo["eventStartDateTime"] as? String
                            
                            tblFields["eventTimezoneOffset"] = userInfo["eventTimezoneOffset"] as? String
                            
                            tblFields["eventDescription"] = userInfo["eventDescription"] as? String
                            
                            tblFields["eventLocation"] = userInfo["eventLocation"] as? String
                            
                            tblFields["eventLatitude"] = userInfo["eventLatitude"] as? String
                            
                            tblFields["eventLongitude"] = userInfo["eventLongitude"] as? String
                            
                            tblFields["timezoneName"] = userInfo["timezoneName"] as? String
                            
                            tblFields["senderName"] = userInfo["senderName"] as? String
                            
                            tblFields["createdAt"] = userInfo["createdAt"] as? String
                            
                            tblFields["updatedAt"] = userInfo["updatedAt"] as? String
                            
                            tblFields["isPosted"] = "1"
                            
                            let objectId = userInfo["eventObjectId"] as! String
                            
                            _ = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "WHERE objectId='\(objectId)'", whereFields: [])
                        }
                    }
                    
                    if (application.applicationState != UIApplicationState.Active) {
                        //userInfo["eventObjectId"] as! String
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let alertVC = storyboard.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
                        
                        alertVC.haveData = true
                        alertVC.alertEventObjectId = userInfo["eventObjectId"] as! String
                        alertVC.alertNotificationType = "eventdataupdated"
                        alertVC.senderObjectId = currentUserId
                        
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(alertVC, animated: false)
                        
                        //alertVC
                    }
                    
                }
                
                if notifType == "invitation"
                {
                    if !checkExistance("objectId", objectId: userInfo["objectId"] as! String, tableName: "Invitations")
                    {
                        var tblFields: Dictionary! = [String: String]()
                        
                        if userInfo["isApproved"] as! String == "true"
                        {
                            tblFields["isApproved"] = "1"
                        }
                        else
                        {
                            tblFields["isApproved"] = "0"
                        }
                        if let _ = userInfo["invitedName"] as? String
                        {
                            
                        }
                        else
                        {
                            tblFields["invitedName"] = ""
                        }
                        
                        if userInfo["isUpdated"] as! String == "true"
                        {
                            tblFields["isUpdated"] = "1"
                        }
                        else
                        {
                            tblFields["isUpdated"] = "0"
                        }
                        
                        if userInfo["isEventUpdated"] as! String == "true"
                        {
                            tblFields["isEventUpdated"] = "1"
                        }
                        else
                        {
                            tblFields["isEventUpdated"] = "0"
                        }
                        
                        tblFields["objectId"] = userInfo["objectId"] as? String
                        tblFields["invitedName"] = userInfo["invitedName"] as? String
                        
                        tblFields["userObjectId"] = userInfo["userObjectId"] as? String
                        
                        tblFields["eventObjectId"] = userInfo["eventObjectId"] as? String
                        
                        tblFields["emailId"] = userInfo["emailId"] as? String
                        tblFields["attendingStatus"] = userInfo["attendingStatus"] as? String
                        
                        tblFields["invitationType"] = userInfo["invitationType"] as? String
                        
                        tblFields["noOfChilds"] = userInfo["noOfChilds"] as? String
                        
                        tblFields["noOfAdults"] = userInfo["noOfAdults"] as? String
                        
                        tblFields["invitationNote"] = userInfo["invitationNote"] as? String
                        
                        tblFields["needsContentApprovel"] = "0"
                        tblFields["createdAt"] = userInfo["createdAt"] as? String
                        
                        tblFields["updatedAt"] = userInfo["updatedAt"] as? String
                        
                        tblFields["isPosted"] = "1"
                        
                        
                        _ = ModelManager.instance.addTableData("Invitations", primaryKey: "invitationId", tblFields: tblFields)
                    }
                    
                    if (application.applicationState != UIApplicationState.Active) {
                        //userInfo["eventObjectId"] as! String
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let alertVC = storyboard.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
                        
                        alertVC.haveData = true
                        alertVC.alertEventObjectId = userInfo["eventObjectId"] as! String
                        alertVC.alertNotificationType = "invitation"
                        alertVC.senderObjectId = currentUserId
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(alertVC, animated: false)
                        
                        //alertVC
                    }
                    
                }
                
                if notifType == "streamaccesschanged"
                {
                    if (application.applicationState == UIApplicationState.Active)
                    {
                        var tblFields: Dictionary! = [String: String]()
                        
                        if userInfo["isApproved"] as! String == "true"
                        {
                            tblFields["isApproved"] = "1"
                        }
                        else
                        {
                            tblFields["isApproved"] = "0"
                        }
                                                if userInfo["isEventUpdated"] as! String == "true"
                        {
                            tblFields["isEventUpdated"] = "1"
                        }
                        else
                        {
                            tblFields["isEventUpdated"] = "0"
                        }
                        

                        
                        let eventObjectId: String = userInfo["eventObjectId"] as! String
                        
                        ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "WHERE eventObjectId='\(eventObjectId)' AND userObjectId='\(currentUserId)'", whereFields: [])
                    }
                    
                    if (application.applicationState != UIApplicationState.Active) {
                        //userInfo["eventObjectId"] as! String
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let alertVC = storyboard.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
                        
                        alertVC.haveData = true
                        alertVC.alertEventObjectId = userInfo["eventObjectId"] as! String
                        alertVC.alertNotificationType = "streamaccesschanged"
                        alertVC.senderObjectId = currentUserId
                        
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(alertVC, animated: false)
                        
                        //alertVC
                    }
                    
                }
                
                if notifType == "invitationresponse"
                {
                    if (application.applicationState == UIApplicationState.Active)
                    {
                        var tblFields: Dictionary! = [String: String]()
                        
                        if userInfo["isUpdated"] as! String == "true"
                        {
                            tblFields["isUpdated"] = "1"
                        }
                        else
                        {
                            tblFields["isUpdated"] = "0"
                        }
                        if let _ = userInfo["invitedName"] as? String
                        {
                            
                        }
                        else
                        {
                            tblFields["invitedName"] = ""
                        }

                        tblFields["objectId"] = userInfo["objectId"] as? String
                        tblFields["invitedName"] = userInfo["invitedName"] as? String
                        
                        tblFields["userObjectId"] = userInfo["userObjectId"] as? String
                        
                        tblFields["eventObjectId"] = userInfo["eventObjectId"] as? String
                        
                        tblFields["attendingStatus"] = userInfo["attendingStatus"] as? String
                        
                        tblFields["noOfChilds"] = userInfo["noOfChilds"] as? String
                        
                        tblFields["noOfAdults"] = userInfo["noOfAdults"] as? String
                        
                        tblFields["invitationNote"] = userInfo["invitationNote"] as? String
                        
                        // tblFields["createdAt"] = userInfo["createdAt"] as! String
                        //tblFields["updatedAt"] = userInfo["updatedAt"] as! String
                        
                        let objectId: String = userInfo["objectId"] as! String
                        
                        _ = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "WHERE objectId='\(objectId)'", whereFields: [])
                    }
                    
                    if (application.applicationState != UIApplicationState.Active) {
                        //userInfo["eventObjectId"] as! String
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let alertVC = storyboard.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
                        
                        alertVC.haveData = true
                        alertVC.alertEventObjectId = userInfo["eventObjectId"] as! String
                        alertVC.alertNotificationType = "invitationresponse"
                        alertVC.senderObjectId = userInfo["userObjectId"] as! String
                        
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(alertVC, animated: false)
                        
                        //alertVC
                    }
                    
                }
                
                if notifType == "likedpost"
                {
                    
                    if !checkExistance("objectId", objectId: userInfo["objectId"] as! String, tableName: "PostLikes")
                    {
                        var tblFields: Dictionary! = [String: String]()
                        
                        if userInfo["isUpdated"] as! String == "true"
                        {
                            tblFields["isUpdated"] = "1"
                        }
                        else
                        {
                            tblFields["isUpdated"] = "0"
                        }

                        tblFields["eventObjectId"] = userInfo["eventObjectId"] as? String

                        tblFields["userObjectId"] = userInfo["userObjectId"] as? String

                        //tblFields["postObjectId"] = userInfo["postObjectId"] as! String

                        tblFields["objectId"] = userInfo["objectId"] as? String

                        _ = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
                    }
                    else
                    {
                        if (application.applicationState == UIApplicationState.Active)
                        {
                            var tblFields: Dictionary! = [String: String]()
                            
                            if userInfo["isUpdated"] as! String == "true"
                            {
                                tblFields["isUpdated"] = "1"
                            }
                            else
                            {
                                tblFields["isUpdated"] = "0"
                            }

                            tblFields["eventObjectId"] = userInfo["eventObjectId"] as? String
                            tblFields["userObjectId"] = userInfo["userObjectId"] as? String
                            //tblFields["postObjectId"] = userInfo["postObjectId"] as! String
                            tblFields["objectId"] = userInfo["objectId"] as? String

                            let objectId = userInfo["objectId"] as! String
                            
                            _ = ModelManager.instance.updateTableData("PostLikes", tblFields: tblFields, whereString: "WHERE objectId='\(objectId)'", whereFields: [])
                        }
                    }
                    
                    if (application.applicationState != UIApplicationState.Active) {
                        //userInfo["eventObjectId"] as! String
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        
                        let alertVC = storyboard.instantiateViewControllerWithIdentifier("AlertsViewController") as! AlertsViewController
                        
                        alertVC.haveData = true
                        alertVC.alertEventObjectId = userInfo["eventObjectId"] as! String
                        alertVC.alertNotificationType = "likedpost"
                        alertVC.senderObjectId = userInfo["userObjectId"] as! String
                        
                        let navigationController = self.window?.rootViewController as! UINavigationController
                        
                        navigationController.pushViewController(alertVC, animated: false)
                        
                       
                    }
                    
                }
                
            }
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        // Initialize mixpanel device notifications
        let mixpanel = Mixpanel.sharedInstance();
        mixpanel.people.addPushDeviceToken(deviceToken)

        // Store the deviceToken in the current Installation and save it to Parse
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackground()
        NSUserDefaults.standardUserDefaults().setObject(deviceToken, forKey: "deviceToken")
        print(NSUserDefaults.standardUserDefaults().setObject(deviceToken, forKey: "deviceToken"))
        print(deviceToken)
        // Initialize Intercom device push notifications
        Intercom.setDeviceToken(deviceToken)
    }
    
    
    func applicationDidBecomeActive(application: UIApplication) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
//resetBadge()
        downloadData()
        downloadInvitation()
        //wakeUpImageView.hidden = true
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication)
    {
          UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        //resetBadge()
        _ = ModelManager.instance.deleteTableData("Events", whereString: "objectId=?", whereFields: [""])
        
    }
    
    
    //MARK: - stringToDate()
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        
        
        let date = dateFormatter.dateFromString(dateString)
        
        
        return date!
    }
    func downloadInvitation()
    {
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            let currentUserrIdd = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "userObjectId = '\(currentUserrIdd)' ORDER BY invitationId DESC", whereFields: [])
        
        var invitationObjectIds: Array<String>
        
        invitationObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                invitationObjectIds.append(resultSet.stringForColumn("eventObjectId"))
            }
        }
        
        resultSet.close()
        
        let invitationObjectIdsString = invitationObjectIds.joinWithSeparator("','")
        
        
        
        var predicate = NSPredicate()
        
        var eventObjectIdsStringPredicate = ""
        
        
        eventObjectIdsStringPredicate = "(eventObjectId IN {'\(invitationObjectIdsString)'})"
        
        
        
        
        predicate = NSPredicate(format: eventObjectIdsStringPredicate)
        
        let query = PFQuery(className:"EventComments", predicate: predicate)
        
        query.orderByDescending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagesError:", errorSelectorParameters:nil)
        
        
           }
    }
    func downloadData()
    {
       if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
       {
        let currentUserrId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "eventCreatorObjectId = '\(currentUserrId)' ORDER BY eventId ASC", whereFields: [])
        
        var eventObjectIds: Array<String>
        
        eventObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                eventObjectIds.append(resultSet.stringForColumn("objectId"))
                
                
            }
        }
        
        resultSet.close()
        
        let eventObjectIdsString = eventObjectIds.joinWithSeparator("','")
        
        var predicate = NSPredicate()
        
        var eventObjectIdsStringPredicate = ""
        
        
        eventObjectIdsStringPredicate = "(eventObjectId IN {'\(eventObjectIdsString)'})"
        
        
        
        
        predicate = NSPredicate(format: eventObjectIdsStringPredicate)
        
        let query = PFQuery(className:"EventComments", predicate: predicate)
        
        query.orderByDescending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllinvitationMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllinvitationMessagesError:", errorSelectorParameters:nil)
        }
       
    }
    func fetchAllMessagesSuccess(timer:NSTimer)
    {
        var objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                if !checkExistance("objectId", objectId: message.objectId!, tableName: "EventComments")
                {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = message.objectId!
                tblFields["messageText"] = message["messageText"] as? String
                tblFields["senderObjectId"] = message["senderObjectId"] as? String
                tblFields["eventObjectId"] = message["eventObjectId"] as? String
                tblFields["senderName"] = message["senderName"] as? String
                //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                
                
                
                var date = ""
                
                if message.createdAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.createdAt)!)
              
                    tblFields["createdAt"] = date
                }
                
                if message.updatedAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.updatedAt)!)
            
                    tblFields["updatetAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                
                if isChatMode == true
                {
                    tblFields["isRead"] = "1"
                }
                else
                {
                    tblFields["isRead"] = "0"
                }
                
                tblFields["timezoneOffset"] = "\(NSTimeZone.localTimeZone().secondsFromGMT)"
                
               /* var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["COUNT(*) as count"], whereString: "objectId = '\(message.objectId!)'", whereFields: [])
                
                resultSet.next()
                
                var messageCount = Int(resultSet.intForColumn("count"))
                
                resultSet.close()
                */
//                if messageCount == 0
//                {
                    var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                //}
                
                fetchedEventObjectIds.append(message["eventObjectId"] as! String)
            }
            }
        }
    }
    
    
    
    func fetchAllMessagesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
       
    }
        
    func fetchAllinvitationMessagesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        if let fetchedobjects = objects {
            
         //   var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                if !checkExistance("objectId", objectId: message.objectId!, tableName: "EventComments")
                {
                    var tblFields: Dictionary! = [String: String]()
                    
                    tblFields["objectId"] = message.objectId!
                    tblFields["messageText"] = message["messageText"] as? String
                    tblFields["senderObjectId"] = message["senderObjectId"] as? String
                    tblFields["eventObjectId"] = message["eventObjectId"] as? String
                    tblFields["senderName"] = message["senderName"] as? String
                    //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                    
                    
                    
                    var date = ""
                    
                    if message.createdAt != nil
                    {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((message.createdAt)!)
                        //println(date)
                        tblFields["createdAt"] = date
                    }
                    
                    if message.updatedAt != nil
                    {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((message.updatedAt)!)
                        //println(date)
                        tblFields["updatetAt"] = date
                    }
                    
                    tblFields["isPosted"] = "1"
                    
                    if isChatMode == true
                    {
                        tblFields["isRead"] = "1"
                    }
                    else
                    {
                        tblFields["isRead"] = "0"
                    }
                    
                    tblFields["timezoneOffset"] = "\(NSTimeZone.localTimeZone().secondsFromGMT)"
                    
                    /* var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["COUNT(*) as count"], whereString: "objectId = '\(message.objectId!)'", whereFields: [])
                    
                    resultSet.next()
                    
                    var messageCount = Int(resultSet.intForColumn("count"))
                    
                    resultSet.close()
                    */
                    //                if messageCount == 0
                    //                {
                    var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                    //}
                    
                    fetchedEventObjectIds.append(message["eventObjectId"] as! String)
                }
            }
        }
    }
    
    
    
    func fetchAllinvitationMessagesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //println("Error: \(error) \(error.userInfo!)")
}

    func checkExistance(objectIdColumn: String, objectId: String, tableName: String)->Bool
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData(tableName, selectColumns: ["count(*) as count"], whereString: "\(objectIdColumn) = '\(objectId)'", whereFields: [])
        
        resultSet.next()
        
        let noOfRows = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        return (noOfRows > 0)
    }
    
    
    
}

