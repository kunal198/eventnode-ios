//
//  InviteFriendsFirstViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import AddressBookUI
import AddressBook
//import PermissionScope

class InviteFriendsFirstViewController: UIViewController {

    @IBOutlet weak var blackView: UIView!

    @IBOutlet weak var getNotificationButton: UIButton!
    @IBOutlet weak var pushPopUpView: UIView!
    @IBOutlet weak var inviteURLWrapper: UIView!
    @IBOutlet weak var inviteTextView: UITextView!
    @IBOutlet var textView : UITextView!

//    let multiPscope = PermissionScope()
//    let noUIPscope = PermissionScope()
//    let pscope = PermissionScope()
    
    var currentUserId: String!
    
    var isFromCreated = false
    var isFromChannel: Bool!
    var isApproved: Bool!
    var isRSVP = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.pushPopUpView.frame.origin.y = -250.0
        getNotificationButton.layer.borderWidth = 2.0
        getNotificationButton.layer.borderColor = UIColor(red: 68.0/255.0, green: 185.0/255.0, blue: 227.0/255.0, alpha: 1.0).CGColor
        getNotificationButton.layer.cornerRadius = 2.0
        getNotificationButton.layer.masksToBounds = true
        
        
        pushPopUpView.layer.cornerRadius = 5.0
        pushPopUpView.layer.masksToBounds = true
        
        
        if let settings = UIApplication.sharedApplication().currentUserNotificationSettings()
        {
            if settings.types.contains([.Alert, .Sound, .Badge])
            {
                pushPopUpView.hidden = true
                blackView.hidden = true
                print("alerts")
            }
            else
            {
                pushPopUpView.hidden = false
                blackView.hidden = false
                self.pushPopUp()
            }
            
        }
        
        self.view.addSubview(wakeUpImageView)
        inviteTextView.textAlignment = .Center
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        isApproved = true
        
        
        
        // Do any additional setup after loading the view.
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        textView.attributedText = NSAttributedString(string: textView.text, attributes:attributes)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
        textView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        
        
        
       
        
        inviteTextView.attributedText = NSAttributedString(string: inviteTextView.text, attributes:attributes)
        inviteTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        inviteTextView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
        inviteTextView.textAlignment = .Center
        //invitationCode.text = currentEvent["socialSharingURL"] as? String
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func pushPopUp()
    {
        
        
        UIView.animateWithDuration(1.0) { () -> Void in
            self.pushPopUpView.frame.origin.y = self.textView.frame.origin.y + self.textView.frame.size.height
        }
        
        
    }

    @IBAction func getNotifiedButton(sender: AnyObject)
    {
        
        UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
        //
        let userNotificationTypes: UIUserNotificationType = ([UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound]);
        
        
        let settings = UIUserNotificationSettings(forTypes: userNotificationTypes, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
        pushPopUpView.hidden = true
        blackView.hidden = true
    }
    
    @IBAction func closePopUpButton(sender: AnyObject)
    {
        pushPopUpView.hidden = true
        blackView.hidden = true
    }
    
    @IBAction func viewTapped(sender : AnyObject)
    {

    }
    
    @IBAction func fetchFacebookFriends(sender: UIButton) {
        
        if isFacebookLogin
        {
            let inviteFbVC = self.storyboard!.instantiateViewControllerWithIdentifier("InviteFacebookFriendsViewController") as! InviteFacebookFriendsViewController
            self.navigationController?.pushViewController(inviteFbVC, animated: false)
        }   
        else
        {
            showLinkfacebookView = true
            
            let ConnectFacebook = self.storyboard!.instantiateViewControllerWithIdentifier("connectFacebookAccount") as! facebookLinkedViewController
            
            ConnectFacebook.isInviteView = true
            
            self.navigationController?.pushViewController(ConnectFacebook, animated: false)
            
        }
    }

  /* rahul @IBAction func fetchEmailContacts(sender: UIButton) {
        //getAddressBookNames()
        
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        {
            NSLog("requesting access...")
            let emptyDictionary: CFDictionaryRef?
            
            let addressBook = !(ABAddressBookCreateWithOptions(emptyDictionary, nil) != nil)
            /*ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
                if success {
                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
                    self.navigationController?.pushViewController(homeVC, animated: false)
                }
                else {
                    NSLog("unable to request access")
                }
            })*/
            
            ABAddressBookRequestAccessWithCompletion(addressBook) {
                (granted: Bool, error: CFError!) in
                dispatch_async(dispatch_get_main_queue()) {
                    if !granted {
                        //println("Just denied")
                        
                        let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide Eventnode with access to your contacts. Go to Settings > Privacy > Contacts and enable Eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                            
                        }))
                        
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                        
                    } else {
                        ////println("Just authorized")
                        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
                        self.navigationController?.pushViewController(homeVC, animated: false)

                    }
                }
            }

            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
            NSLog("access denied")
            
            let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide Eventnode with access to your contacts. Go to Settings > Privacy > Contacts and enable Eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            
        }
        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }*/
    @IBAction func fetchEmailContacts(sender: UIButton) {
        
        
        if #available(iOS 9.0, *)
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
            
            homeVC.isRSVP = "true"
            
            if isApproved == true
            {
                homeVC.isApproved = "true"
            }
            else
            {
                homeVC.isApproved = "true"
            }
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
        else
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SerchAndInviteUsingABAddressBookViewController") as! SerchAndInviteUsingABAddressBookViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
        
        //getAddressBookNames()
        
        //        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        //        if (authorizationStatus == ABAuthorizationStatus.NotDetermined)
        //        {
        //            NSLog("requesting access...")
        //            var error : Unmanaged<CFError>? = nil
        //            let addressBook : ABAddressBook? = ABAddressBookCreateWithOptions(nil, &error).takeRetainedValue()
        //
        //            if addressBook == nil {
        //                print(error)
        //                return
        //            }
        //            /*ABAddressBookRequestAccessWithCompletion(addressBook,{success, error in
        //                if success {
        //                    let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
        //                    self.navigationController?.pushViewController(homeVC, animated: false)
        //                }
        //                else {
        //                    NSLog("unable to request access")
        //                }
        //            })*/
        //
        //            ABAddressBookRequestAccessWithCompletion(addressBook) {
        //                (granted: Bool, error: CFError!) in
        //                dispatch_async(dispatch_get_main_queue()) {
        //                    if !granted {
        //                        print("Just denied")
        //
        //                        let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
        //
        //                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
        //
        //                        }))
        //
        //                        self.presentViewController(refreshAlert, animated: true, completion: nil)
        //
        //                    } else {
        //                        //println("Just authorized")
        //                        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
        //                        self.navigationController?.pushViewController(homeVC, animated: false)
        //
        //                    }
        //                }
        //            }
        //
        //
        //        }
        //        else if (authorizationStatus == ABAuthorizationStatus.Denied || authorizationStatus == ABAuthorizationStatus.Restricted) {
        //            NSLog("access denied")
        //
        //            let refreshAlert = UIAlertController(title: "Provide Access", message: "Provide eventnode with access to your contacts. Go to Settings > Privacy > Contacts in iOS8 and enable eventnode.", preferredStyle: UIAlertControllerStyle.Alert)
        //
        //            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
        //
        //            }))
        //
        //            self.presentViewController(refreshAlert, animated: true, completion: nil)
        //            
        //        }
        //        else if (authorizationStatus == ABAuthorizationStatus.Authorized) {
        //            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("SearchAndInviteContactsViewController") as! SearchAndInviteContactsViewController
        //            self.navigationController?.pushViewController(homeVC, animated: false)
        //        }
    }
    
    
    @IBAction func closeAdjustPhotoButtonClicked(sender : AnyObject){
        
        if isFromCreated == true
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            homeVC.redirect = true
            homeVC.redirectFromInviteView = true
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
        else
        {
            
            self.navigationController?.popViewControllerAnimated(false)
        }
            
            
            
       

    }

    @IBAction func toggleAccess(sender: UIButton) {
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        if isApproved == false
        {
            isApproved = true
            sender.setImage(UIImage(named: "check-box.png"), forState: .Normal)
            
            inviteTextView.attributedText = NSAttributedString(string: "Anyone with this link can join the event and get automatic access to photos & videos", attributes:attributes)
            inviteTextView.textAlignment = .Center
            inviteTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
            inviteTextView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            
        }
        else
        {
            isApproved = false
            sender.setImage(UIImage(named: "checkbox.png"), forState: .Normal)
            
            inviteTextView.attributedText = NSAttributedString(string: "Anyone who joins with this link will have access to the event invitation but not the photos & videos until you manually approve them", attributes:attributes)
            inviteTextView.textAlignment = .Center
            inviteTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
            inviteTextView.textColor = UIColor(red: 74.0/255, green: 74.0/255, blue: 74.0/255, alpha: 1.0)
            
        }
    }
    
    @IBAction func socialShareButton(sender: AnyObject)
    {
        
        let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS9 = iosVersion >= 9
        let iOS7 = iosVersion >= 8 && iosVersion < 9
        
        if iOS9
        {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            branchUniversalObject.title = "\(eventTitle)"
            branchUniversalObject.contentDescription = "My Content Description"
            // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
            branchUniversalObject.addMetadataKey("eventObjectId", value: currentEvent.objectId!)
            branchUniversalObject.addMetadataKey("emailId", value: "noemail")
            branchUniversalObject.addMetadataKey("isApproved", value:"true")
            branchUniversalObject.addMetadataKey("isSocialSharing", value:"true")
            branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId!)")
            branchUniversalObject.addMetadataKey("isRSVP", value: "true")
            
            var isAccess = "YWNjZXNzPTE"
            
            if isApproved == false
            {
                branchUniversalObject.addMetadataKey("isApproved", value:"false")
                isAccess = "YWNjZXNzPTA"
            }
            else
            {
                branchUniversalObject.addMetadataKey("isApproved", value:"true")
            }
            
            
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/view_event/\(currentEvent.objectId!)?\(isAccess)")
            linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/view_event/\(currentEvent.objectId!)?\(isAccess)")
            linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/view_event/\(currentEvent.objectId!)?\(isAccess)")
            
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        let urlLink = url!
                        
                        self.generatelink(urlLink)
                    }
                })
        }
        else
        {
            var data = [
                "eventObjectId": currentEvent.objectId!,
                "emailId": "noemail",
                "eventCreatorId": "\(currentUserId!)",
                "isApproved": "true",
                "isSocialSharing": "true",
                "isRSVP" : "true"
                
            ]
            
            
            if isApproved == false
            {
                data["isApproved"] = "false"
            }
            
            
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                    //println(url!)
                    
                    let urlLink = url!
                    
                    self.generatelink(urlLink)
                    //
                    //                let plainData = (currentEvent.objectId! as NSString).dataUsingEncoding(NSUTF8StringEncoding)
                    //                let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                    //                //println(base64String)
                    //
                    //                var branchUrl = url! as String
                    //
                    //                let plainDataUrl = (branchUrl as NSString).dataUsingEncoding(NSUTF8StringEncoding)
                    //
                    //                let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
                    //                //println(base64UrlString)
                    //
                    //                var urlString = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId=\(base64String)&url=\(base64UrlString)"
                    //
                    //                var eventTitle = currentEvent["eventTitle"] as! String
                    //
                    //                var objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(urlString)"
                    //
                    //                let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
                    //                self.navigationController!.presentViewController(activityVC,
                    //                    animated: true,
                    //                    completion: nil)
                    
                }
                
            })

        }
        
    }
  
    func generatelink(urlLink:String)
    {
        let plainData = (currentEvent.objectId! as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //println(base64String)
        
        let branchUrl = urlLink 
        
//        let plainDataUrl = (branchUrl as NSString).dataUsingEncoding(NSUTF8StringEncoding)
//        
//        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
//        //println(base64UrlString)
//        
//        let urlString = "http://web.eventnode.co/index.php/event/deeplinkredirect?eventObjectId=\(base64String)&url=\(branchUrl)"
        
        let eventTitle = currentEvent["eventTitle"] as! String
        
        let objectsToShare = "Hi, I like to invite you to “\(eventTitle)”. You can respond to this by clicking on this link. \(branchUrl)"
        
        let activityVC = UIActivityViewController(activityItems:[objectsToShare] , applicationActivities: nil)
        self.navigationController!.presentViewController(activityVC,
            animated: true,
            completion: nil)
        
    }

    
}
