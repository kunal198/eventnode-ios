//
//  MyPlacesSearchCell.swift
//  eventnode
//
//  Created by mrinal khullar on 6/3/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class MyPlacesSearchCell: UITableViewCell {

    @IBOutlet weak var locationTitle: UILabel!
    
    @IBOutlet weak var eventFocusLocation: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
