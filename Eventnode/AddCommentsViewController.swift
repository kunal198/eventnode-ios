//
//  AddCommentsViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 8/22/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit

class AddCommentsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate
{

    var chatsArray = [PFObject]();
    var messageRowHeights = [CGFloat]()
     var pricediff = NSArray()
    var currentUserId = ""
    
    var messageIds = [String]()
    
    var senderName = ["fhn","aman","dsd","zfgdcfd","zfddzx","abhi"]
    
    @IBOutlet weak var lblsubTitle: UILabel!
    //var senderId = ["vbnbj","cWKoulXs4G","h5RUuNIIJi","cWKoulXs4G","jhjmn","cWKoulXs4G"]
    
    var eventObject: PFObject!
    var isShared: Bool!
    var isRSVP : Bool!
    var indexcheck : Bool!

    
    var profileImage = ["girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg","girl.jpeg"]
    
    var isrsvp = ""
    
    @IBOutlet weak var wrapperViewChat: UIView!
    @IBOutlet weak var addCommentTableView: UITableView!
    
    var wrapperViewChatY: CGFloat!
    var addCommentTableViewHeight: CGFloat!
    
    @IBOutlet weak var chatHeadBottom: UIView!
    @IBOutlet weak var enterTextMessage: UITextView!
    
    var timer:NSTimer!
    var timer2:NSTimer!

    var textMessage:String = String()
    var messagesRef: Firebase!

    var arrdata = NSMutableArray()
    var arrdatafull = NSMutableArray()
var refreshControl = UIRefreshControl()
    var chckBool = Bool()
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x:105, y: 250, width: 100, height: 150),
        type: .BallScaleMultiple, color: UIColor(red: 220/255.0, green: 203/255.0, blue: 85/255.0, alpha: 1.0), size: CGSize(width: 100, height: 100))
var indextotal = UInt()
    //MARK: - viewDidLoad()
    
    func setupFirebase() {

            self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/GroupChat/\(currentChatEventObjectId)")
        
            //messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/GroupChat/vvTLEbWqnd")
            
            self.messagesRef.queryLimitedToLast(10).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
                
                
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.value["message"] as? String, forKey: "message")
                dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")

                self.arrdata.addObject(dictemp)
                
                let arrtemp = self.arrdata
                self.arrdata = []
                
                for e in arrtemp
                {
                    if !self.arrdata.containsObject(e)
                    {
                        self.arrdata.addObject(e)
                    }
                    
                }
                
                self.addCommentTableView.reloadData()

                self.tableViewScrollToBottom(true)

//                let abc = String(format: "%.0f", self.Timestamp)
//                var tblFieldsfirebase: Dictionary! = [String: String]()
//                
//                tblFieldsfirebase["EventId"] = currentChatEventObjectId
//                tblFieldsfirebase["message"] = snapshot.value["message"] as? String
//                tblFieldsfirebase["userObjectId"] = snapshot.value["userObjectId"] as? String
//                tblFieldsfirebase["timestamp"] = abc as String
//                tblFieldsfirebase["name"] = snapshot.value["name"] as? String
//                tblFieldsfirebase["Keyvalue"] = snapshot.key

                
                
                
//                if !self.checkExistance("Keyvalue", objectId: snapshot.key as String, tableName: "Chat")
//                {
//                    
//                
//                
//                
//                
//                _ = ModelManager.instance.addTableData("Chat", primaryKey: "rowId", tblFields: tblFieldsfirebase)
//
//                }
                // self.finishReceivingMessage()
            })

                // update some UI
        
        
        
    }
    
    
    func updateFirebase() {
        
            // do some task
if arrdata.count > 0
{
    indexcheck = false
    
    var checkvalue = 0
    let count = 10
    if count > 10
    {
   // count = count + 9
    }
      let m = arrdata.valueForKey("count").objectAtIndex(0) as! Int
       var n = m-11
if m < 11
{
    n = 0
        }
   // currentChatEventObjectId = "vvTLEbWqnd"
        self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/GroupChat/\(currentChatEventObjectId)")
    
        
        self.messagesRef.queryOrderedByChild("count").queryStartingAtValue(n).queryEndingAtValue(m-1).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
            
      
            let dictemp = NSMutableDictionary()
            dictemp.setValue(snapshot.value["message"] as? String, forKey: "message")
            dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
            dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
            dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")
            
            self.arrdata.insertObject(dictemp, atIndex: 0)
            
            let arrtemp = self.arrdata
            self.arrdata = []
            
            for e in arrtemp
            {
                if !self.arrdata.containsObject(e)
                {
                    self.arrdata.addObject(e)
                }
                
            }
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "count", ascending: true)
            let sortedResults: NSArray = self.arrdata.sortedArrayUsingDescriptors([descriptor])
            self.arrdata = []
            self.loadpreviousmessage.hidden=true

self.arrdata = sortedResults.mutableCopy() as! NSMutableArray
            checkvalue = checkvalue+1
            
            if n != 0 && checkvalue == 9
            {
            self.addCommentTableView.reloadData()
          
        
                       let indexPath = NSIndexPath(forRow: count+3, inSection: 0)
            self.addCommentTableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .None, animated: false)
//            CGPoint point = theTableView.contentOffset;
//            point .y -= theTableView.rowHeight;
//            theTableView.contentOffset = point;
            
            var point = self.addCommentTableView.contentOffset
            point.y -= self.addCommentTableView.rowHeight
            self.addCommentTableView.contentOffset = point
            }
            else  if  n == 0 && checkvalue >= m-1
            {
                self.addCommentTableView.reloadData()

                let indexPath = NSIndexPath(forRow: m, inSection: 0)
                self.addCommentTableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .None, animated: false)
               
                
                var point = self.addCommentTableView.contentOffset
                point.y -= self.addCommentTableView.rowHeight
                self.addCommentTableView.contentOffset = point
            }
        })
        

             }
    }
    func senditMessage(text: String!, sender: String!) {
        
        let abc = String(format: "%.0f", Timestamp)
        // *** STEP 3: ADD A MESSAGE TO FIREBASE
       // sendMessageparse(text)
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/GroupChat/\(currentChatEventObjectId)")
        var m = Int()
        if arrdata.count < 1
        {
        m = 0
        }
        else{
        m = self.arrdata.valueForKey("count").objectAtIndex(self.arrdata.count - 1) as! Int
        }
        messagesRef.childByAutoId().setValue([
            "message":text,
            "name":NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String,
            "userObjectId":self.currentUserId,
            "timestamp" :abc,
            "count" : m+1,
            "isRSVP" : isrsvp
        
            
            ])
        tableViewScrollToBottom(true)
        
    }
    
    var Timestamp: NSTimeInterval {
        return NSDate().timeIntervalSince1970 * 1000
    }
    
    override func viewDidAppear(animated: Bool) {
//        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentChatEventObjectId)/\(self.currentUserId)")
//        
//        messagesRef.removeValue()
 //fetchfromlocal()
        //self.tableViewScrollToBottom(true)

        var messageIdsString = ""
        if messageIds.count > 0
        {
            messageIdsString = messageIds.joinWithSeparator("','")
            
        }
        else
        {
           // enterTextMessage.becomeFirstResponder()
        }
        
        var tblFields: Dictionary! = [String: String]()
        
        
        tblFields["isRead"] = "1"
        
        
        _ = ModelManager.instance.updateTableData("EventComments", tblFields: tblFields, whereString: "eventObjectId='\(eventObject.objectId!)'", whereFields: [])
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(messageIdsString)'}) AND eventObjectId = '\(eventObject.objectId!)'")
        
        
        let query = PFQuery(className:"EventComments", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
       // ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagesError:", errorSelectorParameters:nil)
        
        
      //  timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("reloadTable"), userInfo: nil, repeats: true)
        
    }
    override func viewWillAppear(animated: Bool) {
       
setupFirebase()
        //updateFirebase()
        addCommentTableView.scrollsToTop = true
        

    }
    override func viewWillDisappear(animated: Bool) {
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentChatEventObjectId)/\(self.currentUserId)")
        
        messagesRef.removeValue()
        
//        messagesRef.removeAllObservers()
        
        activityIndicatorView.stopAnimation()

    }
    override func viewDidLoad()
    {
       
        super.viewDidLoad()
        indextotal = 20
        indexcheck = false
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "")
        self.refreshControl.addTarget(self, action: "checkforreload", forControlEvents: UIControlEvents.ValueChanged)
       // self.addCommentTableView?.addSubview(refreshControl)
       
        activityIndicatorView.center = CGPointMake(view.frame.width/2, view.frame.height/2)
        
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimation()
        
        activityIndicatorView.hidesWhenStopped = true
        if let currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as? String
        {
            self.currentUserId = currentUserId
            
        }

       
        

        isChatMode = true
        
        
        addCommentTableView.separatorColor = UIColor.clearColor()
        wrapperViewChatY = wrapperViewChat.frame.origin.y
        addCommentTableViewHeight = addCommentTableView.frame.height
        
        self.view.addSubview(wakeUpImageView)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
        
        addCommentTableView.separatorColor = UIColor.clearColor()
        
        lblsubTitle.font = UIFont(name: "AvenirNext-Medium", size:10.0)
        
        
        if isRSVP == true
        {
           // eventObject = currentSharedEvent
            isrsvp = "true"
            lblsubTitle.text = "with Event Host & Invitees"
        }
        else
        {
            isrsvp = "false"

            lblsubTitle.text = "with all Channel Members"
            //eventObject = currentEvent
            
        }
        
        if isShared == true
        {
            eventObject = currentSharedEvent
            
           // lblsubTitle.text = "With Event Host & Invitees"
        }
        else
        {
           // lblsubTitle.text = "With all Channel Members"
            eventObject = currentEvent

        }
        
        currentChatEventObjectId = eventObject.objectId!
       
        
        

        //com.eventnode.iospush
        
       
        
        
        //        for (var i = 0; i < senderName.count; i++)
        //        {
        //            var chat = PFObject(className: "EventComments")
        //
        //            chat["senderName"] = senderName[i]
        //
        //            chat["senderId"] = senderId[i]
        //
        //            chat["senderMessage"] = senderMessage[i]
        //
        //            chat["profileImage"] = profileImage[i]
        //
        //            chatsArray.append(chat)
        //        }
        
        //addCommentTableView.reloadData()
        
        
       //rahul refreshList()
        
        if addCommentTableView.contentSize.height > addCommentTableView.frame.height
        {
            addCommentTableView.contentOffset.y = addCommentTableView.contentSize.height - addCommentTableView.frame.height
        }
        
        
        
        
    }
    
    
    
    //MARK: - didReceiveMemoryWarning()
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchfromlocal()
    {
        
        
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            // do some task
            let temp = self.arrdata.valueForKey("count").objectAtIndex(0) as! Int
            
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("Chat", selectColumns: ["*"], whereString: "EventId = '\(currentChatEventObjectId)' limit 10 OFFSET \(temp-11)", whereFields: [])
            
                        dispatch_async(dispatch_get_main_queue()) {
                            if resultSet != nil
                            {
                                while resultSet.next()
                                {
                                    
                                    let dic = NSMutableDictionary()
                                    dic.setValue(resultSet.stringForColumn("message"), forKey: "message")
                                    dic.setValue(resultSet.stringForColumn("name"), forKey: "name")
                                    dic.setValue(resultSet.stringForColumn("timestamp"), forKey: "timestamp")
                                    dic.setValue(resultSet.stringForColumn("userObjectId"), forKey: "userObjectId")
                                    self.arrdata.addObject(dic)
                                    self.addCommentTableView.reloadData()
                                    
                                }
                            }
                            resultSet.close()

                // update some UI
            }
        }

        
        
   
    
    }
    func reloadTable()
    {
        //refreshList()
        
        if isChatUpdated == true
        {
            isChatUpdated = false
            
           //rahul refreshList()
            /*addCommentTableView.reloadData()*/
            
            /*if addCommentTableView.contentOffset.y < 10.0 && addCommentTableView.contentOffset.y > -10.0
            {*/
            if addCommentTableView.contentSize.height > addCommentTableView.frame.height
            {
                addCommentTableView.contentOffset.y = addCommentTableView.contentSize.height - addCommentTableView.frame.height
            }
            //}
            
        }
    }
    
    
    func fetchAllMessagesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        if let fetchedobjects = objects {
            
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = message.objectId!
                tblFields["messageText"] = message["messageText"] as? String
                tblFields["senderObjectId"] = message["senderObjectId"] as? String
                tblFields["eventObjectId"] = message["eventObjectId"] as? String
                tblFields["senderName"] = message["senderName"] as? String
                //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                
                
                
                var date = ""
                
                if message.createdAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.createdAt)!)
                    tblFields["createdAt"] = date
                }
                
                if message.updatedAt != nil
                {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((message.updatedAt)!)
                    tblFields["updatetAt"] = date
                }
                
                tblFields["isPosted"] = "1"
                
                if isChatMode == true
                {
                    tblFields["isRead"] = "1"
                }
                else
                {
                    tblFields["isRead"] = "0"
                }

                tblFields["timezoneOffset"] = "\(NSTimeZone.localTimeZone().secondsFromGMT)"

                let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["COUNT(*) as count"], whereString: "objectId = '\(message.objectId!)'", whereFields: [])

                resultSet.next()

                let messageCount = Int(resultSet.intForColumn("count"))

                resultSet.close()

                if messageCount == 0
                {
                    let insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                }

                fetchedEventObjectIds.append(message["eventObjectId"] as! String)
            }
         //rahul   refreshList()
        }
    }
    
    
    
    func fetchAllMessagesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    //MARK: - sendMessage()
    @IBAction func sendMessage(sender: AnyObject)
    {
        let messageEntered:NSString = self.enterTextMessage.text
        
        if (messageEntered.length > 0)
        {
            textMessage = self.enterTextMessage.text!
            
            senditMessage(textMessage, sender: "")
            
            self.enterTextMessage.text = ""
            
            let chat = PFObject(className: "EventComments")
            
            
            chat["senderObjectId"] = currentUserId
            
            chat["eventObjectId"] = eventObject.objectId!
            
            chat["messageText"] = "\(textMessage)"
            
            chat["profileImage"] = "girl.jpeg"
            
            let senderName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            
            chat["senderName"] = senderName
            
            chat["timeString"] = "just now"
            
            let senderMessageTemp = UITextView()
            
            senderMessageTemp.frame.size.width = self.view.frame.width*(215.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(45.0/568)
            
            senderMessageTemp.text = chat["messageText"] as! String
            
            senderMessageTemp.textAlignment = .Right
            
            senderMessageTemp.font = UIFont(name: "AvenirNext-Medium", size: 13)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            
            var rowHeight: CGFloat = 0
            
            /*if contentSize.height > self.view.frame.height*(25.0/568)
            {
            rowHeight = contentSize.height - (self.view.frame.height*(25.0/568))
            }*/
            
            rowHeight = (90.0*self.view.frame.height/568) + contentSize.height
            
            //println(rowHeight)
            
            messageRowHeights.append(rowHeight)
            
            chatsArray.append(chat)
            pricediff = (chatsArray as? NSArray)!
            _ = NSSortDescriptor(key: "timeDiff", ascending: false)
           // pricediff = (pricediff as NSArray).sortedArrayUsingDescriptors([ageSortDescriptor])

          //rahul  addCommentTableView.reloadData()
            //adjustTableHeight()
            //adjustTableY()
            if addCommentTableView.contentSize.height > addCommentTableView.frame.height
            {
                addCommentTableView.contentOffset.y = addCommentTableView.contentSize.height - addCommentTableView.frame.height
            }
            tableViewScrollToBottom(true)
            ParseOperations.instance.saveData(chat, target: self, successSelector: "sendMessageSuccess:", successSelectorParameters: nil, errorSelector: "sendMessageError:", errorSelectorParameters:nil)
            
        }
        
    }
    
    func checkExistance(objectIdColumn: String, objectId: String, tableName: String)->Bool
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData(tableName, selectColumns: ["count(*) as count"], whereString: "\(objectIdColumn) = '\(objectId)'", whereFields: [])
        
        resultSet.next()
        
        let noOfRows = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        return (noOfRows > 0)
    }

    func sendMessageSuccess(timer: NSTimer)
    {
        let message = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = message.objectId!
        tblFields["messageText"] = message["messageText"] as? String
        tblFields["senderObjectId"] = message["senderObjectId"] as? String
        tblFields["eventObjectId"] = message["eventObjectId"] as? String
        tblFields["senderName"] = message["senderName"] as? String
        tblFields["isRead"] = "1"
        //tblFields["eventCommentId"] = message["eventCommentId"] as? String
        let abc = String(format: "%.0f", Timestamp)
     /*   var tblFieldsfirebase: Dictionary! = [String: String]()
        
        tblFieldsfirebase["EventId"] = currentChatEventObjectId
        tblFieldsfirebase["message"] = message["messageText"] as? String
        tblFieldsfirebase["userObjectId"] = message["senderObjectId"] as? String
        tblFieldsfirebase["timestamp"] = abc as String
        tblFieldsfirebase["name"] = message["senderName"] as? String

        
       
        */
        
        var date = ""
        
        if message.createdAt != nil
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((message.createdAt)!)
            tblFields["createdAt"] = date
           // tblFieldsfirebase["createdAt"] = date

        }
        
        if message.updatedAt != nil
        {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((message.updatedAt)!)
            tblFields["updatetAt"] = date
            //tblFieldsfirebase["createdAt"] = date

        }
        
        tblFields["isPosted"] = "1"
        
        tblFields["timezoneOffset"] = "\(NSTimeZone.localTimeZone().secondsFromGMT)"
        
        
        // var insertedIdd = ModelManager.instance.addTableData("Chat", primaryKey: "timestamp", tblFields: tblFieldsfirebase)
       // var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
        
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(eventObject.objectId!)'")
        
        var query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: message, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        
      //rahul  refreshList()
        
        
        //respondtableView.reloadData()
        //repondTableView2.reloadData()
    }
    
    func sendMessageError(timer: NSTimer)
    {
       // var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
    }
    
    
    
    
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let payloadData = timer.userInfo?.valueForKey("external") as! PFObject
        
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            ///Users/brst981/Desktop/my projects/Eventnode Git/Eventnode/AddCommentsViewController.swift:317:34: Cannot invoke 'fetchData' with an argument list of type '(PFQuery, target: AddCommentsViewController, successSelector: String, successSelectorParameters: [String : String?], errorSelector: String, errorSelectorParameters: nil)'
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            
           
            
            
            
            if isShared == true
            {
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentChatEventObjectId)/\(eventObject.valueForKey("eventCreatorObjectId")!)")
                let abc = String(format: "%.0f", Timestamp)
                print(abc)
                messagesRef.childByAutoId().setValue([
                    "SenderID":self.currentUserId,
                    "timestamp" :abc,
                    "eventid" : currentChatEventObjectId,
                    "isRSVP" : isrsvp
                    
                    ])

            }

            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                    if currentUserId != invitation["userObjectId"] as! String
                    {
                    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentChatEventObjectId)/\(invitation["userObjectId"] as! String)/")
                    let abc = String(format: "%.0f", Timestamp)
                    print(Timestamp)
                    messagesRef.childByAutoId().setValue([
                        "SenderID":self.currentUserId,
                        "timestamp" :abc,
                        "eventid" : currentChatEventObjectId
                        
                        ])
                    }
                    //fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                }
                fetchedobjects[i]["isEventStreamUpdated"] = true
                i++
            }
            
            
            PFObject.saveAllInBackground(fetchedobjects)
            
            let eventCreatorObjectId = eventObject["eventCreatorObjectId"] as! String
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //eventCreatorObjectId
            
            var createdAt = ""
            
            if payloadData.createdAt != nil
            {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                createdAt = dateFormatter.stringFromDate((payloadData.createdAt)!)
            }
            
            var updatedAt = ""
            if payloadData.updatedAt != nil
            {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                updatedAt = dateFormatter.stringFromDate((payloadData.updatedAt)!)
            }
            
            
            let objectId = payloadData.objectId!
            let messageText = payloadData["messageText"] as! String
            let senderObjectId = payloadData["senderObjectId"] as! String
            let eventObjectId = payloadData["eventObjectId"] as! String
            let senderName = payloadData["senderName"] as! String
            
            let eventTitle = eventObject["eventTitle"] as! String
            
            
            /*,
            "objectId" : objectId,
            "messageText" : messageText,
            "senderObjectId" : senderObjectId,
            "eventObjectId" : eventObjectId,
            "senderName" : senderName,
            "createdAt" : createdAt,
            "updatedAt" :  updatedAt,
            "notifType" :  "groupchat"*/
            
            var alert = ""
//            var rsvp = ""
//            if isRSVP == true
//            {
//               rsvp = "true"
//            }
//            else
//            {
//                rsvp = "false"
//            }
//            
//            if  isRSVP == true
//            {
//                alert = "\(senderName) sent you a new message in \(eventTitle)"
//            }
//            else
//            {
                alert = "\(senderName) sent a new messeage in \(eventTitle)"
           // }
            
            
            var data: Dictionary<String, String!> = [
                "alert" : "\(alert)",
                "objectId" : "\(objectId)",
                "messageText" : "\(messageText)",
                "senderObjectId" : "\(senderObjectId)",
                "eventObjectId" : "\(eventObjectId)",
                "senderName" : "\(senderName)",
                "createdAt" : "\(createdAt)",
                "updatedAt" :  "\(updatedAt)",
                "timezoneOffset" :  "\(NSTimeZone.localTimeZone().secondsFromGMT)",
                "isRSVP": "\(isrsvp)",
                //"badge": "",
                "sound" : "default",
                "notifType" :  "groupchat"
            ]
             var predicateString: String!
            let fetchedUserObjectIdsString = fetchedUserObjectIds.joinWithSeparator("','")
            if eventCreatorObjectId == currentUserId
            {
                predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND allowSound = true AND hostActivityNotification = true"
                
                sendParsePush(predicateString, data: data)
                
                 data["sound"] = ""
                
                
                predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND allowSound = false AND hostActivityNotification = true"
                sendParsePush(predicateString, data: data)
            }
            else
            {
                predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND allowSound = true AND guestActivityNotification = true"
                sendParsePush(predicateString, data: data)
                
                data["sound"] = ""
                
                predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND allowSound = false AND guestActivityNotification = true"
                sendParsePush(predicateString, data: data)
            }
            
            
            

             print(fetchedUserObjectIdsString)
 
            
            
            
            
        }
    }
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        let predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)

        let query = PFUser.queryWithPredicate(predicate)
        
        let push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                if let fetchedObjects = objects as? [PFUser]
                {
                    for object in fetchedObjects
                    {
                        let userObjectId = object.objectId!

                        let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")

                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)

                        let query = PFQuery(className: "BadgeRecords", predicate: predicate)

                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in

                            if error == nil
                            {
                                //println(objects?.count)
                                if let fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0

                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")

                                        badgeObject["userObjectId"] = userObjectId
                                    }



                                    var newdata: Dictionary<String, String!>! = data

                                    newdata["badge"] = "\(badgeCount + 1)"

                                    let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")

                                    let query = PFInstallation.queryWithPredicate(predicate)

                                    push.setQuery(query)

                                    push.setData(newdata)
                                    push.sendPushInBackground()

                                    badgeObject["badgeCount"] = badgeCount+1

                                    badgeObject.saveInBackground()

                                }
                            }
                            else
                            {

                            }
                        }
                    }
                }
            }
            else
            {
            }
        }
    }


    func adjustTableHeight()
    {
        var totalHeight:CGFloat = 0
        for var i = 0; i < messageRowHeights.count; i++
        {
            totalHeight += (messageRowHeights[i] + (70*self.view.frame.height/568))
        }
        
        
        
        //if totalHeight < ((404/568)*self.view.frame.height)
        if totalHeight < (wrapperViewChat.frame.origin.y - (chatHeadBottom.frame.origin.y + chatHeadBottom.frame.height))
        {
            addCommentTableView.frame.size.height = totalHeight
        }
        else
        {
            addCommentTableView.frame.size.height = (wrapperViewChat.frame.origin.y - (chatHeadBottom.frame.origin.y + chatHeadBottom.frame.height))
        }
        
        if totalHeight > addCommentTableView.frame.height
        {
            addCommentTableView.contentOffset.y = addCommentTableView.contentSize.height - addCommentTableView.frame.height
        }
        
    }
    
    func adjustTableY()
    {
        
        addCommentTableView.frame.origin.y = wrapperViewChat.frame.origin.y - addCommentTableView.frame.height
        
        //addCommentTableView.frame.origin.y = ((429/568)*self.view.frame.height) - totalHeight + ((74/568)*self.view.frame.height)
        //addCommentTableView.frame.origin.y = ((74/568)*self.view.frame.height)
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK: - UITableViewDelegates and DataSource() Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       // print(chatsArray)
       // return pricediff.count
        if (arrdata.count == 0)
        {
            activityIndicatorView.stopAnimation()

        }
        
        return self.arrdata.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        var temparr = NSMutableArray()
        
            temparr = arrdata
        

        let senderMessage: UITextView = UITextView()
        senderMessage.frame = CGRectMake(0, 0, self.view.frame.width-70,70)
        senderMessage.font = UIFont(name: "AvenirNext-Medium", size: 15)
        senderMessage.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        senderMessage.text = temparr[indexPath.row]["message"] as! String
        
        //println("current User Id = \(currentUserId)")
        let contentSize = senderMessage.sizeThatFits(senderMessage.bounds.size)

        return  (70*self.view.frame.height/568) + contentSize.height
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        activityIndicatorView.stopAnimation()
        refreshControl.endRefreshing()
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)as! AddCommentTableViewCell
        
        tableView.separatorColor = UIColor.clearColor()
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        
        for view in cell.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        let wrapperView:UIView = UIView()
        
        let senderName_lbl:UILabel = UILabel()
        
        let senderImage:UIImageView = UIImageView()
        
        let senderMessage: UITextView = UITextView()
        
        let wrapperMessage:UIView = UIView()
        
        let messageTime:UILabel = UILabel()
        
        
        cell.contentView.addSubview(wrapperView)
        
        wrapperView.addSubview(wrapperMessage)
        
        wrapperView.addSubview(senderImage)
        
        wrapperMessage.addSubview(senderMessage)
        
        wrapperMessage.addSubview(senderName_lbl)
        
       wrapperMessage.addSubview(messageTime)
        
        var temparr = NSMutableArray()
        
        
            temparr = arrdata
        
        //println("current User Id1 = \(currentUserId)")
        
        if currentUserId == temparr[indexPath.row]["userObjectId"] as! String
        {
            senderMessage.editable = false
            //cell.senderMessage.selectable = false
            senderMessage.scrollEnabled = false
            senderMessage.textAlignment = NSTextAlignment.Right
            senderMessage.font = UIFont(name: "AvenirNext-Medium", size: 13)
            senderMessage.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            senderMessage.text = temparr[indexPath.row]["message"] as! String

            //println("current User Id = \(currentUserId)")
            let contentSize = senderMessage.sizeThatFits(senderMessage.bounds.size)
            wrapperView.frame = CGRectMake(0, 0, cell.contentView.frame.width, (70*self.view.frame.height/568) + contentSize.height+10)
            
            wrapperMessage.backgroundColor = UIColor(red: 244.0/255, green: 236.0/255, blue: 194.0/255, alpha: 1.0)
            
            senderMessage.backgroundColor = UIColor(red: 244.0/255, green: 236.0/255, blue: 194.0/255, alpha: 1.0)
            
            senderName_lbl.backgroundColor = UIColor(red: 244.0/255, green: 236.0/255, blue: 194.0/255, alpha: 1.0)
            
            
            //            wrapperMessage.backgroundColor = UIColor.blackColor()
            //
            //            senderMessage.backgroundColor = UIColor.blueColor()
            //
            //            senderName_lbl.backgroundColor = UIColor.redColor()
            
            
            wrapperMessage.frame = CGRectMake(self.view.frame.width*(32/320), self.view.frame.height*(0.0/568), self.view.frame.width*(235.0/320), self.view.frame.height*(senderMessage.frame.height/568))
            
            
            senderName_lbl.frame = CGRectMake(self.view.frame.height*(10.0/568), self.view.frame.height*(10.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
            
            senderName_lbl.textAlignment = NSTextAlignment.Right
            
            
            senderName_lbl.font = UIFont(name: "AvenirNext-DemiBold", size: 13)
            senderName_lbl.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            

            
            senderName_lbl.text = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as? String
            
            senderName_lbl.textAlignment = NSTextAlignment.Right
            
            let senderNameRightMargin = wrapperMessage.frame.width - (senderName_lbl.frame.origin.x + senderName_lbl.frame.width)
            
            senderMessage.frame = CGRectMake( wrapperMessage.frame.width - ((self.view.frame.width*(215.0/320))+senderNameRightMargin)+(self.view.frame.width*(4.0/320)), self.view.frame.height*(27.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(45.0/568))
            
            /*senderMessage.frame = CGRectMake( self.view.frame.height*(1.0/568), self.view.frame.height*(27.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(45.0/568))*/
            
            
//                      let strtemp = temparr[indexPath.row]["timestamp"] as! String
//            messageTime.text = timedifference(strtemp) 
            
//            var strtemp = ""
//            if temparr[indexPath.row].containsObject("timestamp")
//            {
            print(temparr)
               var strtemp = temparr[indexPath.row]["timestamp"] as! String
                messageTime.text = timedifference(strtemp)
                
//            }

            messageTime.font = UIFont(name: "AvenirNext-Regular", size: 10)

           messageTime.textColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)
            messageTime.textAlignment = NSTextAlignment.Right

            senderImage.frame = CGRectMake(self.view.frame.width*(273.0/320), self.view.frame.height*(0.0/568), self.view.frame.height*(38.0/568), self.view.frame.height*(38.0/568))

            let imagePath = "\(documentDirectory)/profilePic.png"

            //println("\(documentDirectory)/profilePic.png")

            let fileManager = NSFileManager.defaultManager()

            if (fileManager.fileExistsAtPath(imagePath))
            {
                //println("FILE AVAILABLE");
                senderImage.image = UIImage(named: imagePath)
            }
            else
            {
                senderImage.image = UIImage(named: "default.png")
            }
           
            senderImage.layer.masksToBounds = true;
            senderImage.layer.cornerRadius = self.view.frame.height*(19.0/568)
            
        }
        else
        {
            
            let senderObjectId = temparr[indexPath.row]["userObjectId"] as! String
            
            let imagePath = "\(documentDirectory)/\(senderObjectId).png"
            
         
            
            senderMessage.textAlignment = NSTextAlignment.Right
            senderMessage.font = UIFont(name: "AvenirNext-Medium", size: 13)
            senderMessage.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            senderMessage.text = temparr[indexPath.row]["message"] as! String
            
            //println("current User Id = \(currentUserId)")
            let contentSize = senderMessage.sizeThatFits(senderMessage.bounds.size)
            wrapperView.frame = CGRectMake(0, 0, cell.contentView.frame.width,  contentSize.height+150)
            ////println("\(documentDirectory)/profilePic.png")

            
            senderImage.frame = CGRectMake(self.view.frame.width*(10/320), self.view.frame.height*(5.0/568), self.view.frame.height*(38.0/568), self.view.frame.height*(38.0/568))
            
            let fileManager = NSFileManager.defaultManager()
            
//            if (fileManager.fileExistsAtPath(imagePath))
//            {
                //println("FILE AVAILABLE");
                senderImage.image = UIImage(contentsOfFile: imagePath)
//            }
//            else
//            {
                //println("FILE NOT AVAILABLE");
                
                senderImage.image = UIImage(named: "default.png")
                
                let s3BucketName = "eventnodepublicpics"
              //  let fileName = "profilePic.png"
                
                let downloadFilePath = "\(documentDirectory)/\(senderObjectId).png"
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(senderObjectId)/profilePic/profilePic.png"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
//                let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
//                    //			println(self)
//                }
//                
//                
//                
//                
//                let tempstr = String("http://d513o5zfkznc5.cloudfront.net/\(senderObjectId)/profilePic/profilePic.png")
//                let tempurl = NSURL(string: tempstr)
//                senderImage.sd_setImageWithURL(tempurl, completed: block)
                
               // loaderCellView.hidden = true
                
             transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                senderImage.image = UIImage(named: "default.png")
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                senderImage.image = UIImage(named: "default.png")
                                break;
                                
                            default:
                                //println("error downloading")
                                senderImage.image = UIImage(named: "default.png")
                                break;
                            }
                        } else {
                            // Unknown error.
                            //println("error downloading")
                            senderImage.image = UIImage(named: "default.png")
                        }
                    }
                    
                    if (task.result != nil) {
                        //println("downloading successfull")
                        
                        senderImage.image = UIImage(contentsOfFile: downloadFilePath)
                    }
                    
                    return nil
                    
                })

            //}
            
            senderImage.layer.masksToBounds = true;
            senderImage.layer.cornerRadius = self.view.frame.height*(19.0/568)
            
            wrapperMessage.frame = CGRectMake(self.view.frame.width*(58/320), self.view.frame.height*(0.0/568), self.view.frame.width*(235.0/320), self.view.frame.height*(senderMessage.frame.height/568))
            
            wrapperMessage.backgroundColor = UIColor(red: 242.0/255, green: 242.0/255, blue: 242.0/255, alpha: 1.0)
            
            
            
            senderName_lbl.backgroundColor = UIColor(red: 242.0/255, green: 242.0/255, blue: 242.0/255, alpha: 1.0)
            
            senderName_lbl.frame = CGRectMake( self.view.frame.width*(10.0/320), self.view.frame.height*(10.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
            
            senderMessage.textAlignment = NSTextAlignment.Left
            
            senderName_lbl.font = UIFont(name: "AvenirNext-DemiBold", size: 13)
            
            senderName_lbl.text = temparr[indexPath.row]["name"] as? String
            
            senderName_lbl.textAlignment = NSTextAlignment.Left
            
            senderMessage.frame = CGRectMake( self.view.frame.width*(6.0/320), self.view.frame.height*(27.0/568), self.view.frame.width*(215.0/320), self.view.frame.height*(45.0/568))
            
            
            senderMessage.text = temparr[indexPath.row]["message"] as! String
            
            senderMessage.font = UIFont(name: "AvenirNext-Medium", size: 13)
            
            //println(chatsArray[indexPath.row]["messageText"] as! String)
            
            
//            let strtemp = temparr[indexPath.row]["timestamp"] as! String
//            messageTime.text = timedifference(strtemp)
            
//            var strtemp = ""
           
            
//            if temparr[indexPath.row].containsObject("timestamp")
//            {
               var strtemp = temparr[indexPath.row]["timestamp"] as! String
                messageTime.text = timedifference(strtemp)
                
//            }
            //            else
            //            {
            //                let timestamp = NSDate().timeIntervalSince1970
            //
            //                strtemp = timestamp as! String
            //            }
            //
            //            guard let time = temparr[indexPath.row]["timestamp"]
            //                strtemp = temparr[indexPath.row]["timestamp"]
            //                else {
            //                return " "
            //            }
            
            //            print(temparr[indexPath.row])
            //            var strtemp = ""
            //
            //            if let timestamp = temparr[indexPath.row]["timestamp"]
            //            {
            //                strtemp = temparr[indexPath.row]["timestamp"] as! String
            //            }
            
            
            
            messageTime.font = UIFont(name: "AvenirNext-Regular", size: 10)

            messageTime.textAlignment = NSTextAlignment.Left
            messageTime.textColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)

            senderMessage.backgroundColor = UIColor(red: 242.0/255, green: 242.0/255, blue: 242.0/255, alpha: 1.0)
            
            
            senderMessage.editable = false
            //cell.senderMessage.selectable = false
            senderMessage.scrollEnabled = false
            
        }
        
        
        let contentSize = senderMessage.sizeThatFits(senderMessage.bounds.size)
        var frame = senderMessage.frame
        frame.size.height = contentSize.height
        senderMessage.frame = frame
        
        wrapperMessage.frame.size.height = frame.height + self.view.frame.height*(61.0/568)
        wrapperMessage.layer.cornerRadius = 5
        wrapperMessage.layer.masksToBounds = true
        
        messageTime.frame = CGRectMake(self.view.frame.height*(10.0/568), self.view.frame.height*(30.0/568) + contentSize.height, self.view.frame.width*(215.0/320), self.view.frame.height*(21.0/568))
        
        //println("SenderMessage Height = \(senderMessage.frame)")
        
        senderName_lbl.backgroundColor = UIColor.clearColor()
        let m = arrdata.valueForKey("count").objectAtIndex(arrdata.count-1) as! Int
      

        if (indexPath.row == 1 && indexcheck == true && m>1)
        {
              let n = arrdata.valueForKey("count").objectAtIndex(1) as! Int
            if (n == arrdata.valueForKey("count").objectAtIndex(indexPath.row)as! Int)
            {
       // updateFirebase()
            loadpreviousmessage.hidden=false
            }
            else
            {
            loadpreviousmessage.hidden=true
            }
        }
        if indexPath.row >= 9
        {
        indexcheck = true
        }
        
        
        if arrdata.valueForKey("count").objectAtIndex(0) as! Int == 1
        {
        loadpreviousmessage.hidden=true
        }
        return cell
        
    }
    func checkforreload ()
    {
        if arrdata.count > 0
        {
      
               updateFirebase()
        activityIndicatorView.startAnimation()
        //}
        }
        //}
    }
    func application(application: UIApplication,  didReceiveRemoteNotification userInfo: [NSObject : AnyObject],  fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
   

    let refreshAlert = UIAlertController(title: "Delete Event", message: "This Event will be deleted now. This cannot be undone.", preferredStyle: UIAlertControllerStyle.Alert)
    
    
    refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
    
    }))
    
    self.presentViewController(refreshAlert, animated: true, completion: nil)

    
    if let notifType: String = userInfo["notifType"] as? String {
        
            if PFUser.currentUser() != nil
            {
                if notifType == "groupchat"
                {
                    if isChatMode == true
                    {
                    }
                    //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                    
                }
            }
        }
        
    }
    
    
   /* func refreshList()
    {
        
        chatsArray = []
        var messageRowHeightsTemp = [CGFloat]()
        //messageIds = []
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["*"], whereString: "eventObjectId = '\(eventObject.objectId!)' ORDER BY eventCommentId ASC", whereFields: [])
        
        if resultSet != nil
        {
            while resultSet.next()
            {
                let chat = PFObject(className: "EventComments")
                
                //println(resultSet.stringForColumn("messageText"))
                
                //chat["objectId"] = resultSet.stringForColumn("objectId")
                chat["senderObjectId"] = resultSet.stringForColumn("senderObjectId")
                
                chat.objectId = resultSet.stringForColumn("objectId")
                
                //println(resultSet.stringForColumn("objectId"))
                
                messageIds.append(resultSet.stringForColumn("objectId"))
                
                //println(messageIds)

                
                let textMessage = resultSet.stringForColumn("messageText")
                
                chat["messageText"] = "\(textMessage)"
                
                chat["profileImage"] = "girl.jpeg"
                
                // TODO(Dimpal): Fix this. App is crashing here. senderName is nil.
                //println(resultSet.stringForColumn("senderName"))
                
                chat["senderName"] = resultSet.stringForColumn("senderName")
                
                let date = NSDate()
                //println("date is = \(date)")

                let messageTimezoneOffset = resultSet.intForColumn("timezoneOffset")
                
                let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
                //println("current Time Stamp = \(currentTimeStamp)")
                
                let dateCreated = stringToDate(resultSet.stringForColumn("createdAt"))
                //println("Date Created = \(dateCreated)")
                ///Users/brst981/eventnode/Eventnode/AddCommentsViewController.swift:1045:40: Could not find an overload for 'init' that accepts the supplied arguments
                ///Users/brst981/eventnode/Eventnode/AddCommentsViewController.swift:1045:40: Could not find an overload for 'init' that accepts the supplied arguments
                let createdTimeStamp = Int64(dateCreated.timeIntervalSince1970*1000) - Int64(NSTimeZone.localTimeZone().secondsFromGMT*1000) + Int64(messageTimezoneOffset*1000)
                //println("Created Time Stamp = \(createdTimeStamp)")

                
                // + Int64(Int(messageTimezoneOffset)*1000)
                
                var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
                
                //var timeDiff = Int64(currentTimeStamp - createdTimeStamp)-timezoneOffset
                let timeDiff = Int64(currentTimeStamp - createdTimeStamp)
                //println(timeDiff)
                
                //timeDiff = 3600000*25*30*24
                
                var nYears = timeDiff / (1000*60*60*24*365)
                
                //println(nYears)
                
                var nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
                
                //println(nMonths)
                
                var nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
                //println(nDays)
                
                var nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
                //println(nHours)
                
                
                var nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
                //println(nMinutes)
                
                var timeMsg = ""
                
                if nYears > 0
                {
                    var yearWord = "years"
                    if nYears == 1
                    {
                        yearWord = "year"
                    }
                    
                    timeMsg = "about \(nYears) \(yearWord) ago"
                }
                else if nMonths > 0
                {
                    var monthWord = "months"
                    if nMonths == 1
                    {
                        monthWord = "month"
                    }
                    
                    timeMsg = "about \(nMonths) \(monthWord) ago"
                }
                else if nDays > 0
                {
                    var dayWord = "days"
                    if nDays == 1
                    {
                        dayWord = "day"
                    }
                    
                    timeMsg = "about \(nDays) \(dayWord) ago"
                }
                else if nHours > 0
                {
                    var hourWord = "hours"
                    if nHours == 1
                    {
                        hourWord = "hour"
                    }
                    
                    timeMsg = "about \(nHours) \(hourWord) ago"
                }
                else if nMinutes > 0
                {
                    var minuteWord = "minutes"
                    if nMinutes == 1
                    {
                        minuteWord = "minute"
                    }
                    
                    timeMsg = "about \(nMinutes) \(minuteWord) ago"
                }
                else
                {
                    timeMsg = "just now"
                }

                
                
                chat["timeString"] = timeMsg
                
                
                var senderMessageTemp = UITextView()
                
                senderMessageTemp.frame.size.width = self.view.frame.width*(215.0/320)
                senderMessageTemp.frame.size.height = self.view.frame.height*(45.0/568)
                
                senderMessageTemp.text = chat["messageText"] as! String
                
                senderMessageTemp.textAlignment = .Right
                
                senderMessageTemp.font = UIFont(name: "AvenirNext-Medium", size: 13)
                
                let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
                var frame = senderMessageTemp.frame
                frame.size.height = contentSize.height
                senderMessageTemp.frame = frame

                var rowHeight: CGFloat = 0

                /*if contentSize.height > self.view.frame.height*(25.0/568)
                {
                    rowHeight = contentSize.height - (self.view.frame.height*(25.0/568))
                }*/

                rowHeight = (90.0*self.view.frame.height/568) + contentSize.height

                

                chat["timeDiff"] = "\(timeDiff)"

                messageRowHeightsTemp.append(rowHeight)

                chatsArray.append(chat)
            }
            
            var chatsArrayTemp = chatsArray
            messageRowHeights = []
            pricediff = chatsArray as NSArray
            print ( pricediff)
            let ageSortDescriptor = NSSortDescriptor(key: "timeDiff", ascending: false)
           // pricediff = (pricediff as NSArray).sortedArrayUsingDescriptors([ageSortDescriptor])
            print ( pricediff)
            
            //chatsArray.sortInPlace{ $0["timeDiff"] as? Int64 > $1["timeDiff"] as? Int64}
            
            for var i = 0; i < chatsArray.count; i++
            {
                for var j = 0; j < chatsArray.count; j++
                {
                    if chatsArray[i].objectId!  == chatsArrayTemp[j].objectId!
                    {
                        messageRowHeights.append(messageRowHeightsTemp[j])
                    }
                }
            }
            
            
            //chatsArray = []
            
            
            
            /*for var i = 0; i < chatsArrayTemp.count; i++
            {
                if timeDiffArray[i]
            }*/
            //println(messageRowHeights)
            
           //rahul addCommentTableView.reloadData()
            //adjustTableHeight()
            //adjustTableY()
        }
        resultSet.close()
        
    }*/
    
    
    
    //MARK: - stringToDate()
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        //println("date formatter = \(dateFormatter)")
        
        let date = dateFormatter.dateFromString(dateString)
        
        
        return date!
    }
    func timedifference(dateString: String)->String
    {
    let date = NSDate()
    
    //let messageTimezoneOffset = resultSet.intForColumn("timezoneOffset")
    
    let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
  
    
   // let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
    
    let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
    
    
    let nYears = timeDiff / (1000*60*60*24*365)
    
    
    let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
    
    
    let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
    
    let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
    
    
    let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
    //println(nMinutes)
    
    var timeMsg = ""
    
    if nYears > 0
    {
    var yearWord = "years"
    if nYears == 1
    {
    yearWord = "year"
    }
    
    timeMsg = "about \(nYears) \(yearWord) ago"
    }
    else if nMonths > 0
    {
    var monthWord = "months"
    if nMonths == 1
    {
    monthWord = "month"
    }
    
    timeMsg = "about \(nMonths) \(monthWord) ago"
    }
    else if nDays > 0
    {
    var dayWord = "days"
    if nDays == 1
    {
    dayWord = "day"
    }
    
    timeMsg = "about \(nDays) \(dayWord) ago"
    }
    else if nHours > 0
    {
    var hourWord = "hours"
    if nHours == 1
    {
    hourWord = "hour"
    }
    
    timeMsg = "about \(nHours) \(hourWord) ago"
    }
    else if nMinutes > 0
    {
    var minuteWord = "minutes"
    if nMinutes == 1
    {
    minuteWord = "minute"
    }
    
    timeMsg = "about \(nMinutes) \(minuteWord) ago"
    }
    else
    {
    timeMsg = "just now"
    }
        
        return timeMsg
}
    
    //MARK: -  viewTapped()
    @IBAction func viewTapped(sender: AnyObject)
    {
        enterTextMessage.resignFirstResponder()
    }
    
    
    
    //MARK: -  keyboardWillShow()
    func keyboardWillShow(sender: NSNotification)
    {
        //        wrapperViewChat.frame.origin.y -= (253.0/568)*self.view.frame.height
        //        addCommentTableView.frame.origin.y -= (253.0/568)*self.view.frame.height
        
        let userInfo = sender.userInfo!
        
        //let animationDuration: NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        let keyboardScreenBeginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        //println("startHeight:\(keyboardScreenBeginFrame.height)")
        //println("endHeight:\(keyboardScreenEndFrame.height)")
        
        wrapperViewChat.frame.origin.y = wrapperViewChatY - keyboardScreenBeginFrame.height
        addCommentTableView.frame.size.height = addCommentTableViewHeight - keyboardScreenBeginFrame.height
        
        if addCommentTableView.contentSize.height > addCommentTableView.frame.height
        {
            addCommentTableView.contentOffset.y = addCommentTableView.contentSize.height - addCommentTableView.frame.height
        }
        
        //adjustTableHeight()
        //adjustTableY()
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        
        let delay = 0.1 * Double(NSEC_PER_SEC)
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        
        dispatch_after(time, dispatch_get_main_queue(), {
            
            let numberOfSections = self.addCommentTableView.numberOfSections
            let numberOfRows = self.addCommentTableView.numberOfRowsInSection(numberOfSections-1)
            
            if numberOfRows > 0 {
                let indexPath = NSIndexPath(forRow: numberOfRows-1, inSection: (numberOfSections-1))
                self.addCommentTableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: animated)
            }
            
        })
    }
    
    //MARK: -  keyboardWillHide()
    func keyboardWillHide(sender: NSNotification)
    {
        
        //        wrapperViewChat.frame.origin.y += (253.0/568)*self.view.frame.height
        //        addCommentTableView.frame.origin.y += (253.0/568)*self.view.frame.height
        
        let userInfo = sender.userInfo!
        
        //let animationDuration: NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        let keyboardScreenBeginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        //println("startHeight:\(keyboardScreenBeginFrame.height)")
        //println("endHeight:\(keyboardScreenEndFrame.height)")
        
        wrapperViewChat.frame.origin.y = wrapperViewChatY
        addCommentTableView.frame.size.height = addCommentTableViewHeight
        
        //adjustTableHeight()
        //adjustTableY()
    }
    
    
    
    //MARK: -  closeEventDetailsButtonClicked()
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        /*let eventPhotosVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventPhotosViewController") as! EventPhotosViewController
        self.navigationController?.pushViewController(eventPhotosVC, animated: false)*/
        
        isChatMode = false
        
       
        //timer2.invalidate()
        
        self.navigationController?.popViewControllerAnimated(false)
        
    }

    @IBOutlet weak var loadpreviousmessage: UIButton!
    @IBAction func loadpreviousmessage(sender: AnyObject) {
        checkforreload()
    }
}