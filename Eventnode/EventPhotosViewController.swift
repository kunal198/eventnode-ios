//
//  EventPhotosViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/27/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import AVFoundation
import MediaPlayer
import Intercom
//import Fabric
//import Crashlytics

var myNewPost:PFObject!
var myNewpostText = NSDictionary()
var totalcount = Int()
var isPostUpdated:Bool! = true

var myEventData = [PFObject]()
var myRowHeights = [CGFloat]()

var eventTextTitle = "Add Text"

var isOnMyEventStream: Bool = false

var i = 0
var j = 0

var isMyeventStreamUpdated: Bool = false

class EventPhotosViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate {
    
    @IBOutlet var tableView : UITableView!
    
    @IBOutlet weak var onboardingView: UIView!
    @IBOutlet weak var selectedImageWrapper: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    var messagesRef: Firebase!
    
    @IBOutlet weak var chatButton: UIButton!
    
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var unreadCount: UILabel!
    
    @IBOutlet var loaderView : UIView!
    
    @IBOutlet var loaderSubView : UIView!
    
    var postHiddenTextView = UITextView()
    var arrbadgechatCount = NSMutableArray()

    var newMedia: Bool = true
    var moviePlayer : MPMoviePlayerController?
    
    
    var moviePlayers = [Int: MPMoviePlayerController]()
    var currentScrollTop = 0
    var hideNow = 0
    var currentUserId: String!
    
    var eventLogoFile: String!
    
    var eventLogoFileUrl: NSURL!
    var redirectFromLink = true
    var currentEventObject: PFObject!
    var updatebadgestimer = NSTimer()
    var unreadMessagesCount = 0
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x:105, y: 250, width: 100, height: 150),
        type: .BallScaleMultiple, color: UIColor(red: 220/255.0, green: 203/255.0, blue: 85/255.0, alpha: 1.0), size: CGSize(width: 100, height: 100))
    var arrdata = NSMutableArray()
    var arrLike = NSMutableArray()
    var arrLikeDic = NSMutableArray()
    var indexcheck : Bool!
    var reloadtst : Bool!
    var messagessRef: Firebase!
    var isRSVP = ""
    var refreshControl = UIRefreshControl()
    var eventCreatorId = ""
    
    func setupFirebase() {
        // *** STEP 2: SETUP FIREBASE
        
        
        
        
                  // do some task
                   self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
            
            self.arrdata = []
            
            
            
            // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
       self.messagesRef .queryLimitedToLast(5).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
                dictemp.setValue(snapshot.value["text"] as? String, forKey: "text")
                dictemp.setValue(snapshot.value["postUrl"] as? String, forKey: "postUrl")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "time")
                dictemp.setValue(snapshot.value["eventObjectId"] as? String, forKey: "objectId")
                dictemp.setValue(snapshot.value["postData"] as? String, forKey: "postData")
                dictemp.setValue(snapshot.value["postHeight"] as? String, forKey: "postHeight")
                dictemp.setValue(snapshot.value["postWidth"] as? String, forKey: "postWidth")
                dictemp.setValue(snapshot.value["eventFolder"] as? String, forKey: "eventFolder")
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
        dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")

                self.arrdata.insertObject(dictemp, atIndex: 0)
        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "count", ascending: false)
        let sortedResults: NSArray = self.arrdata.sortedArrayUsingDescriptors([descriptor])
        self.arrdata = []
        self.arrdata = sortedResults.mutableCopy() as! NSMutableArray
        self.view.userInteractionEnabled = true

                self.tableView.reloadData()
        
        
        if self.arrdata.count == 0
        {
            self.onboardingView.hidden = false
             self.activityIndicatorView.hidden = true
            self.activityIndicatorView.stopAnimation()
        }
        else
        {
            self.onboardingView.hidden = true
        }
        
        
        let queryRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentEvent.objectId!)/\(snapshot.key)")
        //    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
        
//        self.arrLikeDic = []
//        self.arrLikeDic.removeAllObjects()
        
        // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
        queryRef.observeEventType(.ChildAdded, withBlock: { (snapshot) in
            let dictemp = NSMutableDictionary()
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
            dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
            dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
            
            self.arrLikeDic.insertObject(dictemp, atIndex: 0)
            
            let arrtemp = self.arrLikeDic
            self.arrLikeDic = []
            
            for e in arrtemp
            {
                if !self.arrLikeDic.containsObject(e)
                {
                    self.arrLikeDic.addObject(e)
                }
                
            }
            
            
            
            
            self.tableView.reloadData()
            
            
            queryRef.observeEventType(.ChildRemoved, withBlock: { (snapshot) in
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                
                self.arrLikeDic.removeObject(dictemp)
                
                let arrtemp = self.arrLikeDic
                self.arrLikeDic = []
                
                for e in arrtemp
                {
                    if !self.arrLikeDic.containsObject(e)
                    {
                        self.arrLikeDic.addObject(e)
                    }
                    
                }
                
                self.tableView.reloadData()
                
                // self.finishReceivingMessage()
            })

            // self.arrLike.addObject(snapshot.key)
            
            //self.GetLikeDicFirebase()
            
            self.tableView.reloadData()
        })

        
        self.tableView.reloadData()

        
        
        
        if(self.reloadtst == true)
        {
                 self.tableView.setContentOffset(CGPoint.zero, animated:true)
        }
                // self.finishReceivingMessage()
            })
        
        
        
        
        
        
    }
    
    func updateFirebase() {
        
        // do some task
        if arrdata.count > 0
        {
            indexcheck = false
            var count = arrdata.count
            if count > 5
            {
                count = count - 5
            }
            let m = arrdata.valueForKey("count").objectAtIndex(arrdata.count-1) as! Int
            var n = m-6
            if m < 6
            {
                n = 1
            }

            self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")

            
            
            self.messagesRef.queryOrderedByChild("count").queryStartingAtValue(n).queryEndingAtValue(m-1).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
                
                
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
                dictemp.setValue(snapshot.value["text"] as? String, forKey: "text")
                dictemp.setValue(snapshot.value["postUrl"] as? String, forKey: "postUrl")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "time")
                dictemp.setValue(snapshot.value["eventObjectId"] as? String, forKey: "objectId")
                dictemp.setValue(snapshot.value["postData"] as? String, forKey: "postData")
                dictemp.setValue(snapshot.value["postHeight"] as? String, forKey: "postHeight")
                dictemp.setValue(snapshot.value["postWidth"] as? String, forKey: "postWidth")
                dictemp.setValue(snapshot.value["eventFolder"] as? String, forKey: "eventFolder")
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")

                self.arrdata.addObject(dictemp)
                
                let arrtemp = self.arrdata
                self.arrdata = []
                
                for e in arrtemp
                {
                    if !self.arrdata.containsObject(e)
                    {
                        self.arrdata.addObject(e)
                    }
                    
                }
                
                if self.arrdata.count == 0
                {
                    self.onboardingView.hidden = false
                     self.activityIndicatorView.hidden = true
                    self.activityIndicatorView.stopAnimation()
                }
                else
                {
                    self.onboardingView.hidden = true
                }

                
                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "count", ascending: false)
                let sortedResults: NSArray = self.arrdata.sortedArrayUsingDescriptors([descriptor])
                self.arrdata = []
                self.arrdata = sortedResults.mutableCopy() as! NSMutableArray
                self.view.userInteractionEnabled = true
                self.tableView.reloadData()
             let queryRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentEvent.objectId!)/\(snapshot.key)")
                //    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
                
//                self.arrLikeDic = []
//                self.arrLikeDic.removeAllObjects()
                
                // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
                queryRef.observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.arrLikeDic.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.arrLikeDic
                    self.arrLikeDic = []
                    
                    for e in arrtemp
                    {
                        if !self.arrLikeDic.containsObject(e)
                        {
                            self.arrLikeDic.addObject(e)
                        }
                        
                    }
                    
                    self.tableView.reloadData()
                    queryRef.observeEventType(.ChildRemoved, withBlock: { (snapshot) in
                        let dictemp = NSMutableDictionary()
                        dictemp.setValue(snapshot.key , forKey: "keyvalue")
                        dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                        dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                        dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                        dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                        
                        self.arrLikeDic.removeObject(dictemp)
                        
                        let arrtemp = self.arrLikeDic
                        self.arrLikeDic = []
                        
                        for e in arrtemp
                        {
                            if !self.arrLikeDic.containsObject(e)
                            {
                                //self.arrLikeDic.addObject(e)
                            }
                            
                        }
                        
                        self.tableView.reloadData()
                        
                        // self.finishReceivingMessage()
                    })

               // self.arrLike.addObject(snapshot.key)
                
                //self.GetLikeDicFirebase()

                //self.tableView.reloadData()
                })
                
                
            })
            
            
            /*  for var i = m-1 ; i > m - 11 ; i--
            {
            self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/GroupChat/\(currentChatEventObjectId)")
            self.messagesRef.keepSynced(true)
            //messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/GroupChat/Bnt8WMIQ0u")
            
            self.messagesRef.queryOrderedByChild("count").queryStartingAtValue(2).queryEndingAtValue(4).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
            
            
            let dictemp = NSMutableDictionary()
            dictemp.setValue(snapshot.value["message"] as? String, forKey: "message")
            dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
            dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
            dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")
            
            self.arrdata.addObject(dictemp)
            
            let arrtemp = self.arrdata
            self.arrdata = []
            
            for e in arrtemp
            {
            if !self.arrdata.containsObject(e)
            {
            self.arrdata.addObject(e)
            }
            
            }
            
            
            self.addCommentTableView.reloadData()
            
            
            //                let abc = String(format: "%.0f", self.Timestamp)
            //                var tblFieldsfirebase: Dictionary! = [String: String]()
            //
            //                tblFieldsfirebase["EventId"] = currentChatEventObjectId
            //                tblFieldsfirebase["message"] = snapshot.value["message"] as? String
            //                tblFieldsfirebase["userObjectId"] = snapshot.value["userObjectId"] as? String
            //                tblFieldsfirebase["timestamp"] = abc as String
            //                tblFieldsfirebase["name"] = snapshot.value["name"] as? String
            //                tblFieldsfirebase["Keyvalue"] = snapshot.key
            
            
            
            
            //                if !self.checkExistance("Keyvalue", objectId: snapshot.key as String, tableName: "Chat")
            //                {
            //
            //
            //
            //
            //
            //                _ = ModelManager.instance.addTableData("Chat", primaryKey: "rowId", tblFields: tblFieldsfirebase)
            //
            //                }
            // self.finishReceivingMessage()
            })
            
            }*/
        }
    }
    func GetLikeFirebase() {
        // *** STEP 2: SETUP FIREBASE
        
        
       
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
        
        arrLike = []
        
        
        // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
        messagesRef.queryLimitedToLast(5).observeEventType(.ChildAdded, withBlock: { (snapshot) in
            
            let dictemp = NSMutableDictionary()
            
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            
            self.arrLike.addObject(snapshot.key)
            
            self.GetLikeDicFirebase()
            
            // self.finishReceivingMessage()
        })
        
        
    }
    
    func deletelike() {
        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentEvent.objectId!)")
        
        
        
        messagesRef.observeEventType(.ChildRemoved, withBlock: { (snapshot) in
            
            let dictemp = NSMutableDictionary()
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
            dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
            dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
            self.arrLikeDic.removeObject(dictemp)
             self.tableView.reloadData()
            
           // self.GetLikeFirebase()
            //            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            //            dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
            //            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
            //            dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
            //            dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
            
            //            self.arrLikeDic.removeObject(dictemp)
            //
            //
            //
            //
            //
            //            let arrtemp = self.arrLikeDic
            //            self.arrLikeDic = []
            //
            //            for e in arrtemp
            //            {
            //                if !self.arrLikeDic.containsObject(e)
            //                {
            //                    self.arrLikeDic.addObject(e)
            //                }
            //
            //            }
            //            self.tableView.reloadData()
            // self.finishReceivingMessage()
        })
        
    }
    func updatelike() {
        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
        
        
        
        messagesRef.observeEventType(.ChildChanged, withBlock: { (snapshot) in
            
            self.setupFirebase()
            //            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            //            dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
            //            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
            //            dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
            //            dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
            
            //            self.arrLikeDic.removeObject(dictemp)
            //
            //
            //
            //
            //
            //            let arrtemp = self.arrLikeDic
            //            self.arrLikeDic = []
            //
            //            for e in arrtemp
            //            {
            //                if !self.arrLikeDic.containsObject(e)
            //                {
            //                    self.arrLikeDic.addObject(e)
            //                }
            //
            //            }
            //            self.tableView.reloadData()
            // self.finishReceivingMessage()
        })
        
    }
    func GetLikeDicFirebase() {
        // *** STEP 2: SETUP FIREBASE
        
        for var i = 0; i < arrLike.count; i++
        {
            messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentEvent.objectId!)/\(arrLike[i])")
            //    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
            
//            arrLikeDic = []
//            arrLikeDic.removeAllObjects()
            
            // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
            messagesRef.queryLimitedToLast(5).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                
                self.arrLikeDic.insertObject(dictemp, atIndex: 0)
                
                let arrtemp = self.arrLikeDic
                self.arrLikeDic = []
                
                for e in arrtemp
                {
                    if !self.arrLikeDic.containsObject(e)
                    {
                        self.arrLikeDic.addObject(e)
                    }
                    
                }
                
                self.tableView.reloadData()
                
                // self.finishReceivingMessage()
            })
            messagesRef.queryLimitedToLast(5).observeEventType(.ChildRemoved, withBlock: { (snapshot) in
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                
                self.arrLikeDic.insertObject(dictemp, atIndex: 0)
                
                let arrtemp = self.arrLikeDic
                self.arrLikeDic = []
                
                for e in arrtemp
                {
                    if !self.arrLikeDic.containsObject(e)
                    {
                        //self.arrLikeDic.addObject(e)
                    }
                    
                }
                
                self.tableView.reloadData()
                
                // self.finishReceivingMessage()
            })
            
            
        }
        
        
    }
    func deleteFirebase() {
        // *** STEP 2: SETUP FIREBASE
        
        
        //  messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
        
        
        
        
        
        messagesRef.observeEventType(.ChildRemoved, withBlock: { (snapshot) in
            
            let dictemp = NSMutableDictionary()
            dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
            dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
            dictemp.setValue(snapshot.value["text"] as? String, forKey: "text")
            dictemp.setValue(snapshot.value["postUrl"] as? String, forKey: "postUrl")
            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "time")
            dictemp.setValue(snapshot.value["eventObjectId"] as? String, forKey: "objectId")
            dictemp.setValue(snapshot.value["postData"] as? String, forKey: "postData")
            dictemp.setValue(snapshot.value["postHeight"] as? String, forKey: "postHeight")
            dictemp.setValue(snapshot.value["postWidth"] as? String, forKey: "postWidth")
            dictemp.setValue(snapshot.value["eventFolder"] as? String, forKey: "eventFolder")
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")

            self.arrdata.removeObject(dictemp)
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "count", ascending: false)
            let sortedResults: NSArray = self.arrdata.sortedArrayUsingDescriptors([descriptor])
            self.arrdata = []
            self.arrdata = sortedResults.mutableCopy() as! NSMutableArray
            self.view.userInteractionEnabled = true
            
            if self.arrdata.count == 0
            {
                self.onboardingView.hidden = false
                self.activityIndicatorView.hidden = true
                self.activityIndicatorView.stopAnimation()
                
            }
            else
            {
                self.onboardingView.hidden = true
            }
            
            self.tableView.reloadData()

        })
        
        
    }
    
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        reloadtst = false
        self.arrLikeDic = []
        self.arrLikeDic.removeAllObjects()
        
        indexcheck = false
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(currentEvent.objectId!)/\(currentUserId)")
        
        messagesRef.removeValue()
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(currentEvent.objectId!)/\(currentUserId)")
        
        messagesRef.removeValue()
        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
       // GetLikeFirebase()
        updatelike()
       // GetLikeDicFirebase()
        deleteFirebase()
        deletelike()

        
        
        self.view.addSubview(wakeUpImageView)
        unreadCount.hidden = true
        unreadCount.layer.masksToBounds = true
        upgrademessage()
        
        activityIndicatorView.center = CGPointMake(view.frame.width/2, view.frame.height/2)
        
        self.view.addSubview(activityIndicatorView)
        
        if self.arrdata.count == 0
        {
            self.onboardingView.hidden = false
            self.activityIndicatorView.stopAnimation()
        }
        else
        {
            self.onboardingView.hidden = true
            activityIndicatorView.startAnimation()
        }
        
        
        
        
        activityIndicatorView.hidesWhenStopped = true
        
        
        if emailLinkType != ""
        {
            if emailLinkType == "invitationresponse"
            {
                //                if isRSVP == "true"
                //                {
                //
                //                    redirectFromLink = true
                //
                //                    let addTextVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageGuestViewController") as! ManageGuestViewController
                //
                //                    self.navigationController?.pushViewController(addTextVC, animated: false)
                //
                //
                //                }
                //                else
                //                {
                //
                //                    redirectFromLink = true
                //                    let manageFreinds = self.storyboard!.instantiateViewControllerWithIdentifier("ManageFreindsViewController") as! ManageFreindsViewController
                //
                //                    self.navigationController?.pushViewController(manageFreinds, animated: false)
                //
                //                }
                
            }
            else if emailLinkType == "eventdateupdated" || emailLinkType == "eventlocationupdate" || emailLinkType == "eventtimezoneupdate"
            {
                redirectFromLink = true
            }
            else if currentAttendingStatus != ""
            {
                if isRSVP == "true"
                {
                    
                    redirectFromLink = true
                    let addTextVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageGuestViewController") as! ManageGuestViewController
                    addTextVC.isForInvite = "true"
                    self.navigationController?.pushViewController(addTextVC, animated: false)
                }
                else
                {
                    
                    redirectFromLink = true
                    let manageFreinds = self.storyboard!.instantiateViewControllerWithIdentifier("ManageFreindsViewController") as! ManageFreindsViewController
                    manageFreinds.isForInvite = "true"
                    self.navigationController?.pushViewController(manageFreinds, animated: false)
                    
                }
                
            }
        }
        
        var  currentId =   NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        
        if (emailLinkType == "eventcreation" || emailLinkType == "createChannel") && eventCreatorId == currentId
        {
            redirectFromLink = true
            
        }
        
        
        if redirectFromLink == false
        {
            
            if linkForGuest == "true"
            {
                let Alert = UIAlertController(title: "Error", message: "You are the host for this event and cannot respond to an invitation.", preferredStyle: UIAlertControllerStyle.Alert)
                
                Alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                    
                    
                }))
                
                self.presentViewController(Alert, animated: true, completion: nil)
            }
            else
            {
                if isRSVP == "true"
                {
                    
                    redirectFromLink = true
                    let addTextVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageGuestViewController") as! ManageGuestViewController
                    addTextVC.isForInvite = "true"
                    self.navigationController?.pushViewController(addTextVC, animated: false)
                }
                else
                {
                    
                    redirectFromLink = true
                    let manageFreinds = self.storyboard!.instantiateViewControllerWithIdentifier("ManageFreindsViewController") as! ManageFreindsViewController
                    manageFreinds.isForInvite = "true"
                    self.navigationController?.pushViewController(manageFreinds, animated: false)
                    
                }
                
            }
            
            
            
            
        }
        
        if((currentEvent["eventTitle"] as? String) == "")
        {
            eventTitle.text = "No Title"
        }
        else
        {
            let eventTitleString = (currentEvent["eventTitle"] as? String)!
            
            
            currentEvent["eventTitle"] = String(eventTitleString.characters.prefix(1)).capitalizedString + String(eventTitleString.characters.suffix(eventTitleString.characters.count - 1))
            eventTitle.text = currentEvent["eventTitle"] as? String
        }
        
        
        if let isLikesReloaded = NSUserDefaults.standardUserDefaults().objectForKey("isLikesReloaded") as? String
        {
            if isLikesReloaded == "false"
            {
                ModelManager.instance.deleteTableData("PostLikes", whereString: "1", whereFields: [])
                NSUserDefaults.standardUserDefaults().setObject("true", forKey: "isLikesReloaded")
            }
        }
        else
        {
            ModelManager.instance.deleteTableData("PostLikes", whereString: "1", whereFields: [])
        }
        
        
        
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        let loadingMessage = UILabel()
        loadingMessage.text = "Loading..."
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
        
        indicator.startAnimating()
        
        moviePlayer = MPMoviePlayerController()
        
        moviePlayer!.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        
        moviePlayer!.prepareToPlay()
        
        moviePlayer!.shouldAutoplay = false
        moviePlayer!.view.hidden = true
        self.view.addSubview(moviePlayer!.view)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "doneButtonClick:", name: MPMoviePlayerWillExitFullscreenNotification, object: nil)
        
        postHiddenTextView.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.width)
        
        postHiddenTextView.hidden = true
        
        self.view.addSubview(postHiddenTextView)
        
        tableView.separatorColor = UIColor.clearColor()
        
        
        
        
        //ln(currentEvent)
        
        currentEventObject = currentEvent
        
        setupFirebase()
        
    }
    override func viewDidDisappear(animated: Bool) {
        updatebadgestimer.invalidate()
        arrbadgechatCount.removeAllObjects()
//        messagesRef.removeAllObservers()


    }
    func upgrademessage()
    {
        
        
        
        var predicate = NSPredicate()
        
        var eventObjectIdsStringPredicate = ""
        
        
        eventObjectIdsStringPredicate = "(eventObjectId IN {'\(currentEvent.objectId!)'})"
        
        
        
        //ln(eventObjectIdsStringPredicate)
        
        predicate = NSPredicate(format: eventObjectIdsStringPredicate)
        
        let query = PFQuery(className:"EventComments", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllinvitationMessagessSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllinvitationMessagessError:", errorSelectorParameters:nil)
    }
    
    
    
    func checkExistance(objectIdColumn: String, objectId: String, tableName: String)->Bool
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData(tableName, selectColumns: ["count(*) as count"], whereString: "\(objectIdColumn) = '\(objectId)'", whereFields: [])
        
        resultSet.next()
        
        let noOfRows = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        return (noOfRows > 0)
    }
    func fetchAllinvitationMessagessSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        
        if let fetchedobjects = objects {
            
           
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                if !checkExistance("objectId", objectId: message.objectId!, tableName: "EventComments")
                {
                    var tblFields: Dictionary! = [String: String]()
                    
                    tblFields["objectId"] = message.objectId!
                    tblFields["messageText"] = message["messageText"] as? String
                    tblFields["senderObjectId"] = message["senderObjectId"] as? String
                    tblFields["eventObjectId"] = message["eventObjectId"] as? String
                    tblFields["senderName"] = message["senderName"] as? String
                    //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                    
                    
                    
                    var date = ""
                    
                    if message.createdAt != nil
                    {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((message.createdAt)!)
                        //ln(date)
                        tblFields["createdAt"] = date
                    }
                    
                    if message.updatedAt != nil
                    {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((message.updatedAt)!)
                        //ln(date)
                        tblFields["updatetAt"] = date
                    }
                    
                    tblFields["isPosted"] = "1"
                    
                    if isChatMode == true
                    {
                        tblFields["isRead"] = "1"
                    }
                    else
                    {
                        tblFields["isRead"] = "0"
                    }
                    
                    tblFields["timezoneOffset"] = "\(NSTimeZone.localTimeZone().secondsFromGMT)"
                    
                    /* var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["COUNT(*) as count"], whereString: "objectId = '\(message.objectId!)'", whereFields: [])
                    
                    resultSet.next()
                    
                    var messageCount = Int(resultSet.intForColumn("count"))
                    
                    resultSet.close()
                    */
                    //                if messageCount == 0
                    //                {
                    var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                    //}
                    
                    fetchedEventObjectIds.append(message["eventObjectId"] as! String)
                    
                    let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentEvent.objectId!])
                    
                    resultSetCount.next()
                    
                    //unreadMessagesCount = Int(resultSetCount.intForColumn("count"))
                    
                    resultSetCount.close()
                }
            }
        }
    }
    
    
    func checkforreload ()
    {
        if arrdata.count > 0
        {
            reloadtst = true

            
                updateFirebase()
                activityIndicatorView.startAnimation()
            
        }
        //}
    }
    func fetchAllinvitationMessagessError(timer:NSTimer)
    {
       // var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //ln("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    override func viewWillAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.refreshControl.attributedTitle = NSAttributedString(string: "")
        self.refreshControl.addTarget(self, action: "checkforreload", forControlEvents: UIControlEvents.ValueChanged)
        //self.tableView?.addSubview(refreshControl)
            print ("https://eventnode-rt.firebaseio.com/ChatBadges/\(currentEvent.objectId!)/\(currentUserId)")
           messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentEvent.objectId!)/\(currentUserId)")
            messagesRef.observeEventType(.ChildAdded, withBlock: { (snapshot) in
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
                dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
             
                
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                self.arrbadgechatCount.addObject(dictemp)
                let arrtemp = self.arrbadgechatCount
                self.arrbadgechatCount = []
                
                for e in arrtemp
                {
                    if !self.arrbadgechatCount.containsObject(e)
                    {
                        self.arrbadgechatCount.addObject(e)
                    }
                    
                }
                self.unreadMessagesCount = self.arrbadgechatCount.count
                self.updateBadge()
            })
            
             messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentEvent.objectId!)/\(currentUserId)")
            messagesRef.observeEventType(.ChildRemoved, withBlock: { (snapshot) in
                self.arrbadgechatCount.removeAllObjects()
                self.unreadMessagesCount = 0
                self.updateBadge()
                self.unreadCount.hidden = true
                
            })
j = 0
    }
    override func viewDidAppear(animated: Bool) {
        
        
             //  updatebadgestimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("upgrademessage"), userInfo: nil, repeats: true)
        
        //        if var timeZoneName = NSUserDefaults.standardUserDefaults().objectForKey("timeZoneName") as? String
        //        {
        //            if NSTimeZone.localTimeZone().name != timeZoneName
        //            {
        //                NSUserDefaults.standardUserDefaults().setObject("\(NSTimeZone.localTimeZone().name)", forKey: "timeZoneName")
        //                //var isDeleted1 = ModelManager.instance.deleteTableData("Events", whereString: "1", whereFields: [])
        //            }
        //        }
        //        else
        //        {
        //            NSUserDefaults.standardUserDefaults().setObject("\(NSTimeZone.localTimeZone().name)", forKey: "timeZoneName")
        //        }
        
        
        isOnMyEventStream = true
        
        //unreadMessagesCount
        
        
        let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentEvent.objectId!])
        
        resultSetCount.next()
        
       // unreadMessagesCount = Int(resultSetCount.intForColumn("count"))
        
        resultSetCount.close()
        
        
        updateBadge()
        
      //  var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("refreshContent"), userInfo: nil, repeats: true)
        
        if((currentEvent["eventTitle"] as? String) == "")
        {
            eventTitle.text = "No Title"
        }
        else
        {
            eventTitle.text = currentEvent["eventTitle"] as? String
        }
        
        deleteData()
        
        if(isPostUpdated == true ){
            
            myEventData.removeAll()
            refreshList()
        }
        
        downloadData()
    }
    
    
    
    //    @IBAction func crashButtonTapped(sender: AnyObject)
    //    {
    //        var arrayndex = ["1","2","3","4"]
    //
    //        arrayndex.insert("4", atIndex: 7)
    //
    //       // Crashlytics.sharedInstance().crash()
    //    }
    
    
    func doneButtonClick(sender: NSNotification){
        //ln("jiouiop")
        moviePlayer?.setFullscreen(false, animated: true)
        moviePlayer?.stop()
        moviePlayer?.view.hidden = true
    }
    
    
    func refreshList()
    {
        /*     isPostDataUpDated = false
        
        //myEventData.removeAll()
        //myRowHeights.removeAll()
        //ln("fetching....")
        
        let currentEventId = currentEvent.objectId as String!
        
        /*var resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "eventObjectId=?", whereFields: [currentEventId])
        
        resultSetCount.next()
        
        var postCount = resultSetCount.intForColumn("count")
        
        resultSetCount.close()
        
        if(postCount>0)
        {*/
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId=? GROUP BY objectId ORDER BY createdAt DESC LIMIT 0, \(myEventData.count + 10)", whereFields: [currentEventId])
        
        myEventData = []
        myRowHeights.removeAll()
        
        isPostUpdated = false
        ////ln("Successfully retrieved \(postCount) posts.")
        
        var i = 0
        
        if (resultSet != nil) {
        while resultSet.next() {
        
        let userpost = PFObject(className: "EventImages")
        
        userpost["eventImageId"] = Int(resultSet.intForColumn("eventImageId"))
        
        let eventImageId = Int(resultSet.intForColumn("eventImageId"))
        
        userpost["postData"] = resultSet.stringForColumn("postData")
        userpost["eventFolder"] = resultSet.stringForColumn("eventFolder")
        
        userpost["postType"] = resultSet.stringForColumn("postType")
        userpost["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
        userpost["postHeight"] = resultSet.doubleForColumn("postHeight")
        userpost["postWidth"] = resultSet.doubleForColumn("postWidth")
        
        //userpost["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
        //userpost["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
        
        //ln(resultSet.stringForColumn("createdAt"))
        
        if(resultSet.stringForColumn("createdAt") != "" && resultSet.stringForColumn("updatedAt") != "" && resultSet.stringForColumn("createdAt") != nil && resultSet.stringForColumn("updatedAt") != nil)
        {
        userpost.objectId = resultSet.stringForColumn("objectId")
        }
        else
        {
        
        }
        
        
        let isPosted = resultSet.stringForColumn("isPosted")
        
        userpost["isUploading"] = false
        
        if isPosted == "0"
        {
        myEventData.insert(userpost, atIndex: 0)
        
        userpost["isPosted"] = false
        
        if let newEventImageId = myNewPost["eventImageId"] as? Int
        {
        if newEventImageId == eventImageId
        {
        userpost["isUploading"] = true
        }
        }
        }
        else
        {
        
        userpost["isPosted"] = true
        myEventData.append(userpost)
        
        }
        
        
        
        
        /*var isApproved = resultSet.stringForColumn("isApproved")
        
        if(isApproved != nil)
        {
        if isApproved == "0"
        {
        userpost["isApproved"] = false
        }
        else
        {
        userpost["isApproved"] = true
        }
        }
        else
        {
        userpost["isApproved"] = false
        }*/
        
        
        
        
        
        ////ln(userpost["isPosted"]!)
        
        
        var rowHeight:CGFloat = 380.0
        
        if(userpost["postType"] as! String == "text")
        {
        let postText = userpost["postData"] as! String
        
        /*var charCount: CGFloat = CGFloat(count(postText))
        
        var trowCount: CGFloat = (charCount/18)+2*/
        
        let senderMessageTemp = UITextView()
        
        senderMessageTemp.frame.size.width = self.view.frame.width-self.view.frame.width*(40.0/320)
        senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
        
        senderMessageTemp.text = postText
        
        //senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20)
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 8
        let attributes = [NSParagraphStyleAttributeName : style]
        
        
        var attrs = [
        NSFontAttributeName : UIFont.systemFontOfSize(20.0),
        NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
        NSUnderlineStyleAttributeName : 1]
        
        
        senderMessageTemp.attributedText = NSAttributedString(string: postText, attributes:attributes)
        senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20.0)
        
        let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
        var frame = senderMessageTemp.frame
        frame.size.height = contentSize.height
        senderMessageTemp.frame = frame
        
        //ln(rowHeight)
        
        rowHeight = senderMessageTemp.frame.height + (0.120625 * self.view.frame.height)+((20.0/568)*self.view.frame.height)
        //rowHeight = senderMessageTemp.frame.height + 500
        /*if( trowCount < 11)
        {
        rowHeight = 380.0 - (320.0-(trowCount*29.0))
        }*/
        }
        else
        {
        //ln(userpost["postHeight"])
        let rheight = userpost["postHeight"] as! CGFloat
        let rwidth = userpost["postWidth"] as! CGFloat
        
        //ln("height: \(rheight)")
        
        rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+((45.0/568)*self.view.frame.height)
        }
        
        if i == 0
        {
        rowHeight = rowHeight+((30.0/568)*self.view.frame.height)
        }
        
        
        if isPosted == "0"
        {
        rowHeight = rowHeight + 30
        myRowHeights.insert(rowHeight, atIndex: 0)
        }
        else
        {
        myRowHeights.append(rowHeight)
        
        }
        i++
        }
        }
        
        resultSet.close()
        //ln(myRowHeights)
        //rahul  self.tableView.reloadData()
        /*}
        else
        {
        var query = PFQuery(className:"EventImages")
        
        var currentEventId = currentEvent.objectId as String!
        //ln(currentEventId)
        
        query.whereKey("eventObjectId", equalTo:"\(currentEventId)")
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
        
        
        }*/*/
    }
    
    
    func downloadData()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId = '\(currentEvent.objectId!)' GROUP BY objectId ORDER BY eventImageId DESC", whereFields: [])
        
        var postObjectIds: Array<String>
        
        postObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                postObjectIds.append(resultSet.stringForColumn("objectId"))
            }
        }
        
        resultSet.close()
        
        
        let postObjectIdsString = postObjectIds.joinWithSeparator("','")
        
        //ln("Ids: \(postObjectIdsString)")
        
        var postObjectIdsStringPredicate = ""
        
        if postObjectIdsString == ""
        {
            postObjectIdsStringPredicate = "eventObjectId = '\(currentEvent.objectId!)'"
        }
        else
        {
            postObjectIdsStringPredicate = "NOT (objectId IN {'\(postObjectIdsString)'}) AND eventObjectId = '\(currentEvent.objectId!)'"
        }
        
        let predicate = NSPredicate(format: postObjectIdsStringPredicate)
        
        let query = PFQuery(className:"EventImages", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
        
        let resultSetLikes: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["*"], whereString: "eventObjectId = '\(currentEvent.objectId!)'", whereFields: [])
        
        var likeObjectIds: Array<String>
        
        likeObjectIds = []
        
        if (resultSetLikes != nil) {
            while resultSetLikes.next() {
                likeObjectIds.append(resultSetLikes.stringForColumn("objectId"))
            }
        }
        
        resultSetLikes.close()
        
        
        let likeObjectIdsString = likeObjectIds.joinWithSeparator("','")
        
        //ln("Ids: \(likeObjectIdsString)")
        
        let likePredicate = NSPredicate(format: "NOT (objectId IN {'\(likeObjectIdsString)'}) AND eventObjectId = '\(currentEvent.objectId!)'")
        
        let likeQuery = PFQuery(className:"PostLikes", predicate: likePredicate)
        
        ParseOperations.instance.fetchData(likeQuery, target: self, successSelector: "fetchAllLikesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllLikesError:", errorSelectorParameters:nil)
        
    }
    
    
    func deleteData()
    {
        let updatePredicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)'")
        
        let updateQuery = PFQuery(className:"EventImages", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchExistingPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchExistingPostsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchExistingPostsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        //ln("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects
        {
            
            var i = 0
            
            var existingPostObjectIds: Array<String>
            existingPostObjectIds = []
            
            for postObject in fetchedobjects
            {
                existingPostObjectIds.append(postObject.objectId!)
            }
            
            
            let existingPostObjectIdsString = existingPostObjectIds.joinWithSeparator("','")
            
            var whereQuery = ""
            
            if existingPostObjectIdsString != ""
            {
                whereQuery = "eventObjectId = '\(currentEvent.objectId!)' AND objectId NOT IN ('\(existingPostObjectIdsString)') AND objectId != ''"
            }
            else
            {
                whereQuery = "eventObjectId = '\(currentEvent.objectId!)' AND objectId != ''"
            }
            
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["objectId"], whereString: whereQuery, whereFields: [])
            
            
            var nonExistingPostObjectIds: Array<String>
            nonExistingPostObjectIds = []
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    nonExistingPostObjectIds.append(resultSet.stringForColumn("objectId"))
                }
            }
            
            resultSet.close()
            
            let nonExistingPostObjectIdsString = nonExistingPostObjectIds.joinWithSeparator("','")
            
            if nonExistingPostObjectIdsString != ""
            {
                ModelManager.instance.deleteTableData("EventImages", whereString: "objectId IN ('\(nonExistingPostObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("EventLikes", whereString: "postObjectId IN ('\(nonExistingPostObjectIdsString)')", whereFields: [])
                
                refreshList()
            }
            
        }
    }
    
    
    func fetchExistingPostsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //ln("Error: \(error) \(error.userInfo!)")
    }
    
    
    func fetchAllLikesSuccess(timer:NSTimer)
    {
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if let fetchedobjects = objects {
            var i=0;
            for object in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventObjectId"] = object["eventObjectId"] as? String
                tblFields["postObjectId"] = object["postObjectId"] as? String
                tblFields["userObjectId"] = currentUserId
                tblFields["objectId"] = object.objectId!
                tblFields["isUpdated"] = "1"
                
                
                _ = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
            }
            
            self.refreshList()
        }
    }
    
    func fetchAllLikesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //ln("Error: \(error) \(error.userInfo!)")
    }
    
    
    func refreshContent()
    {
        
        ////ln("premessage")
        
        if isMyeventStreamUpdated
        {
            
            ////ln("postmessage")
            
            unreadMessagesCount++
        }
        updateBadge()
        //}
        
    }
    
    func updateBadge()
    {
        isMyeventStreamUpdated = false
        
        
        
        if unreadMessagesCount == 0
        {
            unreadCount.hidden = true
        }
        else
        {
            
            unreadCount.hidden = false
            unreadCount.text = "\(unreadMessagesCount)"
            unreadCount.textAlignment = .Center
            unreadCount.font = UIFont(name: "AvenirNext-Regular", size: 10.0)
            
            
            
            if unreadMessagesCount > 0
            {
                unreadCount.layer.masksToBounds = true
                unreadCount.layer.cornerRadius = 3.0
                //unreadCount.text = "\(totalChatCount)"
                unreadCount.backgroundColor = UIColor.redColor()
                unreadCount.textColor = UIColor.whiteColor()
                let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
                
                if unreadMessagesCount < 10
                {
                    // unreadCount.frame.size.width = CGFloat(0.08333*self.view.frame.height)
                    unreadCount.frame.size.width = CGFloat(20)
                    
                    unreadCount.layer.cornerRadius = unreadCount.frame.size.width/2
                }
                else if unreadMessagesCount < 100
                {
                    unreadCount.frame.size.width = CGFloat(labelWidth+14)
                    unreadCount.layer.cornerRadius = 10
                }
                else
                {
                    unreadCount.frame.size.width = CGFloat(labelWidth+24)
                    unreadCount.layer.cornerRadius = 10
                }
                
                //unreadCount.frame.origin.x = (0.70875*self.view.frame.width)+((0.20*self.view.frame.width/2)-(unreadCount.frame.width/2))
                
                unreadCount.frame.size.height = CGFloat(20)
            }
            else
            {
                unreadCount.text = ""
            }
            unreadCount.textAlignment = .Center
        }
    }
    
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    override func viewWillDisappear(animated: Bool)
    {
        j = 0
        
    }
    
    @IBAction func closeEventDetailsButtonClicked(sender : AnyObject)
    {
        isOnMyEventStream = false
        
        if i == 0
        {
            i++
            self.navigationController?.popViewControllerAnimated(true)
        }
        
        
        
    }
    
    @IBAction func backButton(sender: AnyObject)
    {
        isOnMyEventStream = false
        
        if j == 0
        {
            j++
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func addCommentButtonClicked(sender : AnyObject)
    {
        //ln(currentEvent)
        
        /*  isOnMyEventStream = false
        let addCommentVC = self.storyboard!.instantiateViewControllerWithIdentifier("chatvc") as! MessagesViewController
        
        addCommentVC.isShared = false
        
        self.navigationController?.pushViewController(addCommentVC, animated: true)
        */
        
        let addCommentVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddCommentsViewController") as! AddCommentsViewController
        
        addCommentVC.isShared = false
        
        if isRSVP == "true"
        {
            addCommentVC.isRSVP = true
        }
        else
        {
            addCommentVC.isRSVP = false
        }
        
        self.navigationController?.pushViewController(addCommentVC, animated: true)
        
    }
    
    
    @IBAction func addTextButtonClicked(sender : AnyObject)
    {
        isOnMyEventStream = false
        let temmp = NSDictionary()
        myNewPost = PFObject(className:"EventImages")
        myNewPost["postData"] = ""
        myNewpostText = temmp
        var m = Int()
        if arrdata.count < 1
        {
            m = 0
        }
        else{
            m = self.arrdata.valueForKey("count").objectAtIndex(0) as! Int
        }
totalcount = m
        eventTextTitle = "Add Text"
        
        let addTextVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddtextViewController") as! AddtextViewController
        self.navigationController?.pushViewController(addTextVC, animated: false)
    }
    
    @IBAction func inviteFriendsButtonClicked(sender : AnyObject)
    {
        isOnMyEventStream = false
        
        if currentEvent["isRSVP"] as! Bool == true
        {
            let addTextVC = self.storyboard!.instantiateViewControllerWithIdentifier("ManageGuestViewController") as! ManageGuestViewController
            
            self.navigationController?.pushViewController(addTextVC, animated: false)
            
            
        }
        else
        {
            let manageFreinds = self.storyboard!.instantiateViewControllerWithIdentifier("ManageFreindsViewController") as! ManageFreindsViewController
            
            self.navigationController?.pushViewController(manageFreinds, animated: false)
            
            
        }
    }
    
    @IBAction func settingsButtonClicked(sender : AnyObject)
    {
        isOnMyEventStream = false
        
        let EventSettingsView = self.storyboard!.instantiateViewControllerWithIdentifier("EventSettingsViewController") as! EventSettingsViewController
        self.navigationController?.pushViewController(EventSettingsView, animated: false)
    }
    
    
    @IBAction func capture(sender : UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
            //ln("Button capture")
            
            isOnMyEventStream = false
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.Camera;
            imag.mediaTypes = [kUTTypeImage as String,kUTTypeMovie as String]
            imag.allowsEditing = false
            imag.videoMaximumDuration = 30
            
            newMedia = true
            
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
        }
    }
    
    func showMoreOptions(sender: UIButton) {
        //ln("More button clicked")
        if self.arrdata[sender.tag]["postType"] as! String != "Note"
        {
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete", otherButtonTitles: "Share")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
        }
        else
        {
            let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete", otherButtonTitles: "Share","Edit")
            actionSheet.tag = sender.tag
            actionSheet.showInView(self.view)
        }
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        //ln("ActionSheet clickedButtonAtIndex \(buttonIndex)")
        switch (buttonIndex){
        case 0:
            
                if self.arrdata[actionSheet.tag]["postType"] as! String != "Note"
                {
            self.deleteSelectedPost(actionSheet.tag)
                }
                else
                {
                self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)/\(self.arrdata[actionSheet.tag]["keyvalue"])")
               
                    messagessRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentEvent.objectId!)/\(arrdata[actionSheet.tag]["keyvalue"])")
 self.messagesRef.removeValue()
                }
            break
        case 1:
            ("Cancel")
            // Nothing to do. Popup menu closes.
        case 2:
            //ln("Share")
            shareSelectedPost(actionSheet.tag)
            break
        case 3:
            //ln("Share")
            myNewpostText = arrdata[actionSheet.tag] as! NSDictionary
            eventTextTitle = "Edit Text"
            myNewPost = PFObject(className:"EventImages")
            myNewPost["postData"] = ""
            
            let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddtextViewController") as! AddtextViewController
            self.navigationController?.pushViewController(eventPhototsVC, animated: true)
            
            break
        default:
            ("Default")
            // Should not happen here.
        }
    }
    
    // This is to support share content to various social networks, mail, messages, etc.
    func shareSelectedPost(row: Int) {
        let postType = arrdata[row]["postType"] as! String!
        
        //ln("Share Selected Post \(postType) \(row)")
        
        if(postType == "Note") {
            let textPostToShare : String = arrdata[row]["postData"] as! String
            let objectsToShare : [String] = [textPostToShare]
            
            let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
            self.navigationController!.presentViewController(activityVC,
                animated: true,
                completion: nil)
        }
        
        if(postType == "image" || postType == "video") {
            let fileName = arrdata[row]["postData"] as! String
            
            //  let eventFolder = myEventData[row]["eventFolder"] as! String
            
            let eventMediaFilePath = "\(documentDirectory)/\(fileName)"
            
            //ln(eventMediaFilePath)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventMediaFilePath)) {
                
                if (postType == "image") {
                    let shareImage = UIImage(named: eventMediaFilePath)
                    
                    let objectsToShare : [UIImage] = [shareImage!]
                    
                    let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
                    self.navigationController!.presentViewController(activityVC, animated: true, completion: nil)
                    
                } else {
                    let videoUrl = NSURL(fileURLWithPath: eventMediaFilePath)
                    
                    let objectsToShare : [NSURL] = [videoUrl]
                    
                    let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
                    self.navigationController!.presentViewController(activityVC, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    func deleteSelectedPost(row: Int)
    {
        
       
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)/\(arrdata[row]["keyvalue"])")
        self.messagesRef.removeValue()
       // deleteFirebase()
        //arrdata .removeObjectAtIndex(row)
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(2 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
//            self.messagessRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentEvent.objectId!)/\(self.arrdata[row]["keyvalue"])")
//
//            self.messagessRef.removeValue()
            
        }
        
        
        if arrdata[row]["postType"] as! String != "Note"
        {
            let eventFolder = arrdata[row]["eventFolder"] as! String
            let eventFile = arrdata[row]["postData"] as! String
            
            let deleteRequest = AWSS3DeleteObjectRequest()
            
            
            deleteRequest.bucket = "eventnode1"
            deleteRequest.key = "\(eventFolder)\(eventFile)"
            
            let s3 = AWSS3.defaultS3()
            
            
            
            s3.deleteObject(deleteRequest).continueWithBlock {
                (task: AWSTask!) -> AnyObject! in
                
                if(task.error != nil){
                    //ln("not deleted cropped")
                    
                }else{
                    
                   // self.messagesRef.removeValue()
                    self.loaderView.hidden = true
                    //ln("deleted cropped")
                }
                return nil
            }
        }
        
        
        
        
        /*    if( myEventData[row]["isPosted"] as! Bool == false && myEventData[row].objectId == nil)
        {
        let isDeleted = ModelManager.instance.deleteTableData("EventImages", whereString: "eventImageId=?", whereFields: [myEventData[row]["eventImageId"]!])
        myEventData.removeAtIndex(row)
        myRowHeights.removeAtIndex(row)
        
        tableView.reloadData()
        
        //ln("dwde")
        }
        else
        {
        let timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("deletePostFromParse:"), userInfo: myEventData[row], repeats: false)
        }*/
    }
    
    func deletePostFromParse(timer: NSTimer)
    {
        if MyReachability.isConnectedToNetwork()
        {
            let eventToBeDeleted: PFObject = timer.userInfo as! PFObject
            ParseOperations.instance.deleteData(eventToBeDeleted, target: self, successSelector: "deletePostSuccess:", successSelectorParameters: nil, errorSelector: "deletePostError:", errorSelectorParameters:nil)
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Post cannot be deleted as you seem to be offline. Please check your networks settings.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                self.loaderView.hidden=true
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
            tableView.reloadData()
        }
    }
    
    
    func likeSelectedPost(sender : UIButton)
    {
        
    }
    
    
    func deletePostSuccess(timer:NSTimer)
    {
        
        let eventToBeDeleted: PFObject = timer.userInfo?.valueForKey("internal") as! PFObject
        
        let isDeleted = ModelManager.instance.deleteTableData("EventImages", whereString: "eventImageId=?", whereFields: [eventToBeDeleted["eventImageId"]!])
        
        refreshList()
        
        if eventToBeDeleted["postType"] as! String != "text"
        {
            let eventFolder = eventToBeDeleted["eventFolder"] as! String
            let eventFile = eventToBeDeleted["postData"] as! String
            
            let deleteRequest = AWSS3DeleteObjectRequest()
            
            
            deleteRequest.bucket = "eventnode1"
            deleteRequest.key = "\(eventFolder)\(eventFile)"
            
            let s3 = AWSS3.defaultS3()
            
            
            
            s3.deleteObject(deleteRequest).continueWithBlock {
                (task: AWSTask!) -> AnyObject! in
                
                if(task.error != nil){
                    //ln("not deleted cropped")
                    
                }else{
                    
                    //ln("deleted cropped")
                }
                return nil
            }
        }
    }
    
    func deletePostError(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        //  var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        let refreshAlert = UIAlertController(title: "Error", message: "Something went wrong.", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
            self.loaderView.hidden=true
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
        tableView.reloadData()
        
        //ln("error occured \(error.description)")
    }
    
    
    func playSelectedVideo(sender : UIButton) {
        let videoUrl: String = (arrdata[sender.tag]["postData"] as? String)!
        
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath("\(documentDirectory)/\(videoUrl)"))
        {
            moviePlayer?.contentURL = NSURL(fileURLWithPath: "\(documentDirectory)/\(videoUrl)")
            moviePlayer?.view.hidden = false
            moviePlayer!.setFullscreen(true, animated: true)
            //moviePlayer!.scalingMode = .AspectFill
            moviePlayer!.controlStyle = .Embedded
            moviePlayer!.play()
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Video doesn't exist.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                self.loaderView.hidden=true
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func addMediaButtonClicked(sender : AnyObject){
        
        // TODO(geetikak, dimpal): Why are we not checking for SavedPhotosAlbum ?
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.SavedPhotosAlbum){
            //ln("Button capture")
            //self.loaderView.hidden = false
            
            let imagePicker = UIImagePickerController()
            
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            imagePicker.mediaTypes = [kUTTypeImage as String,kUTTypeMovie as String]
            imagePicker.allowsEditing = false
            imagePicker.videoMaximumDuration = 30
            
            self.presentViewController(imagePicker, animated: true,
                completion: nil)
            newMedia = false
            
        }
        else
        {
            loaderView.hidden=true
        }
    }
    func senditMessage(text: String!, imageData: UIImage!) {
        
        var m = Int()
        if arrdata.count < 1
        {
            m = 0
        }
        else{
            m = self.arrdata.valueForKey("count").objectAtIndex(0) as! Int
        }
        let date = NSDate()
        let currentTimeStamp = String(Int64(date.timeIntervalSince1970*1000))
        // *** STEP 3: ADD A MESSAGE TO FIREBASE
        // sendMessageparse(text)
        messagesRef.childByAutoId().setValue([
            "postData":eventLogoFile,
            "postHeight":String(format: "%d", (imageData?.size.height)!),
            "postWidth":String(format: "%d", (imageData?.size.width)!),
            "postType" :"image",
            "eventObjectId" : currentEvent.objectId!,
            "eventFolder" : "\(self.currentUserId)/\(currentEvent.objectId!)/",
            "count" : m+1,
            "timestamp":currentTimeStamp
           
            
            
            ])
    }
    func resizeImage(image: UIImage) -> UIImage {
        
        var newWidth =  image.size.width
        var newHeight = image.size.height

        if newWidth > newHeight
        {
        if image.size.width > 800
        {
            
            let tempx = (image.size.width)/800
            newWidth = 800
            newHeight = (image.size.height) / tempx
            
        }
        }
        else
        {
        if image.size.height > 800
        {
            
            let tempx = (image.size.height)/800
            newHeight = 800
            newWidth = (image.size.width) / tempx
            
        }

        }
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        NSLog("Success.")
        self.loaderView.hidden = true
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        NSLog(mediaType)
        self.dismissViewControllerAnimated(true, completion: nil)
        
        var uploadAllow: Bool = true
        
        let date = NSDate()
        let currentTimeStamp = String(Int64(date.timeIntervalSince1970*1000))
        
        
        // let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        
        
        if mediaType == (kUTTypeImage as String)
        {
            //ln("image")
            
            let tempimage = resizeImage((info[UIImagePickerControllerOriginalImage] as? UIImage)!)
            // let imageData = info[UIImagePickerControllerOriginalImage] as? UIImage
            let imageData = tempimage
            let heightimage = imageData.size.height
            let widthimage = imageData.size.width

            
            
            
            if newMedia
            {
                UIImageWriteToSavedPhotosAlbum(imageData, self, Selector(), UnsafeMutablePointer<Void>())
            }
            
            eventLogoFile = "\(currentTimeStamp)_\(currentUserId)_eventstreamphoto.png"
            
            myNewPost = PFObject(className:"EventImages")
            myNewPost["postData"] = eventLogoFile
            myNewPost["postHeight"] = heightimage
            myNewPost["postWidth"] = widthimage
            myNewPost["postType"] = "image"
            myNewPost["eventObjectId"] = currentEvent.objectId!
            
            myNewPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
            
            
            eventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(eventLogoFile))
            
            
            //            var scaleSize : CGFloat = 0.2f
            //
            //            UIImage *smallImage = [UIImage imageWithCGImage:image.CGImage
            //                scale:scaleSize
            //                orientation:image.imageOrientation];
            //            var scaleSize: CGFloat = 0.2
            
            let cropdata = UIImageJPEGRepresentation(self.correctlyOrientedImage(tempimage), 0.3)
            //            var smallImage: UIImage = UIImage.imageWithCGImage(image.CGImage, scale: scaleSize, orientation: image.imageOrientation)
            
            let date = NSDate()
            let timestamp = String(Int64(date.timeIntervalSince1970*1000))
            
            
            var isrsvp = ""
            
            if currentEvent["isRSVP"] as! Bool == true
            {
                isrsvp = "true"
                
            }
            else
            {
                isrsvp = "false"
            }
            
            let result = cropdata!.writeToURL(eventLogoFileUrl!, atomically: true)
            messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
            var m = Int()
            if arrdata.count < 1
            {
                m = 0
            }
            else{
                m = self.arrdata.valueForKey("count").objectAtIndex(0) as! Int
            }
            messagesRef.childByAutoId().setValue([
                "postData":eventLogoFile,
                "postHeight":String(format: "%.0f", Double(heightimage)),
                "postWidth":String(format: "%.0f", Double(widthimage)),
                "postType" :"image",
                "userObjectId" : currentUserId,
                "eventFolder" : "\(self.currentUserId)/\(currentEvent.objectId!)/",
                "count" : m+1,
                "timestamp" : "\(currentTimeStamp)",
                "isRSVP": isrsvp
                
                
                ])
            
        }
        
        
        if mediaType == (kUTTypeMovie as String)
        {
            //ln("movie")
            
            //ln(info)
            
            var videoURL = NSURL()
            
            if var videoURLTemp = info[UIImagePickerControllerMediaURL] as? NSURL
            {
                videoURL = videoURLTemp
            }
            else
            {
                
                if var videoURLTemp = info[UIImagePickerControllerReferenceURL] as? NSURL
                {
                    videoURL = videoURLTemp
                }
                else
                {
                    uploadAllow = false
                    self.loaderView.hidden = true
                    let refreshAlert = UIAlertController(title: "Error", message: "The video you selected needs to be fully downloaded into your device before it can be added.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                        
                    }))
                    
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                }
            }
            
            if uploadAllow == true
            {
                let videoURLString = videoURL.relativePath
                
                let splitExt = videoURLString!.componentsSeparatedByString(".")
                
                let ext = splitExt[splitExt.count-1]
                
                //ln(ext)
                
                let assetOptions = [AVURLAssetPreferPreciseDurationAndTimingKey : 1]
                let videoAsset = AVURLAsset(URL: videoURL, options: assetOptions)
                let error:NSError?
                
                var videoAssetReader: AVAssetReader!
                do {
                    videoAssetReader = try AVAssetReader(asset: videoAsset)
                } catch var error1 as NSError {
                    error = error1
                    videoAssetReader = nil
                }
                
                let duration = CMTimeGetSeconds(videoAsset.duration)
                //ln(videoAsset.duration)
                //ln(duration)
                
                if(duration<=31)
                {
                    if newMedia
                    {
                        UISaveVideoAtPathToSavedPhotosAlbum(videoURLString!, self, Selector(), UnsafeMutablePointer<Void>())
                    }
                    
                    if let videoDataTemp: NSData = NSData(contentsOfURL: videoURL)
                    {
                        let videoData: NSData = NSData(contentsOfURL: videoURL)!
                        
                        eventLogoFile = "\(currentTimeStamp)_\(currentUserId)_eventstreamvideo.\(ext)"
                        eventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(eventLogoFile))
                        
                        let result = videoData.writeToURL(eventLogoFileUrl!, atomically: true)
                        
                        
                        let videoFrame = getVideoFrame(eventLogoFileUrl)
                        
                        
                        myNewPost = PFObject(className:"EventImages")
                        myNewPost["postData"] = eventLogoFile
                        myNewPost["postHeight"] = 100
                        myNewPost["postWidth"] = 100
                        
                        myNewPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
                        
                        myNewPost["postType"] = "video"
                        myNewPost["eventObjectId"] = currentEvent.objectId!
                        
                        let date = NSDate()
                        let currentTimeStamp = String(Int64(date.timeIntervalSince1970*1000))
                        
                        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
                        var tempheiht = videoFrame.height
                        var tempwidth = videoFrame.width
                        if tempheiht == 0
                        {
                            tempheiht = 2848
                            tempwidth = 4288
                        }
                        var m = Int()
                        if arrdata.count < 1
                        {
                            m = 0
                        }
                        else
                        {
                            m = self.arrdata.valueForKey("count").objectAtIndex(0) as! Int
                        }
                        
                        var isrsvp = ""
                        if currentEvent["isRSVP"] as! Bool == true
                        {
                            isrsvp = "true"
                        }
                        else
                        {
                            isrsvp = "false"
                        }

                        messagesRef.childByAutoId().setValue([
                            "postData":eventLogoFile,
                            "postHeight":"\(tempheiht)",
                            "postWidth":"\(tempwidth)",
                            "postType" :"video",
                            "userObjectId" : currentUserId,
                            "eventFolder" : "\(self.currentUserId)/\(currentEvent.objectId!)/",
                            "count" : m+1,
                            "timestamp" : "\(currentTimeStamp)",
                            "isRSVP" : isrsvp
                            ])
                    }
                    else
                    {
                        
                        uploadAllow = false
                        self.loaderView.hidden = true
                        let refreshAlert = UIAlertController(title: "Error", message: "The video you selected needs to be fully downloaded into your device before it can be added.", preferredStyle: UIAlertControllerStyle.Alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                            
                        }))
                        
                        self.presentViewController(refreshAlert, animated: true, completion: nil)
                    }
                }
                else
                {
                    uploadAllow = false
                    self.loaderView.hidden = true
                    let refreshAlert = UIAlertController(title: "Error", message: "Videos must be 30 seconds or less.", preferredStyle: UIAlertControllerStyle.Alert)
                    
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                        
                    }))
                    
                    self.presentViewController(refreshAlert, animated: true, completion: nil)
                }
                
            }
            
        }
        
        if uploadAllow
        {
            var tblFields: Dictionary! = [String: String]()
            
            tblFields["postData"] = myNewPost["postData"] as? String
            
            //let postHeight = myNewPost["postHeight"] as! CGFloat
            // tblFields["postHeight"] = "\(postHeight)"
            
            // let postWidth = myNewPost["postWidth"] as! CGFloat
            // tblFields["postWidth"] = "\(postWidth)"
            
            tblFields["eventObjectId"] = myNewPost["eventObjectId"] as? String
            
            let postType = myNewPost["postType"] as! String
            tblFields["postType"] = "\(postType)"
            
            tblFields["eventFolder"] = myNewPost["eventFolder"] as? String
            tblFields["isPosted"] = "0"
            tblFields["objectId"] = ""
            
            let insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
            if insertedId>0
            {
                refreshList()
                
                myNewPost["eventImageId"] = insertedId
                myNewPost["isUploading"] = true
                
                let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                
                uploadRequest.bucket = "eventnode1"
                uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                
                uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(eventLogoFile)"
                uploadRequest.body = eventLogoFileUrl
                upload(uploadRequest, insertedId: insertedId, postToBeUploaded: myNewPost)
            }
            else
            {
                self.loaderView.hidden = true
                Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
            }
            
        }
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.loaderView.hidden = true
        picker .dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int, postToBeUploaded: PFObject) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
                            })
                            break;
                            
                        default:
                            //ln("upload() failed: [\(error)]")
                            self.loaderView.hidden=true
                            self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
                            break;
                        }
                    } else {
                        //ln("upload() failed: [\(error)]")
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
                    }
                } else {
                    //ln("upload() failed: [\(error)]")
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
                }
            }
            
            if let exception = task.exception {
                //ln("upload() failed: [\(exception)]")
                self.loaderView.hidden=true
                self.internetError(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    ParseOperations.instance.saveData(postToBeUploaded, target: self, successSelector: "createPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createPostError:", errorSelectorParameters:postToBeUploaded)
                    
                })
            }
            return nil
        }
    }
    
    func fetchAllPostsSuccess(timer:NSTimer)
    {
        
        self.loaderView.hidden = true
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        isPostUpdated = false
        //ln("Successfully retrieved \(objects!.count) posts.")
        
        if let fetchedobjects = objects {
            
            self.loaderView.hidden=true
            var i=0;
            for post in fetchedobjects
            {
                
                /* var rowHeight:CGFloat = 380.0
                
                if(post["postType"] as! String == "text")
                {
                var postText = post["postData"] as! String
                
                /*var charCount: CGFloat = CGFloat(count(postText))
                
                var trowCount: CGFloat = (charCount/18)+2*/
                
                var senderMessageTemp = UITextView()
                
                senderMessageTemp.frame.size.width = self.view.frame.width-20
                senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
                
                senderMessageTemp.text = postText
                
                senderMessageTemp.font = UIFont(name: "AvenirNext-Medium", size: 12)
                
                let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
                var frame = senderMessageTemp.frame
                frame.size.height = contentSize.height
                senderMessageTemp.frame = frame
                
                //ln(rowHeight)
                
                rowHeight = contentSize.height + (0.120625 * self.view.frame.height)
                }
                else
                {
                var rheight = post["postHeight"] as! CGFloat
                var rwidth = post["postWidth"] as! CGFloat
                
                rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+60.0
                }
                
                if i == 0
                {
                rowHeight = rowHeight+30.0
                }
                */
                //myEventData[i]["isPosted"] = true
                
                //myRowHeights.append(rowHeight)
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["postData"] = post["postData"] as? String
                
                tblFields["isApproved"] = "0"
                
                let postHeight = post["postHeight"] as! CGFloat
                let postWidth = post["postWidth"] as! CGFloat
                
                tblFields["postHeight"] = "\(postHeight)"
                tblFields["postWidth"] = "\(postWidth)"
                tblFields["eventObjectId"] = post["eventObjectId"] as? String
                
                tblFields["eventFolder"] = post["eventFolder"] as? String
                
                tblFields["postType"] = post["postType"] as? String
                
                tblFields["objectId"] = post.objectId
                tblFields["isPosted"] = "1"
                
                var date = ""
                
                if post.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.createdAt)!)
                    //ln(date)
                    tblFields["createdAt"] = date
                }
                
                if post.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.updatedAt)!)
                    //ln(date)
                    tblFields["updatedAt"] = date
                }
                
                let insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
                
                if insertedId>0
                {
                    //myEventData[i]["eventImageId"] = insertedId
                    //ln("Record inserted at \(insertedId).")
                }
                else
                {
                    self.loaderView.hidden = true
                    //ln("Error in inserting record.")
                }
                
                i++;
                
                
            }
            activityIndicatorView.stopAnimation()
            
            //self.tableView.reloadData()
            self.refreshList()
        }
    }
    
    func fetchAllPostsError(timer:NSTimer)
    {

        activityIndicatorView.stopAnimation()
        
        self.loaderView.hidden = true
        //  var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //ln("Error: \(error) \(error.userInfo!)")
    }
    
    func createPostSuccess(timer:NSTimer)
    {
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        let postId = timer.userInfo?.valueForKey("external") as! Int!
        
        self.loaderView.hidden=true
        isPostUpdated = true
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            //ln(date)
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            //ln(date)
            tblFields["updatedAt"] = date
        }
        
        
        let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            //ln("Record Updated Successfully")
            //ln("eventImage")
        } else {
            //ln("Record not Updated Successfully")
        }
        
        
        let predicate = NSPredicate(format: "eventObjectId = '\(currentEvent.objectId!)' AND isApproved = true")
        
        let query = PFQuery(className:"Invitations", predicate: predicate)
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllInvitationsSuccess:", successSelectorParameters: eventObject, errorSelector: "fetchAllInvitationsError:", errorSelectorParameters:nil)
        
        
        self.refreshList()
    }
    
    func createPostError(timer:NSTimer)
    {
         self.loaderView.hidden = true
        // var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        
        let eventObject: PFObject = timer.userInfo?.valueForKey("external") as! PFObject
        
        //ln("error")
        
        let refreshAlert = UIAlertController(title: "Error", message: "There’s bad or no internet connection. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
        self.refreshList()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction) in
        self.loaderView.hidden=false
        if eventObject["postType"] as! String == "text"
        {
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createPostSuccess:", successSelectorParameters: eventObject["eventImageId"] as! Int, errorSelector: "createPostError:", errorSelectorParameters:eventObject)
        }
        else
        {
        let imageName = eventObject["postData"] as! String
        
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        let eventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(imageName))
        
        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead

        uploadRequest.bucket = "eventnode1"
        uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(imageName)"
        uploadRequest.body = eventLogoFileUrl
        self.upload(uploadRequest, insertedId: eventObject["eventImageId"] as! Int, postToBeUploaded: eventObject)
        }
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    
    var Timestamp: NSTimeInterval {
        return NSDate().timeIntervalSince1970 * 1000
    }
    func fetchAllInvitationsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        let postObject = timer.userInfo?.valueForKey("external") as! PFObject
        
        //ln("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            var eventFolder = currentEvent["eventFolder"] as! String!
            var eventImage = currentEvent["eventImage"] as! String!
            
            var eventType = "online"
            if currentEvent["isRSVP"] as! Bool == true
            {
                eventType = "rsvp"
            }
            
            var fetchedUserObjectIds: Array<String>
            fetchedUserObjectIds = []
            var notificationObjects = [PFObject]()
            
            var fetchedUserEmailIds: Array<String>
            fetchedUserEmailIds = []
            
            let eventTitle = currentEvent["eventTitle"] as! String
            let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            let postType = postObject["postType"] as! String
            
            var notifMessage = ""
            
            if currentEvent["isRSVP"] as! Bool == true
            {
                if postType == "video"
                {
                    notifMessage = "\(fullUserName) posted a new video to the event, \(eventTitle)"
                }
                else
                {
                    notifMessage = "\(fullUserName) posted a new photo to the event, \(eventTitle)"
                }
                
            }
            else
            {
                if postType == "video"
                {
                    notifMessage = "\(fullUserName) posted a new video to the channel, \(eventTitle)"
                }
                else
                {
                    notifMessage = "\(fullUserName) posted a new photo to the channel, \(eventTitle)"
                }
            }
            var eventCreatorId = ""
            
            var i = 0
            
            for invitation in fetchedobjects
            {
                if invitation["userObjectId"] as! String != ""
                {
                    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadfirebaseges/\(currentEvent.objectId!)/\(invitation["userObjectId"] as! String)")
                    let abc = String(format: "%.0f", Timestamp)
                    if currentUserId != invitation["userObjectId"] as! String
                    {
                        
                        let date = NSDate()
                        let timestamp = String(Int64(date.timeIntervalSince1970*1000))
                        
                    messagesRef.childByAutoId().setValue([
                        "SenderID":self.currentUserId,
                        "timestamp" :abc,
                         "eventid" :currentEvent.objectId!,
                        
                        ])
                        
                    }
                    fetchedUserObjectIds.append(invitation["userObjectId"] as! String)
                    fetchedUserEmailIds.append(invitation["emailId"] as! String)
                    
                    let notificationObject = PFObject(className: "Notifications")
                    notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
                    notificationObject["notificationImage"] = "profilePic.png"
                    notificationObject["senderId"] = currentUserId
                    notificationObject["receiverId"] = invitation["userObjectId"] as! String
                    notificationObject["notificationActivityMessage"] = notifMessage
                    notificationObject["eventObjectId"] = currentEvent.objectId!
                    notificationObject["notificationType"] = "postnewcontent"
                    
                    notificationObjects.append(notificationObject)
                }
                
                fetchedobjects[i]["isEventStreamUpdated"] = true
                fetchedobjects[i]["isTextUpdated"] = false
                
                i++
                
            }
            
            
            PFObject.saveAllInBackground(fetchedobjects)
            
            
            //            if postType == "image"
            //            {
            //                var newContent = NewContent()
            //
            //                var emailMessage = newContent.emailMessage(currentEvent.objectId!, eventTitle: eventTitle, hostName: fullUserName, imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: eventType)
            //
            //
            //                var sendEmailObject = SendEmail()
            //
            //                sendEmailObject.sendEmail("\(fullUserName) posted a new photo for the event, \(eventTitle)", message: emailMessage, emails: fetchedUserEmailIds)
            //            }
            
            let eventCreatorObjectId = currentUserId
            
            fetchedUserObjectIds.append(eventCreatorObjectId)
            
            //eventCreatorObjectId
            
            PFObject.saveAllInBackground(notificationObjects)
            
            
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            
            let createdAt = dateFormatter.stringFromDate((postObject.createdAt)!)
            
            let updatedAt = dateFormatter.stringFromDate((postObject.updatedAt)!)
            
            let postData =  postObject["postData"] as! String
            
            let postHeight = postObject["postHeight"] as! Double
            let postWidth = postObject["postWidth"] as! Double
            let postFolder = postObject["eventFolder"] as! String
            
            eventCreatorId = currentEvent["eventCreatorObjectId"] as! String
            
            var data: Dictionary<String, String!> = [
                "alert" : "\(notifMessage)",
                "notifType" :  "postnewcontent",
                "objectId" :  postObject.objectId!,
                "eventObjectId" :  currentEvent.objectId!,
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "postHeight": "\(postHeight)",
                "postWidth": "\(postWidth)",
                "eventFolder": "\(postFolder)",
                "postData" : "\(postData)",
                "postType": "\(postType)",
                "badge": "Increment",
                "sound": "default",
                "eventCreatorId" : "\(eventCreatorId)"
            ]
            
            let fetchedUserObjectIdsString = fetchedUserObjectIds.joinWithSeparator("','")
            
            
            var predicateString: String! = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = true"
            
            sendParsePush(predicateString, data: data)
            
            
            data["sound"] = ""
            
            predicateString = "objectId IN {'\(fetchedUserObjectIdsString)'} AND objectId != '\(currentUserId)' AND hostActivityNotification = true AND allowSound = false"
            sendParsePush(predicateString, data: data)
            
            
        }
    }
    
    
    
    func fetchAllInvitationsError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        //ln("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        let predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        let query = PFUser.queryWithPredicate(predicate)
        
        let push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                //ln(objects?.count)
                if let fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        let userObjectId = object.objectId!
                        
                        let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        let query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                //ln(objects?.count)
                                if let fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    let query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }
    
    
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int, postToBeUploaded: PFObject)
    {
        let refreshAlert = UIAlertController(title: "Error", message: "There’s bad or no internet connection. Please check your network settings.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            self.refreshList()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Retry", style: .Default, handler: { (action: UIAlertAction) in
            self.loaderView.hidden=false
            if postToBeUploaded["postType"] as! String == "text"
            {
                ParseOperations.instance.saveData(postToBeUploaded, target: self, successSelector: "createPostSuccess:", successSelectorParameters: postToBeUploaded["eventImageId"] as! Int, errorSelector: "createPostError:", errorSelectorParameters:postToBeUploaded)
            }
            else
            {
                let imageName = postToBeUploaded["postData"] as! String
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                let eventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(imageName))
                
                let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                
                uploadRequest.bucket = "eventnode1"
                uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(imageName)"
                uploadRequest.body = eventLogoFileUrl
                uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                
                self.upload(uploadRequest, insertedId: insertedId, postToBeUploaded: postToBeUploaded)
            }
        }))
        
        
       // self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    private func playVideo(url: NSURL) {
        if let
            moviePlayer = MPMoviePlayerController(contentURL: url) {
                self.moviePlayer = moviePlayer
                
                moviePlayer.view.frame = CGRectMake(self.view.frame.size.width*(20/320),self.view.frame.size.height*(20/568), self.view.frame.size.width*(280/320),self.view.frame.size.height*(400/568))
                
                
                moviePlayer.prepareToPlay()
                moviePlayer.shouldAutoplay = false
                moviePlayer.scalingMode = .AspectFill
                moviePlayer.controlStyle = .None
                self.view.addSubview(moviePlayer.view)
        } else {
            //debug
            //ln("Ops, something wrong when playing video.m4v")
        }
    }
    
    
    func scrollViewDidScroll(_scrollView: UIScrollView){
        /*var newScroll = Int(_scrollView.contentOffset.y)
        if(currentScrollTop <= newScroll){
        if(hideNow > 65){
        headerView.hidden = true
        
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
        self.headerView.frame.origin.y = -70
        }, completion: nil)
        
        }
        hideNow+=Int(_scrollView.contentOffset.y)
        }
        else
        {
        hideNow=0
        headerView.hidden = false
        
        UIView.animateWithDuration(0.3, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
        self.headerView.frame.origin.y = 20
        }, completion: nil)
        
        }
        
        currentScrollTop = Int(_scrollView.contentOffset.y)*/
        
        let newScrollFloat = CGFloat(_scrollView.contentOffset.y)
        
        if newScrollFloat > _scrollView.contentSize.height - _scrollView.frame.height
        {
            refreshList()
        }
        
    }
    
    
    func getrowheight(currentrow: NSInteger) -> CGFloat
    {
        var rowHeight:CGFloat = 380.0
        
        if(arrdata[currentrow]["postType"] as? String == "Note")
        {
            let postText = arrdata[currentrow]["postData"] as! String
            
            /*var charCount: CGFloat = CGFloat(count(postText))
            
            var trowCount: CGFloat = (charCount/18)+2*/
            
            let senderMessageTemp = UITextView()
            
            senderMessageTemp.frame.size.width = self.view.frame.width-self.view.frame.width*(40.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
            
            senderMessageTemp.text = postText
            
            //senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20)
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 8
            let attributes = [NSParagraphStyleAttributeName : style]
            
            
            
            senderMessageTemp.attributedText = NSAttributedString(string: postText, attributes:attributes)
            senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20.0)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            
            //ln(rowHeight)
            
            rowHeight = senderMessageTemp.frame.height + (0.120625 * self.view.frame.height)+((20.0/568)*self.view.frame.height)
            //rowHeight = senderMessageTemp.frame.height + 500
            /*if( trowCount < 11)
            {
            rowHeight = 380.0 - (320.0-(trowCount*29.0))
            }*/
        }
        else
        {
            //ln(userpost["postHeight"])
            let rheiight = (arrdata[currentrow]["postHeight"]) as? String
            let rwiidth = arrdata[currentrow]["postWidth"] as? String
            
            var rheight = CGFloat()
            var rwidth = CGFloat()
            if rheiight != nil
            {
                if let n = NSNumberFormatter().numberFromString(rheiight!) {
                    rheight = CGFloat(n)
                }
                if let ni = NSNumberFormatter().numberFromString(rwiidth!) {
                    rwidth = CGFloat(ni)
                }
                
            }
            else
            {
                rheight = 800
                
                rwidth = 800
                
                
                
            }            //ln("height: \(rheight)")
            if(rheight>0 && rwidth>0)
            {
                rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+((45.0/568)*self.view.frame.height)
            }
            else
            {
                rowHeight = 416.5
            }
        }
        
        if currentrow == 0
        {
            rowHeight = rowHeight+((30.0/568)*self.view.frame.height)
        }
        let screenSize: CGRect = UIScreen.mainScreen().bounds
        if(arrdata[currentrow]["postType"] as? String == "Note")
        {
        if screenSize.height < 600
        {
            rowHeight = rowHeight+8
            }
        }
        return rowHeight
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrdata.count
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
    return 0.01
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        
        let row = section.row
        
        return getrowheight(row)
    }
    
    
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        
        //let messageTimezoneOffset = resultSet.intForColumn("timezoneOffset")
        
        let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
        
        
        // let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        
        
        let nYears = timeDiff / (1000*60*60*24*365)
        
        
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        
        
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        
        
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        //println(nMinutes)
        
        var timeMsg = ""
        
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = "about \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = "about \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = "about \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = "about \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = "about \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }

    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {

        activityIndicatorView.stopAnimation()
        let row = indexPath.row
        
        let postType = arrdata[row]["postType"] as! String!
        
        var cellIdentifier: String! = "EventPhotoCell1"
        
        
        var cell: EventPhotosTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? EventPhotosTableViewCell
        
        
        if (cell == nil)
        {
            cell = EventPhotosTableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: cellIdentifier)
        }
        
        
        for view in cell!.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        var viewTop: CGFloat = 0
        
        if row == 0
        {
            viewTop = (30.0/568)*self.view.frame.height
        }
        
        var heightDiff: CGFloat = 0
        
        let postTextView = UITextView()
        let textBackgroundView = UIView()
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 8
        let attributes = [NSParagraphStyleAttributeName : style]
        
        if self.arrdata.count == 0
        {
            self.onboardingView.hidden = false
             self.activityIndicatorView.hidden = true
            activityIndicatorView.stopAnimation()
        }
        else
        {
            self.onboardingView.hidden = true
        }
        
        print(arrdata[row])
      
        if(postType == "Note")
        {
            let postTextClickableView = UIView()
            
            postTextView.text = arrdata[row]["postData"] as! String
            
            postTextView.attributedText = NSAttributedString(string:postTextView.text, attributes:attributes)
            postTextView.font = UIFont(name: "Tigerlily", size: 20.0)
            postTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            
            postTextView.editable = false
            postTextView.selectable = false
            postTextView.scrollEnabled = false
            
            
            let postText = arrdata[row]["postData"] as! String
            
            
            //  let charCount: CGFloat = CGFloat(postText.characters.count)
            
            //  let postTextHeight: CGFloat = 320
            /*
            var trowCount: CGFloat = (charCount/18) + 2
            
            if( trowCount < 11)
            {
            postTextHeight = trowCount*29
            heightDiff = 320-(trowCount*29)
            }
            */
            /*var charCount: CGFloat = CGFloat(count(postText))
            
            var trowCount: CGFloat = (charCount/18)+2*/
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 8
            let attributes = [NSParagraphStyleAttributeName : style]
            
            
            /*   let attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(20.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]*/
            
            let senderMessageTemp = UITextView()
            
            senderMessageTemp.frame.size.width = cell!.contentView.frame.width-self.view.frame.width*(40.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
            
            senderMessageTemp.text = postText
            
            senderMessageTemp.attributedText = NSAttributedString(string:arrdata[row]["postData"] as! String, attributes:attributes)
            
            //self.view.addSubview(senderMessageTemp)
            
            
            
            //senderMessageTemp.hidden = true
            
            
            senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            postTextView.frame.size.width = cell!.contentView.frame.width-self.view.frame.width*(20.0/320)
            postTextView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop+((20.0/568)*self.view.frame.height), cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height)
            
            postTextView.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 0)
            
            postTextClickableView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop, cell!.contentView.frame.width-self.view.frame.width*(40.0/320)+20, contentSize.height)
            postTextClickableView.frame = CGRectMake(0, viewTop, cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height+50)
            
            
            
            textBackgroundView.frame = CGRectMake(0, viewTop, cell!.contentView.frame.width, contentSize.height+((40.0/568)*self.view.frame.height))
            
            textBackgroundView.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            cell!.contentView.addSubview(textBackgroundView)
            cell!.contentView.addSubview(postTextView)
            cell!.contentView.addSubview(postTextClickableView)
        }
        
        if row == 0
        {
            if(postType == "Note")
            {
                heightDiff = heightDiff-((35.0/568)*self.view.frame.height)
            }
            else
            {
                heightDiff = heightDiff-((30.0/568)*self.view.frame.height)
            }
        }
        let rheiight = (arrdata[row]["postHeight"]) as? String
        let rwiidth = arrdata[row]["postWidth"] as? String
        
        let postImageView = UIImageView()
        var rheight = CGFloat()
        var rwidth = CGFloat()
        postImageView.backgroundColor = UIColor.lightGrayColor()

        if rheiight != nil
        {
            if let n = NSNumberFormatter().numberFromString(rheiight!) {
                rheight = CGFloat(n)
            }
            if let ni = NSNumberFormatter().numberFromString(rwiidth!) {
                rwidth = CGFloat(ni)
            }
            
        }
        else
        {
            rheight = 2000
            
            rwidth = 2000
            
            
        }
        
        if(rheight>0 && rwidth>0)
        {
            
            postImageView.frame.size.width = self.tableView.frame.width
            postImageView.frame.size.height = (rheight/rwidth)*self.tableView.frame.width
            
            postImageView.frame.origin.x = 0
            postImageView.frame.origin.y = viewTop
        }
        
        
        
        if(postType == "image")
        {
            
            let imageName = arrdata[row]["postData"] as! String
            
            let eventFolder = arrdata[row]["eventFolder"] as! String
            
            let eventImagePath = "\(documentDirectory)/\(imageName)"
            
            //ln(eventImagePath)
            
            cell!.contentView.addSubview(postImageView)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventImagePath)) {
                
                let image = UIImage(named: eventImagePath)
                
                postImageView.image = image
                
                //cell!.contentView.addSubview(postImageView)
                
            }
            else
            {
                let s3BucketName = "eventnode1"
                let fileName = imageName
                
                let downloadFilePath = (documentDirectory as NSString).stringByAppendingPathComponent(fileName)
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
                let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                }
                
                
                
                
                let tempstr = String("http://d3a1uesrqnd2ko.cloudfront.net/\(eventFolder)\(fileName)")
                let tempurl = NSURL(string: tempstr)
                postImageView.sd_setImageWithURL(tempurl, completed: block)
                postImageView.backgroundColor = UIColor.lightGrayColor()

                /* transferManager.download(downloadRequest).continueWithSuccessBlock({
                (task: AWSTask!) -> AWSTask! in
                dispatch_async(dispatch_get_main_queue(), {
                var image = UIImage(named: "\(documentDirectory)/\(imageName)")
                
                var postImageView = UIImageView()
                
                postImageView.image = image
                
                cell!.contentView.addSubview(postImageView)
                })
                return nil
                })*/
       
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                //ln("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            //ln("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        //ln("downloading successfull")
                        
                        
                        //let tempurl = NSURL.u
                        let image = UIImage(named: "\(documentDirectory)/\(imageName)")
                        
                        //var postImageView = UIImageView()
                        
                        postImageView.image = image
                        
                        //cell!.contentView.addSubview(postImageView)
                    }
                    
                    return nil
                    
                })
            }
        }
        
        
        if(postType == "video")
        {
            // postImageView.frame.size.width = self.tableView.frame.width
            //postImageView.frame.size.height = 2*self.tableView.frame.width
            
            // postImageView.frame.origin.x = 0
            // postImageView.frame.origin.y = viewTop
            
            cellIdentifier = "EventPhotoCell3"
            
            let videoName = arrdata[row]["postData"] as! String
            
            let eventFolder = arrdata[row]["eventFolder"] as! String
            
            let eventVideoPath = "\(documentDirectory)/\(videoName)"
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventVideoPath)) {
                
                let postPlayButton = UIButton()
                
                postPlayButton.frame = postImageView.frame
                
                postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
                
                postPlayButton.tag = indexPath.row
                postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
                
                
                let videoUrl = NSURL(fileURLWithPath: eventVideoPath)
                self.getFirstFrame(postImageView, videoURL: videoUrl, viewTop: viewTop)
                cell!.contentView.addSubview(postImageView)
                cell!.contentView.addSubview(postPlayButton)
            }
            else
            {
                let s3BucketName = "eventnode1"
                let fileName = videoName
                
                let downloadFilePath = (documentDirectory as NSString).stringByAppendingPathComponent(fileName)
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
               
                
                
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                //ln("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            //ln("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        //ln("downloading successfull")
                        
                        
                        let postPlayButton = UIButton()
                        
                        postPlayButton.frame = postImageView.frame
                        
                        postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
                        postPlayButton.tag = indexPath.row
                        postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
                        
                        let videoUrl = NSURL(fileURLWithPath: eventVideoPath)
                        
                        self.getFirstFrame(postImageView, videoURL: videoUrl, viewTop: viewTop)
                        cell!.contentView.addSubview(postImageView)
                        cell!.contentView.addSubview(postPlayButton)
                    }
                    
                    return nil
                    
                })
            }
        }
        
        /*var pdate: NSDate!
        
        if(myEventData[row].createdAt != nil)
        {
        pdate = myEventData[row].createdAt
        }
        else
        {
        pdate = myEventData[row]["dateCreated"] as! NSDate
        }
        
        //ln(pdate)
        
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .CalendarUnitDay | .CalendarUnitMonth | .CalendarUnitYear, fromDate: pdate!)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        var postDate = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        */
        let infoView = UIView()
        
        if rheight>0 && rwidth>0
        {
            infoView.frame = CGRectMake(0, postImageView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
        }
        else
        {
            infoView.frame = CGRectMake(0, textBackgroundView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
            
        }
        if(postType == "Note")
        {
            if (indexPath.row == 0)
            {
                infoView.frame = CGRectMake(0, textBackgroundView.frame.height-heightDiff-8, cell!.contentView.frame.width, (0.120625)*380)

            }
            else
            {
            infoView.frame = CGRectMake(0, textBackgroundView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
            }
        }
        // Add More Options Button
        let moreOptionsImageView = UIImageView()
        
        moreOptionsImageView.frame = CGRectMake(0, 12, 30, 8)
        
        moreOptionsImageView.image = UIImage(named:"more.png")
        
        let moreButton = UIButton()
        //moreButton.backgroundColor = UIColor.blueColor()
        
        moreButton.addSubview(moreOptionsImageView)
        
        moreButton.frame = CGRectMake(self.view.frame.width - 15 - moreButton.sizeThatFits(moreButton.bounds.size).width, 0, 30,moreButton.sizeThatFits(moreButton.bounds.size).height)
        moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        
        moreButton.tag = indexPath.row
        
        moreButton.addTarget(self, action:"showMoreOptions:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // Add Like Button
        let likeButton = UIButton()
        
        likeButton.setTitle ("", forState: UIControlState.Normal)
        
        likeButton.frame = CGRectMake(15, 0, (0.120625)*380,infoView.frame.height)
        
        likeButton.tag = indexPath.row
        
        likeButton.addTarget(self, action:"likeSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
        
        
        let tempp = arrLikeDic.valueForKey("parentkey") as! NSArray
        
        let bag = NSCountedSet()
        bag.addObjectsFromArray(tempp as [AnyObject])
        let totalLikeCount = bag.countForObject(arrdata[indexPath.row].valueForKey("keyvalue")!)
        // let totalLikeCount = 10
        
        /*rahul    if var postObjId = myEventData[row].objectId
        {
        let resultSetTotalCount: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["count(*) as count"], whereString: "postObjectId=?", whereFields: [myEventData[row].objectId!])
        
        resultSetTotalCount.next()
        
        totalLikeCount = Int(resultSetTotalCount.intForColumn("count"))
        resultSetTotalCount.close()
        }*/
        
        let postLikeText = UILabel()
        
        //postLikeText.frame = CGRectMake(self.view.frame.size.width*(244/320), (infoView.frame.height/2) - (self.view.frame.size.height*(15.5/568)), self.view.frame.size.width*(68/320),self.view.frame.size.height*(31/568))
        
        postLikeText.textAlignment = NSTextAlignment.Right
        postLikeText.text = "\(totalLikeCount) Loved it"
        
        
        postLikeText.textColor = UIColor.grayColor()
        postLikeText.backgroundColor = UIColor.clearColor()
        postLikeText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        postLikeText.textAlignment = .Right
        
        postLikeText.frame = CGRectMake(30, (moreButton.frame.height/2)-(postLikeText.sizeThatFits(postLikeText.bounds.size).height/2), postLikeText.sizeThatFits(postLikeText.bounds.size).width, postLikeText.sizeThatFits(postLikeText.bounds.size).height)
        
        
        
        //ln("likeheight: \(postLikeText.frame.height)")
        
        let likeImageView = UIImageView()
        
        likeImageView.image = UIImage(named:"heart.png")
        
        likeImageView.frame = CGRectMake(15, postLikeText.frame.origin.y+2, postLikeText.frame.height*3/4,postLikeText.frame.height*3/4)
        
        infoView.addSubview(likeImageView)
        infoView.addSubview(likeButton)
        infoView.addSubview(postLikeText)
        
        infoView.addSubview(moreButton)
        
        /*Rahul   if myEventData[indexPath.row][ "isPosted"] as! Bool == false
        {
        let uploadButton = UIButton()
        let uploadButtonLayer = UIButton()
        uploadButton.frame = CGRectMake(self.view.frame.size.width*(137/320),(moreButton.frame.height/2)-(self.view.frame.size.height*(7/568)), self.view.frame.size.width*(14/320),self.view.frame.size.height*(14/568))
        
        uploadButtonLayer.frame = CGRectMake(self.view.frame.size.width*(130/320),self.view.frame.size.height*(0/568), self.view.frame.size.width*(30/320),self.view.frame.size.height*(30/568))
        
        uploadButton.setImage(UIImage(named: "upload.png"), forState: UIControlState.Normal)
        
        uploadButton.tag = indexPath.row
        uploadButtonLayer.tag = indexPath.row
        
        uploadButton.addTarget(self, action:"uploadPost:",forControlEvents: UIControlEvents.TouchUpInside)
        uploadButtonLayer.addTarget(self, action:"uploadPost:",forControlEvents: UIControlEvents.TouchUpInside)
        
        let notUploadedButton = UIButton()
        let notUploadedButtonLayer = UIButton()
        
        notUploadedButton.frame = CGRectMake(self.view.frame.size.width*(175/320),(moreButton.frame.height/2)-(self.view.frame.size.height*(7/568)), self.view.frame.size.width*(14/320),self.view.frame.size.height*(14/568))
        
        
        notUploadedButtonLayer.frame = CGRectMake(self.view.frame.size.width*(175/320),self.view.frame.size.height*(0/568), self.view.frame.size.width*(30/320),self.view.frame.size.height*(30/568))
        
        
        notUploadedButton.setImage(UIImage(named: "not_uploaded.png"), forState: UIControlState.Normal)
        
        notUploadedButton.tag = indexPath.row
        notUploadedButtonLayer.tag = indexPath.row
        
        notUploadedButton.addTarget(self, action:"notUploaded:",forControlEvents: UIControlEvents.TouchUpInside)
        notUploadedButtonLayer.addTarget(self, action:"notUploaded:",forControlEvents: UIControlEvents.TouchUpInside)
        
        infoView.addSubview(uploadButton)
        infoView.addSubview(uploadButtonLayer)
        
        infoView.addSubview(notUploadedButton)
        infoView.addSubview(notUploadedButtonLayer)
        }*/
        
        cell!.contentView.addSubview(infoView)
        
        //var sepImageView = UIImageView()
        let sepImageView = UIView()
        
        let timeLabel = UILabel()
        
        timeLabel.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        timeLabel.textColor = UIColor(red: 145.0/255, green: 145.0/255, blue: 145.0/255, alpha: 1)
        
        timeLabel.textAlignment = .Left
        
        
        if (arrdata[indexPath.row].valueForKey("time") != nil)
        {
            
            let strtemp = arrdata[indexPath.row].valueForKey("time") as! String
            
            let str = timedifference(strtemp)
            
            timeLabel.text = "  \(str) "
            
        }
        
        if(postType == "Note")
        {
            //sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height+((18/568)*self.view.frame.height)), self.view.frame.width,(1/568)*self.view.frame.height)
            let chcktemp = getrowheight(indexPath.row)
            let chcktempint = ((chcktemp - (infoView.frame.origin.y+infoView.frame.height))/2)

            sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height-((11/568)*self.view.frame.height)) , 0.4*self.view.frame.width,1)
            
            timeLabel.frame = CGRectMake(sepImageView.frame.size.width, (infoView.frame.origin.y+infoView.frame.height-(17/568)*self.view.frame.height), 0.5*self.view.frame.width, 15)
            
            print(sepImageView.frame)
            print(timeLabel.text)
            
        }
        else
        {
            sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height-((11/568)*self.view.frame.height)), 0.4*self.view.frame.width,1)
            
            timeLabel.frame = CGRectMake(sepImageView.frame.size.width, (infoView.frame.origin.y+infoView.frame.height-(15/568)*self.view.frame.height), 0.5*self.view.frame.width, 15)
        }
        
        cell!.contentView.addSubview(timeLabel)
        
        
        print(sepImageView.frame)
        
        sepImageView.backgroundColor = UIColor(red: 229.0/255, green: 229.0/255, blue: 229.0/255, alpha: 1)
        
        
        cell!.contentView.addSubview(sepImageView)
        
        
        cell?.selectionStyle = .None
        
        if(indexPath.row == arrdata.count-1)
        {
            sepImageView.hidden = true
        }
        else
        {
            sepImageView.hidden = false
            
        }
        if arrdata.count > 4
        {
        let m = arrdata.valueForKey("count").objectAtIndex(arrdata.count-1) as! Int
                if (indexPath.row == arrdata.count-1 && m > 1)
                {
               // updateFirebase()
                    checkforreload()
                   // view.userInteractionEnabled = false
                }
        }
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let row = indexPath.row
        
        let postType = arrdata[row]["postType"] as! String!
        
        if(postType == "image")
        {
            let manager = NSFileManager.defaultManager()
            
            let eventStreamImageFile = arrdata[row]["postData"] as! String
            
            let eventStreamImagePath = "\(documentDirectory)/\(eventStreamImageFile)"
            
            if (manager.fileExistsAtPath(eventStreamImagePath))
            {
                let image = UIImage(named: eventStreamImagePath)
                let eventPhotoFullScreenVC = self.storyboard?.instantiateViewControllerWithIdentifier("EventPhotoFullScreenViewController") as! EventPhotoFullScreenViewController
                eventPhotoFullScreenVC.image = image
                self.navigationController?.pushViewController(eventPhotoFullScreenVC, animated: false)
            }
            else
            {
                let refreshAlert = UIAlertController(title: "Error", message: "Please wait while the image is downloading.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        }
        
        if(postType == "Note")
        {
            
            myNewpostText = arrdata[row] as! NSDictionary
            eventTextTitle = "Edit Text"
            myNewPost = PFObject(className:"EventImages")
            myNewPost["postData"] = ""

            let eventPhototsVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddtextViewController") as! AddtextViewController
            self.navigationController?.pushViewController(eventPhototsVC, animated: true)
        }
        
    }
    
    func uploadPost(sender: UIButton)
    {
        
        sender.enabled = false
        
        if myNewPost["isUploading"] as? Bool == false
        {
            if myEventData[sender.tag]["postType"] as! String == "text"
            {
                ParseOperations.instance.saveData(myEventData[sender.tag], target: self, successSelector: "createPostSuccess:", successSelectorParameters: myEventData[sender.tag]["eventImageId"] as! Int, errorSelector: "createPostError:", errorSelectorParameters:myEventData[sender.tag])
            }
            else
            {
                let imageName = myEventData[sender.tag]["postData"] as! String
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                let eventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(imageName))
                
                let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                
                uploadRequest.bucket = "eventnode1"
                uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(imageName)"
                uploadRequest.body = eventLogoFileUrl
                uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                
                upload(uploadRequest, insertedId: myEventData[sender.tag]["eventImageId"] as! Int, postToBeUploaded: myEventData[sender.tag])
            }
        }
        
    }
    
    func notUploaded(sender: UIButton)
    {
        self.loaderView.hidden = true
        
        let refreshAlert = UIAlertController(title: "Not Uploaded to Cloud", message: "If you just created this and there’s internet connection, then it’s uploading in the background. If not, try uploading manually.", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
            
        }))
        
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func closeImagePreview(sender: AnyObject!)
    {
        selectedImageWrapper.hidden = true
    }
    
    func getFirstFrame(postImageView: UIImageView, videoURL: NSURL, viewTop: CGFloat)
    {
        let asset : AVAsset = AVAsset(URL:videoURL)
        
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        var error       : NSError? = nil
        let time        : CMTime = CMTimeMake(1, 30)
        var img         : CGImageRef
        var frameImg    : UIImage
        
        do {
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            frameImg = UIImage(CGImage: img)
        } catch let error1 as NSError {
            error = error1
            frameImg = UIImage(named: "eventnode_yellow.png")!
        }
        
        
        postImageView.image = frameImg
        
    }
    
    
    func getVideoFrame(videoURL: NSURL)->CGSize
    {
        let asset : AVAsset = AVAsset(URL:videoURL)
        
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        var error       : NSError? = nil
        let time        : CMTime = CMTimeMake(1, 30)
        var img         : CGImageRef
        var frameImg    : UIImage
        
        do {
            img = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            frameImg = UIImage(CGImage: img)
        } catch let error1 as NSError {
            error = error1
            frameImg = UIImage(named: "eventnode_yellow.png")!
        }
        
        
        return frameImg.size
        
    }
    
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
}
