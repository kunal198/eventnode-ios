//
//  ChannelReminder.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class ChannelInviteReminder
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, type: String, url:String, channelDescription: String,imageUrl: String)-> String
    {
        
        let file = "channel_invite_reminder.html"
        
        
        
        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)
        
        
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        //println(base64String)
        
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("Chandra Kilaru invited you to a Channel", withString:"\(hostName) invited you to a Channel", options: [], range: nil)
        
        
        
       message = message.stringByReplacingOccurrencesOfString("THIS IS CHANNEL DESCRIPTION", withString:"\(channelDescription)", options: [], range: nil)
        
        message = message.stringByReplacingOccurrencesOfString("<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\"http://dcy86hdr5o800.cloudfront.net/image1-online.jpg\" width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", withString:"<td valign=\"top\" class=\"box1\"><img style=\"display:block;\" src=\(imageUrl) width=\"350\" height=\"263\" alt=\"\" class=\"img1\"></td>", options: [], range: nil)
            
            message = message.stringByReplacingOccurrencesOfString("Europe Trip 2014<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by Chandra Kilaru", withString:"\(eventTitle)<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by \(hostName)", options: [], range: nil)
            

            message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Join Channel</a></td>", withString:"<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(url)\" target=\"_blank\">Join Channel</a></td>", options: [], range: nil)

        
        return message
    }
}