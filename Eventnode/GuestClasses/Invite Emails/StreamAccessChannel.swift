//
//  StreamAccess.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class StreamAccessChannel
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String, url:String )-> String
    {
        
        let file = "stream_access_channel.html"
        
        
        
        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)
        
        
        
        //println(path)
        
        
        
        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        //println(base64String)
        
        
        
        
        
        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        
        
        
        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("RRRRRRRRRRRosy's Baby Shower", withString: "\(eventTitle)", options: [], range: nil)
        
        
        
        
        
        message = message.stringByReplacingOccurrencesOfString("Sharon Tucker gave you access to photos/videos/notes for the channel:", withString: "\(hostName) gave you access to the channel:", options: [], range: nil)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("RRRRRRosy’s Baby Showerrrrrr", withString: "\(eventTitle)", options: [], range: nil)

        
        
        
        message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Go to Channel</a></td>", withString:"<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(url)\" target=\"_blank\">Go to Channel</a></td>", options: [], range: nil)
        
        
        
       
        return message
    }
}