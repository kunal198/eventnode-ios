//
//  DeleteChannel.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class DeleteChannel
{
    func emailMessage(eventId: String, eventTitle: String, hostName: String,type: String, url:String)-> String
    {
        
        let file = "delete_channel.html"
        
        
        
        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)
        
        
        
        
        
        
        
        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
        
        
        
        message = message.stringByReplacingOccurrencesOfString("<b>Rosy's Baby Shower</b>", withString:"<b>\(eventTitle)</b>", options: [], range: nil)
        
        
        
        
            message = message.stringByReplacingOccurrencesOfString("Sharon Tucker", withString: "\(hostName)", options: [], range: nil)
        
        message = message.stringByReplacingOccurrencesOfString("<b>Roooooosy’s Baby Shower</b>", withString: "<b>\(eventTitle)</b>", options: [], range: nil)
        
        
        return message
    }
}