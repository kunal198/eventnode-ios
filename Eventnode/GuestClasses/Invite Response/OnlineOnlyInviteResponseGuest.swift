//
//  OnlineOnlyInviteResponseGuest.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class OnlineOnlyInviteResponseGuest
{
    func emailMessage(eventId: String, eventTitle: String, dateString: String, timeString: String, locationString: String, hostName: String, latitude: String, longitude: String, type: String, url:String)-> String
    {
        
        let file = "Online_Only2.html"

        

        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)

        

        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)

        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

        //println(base64String)

        

        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)

        

        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

        

        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)

        

        message = message.stringByReplacingOccurrencesOfString("Rosy's Baby Shower", withString:"\(eventTitle)", options: [], range: nil)
        
        message = message.stringByReplacingOccurrencesOfString("Sharon Tucker", withString:"\(hostName)", options: [], range: nil)

        

        if type == "rsvp"

        {

            message = message.stringByReplacingOccurrencesOfString("April 30,2015 - 7:00PM", withString: "\(dateString) - \(timeString)", options: [], range: nil)

            

            message = message.stringByReplacingOccurrencesOfString("3444 Spectrum, Irvine, CA 92618", withString: "\(locationString)", options: [], range: nil)

            

            message = message.stringByReplacingOccurrencesOfString("goooooooooooooooooogggggggglllllllll", withString:"http://maps.google.com/maps?q=\(latitude),\(longitude)", options: [], range: nil)

            

        }

        

        message = message.stringByReplacingOccurrencesOfString("deeeeeeeeeeeeeeeppppppppp", withString: "\(url)", options: [], range: nil)
        
        return message
    }
}