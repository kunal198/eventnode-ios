//
//  MaybeReminder.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class MaybeReminder
{
    func emailMessage(eventId: String, eventTitle: String, dateString: String, timeString: String, locationString: String, hostName: String, latitude: String, longitude: String, type: String, url:String)-> String
    {
        
        let file = "Maybe_Reminder.html"

        

        let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)

        

        let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)

        let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

        //println(base64String)

        

        let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)

        

        let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

        

        var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)

        

         message = message.stringByReplacingOccurrencesOfString("RRRRosy's Baby Shower<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by SSSharon Tucker", withString: "\(eventTitle)<br><span style=\"font-size:12px; color:#9B9B9B;\"><em>by \(hostName)", options: [], range: nil)

        

        if type == "rsvp"

        {

            message = message.stringByReplacingOccurrencesOfString("April 300000,2015 - 7:000000PM", withString: "\(dateString) - \(timeString)", options: [], range: nil)

            

            message = message.stringByReplacingOccurrencesOfString("33333444 Spectrum, Irvine, CA 92618", withString: "\(locationString)", options: [], range: nil)

            

            message = message.stringByReplacingOccurrencesOfString("Goooooooooooogggggggleeeeeeeeeeeemaaaaappppssssssss", withString:"http://maps.google.com/maps?q=\(latitude),\(longitude)", options: [], range: nil)

            

        }

        

        message = message.stringByReplacingOccurrencesOfString("<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"#\" target=\"_blank\">Change your Response</a></td>", withString: "<td height=\"30\" align=\"center\" style=\"font-family:'Raleway', Tahoma,Verdana,Segoe,sans-serif; font-size:12px;color:#4A4A4A; text-align:center; line-height:18px; padding-bottom:2px; font-weight:700;\"><a style=\"color: #4A4A4A; text-align:center;text-decoration: none;\" href=\"\(url)\" target=\"_blank\">Change your Response</a></td>", options: [], range: nil)
        
        return message
    }
}