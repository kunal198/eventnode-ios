//
//  LocationChange.swift
//  eventnode
//
//  Created by mrinal khullar on 7/25/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import Foundation


class LocationChange
{
    func emailMessage(eventId: String, eventTitle: String,  locationString: String, hostName: String, type: String, url:String  )-> String
{

let file = "location_change2.html"



let path = (documentDirectory as NSString).stringByAppendingPathComponent(file)



    let plainData = (eventId as NSString).dataUsingEncoding(NSUTF8StringEncoding)

    let base64String = plainData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

    //println(base64String)



    let plainDataUrl = (url as NSString).dataUsingEncoding(NSUTF8StringEncoding)

    

    let base64UrlString = plainDataUrl!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))

    



var message = try! String(contentsOfFile: path, encoding: NSUTF8StringEncoding)



message = message.stringByReplacingOccurrencesOfString("Rosy's Baby Shower", withString: "\(eventTitle)", options: [], range: nil)



if type == "rsvp"

{

    message = message.stringByReplacingOccurrencesOfString("Sharon Tucker", withString: "\(hostName)", options: [], range: nil)

    

    message = message.stringByReplacingOccurrencesOfString("Rooooooooooooooosy’s Baby Shower", withString: "\(eventTitle)", options: [], range: nil)

    

    message = message.stringByReplacingOccurrencesOfString("<b>3444 Spectrum, Irvine, CA 92618</b>", withString:"<b>\(locationString)</b>", options: [], range: nil)

    

    message = message.stringByReplacingOccurrencesOfString("deeeeeeeeeeeeeeeeppppppppppppppplllllllllllllliiiiiiiiinnnnnnnkkkkkkkkkkk", withString:"\(url)", options: [], range: nil)
    
}


return message
}
}