//
//  AbusiveReportEmail.swift
//  Eventnode
//
//  Created by mrinal khullar on 2/24/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//

import Foundation


class AbusiveReportEmail
{
    func sendEmail(emailSubject: String, message: String, emails: [String])
    {
        let sns = AWSSES.defaultSES()
        
        let messageBody = AWSSESContent()
        let subject = AWSSESContent()
        let body = AWSSESBody()
        
        
        
        subject.data = emailSubject
        
        messageBody.data = message
        
        //body.text = messageBody
        body.html = messageBody
        
        let theMessage = AWSSESMessage()
        theMessage.subject = subject
        
        theMessage.body = body
        
        //email = "dimpal1990@gmail.com"
        
        let destination = AWSSESDestination()
        destination.toAddresses = emails
        
        //println(emails)
        
        let username = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        //println(username)
        let send = AWSSESSendEmailRequest()
        send.source = "<admin@eventnode.co>"
        send.destination = destination
        send.message = theMessage
        send.returnPath = "noreply@eventnode.co"
        
        
        
        sns.sendEmail(send).continueWithBlock {(task: AnyObject!) -> AWSTask! in
            
            if task.error != nil
            {
                //println(task.error.debugDescription)
            }
            else
            {
                //println("success")
            }
            
            return nil
            
        }
    }
}