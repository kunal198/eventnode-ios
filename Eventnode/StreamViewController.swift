//
//  StreamViewController.swift
//  eventnode
//
//  Created by mrinal khullar on 4/28/15.
//  Copyright (c) 2015 eventnode LLC. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import AVFoundation
import MediaPlayer
import EventKit
import Intercom

var isSharedPostUpdated:Bool! = true

var isOnEventStream:Bool! = false
var isEventStreamUpdated:Bool! = false
var noOfStreamUpdates = 0

var mySharedEventData = [PFObject]()
var mySharedRowHeights = [CGFloat]()
var SharedEventTextTitle = "Add Text"

var k = 0
var l = 0

class StreamViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate {
    
    
    @IBOutlet var gobtnclicked: UIButton!
    @IBOutlet var tableView : UITableView!
    
    @IBOutlet weak var onboardingScreen: UIView!
    @IBOutlet weak var selectedImageWrapper: UIView!
    @IBOutlet weak var selectedImage: UIImageView!
    
    @IBOutlet weak var invitationView: UIView!
    @IBOutlet weak var streamView: UIView!
    
    @IBOutlet var unreadCount: UILabel!
    @IBOutlet weak var adultsLabel: UILabel!
    @IBOutlet weak var childLabel: UILabel!
    
    @IBOutlet weak var invitationBottom: UIView!
    @IBOutlet weak var streamBottom: UIView!
    
    @IBOutlet var newUpdatesButton: UIButton!
    
    var currentStatus:String!
    var statusToBeChanged:String!
    var unreadMsgCount = Int()
    var linkForHost = ""
    var invitationNoteViewHeight: CGFloat = 0
    
    @IBOutlet var statusInfo: UILabel!
    @IBOutlet weak var openMeassageButtonView: UIButton!
    
    @IBOutlet var messagePopupView: UIView!
    @IBOutlet var statusButtonsView: UIView!
    @IBOutlet var statusInfoView: UIView!
    @IBOutlet var noOfChildText: UITextField!
    @IBOutlet var noOfAdultsText: UITextField!
    
    @IBOutlet var messageTitle: UITextView!
    @IBOutlet var noOfChildWrapper: UIView!
    @IBOutlet var noOfAdultsWrapper: UIView!
    
    @IBOutlet var invitationNoteView: UITextView!
    @IBOutlet var attendingStatusImageView: UIImageView!
    var arrbadgechatCount = NSMutableArray()
    
    
    @IBOutlet weak var secondSubView: UIView!
    @IBOutlet weak var hostNameBottom: UILabel!
    @IBOutlet weak var hostName: UILabel!
    @IBOutlet weak var eventLocationText: UITextView!
    
    @IBOutlet weak var senderName: UILabel!
    
    @IBOutlet weak var eventHeaderTitle: UILabel!
    
    @IBOutlet weak var eventTitleText: UITextView!
    @IBOutlet weak var eventStartText: UITextView!
    @IBOutlet weak var eventEndText: UITextView!
    @IBOutlet weak var eventLocalStartText: UITextView!
    
    @IBOutlet weak var notefromhost: UILabel!
    
    @IBOutlet weak var eventDescriptionText: UITextView!
    
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var attendEvent: UIButton!
    @IBOutlet weak var notAttendEvent: UIButton!
    @IBOutlet weak var notSure: UIButton!
    @IBOutlet weak var followOnline: UIButton!
    @IBOutlet weak var invitationStreamButtonInnerView: UIView!
    @IBOutlet weak var postMessageButtonInnerView: UIView!
    
    //var eventTitle: String!
    var eventObject: PFObject!
    var eventStore : EKEventStore = EKEventStore()
    
    
    var newMedia: Bool = true
    
    var moviePlayer : MPMoviePlayerController?
    
    @IBOutlet weak var invitationMessageView: UIView!
    @IBOutlet weak var invitationStreamView: UIView!
    @IBOutlet weak var postMessageView: UIView!
    @IBOutlet weak var postStreamView: UIView!
    
    var moviePlayers = [Int: MPMoviePlayerController]()
    var currentScrollTop = 0
    var hideNow = 0
    var currentUserId: String!
    
    var eventLogoFile: String!
    
    var eventLogoFileUrl: NSURL!
    var activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x:105, y: 250, width: 100, height: 150),
        type: .BallScaleMultiple, color: UIColor(red: 220/255.0, green: 203/255.0, blue: 85/255.0, alpha: 1.0), size: CGSize(width: 100, height: 100))
    
    var likeObjectIds = [String]()
    
    var fullUserName: String!
    var eventTitle: String!
    var updatebadgestimer = NSTimer()
    @IBOutlet weak var eventTitleLabel: UILabel!
    var messagesRef: Firebase!
    var arrdata = NSMutableArray()
    var arrLike = NSMutableArray()
    var arrLikeDic = NSMutableArray()
    var isOpenInvitScreen = ""
    
    
    var isGuestListChange = false
    
    
    
    func setupFirebase() {
        // *** STEP 2: SETUP FIREBASE
        
        
      
            // do some task
            
            //messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/YdCLUOrjMr")
            
            // messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/YdCLUOrjMr")
            self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
            
            self.arrdata = []
            
            
            //print(arrdata)
            // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
            self.messagesRef.queryLimitedToLast(5).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                //print(  snapshot.key )
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
                dictemp.setValue(snapshot.value["text"] as? String, forKey: "text")
                dictemp.setValue(snapshot.value["postUrl"] as? String, forKey: "postUrl")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "time")
                dictemp.setValue(snapshot.value["eventObjectId"] as? String, forKey: "objectId")
                dictemp.setValue(snapshot.value["postData"] as? String, forKey: "postData")
                dictemp.setValue(snapshot.value["postHeight"] as? String, forKey: "postHeight")
                dictemp.setValue(snapshot.value["postWidth"] as? String, forKey: "postWidth")
                dictemp.setValue(snapshot.value["eventFolder"] as? String, forKey: "eventFolder")
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")

                self.arrdata.insertObject(dictemp, atIndex: 0)
                //print (self.arrdata)
                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "count", ascending: false)
                let sortedResults: NSArray = self.arrdata.sortedArrayUsingDescriptors([descriptor])
                self.arrdata = []
                self.arrdata = sortedResults.mutableCopy() as! NSMutableArray
                //self.arrLike.addObject(snapshot.key)
                
               if self.arrdata.count == 0
               {
                    self.onboardingScreen.hidden = false
               }
               else
               {
                    self.onboardingScreen.hidden = true
               }
                
                
               // self.GetLikeDicFirebase()
                let queryRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentSharedEvent.objectId!)/\(snapshot.key)")
                //    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
                
                self.arrLikeDic = []
                
                //print(arrdata)
                // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
                queryRef.observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    //print(  snapshot.key )
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.arrLikeDic.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.arrLikeDic
                    self.arrLikeDic = []
                    
                    for e in arrtemp
                    {
                        if !self.arrLikeDic.containsObject(e)
                        {
                            self.arrLikeDic.addObject(e)
                        }
                        
                    }
                    
                    //print (self.arrLikeDic)
                    self.tableView.reloadData()
                    
                    // self.arrLike.addObject(snapshot.key)
                    
                    //self.GetLikeDicFirebase()
                    
                    //self.tableView.reloadData()
                })
                
                
           


                //self.tableView.reloadData()
                
                // self.finishReceivingMessage()
            })
            
        
        
        
        
        
        
        
        
    }
    func updateFirebase() {
        
        // do some task
        if arrdata.count > 0
        {
           
            var count = arrdata.count
            if count > 5
            {
                count = count - 5
            }
            let m = arrdata.valueForKey("count").objectAtIndex(arrdata.count-1) as! Int
            var n = m-6
            if m < 6
            {
                n = 1
            }
            
            self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
            
            
            
            self.messagesRef.queryOrderedByChild("count").queryStartingAtValue(n).queryEndingAtValue(m-1).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
                
                
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
                dictemp.setValue(snapshot.value["text"] as? String, forKey: "text")
                dictemp.setValue(snapshot.value["postUrl"] as? String, forKey: "postUrl")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "time")
                dictemp.setValue(snapshot.value["eventObjectId"] as? String, forKey: "objectId")
                dictemp.setValue(snapshot.value["postData"] as? String, forKey: "postData")
                dictemp.setValue(snapshot.value["postHeight"] as? String, forKey: "postHeight")
                dictemp.setValue(snapshot.value["postWidth"] as? String, forKey: "postWidth")
                dictemp.setValue(snapshot.value["eventFolder"] as? String, forKey: "eventFolder")
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")
                
                self.arrdata.addObject(dictemp)
                
                let arrtemp = self.arrdata
                self.arrdata = []
                
                
                for e in arrtemp
                {
                    if !self.arrdata.containsObject(e)
                    {
                        self.arrdata.addObject(e)
                    }
                    
                }
                print(self.arrdata)
                
                if self.arrdata.count == 0
                {
                    self.onboardingScreen.hidden = false
                }
                else
                {
                    self.onboardingScreen.hidden = true
                }
                self.tableView.reloadData()
                
                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "count", ascending: false)
                let sortedResults: NSArray = self.arrdata.sortedArrayUsingDescriptors([descriptor])
                self.arrdata = []
                self.arrdata = sortedResults.mutableCopy() as! NSMutableArray
                self.view.userInteractionEnabled = true
                
                
               // self.arrLike.addObject(snapshot.key)
                
                //self.GetLikeDicFirebase()
               // self.tableView.reloadData()
                
                // self.GetLikeDicFirebase()
                let queryRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentSharedEvent.objectId!)/\(snapshot.key)")
                //    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
                
//                self.arrLikeDic = []
//                self.arrLikeDic.removeAllObjects()
                
                //print(arrdata)
                // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
                queryRef.observeEventType(.ChildAdded, withBlock: { (snapshot) in
                    //print(  snapshot.key )
                    let dictemp = NSMutableDictionary()
                    dictemp.setValue(snapshot.key , forKey: "keyvalue")
                    dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                    dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                    dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                    dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                    
                    self.arrLikeDic.insertObject(dictemp, atIndex: 0)
                    
                    let arrtemp = self.arrLikeDic
                    self.arrLikeDic = []
                    
                    for e in arrtemp
                    {
                        if !self.arrLikeDic.containsObject(e)
                        {
                            self.arrLikeDic.addObject(e)
                        }
                        
                    }
                    
                    //print (self.arrLikeDic)
                    self.tableView.reloadData()
                    
                    // self.arrLike.addObject(snapshot.key)
                    
                    //self.GetLikeDicFirebase()
                    
                    //self.tableView.reloadData()
                })

                self.tableView.reloadData()

            })
            
            
                 }
    }
    
    func GetLikeFirebase() {
        // *** STEP 2: SETUP FIREBASE
        
        
        // messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/Yiigt8gMwn")
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
        
        arrLike = []
        
        
        // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
        messagesRef.queryLimitedToLast(5).observeEventType(.ChildAdded, withBlock: { (snapshot) in
            
            let dictemp = NSMutableDictionary()
            
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            
            self.arrLike.addObject(snapshot.key)
            
            self.GetLikeDicFirebase()
            
            // self.finishReceivingMessage()
        })
        
        
    }
    func GetLikeDicFirebase() {
        // *** STEP 2: SETUP FIREBASE
        
        for var i = 0; i < arrLike.count; i++
        {
            messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentSharedEvent.objectId!)/\(arrLike[i])")
            //    messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
            
            arrLikeDic = []
            arrLikeDic.removeAllObjects()
            
            //print(arrdata)
            // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
            messagesRef.queryLimitedToLast(5).observeEventType(.ChildAdded, withBlock: { (snapshot) in
                //print(  snapshot.key )
                let dictemp = NSMutableDictionary()
                dictemp.setValue(snapshot.key , forKey: "keyvalue")
                dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
                dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
                dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
                dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
                
                self.arrLikeDic.insertObject(dictemp, atIndex: 0)
                
                let arrtemp = self.arrLikeDic
                self.arrLikeDic = []
                
                for e in arrtemp
                {
                    if !self.arrLikeDic.containsObject(e)
                    {
                        self.arrLikeDic.addObject(e)
                    }
                    
                }
                //print (self.arrLikeDic)
                self.tableView.reloadData()
                
                // self.finishReceivingMessage()
            })
            
        }
        
        
    }
    
    func deletelike() {
        
        
        
        
        messagesRef.observeEventType(.ChildRemoved, withBlock: { (snapshot) in
            
            let dictemp = NSMutableDictionary()
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
            dictemp.setValue(snapshot.value["userObjectId"] as? String, forKey: "userObjectId")
            dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
            
            self.arrLikeDic.removeObject(dictemp)
            
                  
            let arrtemp = self.arrLikeDic
            self.arrLikeDic = []
            
            for e in arrtemp
            {
                if !self.arrLikeDic.containsObject(e)
                {
                    self.arrLikeDic.addObject(e)
                }
                
            }
            //print (self.arrLikeDic)
            self.tableView.reloadData()
            // self.finishReceivingMessage()
        })
        
    }
    
    
    func deleteFirebase() {
        
        self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentSharedEvent.objectId!)")
        messagesRef .observeEventType(.ChildRemoved, withBlock: { (snapshot) in
            //print(  snapshot.key )
            let dictemp = NSMutableDictionary()
            dictemp.setValue(snapshot.value["postType"] as? String, forKey: "postType")
            dictemp.setValue(snapshot.value["name"] as? String, forKey: "name")
            dictemp.setValue(snapshot.value["text"] as? String, forKey: "text")
            dictemp.setValue(snapshot.value["postUrl"] as? String, forKey: "postUrl")
            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "time")
            dictemp.setValue(snapshot.value["eventObjectId"] as? String, forKey: "objectId")
            dictemp.setValue(snapshot.value["postData"] as? String, forKey: "postData")
            dictemp.setValue(snapshot.value["postHeight"] as? String, forKey: "postHeight")
            dictemp.setValue(snapshot.value["postWidth"] as? String, forKey: "postWidth")
            dictemp.setValue(snapshot.value["eventFolder"] as? String, forKey: "eventFolder")
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            dictemp.setValue(snapshot.value["count"] as? Int, forKey: "count")

            self.arrdata.removeObject(dictemp)
            //print (self.arrdata)
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "count", ascending: false)
            let sortedResults: NSArray = self.arrdata.sortedArrayUsingDescriptors([descriptor])
            self.arrdata = []
            self.arrdata = sortedResults.mutableCopy() as! NSMutableArray
            
            if self.arrdata.count == 0
            {
                self.onboardingScreen.hidden = false
            }
            else
            {
                self.onboardingScreen.hidden = true
            }

            
            self.view.userInteractionEnabled = true
            self.tableView.reloadData()
            
            // self.finishReceivingMessage()
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        self.arrLikeDic.removeAllObjects()

        setupFirebase()
       
        
            
        self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentSharedEvent.objectId!)")
        self.deletelike()
        self.deleteFirebase()
       

        
        
        
        //GetLikeFirebase()
        //GetLikeDicFirebase()
        

        
        isEventStreamUpdated = false
        noOfStreamUpdates = 0
        downloadInvitation()
        self.view.addSubview(wakeUpImageView)
        
        gobtnclicked.layer.cornerRadius = gobtnclicked.frame.size.width/2
        gobtnclicked.layer.masksToBounds = true
        unreadCount.hidden = true
        unreadCount.layer.masksToBounds = true
        unreadCount.layer.cornerRadius = 8
        
        activityIndicatorView.center = CGPointMake(view.frame.width/2, view.frame.height/2)
        
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimation()
        
        activityIndicatorView.hidesWhenStopped = true
        
        let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentSharedEvent.objectId!])
        
        resultSetCount.next()
        
        // let unreadMsgCount = resultSetCount.intForColumn("count")
        
        if unreadMsgCount == 0
        {
            unreadCount.hidden = true
        }
        else
        {
            unreadCount.hidden = false
            unreadCount.text = "\(unreadMsgCount)"
            unreadCount.textAlignment = .Center
            unreadCount.font = UIFont(name: "AvenirNext-Regular", size: 10.0)
            
            let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
            
            if unreadMsgCount > 0
            {
                unreadCount.layer.masksToBounds = true
                unreadCount.layer.cornerRadius = 3.0
                //unreadCount.text = "\(totalChatCount)"
                unreadCount.backgroundColor = UIColor.redColor()
                unreadCount.textColor = UIColor.whiteColor()
                let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
                
                if unreadMsgCount < 10
                {
                    // unreadCount.frame.size.width = CGFloat(0.08333*self.view.frame.height)
                    unreadCount.frame.size.width = CGFloat(20)
                    
                    unreadCount.layer.cornerRadius = unreadCount.frame.size.width/2
                }
                else if unreadMsgCount < 100
                {
                    unreadCount.frame.size.width = CGFloat(labelWidth+14)
                    unreadCount.layer.cornerRadius = 10
                }
                else
                {
                    unreadCount.frame.size.width = CGFloat(labelWidth+24)
                    unreadCount.layer.cornerRadius = 10
                }
                
                //unreadCount.frame.origin.x = (0.70875*self.view.frame.width)+((0.20*self.view.frame.width/2)-(unreadCount.frame.width/2))
                
                unreadCount.frame.size.height = CGFloat(20)
            }
            else
            {
                unreadCount.text = ""
            }
            unreadCount.textAlignment = .Center
        }
        
        resultSetCount.close()
        
        
        fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        eventTitle = currentSharedEvent["eventTitle"] as! String
        
        
        if let isLikesReloaded = NSUserDefaults.standardUserDefaults().objectForKey("isLikesReloaded") as? String
        {
            if isLikesReloaded == "false"
            {
                ModelManager.instance.deleteTableData("PostLikes", whereString: "1", whereFields: [])
                NSUserDefaults.standardUserDefaults().setObject("true", forKey: "isLikesReloaded")
            }
        }
        else
        {
            ModelManager.instance.deleteTableData("PostLikes", whereString: "1", whereFields: [])
        }
        
        
        
        isOnEventStream = true
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributes = [NSParagraphStyleAttributeName : style]
        
        var attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(15.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]
        
        invitationNoteView.attributedText = NSAttributedString(string:invitationNoteView.text, attributes:attributes)
        invitationNoteView.font = UIFont(name:"AvenirNext-Medium", size: 15.0)
        invitationNoteView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        newUpdatesButton.layer.cornerRadius = 0
        newUpdatesButton.hidden = true
        newUpdatesButton.backgroundColor = UIColor(red: 243/255, green: 242/255, blue: 242/255, alpha: 1.0)
        //        newUpdatesButton.layer.borderWidth = 0
        //        newUpdatesButton.layer.borderColor = UIColor(red: 81/255, green: 51.0/255, blue: 103.0/255, alpha: 1.0).CGColor
        newUpdatesButton.alpha = 1
        
        eventDescriptionText.attributedText = NSAttributedString(string:eventDescriptionText.text, attributes:attributes)
        eventDescriptionText.font = UIFont(name:"AvenirNext-Regular", size: 13.0)
        eventDescriptionText.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        
        eventObject = currentSharedEvent
        
        statusInfoView.layer.borderWidth = 0.3
        statusInfoView.layer.borderColor = UIColor(red: 145.0/255, green: 145.0/255, blue: 145.0/255, alpha: 0.7).CGColor
        
        
        if((eventObject["isRSVP"] as! Bool) == true)
        {
            invitationView.hidden = false
            
            
            
            subView.layer.cornerRadius = 3
            subView.layer.borderWidth = 0.3
            
            subView.backgroundColor = UIColor(red: 242/255, green:  242/255, blue: 242/255, alpha: 1.0)
            subView.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
            
            secondSubView.layer.cornerRadius = 3
            secondSubView.layer.borderWidth = 0.3
            
            secondSubView.layer.borderColor = UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0).CGColor
            
            
            /*var firstLine : UIView! = UIView(frame: CGRectMake(156, 3, 1, 27))
            firstLine.backgroundColor = UIColor(red: 234/255, green: 234/255, blue: 234/255, alpha: 1.0)
            self.secondSubView.addSubview(firstLine)*/
            
            
            // var latitude = eventObject["eventLatitude"] as! Double
            // var longitude = eventObject["eventLongitude"] as! Double
            // var eventLocation = eventObject["eventLocation"] as! String
            ////println(eventObject["eventStartDateTime"] as! NSDate)
            
            //
            
            
            //let sdate = eventObject["eventStartDateTime"] as! NSDate
            //let edate = eventObject["eventEndDateTime"] as! NSDate
            
            let eventStartDate = eventObject["eventStartDateTime"] as! NSDate
            
            /*var startTimeStamp = Int64(eventStartDate.timeIntervalSince1970)
            var timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
            
            var eventTimezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT
            
            var timeStampToBeShown = Int64(startTimeStamp-timezoneOffset+eventTimezoneOffset)
            
            var sdate = NSDate(timeIntervalSince1970: Double(timeStampToBeShown))*/
            
            //tblFields["eventTimezoneOffset"] = timezoneOffset
            
            
            
            
            var formatter = NSDateFormatter();
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
            
            formatter.timeZone = NSTimeZone(name: currentTimeZone)
            
            var timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
            
            var localTimezoneOffset = Double(NSTimeZone.localTimeZone().secondsFromGMT)
            
            
            
            var sdate = NSDate(timeIntervalSince1970: (eventObject["eventStartDateTime"] as! NSDate).timeIntervalSince1970 - timezoneOffset + localTimezoneOffset)
            
            
            let calendar = NSCalendar.currentCalendar()
            
            let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
            
            var sam = "AM"
            var shour: Int!
            if(scomponents.hour >= 12)
            {
                if(scomponents.hour > 12)
                {
                    shour = scomponents.hour-12
                }
                else
                {
                    shour = 12
                }
                
                sam = "PM"
            }
            else
            {
                shour = scomponents.hour
                sam = "AM"
                if(scomponents.hour==0){
                    shour = 12
                }
            }
            
            var sminute = "\(scomponents.minute)"
            let sday = scomponents.day
            let smonth = scomponents.month
            let syear = scomponents.year
            
            if(scomponents.minute<10)
            {
                sminute="0\(sminute)"
            }
            
            
            var startDate = "\(monthsArray[smonth-1]) \(sday), \(syear) @ \(shour):\(sminute) \(sam)"
            
            
            
            
            let ecomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: eventStartDate)
            
            var eam = "AM"
            var ehour: Int!
            if(ecomponents.hour > 12)
            {
                if(ecomponents.hour > 12)
                {
                    ehour = ecomponents.hour-12
                }
                else
                {
                    ehour = 12
                }
                
                eam = "PM"
            }
            else
            {
                ehour = ecomponents.hour
                eam = "AM"
                if(ecomponents.hour==0){
                    ehour = 12
                }
            }
            
            var eminute = "\(ecomponents.minute)"
            let eday = ecomponents.day
            let emonth = ecomponents.month
            let eyear = ecomponents.year
            
            if(ecomponents.minute<10)
            {
                eminute = "0\(eminute)"
            }
            
            var startLocalDate = "\(monthsArray[emonth-1]) \(eday) @ \(ehour):\(eminute) \(eam) (Your Time)"
            
            
            ////println(startDate)
            //////println(endDate)
            ////println(eventTitle)
            
            invitationNoteViewHeight = invitationNoteView.frame.height
            
            eventTitleText.text = eventObject["eventTitle"] as! String
            eventTitleText.textAlignment = NSTextAlignment.Center
            eventTitleText.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
            
            
            eventStartText.text = startDate
            
            eventStartText.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
            eventStartText.textAlignment = NSTextAlignment.Center
            
            //eventLocalStartText.text = startLocalDate
            
            eventLocalStartText.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
            eventLocalStartText.textAlignment = NSTextAlignment.Center
            
            print(eventObject)
            
            eventLocationText.text = eventObject["eventLocation"] as! String!
            eventLocationText.textAlignment = NSTextAlignment.Center
            eventLocationText.font = UIFont(name: "AvenirNext-Medium", size: 13.0)
            
            
            
            eventDescriptionText.text = eventObject["eventDescription"] as! String!
            eventDescriptionText.textColor = UIColor.blackColor()
            
            
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 5
            let attributes = [NSParagraphStyleAttributeName : style]
            
            eventDescriptionText.attributedText = NSAttributedString(string:eventDescriptionText.text, attributes:attributes)
            eventDescriptionText.font = UIFont(name: "AvenirNext-DemiBold", size: 13.0)
            eventDescriptionText.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            
            let by = "by"
            
            let host =  eventObject["senderName"] as! String
            
            hostName.text = "\(by) \(host)"
            hostNameBottom.text = eventObject["senderName"] as? String
            
        }
        else
        {
            
            
            invitationView.hidden = true
        }
        
        
        if((eventObject["isApproved"] as! Bool) == true)
        {
            streamView.hidden = false
        }
        else
        {
            streamView.hidden = true
        }
        
        currentStatus = currentSharedEvent["attendingStatus"] as! String
        statusToBeChanged = currentSharedEvent["attendingStatus"] as! String
        
        if (eventObject["isApproved"] as! Bool) == true && (eventObject["isRSVP"] as! Bool) == true
        {
            if currentStatus != ""
            {
                streamButtonClicked(UIButton())
            }
            
            invitationMessageView.hidden = false
            invitationStreamView.hidden = false
            postMessageView.hidden = false
            postStreamView.hidden = false
        }
        else if(eventObject["isApproved"] as! Bool) == true && (eventObject["isRSVP"] as! Bool) == false
        {
            invitationMessageView.hidden = true
            invitationStreamView.hidden = true
            postMessageView.hidden = true
            postStreamView.hidden = true
            
            invitationBottom.hidden = true
            streamBottom.hidden = true
            
            tableView.frame.size.height = self.view.frame.height - (tableView.frame.origin.y)
            
            postStreamView.frame.size.width = self.view.frame.width
            
            postStreamView.frame.origin.x = 0
        }
        else if(eventObject["isApproved"] as! Bool) == false && (eventObject["isRSVP"] as! Bool) == true
        {
            //invitationMessageView.hidden = true
            //invitationStreamView.hidden = true
            postMessageView.hidden = false
            postStreamView.hidden = true
            
            let originalPostMessageViewWidth = postMessageButtonInnerView.frame.width
            
            postMessageView.frame.size.width = self.view.frame.width
            postMessageButtonInnerView.frame.size.width = originalPostMessageViewWidth
            
            postMessageView.frame.origin.x = 0
            postMessageButtonInnerView.frame.origin.x = (postMessageView.frame.width/2) - (originalPostMessageViewWidth/2)
        }
        
        
        
        
        messagePopupView.hidden = true
        
        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        if currentStatus != ""
        {
            
            if currentStatus == "yes"
            {
                statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
                attendingStatusImageView.image = UIImage(named:"check-green.png")
            }
            
            if currentStatus == "online"
            {
                statusInfo.text = "I will follow online"
                attendingStatusImageView.image = UIImage(named:"web-globe.png")
            }
            
            if currentStatus == "maybe"
            {
                statusInfo.text = "I am not sure"
                attendingStatusImageView.image = UIImage(named:"questionmark.png")
            }
            
            if currentStatus == "no"
            {
                statusInfo.text = "I am not attending"
                attendingStatusImageView.image = UIImage(named:"cross-red.png")
            }
            
            
            openMeassageButtonView.hidden = false
            statusButtonsView.hidden = true
            statusInfoView.hidden = false
        }
        else
        {
            
            openMeassageButtonView.hidden = true
            statusButtonsView.hidden = false
            statusInfoView.hidden = true
        }
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        
        
        //senderName.text = eventObject["senderName"] as? String
        
        //styleLabelFont(senderName)
        
        
        
        if((currentSharedEvent["eventTitle"] as? String) == "")
        {
            eventTitleLabel.text = "No Title"
        }
        else
        {
            let eventTitleString = (currentSharedEvent["eventTitle"] as? String)!
            
            
            currentSharedEvent["eventTitle"] = String(eventTitleString.characters.prefix(1)).capitalizedString + String(eventTitleString.characters.suffix(eventTitleString.characters.count - 1))
            eventTitleLabel.text = currentSharedEvent["eventTitle"] as? String
        }
        
        
        moviePlayer = MPMoviePlayerController()
        
        moviePlayer!.view.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        
        moviePlayer!.prepareToPlay()
        
        moviePlayer!.shouldAutoplay = false
        moviePlayer!.view.hidden = true
        self.view.addSubview(moviePlayer!.view)
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "doneButtonClick:", name: MPMoviePlayerWillExitFullscreenNotification, object:nil)
        
        
        tableView.separatorColor = UIColor.clearColor()
        if isOpenInvitScreen == "true"
        {
            invitationView.hidden=false
            streamView.hidden=true
        }
        
        ////println(currentSharedEvent.objectId!)
        
//        var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("refreshContent"), userInfo: nil, repeats: true)
//        
//        updatebadgestimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("downloadInvitation"), userInfo: nil, repeats: true)
    }
    func downloadInvitation()
    {
        
        
        
        
        
        var predicate = NSPredicate()
        
        var eventObjectIdsStringPredicate = ""
        
        
        eventObjectIdsStringPredicate = "(eventObjectId IN {'\(currentSharedEvent.objectId!)'})"
        
        
        
        ////println(eventObjectIdsStringPredicate)
        
        predicate = NSPredicate(format: eventObjectIdsStringPredicate)
        
        let query = PFQuery(className:"EventComments", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllMessagessSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllMessagessError:", errorSelectorParameters:nil)
        //////println("NOT (objectId IN {'\(invitationObjectIdsString)'}) AND userObjectId = '\(self.currentUserId)'")
        
        
        
    }
    func checkExistance(objectIdColumn: String, objectId: String, tableName: String)->Bool
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData(tableName, selectColumns: ["count(*) as count"], whereString: "\(objectIdColumn) = '\(objectId)'", whereFields: [])
        
        resultSet.next()
        
        let noOfRows = Int(resultSet.intForColumn("count"))
        
        resultSet.close()
        
        return (noOfRows > 0)
    }
    
    func fetchAllMessagessSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        ////println("Successfully retrieved \(objects!.count) events.")
        
        if var fetchedobjects = objects {
            
            var i = 0
            
            var fetchedEventObjectIds: Array<String>
            fetchedEventObjectIds = []
            
            for message in fetchedobjects
            {
                if !checkExistance("objectId", objectId: message.objectId!, tableName: "EventComments")
                {
                    var tblFields: Dictionary! = [String: String]()
                    
                    tblFields["objectId"] = message.objectId!
                    tblFields["messageText"] = message["messageText"] as? String
                    tblFields["senderObjectId"] = message["senderObjectId"] as? String
                    tblFields["eventObjectId"] = message["eventObjectId"] as? String
                    tblFields["senderName"] = message["senderName"] as? String
                    //tblFields["eventCommentId"] = message["eventCommentId"] as? String
                    
                    
                    
                    var date = ""
                    
                    if message.createdAt != nil
                    {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((message.createdAt)!)
                        ////println(date)
                        tblFields["createdAt"] = date
                    }
                    
                    if message.updatedAt != nil
                    {
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                        date = dateFormatter.stringFromDate((message.updatedAt)!)
                        ////println(date)
                        tblFields["updatetAt"] = date
                    }
                    
                    tblFields["isPosted"] = "1"
                    
                    if isChatMode == true
                    {
                        tblFields["isRead"] = "1"
                    }
                    else
                    {
                        tblFields["isRead"] = "0"
                    }
                    
                    tblFields["timezoneOffset"] = "\(NSTimeZone.localTimeZone().secondsFromGMT)"
                    
                    /* var resultSet: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["COUNT(*) as count"], whereString: "objectId = '\(message.objectId!)'", whereFields: [])
                    
                    resultSet.next()
                    
                    var messageCount = Int(resultSet.intForColumn("count"))
                    
                    resultSet.close()
                    */
                    //                if messageCount == 0
                    //                {
                    var insertedId = ModelManager.instance.addTableData("EventComments", primaryKey: "eventCommentId", tblFields: tblFields)
                    //}
                    
                    fetchedEventObjectIds.append(message["eventObjectId"] as! String)
                }
            }
            let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentSharedEvent.objectId!])
            
            resultSetCount.next()
            
            //let unreadMsgCount = resultSetCount.intForColumn("count")
            
            if unreadMsgCount == 0
            {
                unreadCount.hidden = true
            }
            else
            {
                unreadCount.hidden = false
                unreadCount.text = "\(unreadMsgCount)"
                unreadCount.textAlignment = .Center
                unreadCount.font = UIFont(name: "AvenirNext-Regular", size: 10.0)
                
                let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
                
                if unreadMsgCount > 0
                {
                    unreadCount.layer.masksToBounds = true
                    unreadCount.layer.cornerRadius = 3.0
                    //unreadCount.text = "\(totalChatCount)"
                    unreadCount.backgroundColor = UIColor.redColor()
                    unreadCount.textColor = UIColor.whiteColor()
                    let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
                    
                    if unreadMsgCount < 10
                    {
                        // unreadCount.frame.size.width = CGFloat(0.08333*self.view.frame.height)
                        unreadCount.frame.size.width = CGFloat(20)
                        
                        unreadCount.layer.cornerRadius = unreadCount.frame.size.width/2
                    }
                    else if unreadMsgCount < 100
                    {
                        unreadCount.frame.size.width = CGFloat(labelWidth+14)
                        unreadCount.layer.cornerRadius = 10
                    }
                    else
                    {
                        unreadCount.frame.size.width = CGFloat(labelWidth+24)
                        unreadCount.layer.cornerRadius = 10
                    }
                    
                    //unreadCount.frame.origin.x = (0.70875*self.view.frame.width)+((0.20*self.view.frame.width/2)-(unreadCount.frame.width/2))
                    
                    unreadCount.frame.size.height = CGFloat(20)
                }
                else
                {
                    unreadCount.text = ""
                }
                unreadCount.textAlignment = .Center
                
            }
            
            resultSetCount.close()
            
        }
    }
    
    
    
    func fetchAllMessagessError(timer:NSTimer)
    {
        //  var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        ////println("Error: \(error) \(error.userInfo!)")
        
        //var resultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["*"], whereString: "objectId IN ( SELECT eventObjectId FROM Invitations WHERE userObjectId = '\(self.currentUserId)') ORDER BY eventId DESC", whereFields: [])
    }
    
    
    func refreshContent()
    {
        if isEventStreamUpdated == true
        {
            if noOfStreamUpdates > 0
            {
                if noOfStreamUpdates == 1
                {
                    newUpdatesButton.setTitle("\(noOfStreamUpdates) new update", forState: UIControlState.Normal)
                }
                else
                {
                    newUpdatesButton.setTitle("\(noOfStreamUpdates) new updates", forState: UIControlState.Normal)
                }
                
                //isOnEventStream = false
                
                //sendButton.setTitleColor(UIColor(red: 68.0/255, green: 185.0/255, blue: 227.0/255, alpha: 1.0), forState: UIControlState.Normal)
                newUpdatesButton.titleLabel?.font = UIFont(name: "AvenirNext-DemiBold", size: 12.0)
                
                newUpdatesButton.hidden = false
            }
            
            let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentSharedEvent.objectId!])
            
            resultSetCount.next()
            
            //let unreadMsgCount = resultSetCount.intForColumn("count")
            
            if unreadMsgCount == 0
            {
                unreadCount.hidden = true
            }
            else
            {
                unreadCount.hidden = false
                //  unreadCount.text = "\(unreadMsgCount)"
                unreadCount.textAlignment = .Center
                unreadCount.font = UIFont(name: "AvenirNext-Regular", size: 10.0)
                
              //  let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
                
                if unreadMsgCount > 0
                {
                    unreadCount.layer.masksToBounds = true
                    unreadCount.layer.cornerRadius = 3.0
                    //unreadCount.text = "\(totalChatCount)"
                    unreadCount.backgroundColor = UIColor.redColor()
                    unreadCount.textColor = UIColor.whiteColor()
                    let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
                    
                    if unreadMsgCount < 10
                    {
                        // unreadCount.frame.size.width = CGFloat(0.08333*self.view.frame.height)
                        unreadCount.frame.size.width = CGFloat(20)
                        
                        unreadCount.layer.cornerRadius = unreadCount.frame.size.width/2
                    }
                    else if unreadMsgCount < 100
                    {
                        unreadCount.frame.size.width = CGFloat(labelWidth+14)
                        unreadCount.layer.cornerRadius = 10
                    }
                    else
                    {
                        unreadCount.frame.size.width = CGFloat(labelWidth+24)
                        unreadCount.layer.cornerRadius = 10
                    }
                    
                    //unreadCount.frame.origin.x = (0.70875*self.view.frame.width)+((0.20*self.view.frame.width/2)-(unreadCount.frame.width/2))
                    
                    unreadCount.frame.size.height = CGFloat(20)
                }
                else
                {
                    unreadCount.text = ""
                }
                unreadCount.textAlignment = .Center
                
            }
            
            resultSetCount.close()
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func styleTextFont(textView: UITextView)
    {
        textView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        textView.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
    }
    
    
    
    func styleLabelFont(textLabel: UILabel)
    {
        textLabel.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        textLabel.font = UIFont(name: "AvenirNext-DemiBold", size: 15.0)
    }
    
    override func viewDidDisappear(animated: Bool) {
//        messagesRef.removeAllObservers()

        updatebadgestimer.invalidate()
    }
    override func viewDidAppear(animated: Bool) {
        
        
        isOnEventStream = true
        
        let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("EventComments", selectColumns: ["count(*) as count"], whereString: "eventObjectId=? AND isRead=0", whereFields: [currentSharedEvent.objectId!])
        
        resultSetCount.next()
        
        // let unreadMsgCount = resultSetCount.intForColumn("count")
        
        if unreadMsgCount == 0
        {
            unreadCount.hidden = true
        }
        else
        {
            unreadCount.hidden = false
            // unreadCount.text = "\(unreadMsgCount)"
            unreadCount.textAlignment = .Center
            unreadCount.font = UIFont(name: "AvenirNext-Regular", size: 10.0)
            
            let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
            
            if unreadMsgCount > 0
            {
                unreadCount.layer.masksToBounds = true
                unreadCount.layer.cornerRadius = 3.0
                //unreadCount.text = "\(totalChatCount)"
                unreadCount.backgroundColor = UIColor.redColor()
                unreadCount.textColor = UIColor.whiteColor()
                let labelWidth = unreadCount.sizeThatFits(unreadCount.bounds.size).width
                
                if unreadMsgCount < 10
                {
                    // unreadCount.frame.size.width = CGFloat(0.08333*self.view.frame.height)
                    unreadCount.frame.size.width = CGFloat(20)
                    
                    unreadCount.layer.cornerRadius = unreadCount.frame.size.width/2
                }
                else if unreadMsgCount < 100
                {
                    unreadCount.frame.size.width = CGFloat(labelWidth+14)
                    unreadCount.layer.cornerRadius = 10
                }
                else
                {
                    unreadCount.frame.size.width = CGFloat(labelWidth+24)
                    unreadCount.layer.cornerRadius = 10
                }
                
                //unreadCount.frame.origin.x = (0.70875*self.view.frame.width)+((0.20*self.view.frame.width/2)-(unreadCount.frame.width/2))
                
                unreadCount.frame.size.height = CGFloat(20)
            }
            else
            {
                unreadCount.text = ""
            }
            unreadCount.textAlignment = .Center
            
            
        }
        
        resultSetCount.close()
        
        
        if((currentSharedEvent["eventTitle"] as? String) == "")
        {
            eventTitleLabel.text = "No Title"
        }
        else
        {
            eventTitleLabel.text = currentSharedEvent["eventTitle"] as? String
        }
        
        mySharedEventData.removeAll()
        //deleteData()
        //refreshList()
        downloadData()
        //updateData()
        
//        self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(currentSharedEvent.objectId!)/\(self.currentUserId)")
//                    self.messagesRef.removeValue()
//                                 self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(currentSharedEvent.objectId!)/\(self.currentUserId)")
//                                      self.messagesRef.removeValue()

               let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(8 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            
            self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/StreamBadges/\(currentSharedEvent.objectId!)/\(self.currentUserId)")
            self.messagesRef.removeValue()
                         self.messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(currentSharedEvent.objectId!)/\(self.currentUserId)")
                              self.messagesRef.removeValue()
        }
        
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func addCommentButtonClicked(sender : AnyObject){
        
        isOnEventStream = false
        
        let addCommentVC = self.storyboard!.instantiateViewControllerWithIdentifier("AddCommentsViewController") as! AddCommentsViewController
        
        addCommentVC.isShared = true
        addCommentVC.isRSVP = true
        self.navigationController?.pushViewController(addCommentVC, animated: true)
        
    }
    
    
    @IBAction func invitationButtonClicked(sender : AnyObject){
        
        invitationView.hidden=false
        streamView.hidden=true
    }
    
    @IBAction func streamButtonClicked(sender : AnyObject){
        
        //NSLog("sdd")
        invitationView.hidden=true
        streamView.hidden=false
    }
    
    
    override func viewWillAppear(animated: Bool)
    {
       
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentSharedEvent.objectId!)/\(currentUserId)")
        messagesRef.observeEventType(.ChildRemoved, withBlock: { (snapshot) in
            self.unreadCount.text = ""
            self.unreadCount.hidden = true

         })

        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/ChatBadges/\(currentSharedEvent.objectId!)/\(currentUserId)")
        messagesRef.observeEventType(.ChildAdded, withBlock: { (snapshot) in
            //print(  snapshot.key )
            let dictemp = NSMutableDictionary()
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            dictemp.setValue(snapshot.value["eventid"] as? String, forKey: "eventid")
            dictemp.setValue(snapshot.value["timestamp"] as? String, forKey: "timestamp")
            dictemp.setValue(snapshot.value["SenderID"] as? String, forKey: "SenderID")
            dictemp.setValue(snapshot.value["parentkey"] as? String, forKey: "parentkey")
        
            
            dictemp.setValue(snapshot.key , forKey: "keyvalue")
            self.arrbadgechatCount.addObject(dictemp)
            print ("Count")
            let arrtemp = self.arrbadgechatCount
            self.arrbadgechatCount = []
            
            for e in arrtemp
            {
                if !self.arrbadgechatCount.containsObject(e)
                {
                    self.arrbadgechatCount.addObject(e)
                }
                
            }
            print (self.arrbadgechatCount.count)
            self.unreadMsgCount = self.arrbadgechatCount.count
            if self.unreadMsgCount == 0
            {
                self.unreadCount.hidden = true
            }
            else
            {
                self.unreadCount.hidden = false
                self.unreadCount.text = "\( self.unreadMsgCount)"
                self.unreadCount.textAlignment = .Center
                self.unreadCount.font = UIFont(name: "AvenirNext-Regular", size: 10.0)
                
                let labelWidth =  self.unreadCount.sizeThatFits( self.unreadCount.bounds.size).width
                
                if  self.unreadMsgCount > 0
                {
                    self.unreadCount.layer.masksToBounds = true
                    self.unreadCount.layer.cornerRadius = 3.0
                    //unreadCount.text = "\(totalChatCount)"
                    self.unreadCount.backgroundColor = UIColor.redColor()
                    self.unreadCount.textColor = UIColor.whiteColor()
                    let labelWidth =  self.unreadCount.sizeThatFits( self.unreadCount.bounds.size).width
                    
                    if self.unreadMsgCount < 10
                    {
                        // unreadCount.frame.size.width = CGFloat(0.08333*self.view.frame.height)
                        self.unreadCount.frame.size.width = CGFloat(20)
                        
                        self.unreadCount.layer.cornerRadius = self.unreadCount.frame.size.width/2
                    }
                    else if self.unreadMsgCount < 100
                    {
                        self.unreadCount.frame.size.width = CGFloat(labelWidth+14)
                        self.unreadCount.layer.cornerRadius = 10
                    }
                    else
                    {
                        self.unreadCount.frame.size.width = CGFloat(labelWidth+24)
                        self.unreadCount.layer.cornerRadius = 10
                    }
                    
                    //unreadCount.frame.origin.x = (0.70875*self.view.frame.width)+((0.20*self.view.frame.width/2)-(unreadCount.frame.width/2))
                    
                    self.unreadCount.frame.size.height = CGFloat(20)
                }
                else
                {
                    self.unreadCount.text = ""
                }
                self.unreadCount.textAlignment = .Center
            }
        })
        
     
        
        l = 0
        k = 0
    }
    
    override func viewWillDisappear(animated: Bool)
    {
        l = 0
        k = 0
    }
    
    
    @IBAction func closeButton(sender: AnyObject)
    {
        if l == 0
        {
            l++
            self.navigationController?.popViewControllerAnimated(false)
        }
        
        
        //isEventStreamUpdated = false
        isOnEventStream = false
        noOfStreamUpdates = 0
    }
    
    
    @IBAction func backButtonClicked(sender : AnyObject){
        
        if k == 0
        {
            k++
            self.navigationController?.popViewControllerAnimated(false)
        }
    }
    
    
    @IBAction func addToCalender(sender: AnyObject)
    {
        let eventStore : EKEventStore = EKEventStore()
        // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
        eventStore.requestAccessToEntityType(EKEntityType.Event, completion: {
            granted, error in
            if (granted) && (error == nil) {
                ////println("granted \(granted)")
                ////println("error  \(error)")
                
                let event:EKEvent = EKEvent(eventStore: eventStore)
                event.title = self.eventTitle
                
                
                let formatter = NSDateFormatter();
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
                
                formatter.timeZone = NSTimeZone(name: currentTimeZone)
                
                let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
                
                let localTimezoneOffset = Double(NSTimeZone.localTimeZone().secondsFromGMT)
                
                
                
                let sdate = NSDate(timeIntervalSince1970: (self.eventObject["eventStartDateTime"] as! NSDate).timeIntervalSince1970 - timezoneOffset + localTimezoneOffset)
                
                let startDate = sdate as! NSDate
                
                
                
                event.startDate = startDate
                
                
                
                let endDate = startDate.dateByAddingTimeInterval(+3600)
                
                event.endDate = endDate
                
                event.notes = self.eventObject["eventDescription"] as! String!
                // let latitude = self.eventObject["eventLatitude"] as! Double
                //let longitude = self.eventObject["eventLongitude"] as! Double
                event.location = self.eventObject["eventLocation"] as? String
                event.calendar = eventStore.defaultCalendarForNewEvents
                do {
                    try eventStore.saveEvent(event, span: .ThisEvent)
                } catch _ {
                }
                
                let refreshAlert = UIAlertController(title: "Event Saved", message: "Your event has been saved to calender.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        })
    }
    
    
    
    @IBAction func goToMapsButton(sender: AnyObject)
    {
        
        let latitude = eventObject["eventLatitude"] as! Double
        let longitude = eventObject["eventLongitude"] as! Double
        
        
        let openLink = NSURL(string : "http://maps.google.com/maps?q=\(latitude),\(longitude)")
        UIApplication.sharedApplication().openURL(openLink!)
    }
    
    
    func scrollViewDidScroll(_scrollView: UIScrollView){
        let newScroll = CGFloat(_scrollView.contentOffset.y)
        
        if newScroll > _scrollView.contentSize.height - _scrollView.frame.height
        {
            refreshList()
        }
        
    }
    
    
    func downloadData()
    {
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId = '\(currentSharedEvent.objectId!)' GROUP BY objectId ORDER BY createdAt DESC", whereFields: [])
        
        var postObjectIds: Array<String>
        
        postObjectIds = []
        
        if (resultSet != nil) {
            while resultSet.next() {
                postObjectIds.append(resultSet.stringForColumn("objectId"))
            }
        }
        
        resultSet.close()
        
        
        let postObjectIdsString = postObjectIds.joinWithSeparator("','")
        
        ////println("Ids: \(postObjectIdsString)")
        
        let predicate = NSPredicate(format: "NOT (objectId IN {'\(postObjectIdsString)'}) AND eventObjectId = '\(currentSharedEvent.objectId!)'")
        
        let query = PFQuery(className:"EventImages", predicate: predicate)
        
        query.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
        
        let resultSetLikes: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["*"], whereString: "eventObjectId = '\(currentSharedEvent.objectId!)'", whereFields: [])
        
        var likeObjectIds: Array<String>
        
        likeObjectIds = []
        
        if (resultSetLikes != nil) {
            while resultSetLikes.next() {
                likeObjectIds.append(resultSetLikes.stringForColumn("objectId"))
            }
        }
        
        resultSetLikes.close()
        
        
        let likeObjectIdsString = likeObjectIds.joinWithSeparator("','")
        
        ////println("Ids: \(likeObjectIdsString)")
        
        let likePredicate = NSPredicate(format: "NOT (objectId IN {'\(likeObjectIdsString)'}) AND eventObjectId = '\(currentSharedEvent.objectId!)'")
        
        let likeQuery = PFQuery(className:"PostLikes", predicate: likePredicate)
        
        ParseOperations.instance.fetchData(likeQuery, target: self, successSelector: "fetchAllLikesSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllLikesError:", errorSelectorParameters:nil)
        
    }
    
    
    
    func updateData()
    {
        let invitationId = currentSharedEvent["invitationId"] as! String
        
        let textUpdatePredicate = NSPredicate(format: "objectId = '\(invitationId)' AND eventObjectId = '\(currentSharedEvent.objectId!)' AND isTextUpdated = true")
        
        var likeQuery = PFQuery(className:"Invitations", predicate: textUpdatePredicate)
        
        //ParseOperations.instance.fetchData(likeQuery, target: self, successSelector:  "fetchTextUpdatesSuccess:", successSelectorParameters: nil, errorSelector: "fetchTextUpdatesError:", errorSelectorParameters:nil)
    }
    
    func fetchTextUpdatesSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if var fetchedobjects = objects
        {
            if fetchedobjects.count > 0
            {
                var i = 0
                
                for invitation in fetchedobjects
                {
                    fetchedobjects[i]["isTextUpdated"] = false
                    i++
                }
                
                PFObject.saveAllInBackground(fetchedobjects)
                
                let predicate = NSPredicate(format: "postType = 'text' AND eventObjectId = '\(currentSharedEvent.objectId!)'")
                
                let query = PFQuery(className:"EventImages", predicate: predicate)
                
                query.orderByAscending("createdAt")
                
                ParseOperations.instance.fetchData(query, target: self, successSelector: "fetchAllPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchAllPostsError:", errorSelectorParameters:nil)
            }
            
        }
    }
    
    func fetchTextUpdatesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        ////println("Error: \(error) \(error.userInfo!)")
    }
    
    func fetchAllLikesSuccess(timer:NSTimer)
    {
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        if let fetchedobjects = objects {
            let i=0;
            for object in fetchedobjects
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["eventObjectId"] = object["eventObjectId"] as? String
                tblFields["postObjectId"] = object["postObjectId"] as? String
                tblFields["userObjectId"] = object["userObjectId"] as? String
                tblFields["objectId"] = object.objectId!
                tblFields["isUpdated"] = "1"
                
                
                var insertedId = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
            }
            
            self.refreshList()
        }
    }
    
    func fetchAllLikesError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        ////println("Error: \(error) \(error.userInfo!)")
    }
    
    
    func deleteData()
    {
        let updatePredicate = NSPredicate(format: "eventObjectId = '\(currentSharedEvent.objectId!)'")
        
        let updateQuery = PFQuery(className:"EventImages", predicate: updatePredicate)
        
        updateQuery.orderByAscending("createdAt")
        
        ParseOperations.instance.fetchData(updateQuery, target: self, successSelector: "fetchExistingPostsSuccess:", successSelectorParameters: nil, errorSelector: "fetchExistingPostsError:", errorSelectorParameters:nil)
    }
    
    
    func fetchExistingPostsSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        ////println("Successfully retrieved \(objects!.count) events.")
        
        if let fetchedobjects = objects
        {
            
            var i = 0
            
            var existingPostObjectIds: Array<String>
            existingPostObjectIds = []
            
            for postObject in fetchedobjects
            {
                existingPostObjectIds.append(postObject.objectId!)
            }
            
            let existingPostObjectIdsString = existingPostObjectIds.joinWithSeparator("','")
            
            
            
            var whereQuery = ""
            
            if existingPostObjectIdsString != ""
            {
                whereQuery = "eventObjectId = '\(currentSharedEvent.objectId!)' AND objectId NOT IN ('\(existingPostObjectIdsString)') AND objectId != ''"
            }
            else
            {
                whereQuery = "eventObjectId = '\(currentSharedEvent.objectId!)' AND objectId != ''"
            }
            
            let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["objectId"], whereString: whereQuery, whereFields: [])
            
            
            var nonExistingPostObjectIds: Array<String>
            nonExistingPostObjectIds = []
            
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    nonExistingPostObjectIds.append(resultSet.stringForColumn("objectId"))
                }
            }
            print ("1")
            activityIndicatorView.stopAnimation()
            
            resultSet.close()
            
            let nonExistingPostObjectIdsString = nonExistingPostObjectIds.joinWithSeparator("','")
            
            if nonExistingPostObjectIdsString != ""
            {
                ModelManager.instance.deleteTableData("EventImages", whereString: "objectId IN ('\(nonExistingPostObjectIdsString)')", whereFields: [])
                
                ModelManager.instance.deleteTableData("PostLikes", whereString: "postObjectId IN ('\(nonExistingPostObjectIdsString)')", whereFields: [])
                
                refreshList()
            }
            
        }
    }
    
    
    func fetchExistingPostsError(timer:NSTimer)
    {
        print ("2")

        activityIndicatorView.stopAnimation()
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        ////println("Error: \(error) \(error.userInfo!)")
    }
    
    
    
    
    func fetchAllPostsSuccess(timer:NSTimer)
    {
        
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        isSharedPostUpdated = false
        ////println("Successfully retrieved \(objects!.count) posts.")
        
        if let fetchedobjects = objects {
            mySharedEventData = fetchedobjects
            var i=0;
            for post in mySharedEventData
            {
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["postData"] = post["postData"] as? String
                
                tblFields["isApproved"] = "0"
                tblFields["isRead"] = "1"
                
                let postHeight = post["postHeight"] as! CGFloat
                let postWidth = post["postWidth"] as! CGFloat
                
                tblFields["postHeight"] = "\(postHeight)"
                tblFields["postWidth"] = "\(postWidth)"
                tblFields["eventObjectId"] = post["eventObjectId"] as? String
                
                tblFields["eventFolder"] = post["eventFolder"] as? String
                
                tblFields["postType"] = post["postType"] as? String
                
                tblFields["objectId"] = post.objectId!
                tblFields["isPosted"] = "1"
                
                var date = ""
                
                if post.createdAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.createdAt)!)
                    ////println(date)
                    tblFields["createdAt"] = date
                }
                
                if post.updatedAt != nil {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
                    date = dateFormatter.stringFromDate((post.updatedAt)!)
                    ////println(date)
                    tblFields["updatedAt"] = date
                }
                
                
                let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["count(*) as count"], whereString: "objectId = '\(post.objectId!)'", whereFields: [])
                
                resultSet.next()
                
                let noOfPosts = Int(resultSet.intForColumn("count"))
                
                resultSet.close()
                
                if noOfPosts > 0
                {
                    let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "objectId=?", whereFields: [post.objectId!])
                }
                else
                {
                    var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFields)
                }
                
            }
            print ("3")

            activityIndicatorView.stopAnimation()
            
            self.refreshList()
        }
    }
    
    func fetchAllPostsError(timer:NSTimer)
    {
        print ("4")

        activityIndicatorView.stopAnimation()
        
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        ////println("Error: \(error) \(error.userInfo!)")
    }
    
    
    
    func doneButtonClick(sender: NSNotification){
        ////println("jiouiop")
        moviePlayer?.setFullscreen(false, animated: true)
        moviePlayer?.stop()
        moviePlayer?.view.hidden = true
    }
    
    
    func refreshList()
    {
        //isPostDataUpDated = false
        
        //mySharedEventData.removeAll()
        //mySharedRowHeights.removeAll()
        ////println("fetching....")
        
        let currentEventId = currentSharedEvent.objectId as String!
        
        
        let resultSet: FMResultSet! = ModelManager.instance.getTableData("EventImages", selectColumns: ["*"], whereString: "eventObjectId=? ORDER BY createdAt DESC LIMIT 0, \(mySharedEventData.count + 10)", whereFields: [currentEventId])
        
        mySharedEventData = []
        mySharedRowHeights.removeAll()
        
        likeObjectIds = []
        var i = 0
        
        if (resultSet != nil) {
            while resultSet.next() {
                
                let userpost = PFObject(className: "EventImages")
                
                userpost["eventImageId"] = Int(resultSet.intForColumn("eventImageId"))
                userpost["postData"] = resultSet.stringForColumn("postData")
                userpost["eventFolder"] = resultSet.stringForColumn("eventFolder")
                
                
                userpost["postType"] = resultSet.stringForColumn("postType")
                userpost["eventObjectId"] = resultSet.stringForColumn("eventObjectId")
                userpost["postHeight"] = resultSet.doubleForColumn("postHeight")
                userpost["postWidth"] = resultSet.doubleForColumn("postWidth")
                
                
                
                userpost["dateCreated"] = stringToDate(resultSet.stringForColumn("dateCreated"))
                userpost["dateUpdated"] = stringToDate(resultSet.stringForColumn("dateUpdated"))
                
                ////println(resultSet.stringForColumn("createdAt"))
                
                if(resultSet.stringForColumn("createdAt") != "" && resultSet.stringForColumn("updatedAt") != "" && resultSet.stringForColumn("createdAt") != nil && resultSet.stringForColumn("updatedAt") != nil)
                {
                    userpost.objectId = resultSet.stringForColumn("objectId")
                }
                else
                {
                    
                }
                
                
                let isPosted = resultSet.stringForColumn("isPosted")
                
                
                if isPosted == "0"
                {
                    userpost["isPosted"] = false
                }
                else
                {
                    userpost["isPosted"] = true
                }
                
                
                let isApproved = resultSet.stringForColumn("isApproved")
                
                if(isApproved != nil)
                {
                    if isApproved == "0"
                    {
                        userpost["isApproved"] = false
                    }
                    else
                    {
                        userpost["isApproved"] = true
                    }
                }
                else
                {
                    userpost["isApproved"] = false
                }
                
                
                userpost["isDownloading"] = true
                
                mySharedEventData.append(userpost)
                
                ////println(userpost["isPosted"]!)
                
                
                var rowHeight:CGFloat = 380.0
                
                let extraHeight:CGFloat = 0
                
                if(userpost["postType"] as! String == "text")
                {
                    //extraHeight = 36.0
                    let postText = userpost["postData"] as! String
                    
                    let senderMessageTemp = UITextView()
                    
                    senderMessageTemp.frame.size.width = self.view.frame.width-self.view.frame.width*(40.0/320)
                    //senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
                    
                    senderMessageTemp.text = postText
                    
                    let style = NSMutableParagraphStyle()
                    style.lineSpacing = 8
                    let attributes = [NSParagraphStyleAttributeName : style]
                    
                    
                    var attrs = [
                        NSFontAttributeName : UIFont.systemFontOfSize(20.0),
                        NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
                        NSUnderlineStyleAttributeName : 1]
                    
                    
                    senderMessageTemp.attributedText = NSAttributedString(string: postText, attributes:attributes)
                    senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20.0)
                    
                    let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
                    var frame = senderMessageTemp.frame
                    frame.size.height = contentSize.height
                    senderMessageTemp.frame = frame
                    
                    ////println(rowHeight)
                    
                    rowHeight = contentSize.height + (0.120625 * self.view.frame.height) + (58.0 * self.view.frame.height)/568
                }
                else
                {
                    ////println(userpost["postHeight"])
                    let rheight = userpost["postHeight"] as! CGFloat
                    let rwidth = userpost["postWidth"] as! CGFloat
                    
                    ////println("height: \(rheight)")
                    
                    rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+((52.0/568)*self.view.frame.height)
                }
                
                if i == 0
                {
                    //rowHeight = rowHeight
                }
                
                mySharedRowHeights.append(rowHeight+extraHeight)
                
                i++
            }
        }
        
        resultSet.close()
        ////println(mySharedRowHeights)
        
        for var j = 0; j < mySharedEventData.count; j++
        {
            
            let resultSetCount: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["count(*) as count"], whereString: "postObjectId=? AND userObjectId=?", whereFields: [mySharedEventData[j].objectId!, currentUserId])
            
            resultSetCount.next()
            
            let userLikeCount = resultSetCount.intForColumn("count")
            resultSetCount.close()
            
            if userLikeCount > 0
            {
                
                let resultSetObjectId: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["*"], whereString: "postObjectId=? AND userObjectId=?", whereFields: [mySharedEventData[j].objectId!, currentUserId])
                
                resultSetObjectId.next()
                
                let userLikeObjectId = resultSetObjectId.stringForColumn("objectId")
                resultSetObjectId.close()
                
                likeObjectIds.append(userLikeObjectId)
            }
            else
            {
                likeObjectIds.append("")
            }
            
            
        }
        
        
        self.tableView.reloadData()
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["isRead"] = "1"
        
        let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventObjectId=?", whereFields: [currentSharedEvent.objectId!])
        if isUpdated {
            ////println("Record Updated Successfully")
            ////println("eventImage")
        } else {
            ////println("Record not Updated Successfully")
        }
        
        
    }
    
    
    func stringToDate(dateString: String)->NSDate
    {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let date = dateFormatter.dateFromString(dateString)
        
        return date!
    }
    var Timestamp: NSTimeInterval {
        return NSDate().timeIntervalSince1970 * 1000
    }
    
    func likeSelectedPost(sender : UIButton)
    {
        var isrsvp = ""
        
        if currentSharedEvent["isRSVP"] as! Bool == true
        {
            isrsvp = "true"
        }
        else
        {
            isrsvp = "false"
        }

        
        self.view.userInteractionEnabled = false
        let abc = String(format: "%.0f", Timestamp)
        
        // messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentSharedEvent.objectId!)/\(arrdata[sender.tag]["keyvalue"])")
        let button = sender as UIButton
        if !button.selected
        {            sender.enabled = true
            
            messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentSharedEvent.objectId!)/\(arrdata[sender.tag]["keyvalue"])")
            
            
            
            
            
            messagesRef.childByAutoId().setValue([
                "timestamp":abc,
                "postType" :arrdata[sender.tag]["postType"],
                "userObjectId" : currentUserId,
                "parentkey" :arrdata[sender.tag]["keyvalue"],
                "isRSVP":isrsvp
                
                ], withCompletionBlock: {
                    (error:NSError?, ref:Firebase!) in
                    if (error != nil) {
                        self.view.userInteractionEnabled = true
                        
                        //print("Data could not be saved.")
                    } else {
                        self.view.userInteractionEnabled = true
                        self.tableView.reloadData()
                        
                        //print("Data saved successfully!")
                    }
            })
        }
        else
        {
            sender.enabled = false
            var checkbool = String()
            var tempint = Int()
            for var i = 0; i < arrLikeDic.count; i++
            {
                let temp1 = arrLikeDic[i].valueForKey("parentkey") as! String
                if temp1 == arrdata[sender.tag].valueForKey("keyvalue") as! String
                {
                    if  arrLikeDic[i].valueForKey("userObjectId")as! String == currentUserId
                    {
                        checkbool = arrLikeDic[i].valueForKey("keyvalue")as! String
                        tempint = i
                        break
                    }
                }
                checkbool = "hi"
            }
            if checkbool != "hi"
            {
                
                //messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes")
                messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Likes/\(currentSharedEvent.objectId!)/\(arrdata[sender.tag]["keyvalue"])/\(checkbool)")
                
                messagesRef.removeValue()
                
                deletelike()
                
                self.arrLikeDic.removeObjectAtIndex(tempint)
              
                self.view.userInteractionEnabled = true
                
                self.tableView.reloadData()
                
                // self.finishReceivingMessage()
                
                
            }
            else
            {
                self.view.userInteractionEnabled = true
                
            }
        }
        
        var likeObject: PFObject!
        likeObject = PFObject(className: "PostLikes")
        likeObject["eventObjectId"] = currentSharedEvent.objectId!
        likeObject["postObjectId"] = mySharedEventData[0].objectId!
        likeObject["userObjectId"] = currentUserId
        likeObject["isUpdated"] = true
        
        var postType = arrdata[sender.tag]["postType"] as! String
        
        if postType == "text"
        {
            postType = "note"
        }
        if !button.selected
        {
            let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            let eventTitle = currentSharedEvent["eventTitle"] as! String
            
            let notifMessage = "\(fullUserName) liked a \(postType) in your event, \(eventTitle)."
            let userObjectId = currentSharedEvent["eventCreatorObjectId"] as! String
            
            let notificationObject = PFObject(className: "Notifications")
            notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
            notificationObject["notificationImage"] = "profilePic.png"
            notificationObject["senderId"] = currentUserId
            notificationObject["receiverId"] = userObjectId
            notificationObject["notificationActivityMessage"] = notifMessage
            notificationObject["eventObjectId"] = currentSharedEvent.objectId!
            notificationObject["notificationType"] = "likedpost"
            
            
            
            
            notificationObject.saveInBackground()
            
            //var createdAt = ""
            //var updatedAt = ""
            
            
            var data: Dictionary<String, String!> = [
                "alert" : "\(notifMessage)",
                "notifType" :  "likedpost",
                "eventObjectId": currentSharedEvent.objectId!,
                "userObjectId": currentUserId,
                //"postObjectId": postObjectId,
                "objectId" : "\(currentSharedEvent.objectId!)",
                "isUpdated" : "\(isUpdated)",
                //"createdAt": "\(createdAt)",
                //"updatedAt": "\(updatedAt)",
                "badge": "Increment",
                "sound" : "default"
            ]
            
            
            
            
            
            //        if currentSharedEvent["isRSVP"] as! Bool == true
            //        {
            //            eventType = "rsvp"
            //        }
            
            /* var eventFolder = currentSharedEvent["eventFolder"] as! String!
            var eventImage = currentSharedEvent["eventImage"] as! String!
            
            let emailPredicate = NSPredicate(format: "objectId IN {'\(userObjectId)'}")
            
            let emailQuery = PFUser.queryWithPredicate(emailPredicate)
            
            
            emailQuery!.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            ////println(users!.count)
            if let users = users as? [PFUser]
            {
            for user in users
            {
            var contentLike = ContentLike()
            
            var emailMessage = contentLike.emailMessage(currentSharedEvent.objectId!, eventTitle: currentSharedEvent["eventTitle"] as! String, imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: eventType, guestName: fullUserName)
            
            var sendEmailObject = SendEmail()
            
            sendEmailObject.sendEmail(notifMessage, message: emailMessage, emails:[user.email!])
            
            }
            }
            }*/
            
            
            
            messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(currentSharedEvent.objectId!)/\(userObjectId)")
            // let abc = String(format: "%.0f", Timestamp)
            
            messagesRef.childByAutoId().setValue([
                "SenderID":self.currentUserId,
                "timestamp" :abc,
                "eventid" :currentSharedEvent.objectId!,
                "isRSVP":isrsvp
                
                
                ])
            
            
            var predicateString: String! = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = true"
            
            sendParsePush(predicateString, data: data)
            
            
            data["sound"] = ""
            
            predicateString = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = false"
            
            sendParsePush(predicateString, data: data)
        }
        
        
    }
    
    
    func likePostSuccess(timer:NSTimer)
    {
        
        let object = timer.userInfo?.valueForKey("internal") as! PFObject
        
        let postType = timer.userInfo?.valueForKey("external") as! String
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["eventObjectId"] = object["eventObjectId"] as? String
        tblFields["postObjectId"] = object["postObjectId"] as? String
        tblFields["userObjectId"] = currentUserId
        tblFields["objectId"] = object.objectId!
        tblFields["isUpdated"] = "1"
        
        let insertedId = ModelManager.instance.addTableData("PostLikes", primaryKey: "postLikeId", tblFields: tblFields)
        if insertedId > 0 {
                    } else {
        }
        
        
        let fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        let eventTitle = currentSharedEvent["eventTitle"] as! String
        
        let notifMessage = "\(fullUserName) liked a \(postType) in your event, \(eventTitle)."
        let userObjectId = currentSharedEvent["eventCreatorObjectId"] as! String
        
        let notificationObject = PFObject(className: "Notifications")
        notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
        notificationObject["notificationImage"] = "profilePic.png"
        notificationObject["senderId"] = currentUserId
        notificationObject["receiverId"] = userObjectId
        notificationObject["notificationActivityMessage"] = notifMessage
        notificationObject["eventObjectId"] = currentSharedEvent.objectId!
        notificationObject["notificationType"] = "likedpost"
        
        
        
        
        notificationObject.saveInBackground()
        
        //var createdAt = ""
        //var updatedAt = ""
        
        let postObjectId = object["postObjectId"] as! String
        let isUpdated = object["isUpdated"] as! Bool
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        let createdAt = dateFormatter.stringFromDate((object.createdAt)!)
        
        let updatedAt = dateFormatter.stringFromDate((object.updatedAt)!)
        
        var data: Dictionary<String, String!> = [
            "alert" : "\(notifMessage)",
            "notifType" :  "likedpost",
            "eventObjectId": currentSharedEvent.objectId!,
            "userObjectId": currentUserId,
            "postObjectId": postObjectId,
            "objectId" : "\(object.objectId!)",
            "isUpdated" : "\(isUpdated)",
            "createdAt": "\(createdAt)",
            "updatedAt": "\(updatedAt)",
            "badge": "Increment",
            "sound" : "default"
        ]
        
        
        
        
        
        //        if currentSharedEvent["isRSVP"] as! Bool == true
        //        {
        //            eventType = "rsvp"
        //        }
        
        /* var eventFolder = currentSharedEvent["eventFolder"] as! String!
        var eventImage = currentSharedEvent["eventImage"] as! String!
        
        let emailPredicate = NSPredicate(format: "objectId IN {'\(userObjectId)'}")
        
        let emailQuery = PFUser.queryWithPredicate(emailPredicate)
        
        
        emailQuery!.findObjectsInBackgroundWithBlock {
        (users: [AnyObject]?, error: NSError?) -> Void in
        ////println(users!.count)
        if let users = users as? [PFUser]
        {
        for user in users
        {
        var contentLike = ContentLike()
        
        var emailMessage = contentLike.emailMessage(currentSharedEvent.objectId!, eventTitle: currentSharedEvent["eventTitle"] as! String, imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: eventType, guestName: fullUserName)
        
        var sendEmailObject = SendEmail()
        
        sendEmailObject.sendEmail(notifMessage, message: emailMessage, emails:[user.email!])
        
        }
        }
        }*/
        
        var isrsvp = ""
        
        if currentSharedEvent["isRSVP"] as! Bool == true
        {
            isrsvp = "true"
        }
        else
        {
            isrsvp = "false"
        }

        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/LikeBadges/\(currentSharedEvent.objectId!)/\(userObjectId)")
        let abc = String(format: "%.0f", Timestamp)
        
        messagesRef.childByAutoId().setValue([
            "SenderID":self.currentUserId,
            "timestamp" :abc,
            "eventid" :currentSharedEvent.objectId!,
            "isRSVP":isrsvp
            ])
        
        
        var predicateString: String! = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = true"
        
        sendParsePush(predicateString, data: data)
        
        
        data["sound"] = ""
        
        predicateString = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = false"
        
        sendParsePush(predicateString, data: data)
        
        
        refreshList()
        
    }
    
    func likePostError(timer:NSTimer)
    {
        //var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        ////println("Error: \(error) \(error.userInfo!)")
    }
    
    
    
    
    func sendParsePush(predicateString: String!, data: Dictionary<String, String!>)
    {
        let predicate = NSPredicate(format: predicateString)
        
        //var query = PFInstallation.queryWithPredicate(predicate)
        
        let query = PFUser.queryWithPredicate(predicate)
        
        let push = PFPush()
        
        query!.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]?, error: NSError?) -> Void in
            if error == nil
            {
                if let fetchedObjects = objects as? [PFUser]
                {
                    
                    for object in fetchedObjects
                    {
                        let userObjectId = object.objectId!
                        
                        let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                        
                        //var query = PFInstallation.queryWithPredicate(predicate)
                        //var query = PFUser.queryWithPredicate(predicate)
                        
                        let query = PFQuery(className: "BadgeRecords", predicate: predicate)
                        
                        query.findObjectsInBackgroundWithBlock {
                            (objects: [AnyObject]?, error: NSError?) -> Void in
                            
                            if error == nil
                            {
                                ////println(objects?.count)
                                if let fetchedObjects = objects as? [PFObject]
                                {
                                    var badgeObject: PFObject!
                                    var badgeCount = 0
                                    
                                    if fetchedObjects.count > 0
                                    {
                                        for object in fetchedObjects
                                        {
                                            badgeObject = object
                                            badgeCount = badgeObject["badgeCount"] as! Int
                                        }
                                    }
                                    else
                                    {
                                        badgeObject = PFObject(className: "BadgeRecords")
                                        
                                        badgeObject["userObjectId"] = userObjectId
                                    }
                                    
                                    
                                    
                                    var newdata: Dictionary<String, String!>! = data
                                    
                                    newdata["badge"] = "\(badgeCount + 1)"
                                    
                                    let predicate = NSPredicate(format: "userObjectId = '\(userObjectId)'")
                                    
                                    let query = PFInstallation.queryWithPredicate(predicate)
                                    
                                    push.setQuery(query)
                                    
                                    push.setData(newdata)
                                    push.sendPushInBackground()
                                    
                                    badgeObject["badgeCount"] = badgeCount+1
                                    
                                    badgeObject.saveInBackground()
                                    
                                }
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            else
            {
            }
        }
    }
    
    
    func deletePostLikeSuccess(timer:NSTimer)
    {
        
        let object = timer.userInfo?.valueForKey("internal") as! PFObject
        
        let isDeleted = ModelManager.instance.deleteTableData("PostLikes", whereString: "objectId=?", whereFields: [object.objectId!])
        if isDeleted {
            
        } else {
        }
        
        refreshList()
        
        
    }
    
    func deletePostLikeError(timer:NSTimer)
    {
        
        let likeObject: PFObject = timer.userInfo?.valueForKey("external") as! PFObject
        
        let isDeleted = ModelManager.instance.deleteTableData("PostLikes", whereString: "objectId=?", whereFields: [likeObject.objectId!])
        if isDeleted {
          
        } else {
        }
        
        refreshList()
        
    }
    
    func showImageInFullScreenView(sender: UITapGestureRecognizer) {
        
        let row = sender.view!.tag
        
        let postType = arrdata[row]["postType"] as! String!
        
        if(postType == "image")
        {
            let manager = NSFileManager.defaultManager()
            
            let eventStreamImageFile = arrdata[row]["postData"] as! String
            
            let eventStreamImagePath = "\(documentDirectory)/\(eventStreamImageFile)"
            
            if (manager.fileExistsAtPath(eventStreamImagePath))
            {
                let image = UIImage(named: eventStreamImagePath)
                
                let eventPhotoFullScreenVC = self.storyboard?.instantiateViewControllerWithIdentifier("EventPhotoFullScreenViewController") as! EventPhotoFullScreenViewController
                eventPhotoFullScreenVC.image = image
                self.navigationController?.pushViewController(eventPhotoFullScreenVC, animated: false)
            }
            else
            {
                let refreshAlert = UIAlertController(title: "Error", message: "Please wait while the image is downloading.", preferredStyle: UIAlertControllerStyle.Alert)
                
                refreshAlert.addAction(UIAlertAction(title: "OK", style: .Default, handler: { (action: UIAlertAction) in
                    
                }))
                
                self.presentViewController(refreshAlert, animated: true, completion: nil)
            }
        }
    }
    
    func playSelectedVideo(sender : UIButton) {
        let videoUrl: String = (arrdata[sender.tag]["postData"] as? String)!
        
        let manager = NSFileManager.defaultManager()
        if (manager.fileExistsAtPath("\(documentDirectory)/\(videoUrl)"))
        {
            moviePlayer?.contentURL = NSURL(fileURLWithPath: "\(documentDirectory)/\(videoUrl)")
            moviePlayer?.view.hidden = false
            moviePlayer!.setFullscreen(true, animated: true)
            //moviePlayer!.scalingMode = .AspectFill
            moviePlayer!.controlStyle = .Embedded
            moviePlayer!.play()
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Video doesn't exist.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    func getFirstFrame(postImageView: UIImageView, videoURL: NSURL, viewTop: CGFloat)
    {
        let asset : AVAsset = AVAsset(URL:videoURL)
        
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        var error       : NSError? = nil
        let time        : CMTime = CMTimeMake(1, 30)
        let img         : CGImageRef! = try? assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
        let frameImg    : UIImage = UIImage(CGImage: img)
        
        
        postImageView.image = frameImg
        
    }
    
    @IBAction func closeImagePreview(sender: AnyObject!)
    {
        selectedImageWrapper.hidden = true
    }
    
    
    @IBAction func attendingButtonClicked(sender: UIButton)
    {
        //updateStatus("yes")
        statusToBeChanged = "yes"
        
        messageTitle.text = "Awesome! Glad you can attend. Tell your host how many from your side will be attending the event."
        messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        noOfAdultsWrapper.hidden = false
        noOfChildWrapper.hidden = false
        
        adultsLabel.hidden = false
        childLabel.hidden = false
        
        messagePopupView.hidden = false
        
        
        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        invitationNoteView.frame.origin.y = (self.view.frame.height*259)/568
    }
    
    
    @IBAction func maybeButtonClicked(sender: UIButton)
    {
        //updateStatus("maybe")
        statusToBeChanged = "maybe"
        
        messageTitle.text = "You haven't decided yet. That's alright. Want to leave a message to the host?"
        messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        invitationNoteView.frame.origin.y = (self.view.frame.height*110)/568
        
        noOfAdultsWrapper.hidden = true
        noOfChildWrapper.hidden = true
        
        adultsLabel.hidden = true
        childLabel.hidden = true
        
        messagePopupView.hidden = false
    }
    
    
    @IBAction func notAttendingButtonClicked(sender: UIButton)
    {
        //updateStatus("no")
        statusToBeChanged = "no"
        
        messageTitle.text = "Bummer! It's sad you can't make it. Want to leave a message to the host?"
        messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        invitationNoteView.frame.origin.y = (self.view.frame.height*110)/568
        
        noOfAdultsWrapper.hidden = true
        noOfChildWrapper.hidden = true
        
        adultsLabel.hidden = true
        childLabel.hidden = true
        
        messagePopupView.hidden = false
    }
    
    
    @IBAction func onlineButtonClicked(sender: UIButton)
    {
        //updateStatus("online")
        statusToBeChanged = "online"
        
        messageTitle.text = "Great! You are going to follow the event online. Want to leave a message to the host?"
        messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
        messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
        
        invitationNoteView.frame.origin.y = (self.view.frame.height*110)/568
        
        noOfAdultsWrapper.hidden = true
        noOfChildWrapper.hidden = true
        
        adultsLabel.hidden = true
        childLabel.hidden = true
        
        messagePopupView.hidden = false
        
    }
    
    
    @IBAction func goButtonClicked(sender: UIButton)
    {
        updateStatus(statusToBeChanged)
        
    }
    
    
    @IBAction func reselectStatusButtonClicked(sender: UIButton)
    {
        
        openMeassageButtonView.hidden = true
        
        statusButtonsView.hidden = false
        statusInfoView.hidden = true
    }
    
    
    @IBAction func editStatusInfoButtonClicked(sender: UIButton)
    {
        
        if currentStatus == "yes"
        {
            
            noOfAdultsWrapper.hidden = false
            noOfChildWrapper.hidden = false
            
            adultsLabel.hidden = false
            childLabel.hidden = false
            
            messagePopupView.hidden = false
            
            messageTitle.text = "Awesome! Glad you can attend. Tell your host how many from your side will be attending the event."
            messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            
            invitationNoteView.frame.origin.y = (self.view.frame.height*259)/568
        }
        else
        {
            noOfAdultsWrapper.hidden = true
            noOfChildWrapper.hidden = true
            
            adultsLabel.hidden = true
            childLabel.hidden = true
            
            messagePopupView.hidden = false
            
            messageTitle.text = "Your message to the host."
            messageTitle.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            messageTitle.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
            
            invitationNoteView.frame.origin.y = (self.view.frame.height*100)/568
        }
        
        
        let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
        let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
        
        
        noOfAdultsText.text = "\(noOfAdults)"
        noOfChildText.text = "\(noOfChilds)"
        
        
        invitationNoteView.text = currentSharedEvent["invitationNote"] as! String
    }
    
    
    @IBAction func closePopupButtonClicked(sender: UIButton)
    {
        messagePopupView.hidden = true
        print (currentSharedEvent)
        
        if currentStatus != ""
        {
            
            if currentStatus == "yes"
            {
                let noOfAdults = currentSharedEvent["noOfAdults"] as! Int
                let noOfChilds = currentSharedEvent["noOfChilds"] as! Int
                
                statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
                attendingStatusImageView.image = UIImage(named:"check-green.png")
            }
            
            if currentStatus == "online"
            {
                statusInfo.text = "I will follow online"
                attendingStatusImageView.image = UIImage(named:"web-globe.png")
            }
            
            if currentStatus == "maybe"
            {
                statusInfo.text = "I am not sure"
                attendingStatusImageView.image = UIImage(named:"questionmark.png")
            }
            
            if currentStatus == "no"
            {
                statusInfo.text = "I will not attend"
                attendingStatusImageView.image = UIImage(named:"cross-red.png")
            }
            
            
            openMeassageButtonView.hidden = false
            statusButtonsView.hidden = true
            statusInfoView.hidden = false
        }
        else
        {
            
            openMeassageButtonView.hidden = true
            statusButtonsView.hidden = false
            statusInfoView.hidden = true
        }
        
        hideKeyBoard()
        
    }
    
    
    func updateStatus(status: String)
    {
        var invitationObject: PFObject!
        invitationObject = PFObject(className: "Invitations")
        invitationObject.objectId = currentSharedEvent["invitationId"] as? String
        invitationObject["attendingStatus"] = status
        invitationObject["isUpdated"] = true
        
        
        
        var noOfAdults = Int(noOfAdultsText.text!)
        var noOfChilds = Int(noOfChildText.text!)
        
        if noOfAdultsText.text == ""
        {
            noOfAdults = 0
        }
        if noOfChildText.text == ""
        {
            
            noOfChilds = 0
        }
        
        
        
        
        print(noOfChilds)
        
        
        
        
        invitationObject["noOfAdults"] = noOfAdults
        invitationObject["noOfChilds"] = noOfChilds
        
        invitationObject["invitationNote"] = invitationNoteView.text!
        
        if ((noOfAdults! + noOfChilds!) > 0 && status == "yes") || (status != "yes")
        {
            ParseOperations.instance.saveData(invitationObject, target: self, successSelector: "updateStatusSuccess:", successSelectorParameters: status, errorSelector: "updateStatusError:", errorSelectorParameters: nil)
        }
        else
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Please enter atleast one Adult or Child.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
    }
    
    
    func updateStatusSuccess(timer:NSTimer)
    {
        
        let invitation = timer.userInfo?.valueForKey("internal") as! PFObject
        let status = timer.userInfo?.valueForKey("external") as! String
        
        print(invitation)
        
        //////println("Successfully retrieved \(object!.count) posts.")
        
        
        let noOfAdults = invitation["noOfAdults"] as! Int
        let noOfChilds = invitation["noOfChilds"] as! Int
        let invitationNote = invitation["invitationNote"] as! String
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["attendingStatus"] = status
        tblFields["noOfAdults"] = "\(noOfAdults)"
        tblFields["noOfChilds"] = "\(noOfChilds)"
        
        tblFields["invitationNote"] = invitationNote
        
        let isUpdated = ModelManager.instance.updateTableData("Invitations", tblFields: tblFields, whereString: "objectId=?", whereFields: [invitation.objectId!])
        if isUpdated {
            ////println("Record Updated Successfully")
            ////println("eventImage")
        } else {
            ////println("Record not Updated Successfully")
        }
        
        
        
        let userObjectId = currentSharedEvent["eventCreatorObjectId"] as! String
        
        
        let noOfChildNew = currentSharedEvent["noOfChilds"] as! Int
        let noOfAdultsNew = currentSharedEvent["noOfAdults"] as! Int
        
        let predicate = NSPredicate(format: "objectId IN {'\(userObjectId)'}")
        
        var query = PFQuery(className:"User", predicate: predicate)
        
        var eventType = "online"
        
        if currentSharedEvent["isRSVP"] as! Bool == true
        {
            eventType = "rsvp"
        }
        
        var eventFolder = currentSharedEvent["eventFolder"] as! String!
        var eventImage = currentSharedEvent["eventImage"] as! String!
        
        let emailPredicate = NSPredicate(format: "objectId IN {'\(userObjectId)'}")
        
        let emailQuery = PFUser.queryWithPredicate(emailPredicate)
        
        let currentStatusTemp = self.currentStatus
        
        emailQuery!.findObjectsInBackgroundWithBlock {
            (users: [AnyObject]?, error: NSError?) -> Void in
            ////println(users!.count)
            if let users = users as? [PFUser]
            {
                for user in users
                {
                    self.fetchHostDataSuccess(user, attendees: [noOfAdults, noOfChilds, noOfAdultsNew, noOfChildNew], currentStatus: currentStatusTemp,invitation:invitation,invitationNote:invitationNote)
                }
            }
        }
        
        
        
        var notifMessage = ""
        
        if currentStatus == ""
        {
            notifMessage = "\(fullUserName) responded to the invitation for your event, \(eventTitle)."
            
        }
        else
        {
            if statusToBeChanged == "yes" && currentStatus == "yes"
            {
                if (currentSharedEvent["noOfAdults"] as! Int) != noOfAdults || (currentSharedEvent["noOfChilds"] as! Int) != noOfChilds
                {
                    notifMessage = "\(fullUserName) changed guest count for your event, \(eventTitle)."
                    
                }
            }
            else if statusToBeChanged != currentStatus
            {
                notifMessage = "\(fullUserName) changed status for your event, \(eventTitle)."
            }
        }
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
        
        //var createdAt = dateFormatter.stringFromDate((invitation.createdAt)!)
        
        //var updatedAt = dateFormatter.stringFromDate((invitation.updatedAt)!)
        
        let createdAt = ""
        
        let updatedAt = ""
        
        
        let eventObjectId = currentSharedEvent.objectId!
        
        let email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        
        let eventCreatorId = currentSharedEvent["eventCreatorObjectId"] as! String
        
        if notifMessage != ""
        {
            var data: Dictionary<String, String!> = [
                "alert" : "\(notifMessage)",
                "notifType" :  "invitationresponse",
                "userObjectId": "\(currentUserId)",
                "eventObjectId": "\(eventObjectId)",
                "objectId": "\(invitation.objectId!)",
                "invitedName": "\(fullUserName)",
                "attendingStatus" : "\(status)",
                "noOfChilds": "\(noOfChilds)",
                "noOfAdults": "\(noOfAdults)",
                "invitationNote": "\(invitationNote)",
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "isUpdated": "\(isUpdated)",
                "emailId": "\(email)",
                "badge": "Increment",
                "sound" : "default",
                "eventCreatorId" : "\(eventCreatorId)"
            ]
            
            ////println(data)
            
            
            let notificationObject = PFObject(className: "Notifications")
            notificationObject["notificationFolder"] = "\(currentUserId)/profilePic/"
            notificationObject["notificationImage"] = "profilePic.png"
            notificationObject["senderId"] = currentUserId
            notificationObject["receiverId"] = userObjectId
            notificationObject["notificationActivityMessage"] = notifMessage
            notificationObject["eventObjectId"] = currentSharedEvent.objectId!
            notificationObject["notificationType"] = "invitationresponse"
            
            notificationObject.saveInBackground()
            
            
            var predicateString: String! = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = true"
            
            sendParsePush(predicateString, data: data)
            
            
            data["sound"] = ""
            
            predicateString = "objectId IN {'\(userObjectId)'} AND guestActivityNotification = true AND allowSound = false"
            
            sendParsePush(predicateString, data: data)
            
        }
        
        
        currentSharedEvent["noOfAdults"] = noOfAdults
        currentSharedEvent["noOfChilds"] = noOfChilds
        
        currentSharedEvent["invitationNote"] = invitation["invitationNote"] as! String
        
        
        messagePopupView.hidden = true
        
        currentStatus = statusToBeChanged
        
        if currentStatus == "yes"
        {
            statusInfo.text = "I am attending (\(noOfAdults+noOfChilds))"
            attendingStatusImageView.image = UIImage(named:"check-green.png")
        }
        
        if currentStatus == "online"
        {
            statusInfo.text = "I will follow online"
            attendingStatusImageView.image = UIImage(named:"web-globe.png")
        }
        
        if currentStatus == "maybe"
        {
            statusInfo.text = "I am not sure"
            attendingStatusImageView.image = UIImage(named:"questionmark.png")
        }
        
        if currentStatus == "no"
        {
            statusInfo.text = "I will not attend"
            attendingStatusImageView.image = UIImage(named:"cross-red.png")
        }
        
        
        openMeassageButtonView.hidden = false
        statusButtonsView.hidden = true
        statusInfoView.hidden = false
        
    }
    
    func updateStatusError(timer:NSTimer)
    {
        var error: NSError = timer.userInfo?.valueForKey("internal") as! NSError
        ////println("Error: \(error) \(error.userInfo!)")
    }
    
    
    
    func getAttendingStatus(userevent: PFObject) -> String
    {
        let statusResultSet: FMResultSet! = ModelManager.instance.getTableData("Invitations", selectColumns: ["*"], whereString: "eventObjectId = '\(userevent.objectId!)' AND userObjectId = '\(currentUserId)' LIMIT 1", whereFields: [])
        
        statusResultSet.next()
        
        let attendingStatus = statusResultSet.stringForColumn("attdendingStatus")
        
        statusResultSet.close()
        
        return attendingStatus
    }
    
    
    func fetchHostDataSuccess(user:PFUser, attendees: [Int], currentStatus: String,invitation:PFObject,invitationNote:String)
    {
        
        
        var guestActivityNotification = Bool()
        guestActivityNotification = user["guestActivityEmail"] as! Bool
        print(user["guestActivityEmail"])
        
        
        if guestActivityNotification == true
        {
            var objectId = invitation.objectId!
            
            
            let hostEmail = user.email!
            
            
            ////println("host email: \(hostEmail)")
            
            let email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
            //var fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
            //var eventTitle = currentSharedEvent["eventTitle"] as! String
            
            let eventLatitude = currentSharedEvent["eventLatitude"] as! Double
            let eventLongitude = currentSharedEvent["eventLongitude"] as! Double
            
            let createdAt = ""
            let updatedAt = ""
            
            let localNotification = UILocalNotification()
            
            localNotification.fireDate = NSDate()
            
            var notifMessage = ""
            
            ////println(hostEmail)
            //
            let eventCreatorId = currentSharedEvent["eventCreatorObjectId"] as! String
            
            
            let Device = UIDevice.currentDevice()
            
            let iosVersion = NSString(string: Device.systemVersion).doubleValue
            
            let iOS9 = iosVersion >= 9
            
            var type = ""
            
            if self.statusToBeChanged == "no" || self.statusToBeChanged == "maybe"
            {
                
                type = "changeresponse"
            }
            else
            {
                type = "invitationresponse"
                
            }
            
            
            
            
            if iOS9
            {
                
                let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
                branchUniversalObject.title = "\(eventTitle)"
                branchUniversalObject.contentDescription = "My Content Description"
                // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
                branchUniversalObject.addMetadataKey("alert", value:"\(notifMessage)")
                branchUniversalObject.addMetadataKey("notifType", value: "\(type)")
                branchUniversalObject.addMetadataKey("eventObjectId", value:currentSharedEvent.objectId!)
                branchUniversalObject.addMetadataKey("objectId", value:"\(objectId)")
                branchUniversalObject.addMetadataKey("invitedName", value: "noemail")
                branchUniversalObject.addMetadataKey("attendingStatus", value:"\(self.statusToBeChanged)")
                branchUniversalObject.addMetadataKey("noOfChilds", value:"\(attendees[1])")
                branchUniversalObject.addMetadataKey("noOfAdults", value:"\(attendees[0])")
                branchUniversalObject.addMetadataKey("invitationNote", value: "\(invitationNote)")
                
                branchUniversalObject.addMetadataKey("createdAt", value:"\(createdAt)")
                branchUniversalObject.addMetadataKey("updatedAt", value:"\(updatedAt)")
                branchUniversalObject.addMetadataKey("isUpdated", value:"\(isUpdated)")
                branchUniversalObject.addMetadataKey("emailId", value:"\(email)")
                branchUniversalObject.addMetadataKey("userObjectId", value: "\(currentUserId)")
                branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(eventCreatorId)")
                branchUniversalObject.addMetadataKey("isRSVP", value:"true")
                branchUniversalObject.addMetadataKey("linkForguest", value:"false")
                let linkProperties: BranchLinkProperties = BranchLinkProperties()
                linkProperties.feature = "sharing"
                //linkProperties.channel = "email"
                
                linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/invite/\(currentSharedEvent.objectId!)")
                linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/invite/\(currentSharedEvent.objectId!)")
                linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/invite/\(currentSharedEvent.objectId!)")
                
                
                
                
                let linkForHost: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
                linkForHost.title = "\(eventTitle)"
                linkForHost.contentDescription = "My Content Description"
                // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
                linkForHost.addMetadataKey("alert", value:"\(notifMessage)")
                
                print(type)
                
                linkForHost.addMetadataKey("notifType", value: "\(type)")
                linkForHost.addMetadataKey("eventObjectId", value:currentSharedEvent.objectId!)
                linkForHost.addMetadataKey("objectId", value:"\(objectId)")
                linkForHost.addMetadataKey("invitedName", value: "noname")
                linkForHost.addMetadataKey("attendingStatus", value:"\(currentStatus)")
                linkForHost.addMetadataKey("noOfChilds", value:"\(attendees[1])")
                linkForHost.addMetadataKey("noOfAdults", value:"\(attendees[0])")
                linkForHost.addMetadataKey("invitationNote", value: "\(invitationNote)")
                linkForHost.addMetadataKey("createdAt", value:"\(createdAt)")
                linkForHost.addMetadataKey("updatedAt", value:"\(updatedAt)")
                linkForHost.addMetadataKey("isUpdated", value:"\(isUpdated)")
                linkForHost.addMetadataKey("emailId", value:"\(email)")
                linkForHost.addMetadataKey("userObjectId", value: "\(currentUserId)")
                linkForHost.addMetadataKey("eventCreatorId", value:"\(eventCreatorId)")
                linkForHost.addMetadataKey("isRSVP", value:"true")
                linkForHost.addMetadataKey("linkForguest", value:"true")
                
                //          self.linkForHost = BranchLinkfor("\(notifMessage)", eventObjectId: currentSharedEvent.objectId!, attendingStatus: "\(currentStatus)", noOfChilds: "\(attendees[0])", noOfAdults: "\(attendees[0])", invitationNote: "\(invitationNote)", createdAt: "\(createdAt)", updatedAt: "\(updatedAt)", isUpdated: "\(isUpdated)", emailId: "\(email)", userObjectId: "\(currentUserId)", eventCreatorId: "\(eventCreatorId)")
                
                
                let linkHostProperties: BranchLinkProperties = BranchLinkProperties()
                linkHostProperties.feature = "sharing"
                //linkProperties.channel = "email"
                linkHostProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/invite/\(invitation.objectId!)")
                linkHostProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/invite/\(invitation.objectId!)")
                linkHostProperties.addControlParam("$android_url", withValue:"http://eventnode.co/invite/\(invitation.objectId!)")
                var urlString = String()
                
                
                let formatter = NSDateFormatter();
                var timeZoneName = currentSharedEvent["timezoneName"] as! String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
                
                formatter.timeZone = NSTimeZone(name: timeZoneName)
                
                let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
                
                let localTimezoneOffset = Double(NSTimeZone.localTimeZone().secondsFromGMT)
                
                ////println(formatter.dateFromString(formatter.stringFromDate(eventObject["eventStartDateTime"] as! NSDate)))
                
                
                let sdate =  NSDate(timeIntervalSince1970: (currentSharedEvent["eventStartDateTime"] as! NSDate).timeIntervalSince1970 + timezoneOffset - localTimezoneOffset)
                
                
                let calendar = NSCalendar.currentCalendar()
                
                let scomponents = calendar.components([.Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
                
                var sam = "AM"
                var shour: Int!
                if(scomponents.hour >= 12)
                {
                    if(scomponents.hour > 12)
                    {
                        shour = scomponents.hour-12
                    }
                    else
                    {
                        shour = 12
                    }
                    sam = "PM"
                }
                else
                {
                    shour = scomponents.hour
                    sam = "AM"
                    if(scomponents.hour==0){
                        shour = 12
                    }
                }
                
                var sminute = "\(scomponents.minute)"
                let sday = scomponents.day
                let smonth = scomponents.month
                let syear = scomponents.year
                
                if(scomponents.minute<10)
                {
                    sminute="0\(sminute)"
                }
                
                
                var timeeString = "\(shour):\(sminute) \(sam)"
                let dateeString = "\(monthsArray[smonth-1]) \(sday), \(syear)"
                
                
                
                // //print(currentEvent)
                ////println(sweekday)
                
                let path = NSBundle.mainBundle().pathForResource("timezones", ofType: "plist")
                let dict = NSDictionary(contentsOfFile: path!)
                
                let tzDict = dict!.objectForKey("TimeZones") as! NSDictionary
                
                let str = tzDict.allKeysForObject(timeZoneName) as NSArray
                let strtime = str.objectAtIndex(0) as! String
                
                
                
                
                timeZoneName = String (format: " (%@ time)",  strtime)
                timeeString = "\(timeeString) \(timeZoneName)"
                
                
                branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                    { (url: String?, error: NSError?) -> Void in
                        if error == nil
                        {
                            
                            self.linkForHost = url!
                            
                            if currentStatus == ""
                            {
                                notifMessage = "\(self.fullUserName) responded to the invitation for your event, \(self.eventTitle)."
                                
                                if self.statusToBeChanged == "yes"
                                {
                                    //
                                    
                                    let attending = Attending()
                                    
                                    let emailMessage = attending.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, adultsCount: attendees[0], childsCount: attendees[1], type:"rsvp",url:self.linkForHost)
                                    
                                    let sendEmailObject = SendEmail()
                                    
                                    
                                    sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                    
                                    
                                }
                                if self.statusToBeChanged == "online"
                                {
                                    let online = OnlineOnly()
                                    
                                    let emailMessage = online.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:self.linkForHost)
                                    
                                    let sendEmailObject = SendEmail()
                                    
                                    
                                    sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                    
                                    
                                }
                                
                                if self.statusToBeChanged == "maybe"
                                {
                                    let mayBe = Maybe()
                                    
                                    let emailMessage = mayBe.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:self.linkForHost)
                                    
                                    let sendEmailObject = SendEmail()
                                    
                                    //
                                    sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                    
                                    
                                }
                                if self.statusToBeChanged == "no"
                                {
                                    let notAttending = NotAttending()
                                    
                                    let emailMessage = notAttending.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:self.linkForHost)
                                    
                                    let sendEmailObject = SendEmail()
                                    
                                    //
                                    
                                    
                                    sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                    
                                    
                                }
                                
                            }
                            else
                            {
                                if self.statusToBeChanged == "yes" && currentStatus == "yes"
                                {
                                    if attendees[2] != attendees[0] || attendees[3] != attendees[1]
                                    {
                                        notifMessage = "\(self.fullUserName) changed guest count for your event, \(self.eventTitle)."
                                        
                                        let guestListChange = GuestListChange()
                                        self.isGuestListChange = true
                                        let emailMessage = guestListChange.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, oldAdultCount: attendees[2], oldChildsCount: attendees[3], newAdultCount: attendees[0], newChildCount: attendees[1], type: "rsvp",url:self.linkForHost)
                                        
                                        let sendEmailObject = SendEmail()
                                        
                                        sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                        
                                        
                                    }
                                }
                                else if self.statusToBeChanged != currentStatus
                                {
                                    var prevoiusStatus = ""
                                    var newStatus = ""
                                    
                                    if currentStatus == "yes"
                                    {
                                        prevoiusStatus = "Attending"
                                    }
                                    
                                    if currentStatus == "no"
                                    {
                                        prevoiusStatus = "Not attending"
                                    }
                                    
                                    if currentStatus == "maybe"
                                    {
                                        prevoiusStatus = "May be"
                                    }
                                    
                                    if currentStatus == "online"
                                    {
                                        prevoiusStatus = "Online"
                                    }
                                    
                                    
                                    if self.statusToBeChanged == "yes"
                                    {
                                        newStatus = "Attending"
                                    }
                                    
                                    if self.statusToBeChanged == "no"
                                    {
                                        newStatus = "Not attending"
                                    }
                                    
                                    if self.statusToBeChanged == "maybe"
                                    {
                                        newStatus = "May be"
                                    }
                                    
                                    if self.statusToBeChanged == "online"
                                    {
                                        newStatus = "Online"
                                    }
                                    
                                    let guestResponseChange = GuestResponseChange()
                                    
                                    let emailMessage = guestResponseChange.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, prevoiusStatus: prevoiusStatus, guestName: self.fullUserName, newStatus: newStatus, type: "rsvp",url:self.linkForHost)
                                    //
                                    
                                    
                                    let sendEmailObject = SendEmail()
                                    
                                    
                                    sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                    
                                    
                                }
                            }
                            
                        }
                })
                
                
                linkForHost.getShortUrlWithLinkProperties(linkHostProperties,  andCallback:
                    { (url: String?, error: NSError?) -> Void in
                        if error == nil
                        {
                            urlString = url!
                            
                            
                            
                            if self.statusToBeChanged == "yes"
                            {
                                
                                if self.isGuestListChange == false
                                {
                                    localNotification.alertBody = "Hooray! You are going to \(self.eventTitle)"
                                    
                                    //
                                    let attendingInviteResponse = AttendingInviteResponseGuest()
                                    
                                    let attendingEmailMessage = attendingInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: dateeString, timeString: timeeString, locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                                    
                                    let sendEmailObject = SendEmail()
                                    
                                    sendEmailObject.sendEmail("Hooray! You are attending an event.", message: attendingEmailMessage, emails: [email])
                                }
                                
                                
                                
                                
                            }
                            if self.statusToBeChanged == "online"
                            {
                                localNotification.alertBody = "Cheers! You can’t make it to \(self.eventTitle), but you will follow the event online."
                                
                                let onlineInviteResponse = OnlineOnlyInviteResponseGuest()
                                
                                let onlineEmailMessage = onlineInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: dateeString, timeString: timeeString, locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                                //
                                let sendEmailObject = SendEmail()
                                
                                sendEmailObject.sendEmail("You are going to follow this event online using the iOS app.", message: onlineEmailMessage, emails: [email])
                            }
                            if self.statusToBeChanged == "maybe"
                            {
                                localNotification.alertBody = " Hmmm! You are not sure whether you can attend \(self.eventTitle). Don’t forget to change your status once you know more."
                                
                                let maybeInviteResponse = MaybeInviteResponseGuest()
                                
                                let maybeEmailMessage = maybeInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: dateeString, timeString:timeeString, locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                                
                                let sendEmailObject = SendEmail()
                                
                                sendEmailObject.sendEmail("You are not sure you can attend this event.", message: maybeEmailMessage, emails:[email] )
                                
                            }
                            if self.statusToBeChanged == "no"
                            {
                                localNotification.alertBody = "Bummer! You are not going to \(self.eventTitle). Don’t forget, you can change your status anytime before the event starts."
                                
                                let notAttendingInviteResponse = NotAttendingInviteResponseGuest()
                                
                                let notAttendingEmailMessage = notAttendingInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: dateeString, timeString: timeeString, locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                                
                                let sendEmailObject = SendEmail()
                                
                                sendEmailObject.sendEmail("Bummer! You cannot attend this event.", message: notAttendingEmailMessage, emails:[email] )
                            }
                            
                            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                            
                            
                        }
                })
            }
                
            else
            {
                let data = [
                    "alert" : "\(notifMessage)",
                    "notifType" :  "invitationresponse",
                    "eventObjectId": currentSharedEvent.objectId!,
                    "objectId": "\(invitation.objectId!)",
                    "invitedName": "\(fullUserName)",
                    "attendingStatus" : "\(currentStatus)",
                    "noOfChilds": "\(attendees[1])",
                    "noOfAdults": "\(attendees[0])",
                    "invitationNote": "\(invitationNote)",
                    "createdAt": "\(createdAt)",
                    "updatedAt": "\(updatedAt)",
                    "isUpdated": "\(isUpdated)",
                    "emailId": "\(email)",
                    "userObjectId": "\(currentUserId)",
                    "eventCreatorId" : "\(eventCreatorId)",
                    "isRSVP" : "true"
                ]
                
                
                var urlString = String()
                
                Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                    
                    if error == nil
                    {
                        ////println(url!)
                        
                        urlString = url!
                        
                        if currentStatus == ""
                        {
                            notifMessage = "\(self.fullUserName) responded to the invitation for your event, \(self.eventTitle)."
                            
                            if self.statusToBeChanged == "yes"
                            {
                                //
                                
                                let attending = Attending()
                                
                                let emailMessage = attending.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, adultsCount: attendees[0], childsCount: attendees[1], type: "rsvp",url:urlString)
                                
                                let sendEmailObject = SendEmail()
                                
                                
                                sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                
                                
                            }
                            if self.statusToBeChanged == "online"
                            {
                                let online = OnlineOnly()
                                
                                let emailMessage = online.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:urlString)
                                
                                let sendEmailObject = SendEmail()
                                
                                
                                sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                
                                
                            }
                            
                            if self.statusToBeChanged == "maybe"
                            {
                                let mayBe = Maybe()
                                
                                let emailMessage = mayBe.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:urlString)
                                
                                let sendEmailObject = SendEmail()
                                
                                //
                                sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                
                                
                            }
                            if self.statusToBeChanged == "no"
                            {
                                let notAttending = NotAttending()
                                
                                let emailMessage = notAttending.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, type: "rsvp",url:urlString)
                                
                                let sendEmailObject = SendEmail()
                                
                                //
                                
                                
                                sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                
                                
                            }
                            
                        }
                        else
                        {
                            if self.statusToBeChanged == "yes" && currentStatus == "yes"
                            {
                                if attendees[2] != attendees[0] || attendees[3] != attendees[1]
                                {
                                    notifMessage = "\(self.fullUserName) changed guest count for your event, \(self.eventTitle)."
                                    
                                    let guestListChange = GuestListChange()
                                    
                                    let emailMessage = guestListChange.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, guestName: self.fullUserName, oldAdultCount: attendees[2], oldChildsCount: attendees[3], newAdultCount: attendees[0], newChildCount: attendees[1], type: "rsvp",url:urlString)
                                    
                                    let sendEmailObject = SendEmail()
                                    
                                    sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                    
                                    ////println(hostEmail)
                                }
                            }
                            else if self.statusToBeChanged != currentStatus
                            {
                                var prevoiusStatus = ""
                                var newStatus = ""
                                
                                if currentStatus == "yes"
                                {
                                    prevoiusStatus = "Attending"
                                }
                                
                                if currentStatus == "no"
                                {
                                    prevoiusStatus = "Not attending"
                                }
                                
                                if currentStatus == "maybe"
                                {
                                    prevoiusStatus = "May be"
                                }
                                
                                if currentStatus == "online"
                                {
                                    prevoiusStatus = "Online"
                                }
                                
                                
                                if self.statusToBeChanged == "yes"
                                {
                                    newStatus = "Attending"
                                }
                                
                                if self.statusToBeChanged == "no"
                                {
                                    newStatus = "Not attending"
                                }
                                
                                if self.statusToBeChanged == "maybe"
                                {
                                    newStatus = "May be"
                                }
                                
                                if self.statusToBeChanged == "online"
                                {
                                    newStatus = "Online"
                                }
                                
                                let guestResponseChange = GuestResponseChange()
                                
                                let emailMessage = guestResponseChange.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, prevoiusStatus: prevoiusStatus, guestName: self.fullUserName, newStatus: newStatus, type: "rsvp",url:urlString)
                                //
                                
                                
                                let sendEmailObject = SendEmail()
                                
                                
                                sendEmailObject.sendEmail("Guest Alert – \(self.eventTitle)", message: emailMessage, emails: [hostEmail])
                                
                                
                            }
                        }
                        
                        if self.statusToBeChanged == "yes"
                        {
                            localNotification.alertBody = "Hooray! You are going to \(self.eventTitle)"
                            
                            //
                            let attendingInviteResponse = AttendingInviteResponseGuest()
                            
                            let attendingEmailMessage = attendingInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: self.dateStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), timeString: self.timeStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                            
                            let sendEmailObject = SendEmail()
                            
                            sendEmailObject.sendEmail("Hooray! You are attending an event.", message: attendingEmailMessage, emails: [email])
                            
                            
                        }
                        if self.statusToBeChanged == "online"
                        {
                            localNotification.alertBody = "Cheers! You can’t make it to \(self.eventTitle), but you will follow the event online."
                            
                            let onlineInviteResponse = OnlineOnlyInviteResponseGuest()
                            
                            let onlineEmailMessage = onlineInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: self.dateStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), timeString: self.timeStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                            //
                            let sendEmailObject = SendEmail()
                            
                            sendEmailObject.sendEmail("You are going to follow this event online using the iOS app.", message: onlineEmailMessage, emails: [email])
                        }
                        if self.statusToBeChanged == "maybe"
                        {
                            localNotification.alertBody = " Hmmm! You are not sure whether you can attend \(self.eventTitle). Don’t forget to change your status once you know more."
                            
                            let maybeInviteResponse = MaybeInviteResponseGuest()
                            
                            let maybeEmailMessage = maybeInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: self.dateStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), timeString: self.timeStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                            
                            let sendEmailObject = SendEmail()
                            
                            sendEmailObject.sendEmail("You are not sure you can attend this event.", message: maybeEmailMessage, emails:[email] )
                            
                        }
                        if self.statusToBeChanged == "no"
                        {
                            localNotification.alertBody = "Bummer! You are not going to \(self.eventTitle). Don’t forget, you can change your status anytime before the event starts."
                            
                            let notAttendingInviteResponse = NotAttendingInviteResponseGuest()
                            
                            let notAttendingEmailMessage = notAttendingInviteResponse.emailMessage(currentSharedEvent.objectId!, eventTitle: self.eventTitle, dateString: self.dateStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), timeString: self.timeStringFromNSDate(currentSharedEvent["eventStartDateTime"] as! NSDate), locationString: currentSharedEvent["eventLocation"] as! String, hostName: user["fullUserName"] as! String, latitude: "\(eventLatitude)", longitude: "\(eventLongitude)", type: "rsvp",url:urlString)
                            
                            let sendEmailObject = SendEmail()
                            
                            sendEmailObject.sendEmail("Bummer! You cannot attend this event.", message: notAttendingEmailMessage, emails:[email] )
                        }
                        
                        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
                        
                    }
                    
                })
                
                
            }

        }
        
        
    }
    
    
    
    func BranchLinkfor(notifMessage:String,eventObjectId:String,attendingStatus:String,noOfChilds:String,noOfAdults:String,invitationNote:String,createdAt:String,updatedAt:String,isUpdated:String,emailId:String,userObjectId:String,eventCreatorId:String)->String
    {
        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
        branchUniversalObject.title = "\(eventTitle)"
        branchUniversalObject.contentDescription = "My Content Description"
        // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
        branchUniversalObject.addMetadataKey("alert", value:"\(notifMessage)")
        branchUniversalObject.addMetadataKey("notifType", value: "invitationresponse")
        branchUniversalObject.addMetadataKey("eventObjectId", value:currentSharedEvent.objectId!)
        branchUniversalObject.addMetadataKey("objectId", value:"\(notifMessage)")
        branchUniversalObject.addMetadataKey("invitedName", value: "noemail")
        branchUniversalObject.addMetadataKey("attendingStatus", value:"\(attendingStatus)")
        branchUniversalObject.addMetadataKey("noOfChilds", value:"\(noOfChilds)")
        branchUniversalObject.addMetadataKey("noOfAdults", value:"\(noOfAdults)")
        branchUniversalObject.addMetadataKey("invitationNote", value: "\(invitationNote)")
        
        branchUniversalObject.addMetadataKey("createdAt", value:"\(createdAt)")
        branchUniversalObject.addMetadataKey("updatedAt", value:"\(updatedAt)")
        branchUniversalObject.addMetadataKey("isUpdated", value:"\(isUpdated)")
        branchUniversalObject.addMetadataKey("emailId", value:"\(emailId)")
        branchUniversalObject.addMetadataKey("userObjectId", value: "\(currentUserId)")
        branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(createdAt)")
        branchUniversalObject.addMetadataKey("isRSVP", value:"true")
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"
        //linkProperties.channel = "email"
        
        
        var urllink = ""
        
        linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/manage_event/\(currentSharedEvent.objectId!)")
        linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/manage_event/\(currentSharedEvent.objectId!)")
        linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/manage_event/\(currentSharedEvent.objectId!)")
        branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
            { (url: String?, error: NSError?) -> Void in
                if error == nil
                {
                    
                    urllink = url!
                    
                }
        })
        
        
        
        return urllink
    }
    
    
    func fetchHostDataError(timer:NSTimer)
    {
        
    }
    
    func dateStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        let sday = scomponents.day
        let smonth = scomponents.month
        let syear = scomponents.year
        
        let sweekday = scomponents.weekday
        
        let dateString = "\(monthsArray[smonth-1]) \(sday), \(syear)"
        
        return dateString
    }
    
    
    func timeStringFromNSDate(sdate: NSDate)->String
    {
        let calendar = NSCalendar.currentCalendar()
        
        let scomponents = calendar.components([.Weekday, .Hour, .Minute, .Day, .Month, .Year], fromDate: sdate)
        
        var sam = "AM"
        var shour: Int!
        if(scomponents.hour >= 12)
        {
            if(scomponents.hour > 12)
            {
                shour = scomponents.hour-12
            }
            else
            {
                shour = 12
            }
            
            sam = "PM"
        }
        else
        {
            shour = scomponents.hour
            sam = "AM"
            if(scomponents.hour==0){
                shour = 12
            }
        }
        
        var sminute = "\(scomponents.minute)"
        
        if(scomponents.minute<10)
        {
            sminute="0\(sminute)"
        }
        
        let timeString = "\(shour):\(sminute) \(sam)"
        
        return timeString
    }
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        if image.imageOrientation == UIImageOrientation.Up {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    
    
    private func playVideo(url: NSURL) {
        if let
            moviePlayer = MPMoviePlayerController(contentURL: url) {
                self.moviePlayer = moviePlayer
                moviePlayer.view.frame = CGRectMake(20, 20, 280, 400)
                moviePlayer.prepareToPlay()
                moviePlayer.shouldAutoplay = false
                moviePlayer.scalingMode = .AspectFill
                moviePlayer.controlStyle = .None
                self.view.addSubview(moviePlayer.view)
        } else {
            // debug////println("Oops, something wrong when playing video.m4v")
        }
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    func textViewShouldReturn(textView: UITextView) -> Bool
    {
        textView.resignFirstResponder()
        return true
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool
    {
        if statusToBeChanged == "yes"
        {
            invitationNoteView.frame.size.height = invitationNoteViewHeight/2
        }
        else
        {
            invitationNoteView.frame.size.height = invitationNoteViewHeight
        }
        
        return true
    }
    
    func textViewShouldEndEditing(textView: UITextView) -> Bool
    {
        
        invitationNoteView.frame.size.height = invitationNoteViewHeight
        
        return true
    }
    
    
    @IBAction func hideKeyBoard()
    {
        noOfChildText.resignFirstResponder()
        noOfAdultsText.resignFirstResponder()
        invitationNoteView.resignFirstResponder()
    }
    
    func getrowheight(currentrow: NSInteger) -> CGFloat
    {
        var rowHeight:CGFloat = 380.0
        
        if(arrdata[currentrow]["postType"] as! String == "Note")
        {
            let postText = arrdata[currentrow]["postData"] as! String
            
            /*var charCount: CGFloat = CGFloat(count(postText))
            
            var trowCount: CGFloat = (charCount/18)+2*/
            
            let senderMessageTemp = UITextView()
            
            senderMessageTemp.frame.size.width = self.view.frame.width-self.view.frame.width*(40.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
            
            senderMessageTemp.text = postText
            
            //senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20)
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 8
            let attributes = [NSParagraphStyleAttributeName : style]
            
            
            
            senderMessageTemp.attributedText = NSAttributedString(string: postText, attributes:attributes)
            senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20.0)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            
            //ln(rowHeight)
            
            rowHeight = senderMessageTemp.frame.height + (0.120625 * self.view.frame.height)+((20.0/568)*self.view.frame.height)+30
            
            let screenSize: CGRect = UIScreen.mainScreen().bounds
            
            if screenSize.height < 600
            {
                rowHeight = senderMessageTemp.frame.height + (0.120625 * self.view.frame.height)+((20.0/568)*self.view.frame.height)+38
            }
            //rowHeight = senderMessageTemp.frame.height + 500
            /*if( trowCount < 11)
            {
            rowHeight = 380.0 - (320.0-(trowCount*29.0))
            }*/
        }
        else
        {
            //ln(userpost["postHeight"])
            let rheiight = (arrdata[currentrow]["postHeight"]) as? String
            let rwiidth = arrdata[currentrow]["postWidth"] as? String
            
            let postImageView = UIImageView()
            var rheight = CGFloat()
            var rwidth = CGFloat()
            
            if rheiight != nil
            {
                if let n = NSNumberFormatter().numberFromString(rheiight!) {
                    rheight = CGFloat(n)
                }
                if let ni = NSNumberFormatter().numberFromString(rwiidth!) {
                    rwidth = CGFloat(ni)
                }
                
            }
            else
            {
                rheight = 2000
                
                rwidth = 2000
                
                
            }
            
            
            
            //ln("height: \(rheight)")
            
            if(rheight>0 && rwidth>0)
            {
                rowHeight = ((rheight/rwidth)*self.tableView.frame.width)+((45.0/568)*self.view.frame.height)
            }
            if currentrow == 0
            {
                //rowHeight = rowHeight+((30.0/568)*self.view.frame.height)
            }
            
        }
        
        return rowHeight
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrdata.count
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath section: NSIndexPath) -> CGFloat
    {
        
        /* let row = section.row
        
        let postType = arrdata[row]["postType"] as! String!
        
        let extraHeight: CGFloat = 0.0
        
        if postType == "Note"
        {
        //extraHeight = 10.0
        }
        return mySharedRowHeights[row] + extraHeight*/
        let row = section.row
        
        return getrowheight(row)
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        

        activityIndicatorView.stopAnimation()

        newUpdatesButton.hidden = true
        noOfStreamUpdates = 0
        let row = indexPath.row
        
        let postType = arrdata[row]["postType"] as! String!
        
        let cellIdentifier: String! = "SharedEventPhotoCell1"
        
        
        var cell: SharedPhotosTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String, forIndexPath: indexPath) as? SharedPhotosTableViewCell
        
        
        if (cell == nil)
        {
            cell = SharedPhotosTableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: cellIdentifier)
        }
        
        
        for view in cell!.contentView.subviews
        {
            view.removeFromSuperview()
        }
        
        var viewTop: CGFloat = 0
        
        if row == 0 && postType == "Note"
        {
            viewTop = (0/568)*self.view.frame.height
        }
        
        let heightDiff: CGFloat = 0
        
        let postTextView = UITextView()
        let textBackgroundView = UIView()
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 8
        let attributes = [NSParagraphStyleAttributeName : style]
        
        /*  var attrs = [
        NSFontAttributeName : UIFont.systemFontOfSize(20),
        NSForegroundColorAttributeName : UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0),
        NSUnderlineStyleAttributeName : 1]*/
        
        print(arrdata[row])
        
        
//        if self.arrdata.count == 0
//        {
//            self.onboardingScreen.hidden = false
//        }
//        else
//        {
//            self.onboardingScreen.hidden = true
//        }

        
        if(postType == "Note")
        {
            let postTextClickableView = UIView()
            
            postTextView.text = arrdata[row]["postData"] as! String
            
            
            
            postTextView.attributedText = NSAttributedString(string:postTextView.text, attributes:attributes)
            postTextView.font = UIFont(name:"Tigerlily", size: 20.0)
            postTextView.textColor = UIColor(red: 74/255, green: 74/255, blue: 74/255, alpha: 1.0)
            
            
            postTextView.editable = false
            postTextView.selectable = false
            postTextView.scrollEnabled = false
            
            let postText = arrdata[row]["postData"] as! String
            
            
            /* var charCount: CGFloat = CGFloat(count(postText))
            
            var postTextHeight: CGFloat = 320
            
            var trowCount: CGFloat = (charCount/18) + 2
            
            if( trowCount < 11)
            {
            postTextHeight = trowCount*29
            heightDiff = 320-(trowCount*29)
            }*/
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 8
            let attributes = [NSParagraphStyleAttributeName : style]
            
            
            /*   var attrs = [
            NSFontAttributeName : UIFont.systemFontOfSize(20.0),
            NSForegroundColorAttributeName : UIColor(red: 117.0/255, green: 185.0/255, blue: 225.0/255, alpha: 1.0),
            NSUnderlineStyleAttributeName : 1]*/
            
            
            let senderMessageTemp = UITextView()
            
            
            
            senderMessageTemp.frame.size.width = self.view.frame.width-self.view.frame.width*(40.0/320)
            senderMessageTemp.frame.size.height = self.view.frame.height*(30.0/568)
            
            senderMessageTemp.text = postText
            
            senderMessageTemp.attributedText = NSAttributedString(string:postText, attributes:attributes)
            
            senderMessageTemp.font = UIFont(name: "Tigerlily", size: 20.0)
            
            let contentSize = senderMessageTemp.sizeThatFits(senderMessageTemp.bounds.size)
            var frame = senderMessageTemp.frame
            frame.size.height = contentSize.height
            senderMessageTemp.frame = frame
            postTextView.frame.size.width = cell!.contentView.frame.width-self.view.frame.width*(20.0/320)
            
            
            postTextView.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 0)
            if  contentSize.height < 100
            {
                postTextClickableView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop, cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height+20)
                postTextView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop+((20.0/568)*self.view.frame.height), cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height+20)
                textBackgroundView.frame = CGRectMake(0, viewTop, cell!.contentView.frame.width, contentSize.height+((20.0/568)*self.view.frame.height)+60)
            }
            else
            {
                postTextClickableView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop, cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height+20)
                postTextView.frame = CGRectMake(self.view.frame.width*(20.0/320), viewTop+((20.0/568)*self.view.frame.height), cell!.contentView.frame.width-self.view.frame.width*(40.0/320), contentSize.height+20)
                textBackgroundView.frame = CGRectMake(0, viewTop, cell!.contentView.frame.width, contentSize.height+((20.0/568)*self.view.frame.height)+60)
            }
            
            textBackgroundView.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            cell!.contentView.addSubview(textBackgroundView)
            cell!.contentView.addSubview(postTextView)
            cell!.contentView.addSubview(postTextClickableView)
        }
        
        if row == 0
        {
            //heightDiff = heightDiff-45
        }
        
        let rheiight = (arrdata[row]["postHeight"]) as? String
        let rwiidth = arrdata[row]["postWidth"] as? String
        
        var rheight = CGFloat()
        var rwidth = CGFloat()
        
        if rheiight != nil
        {
            if let n = NSNumberFormatter().numberFromString(rheiight!) {
                rheight = CGFloat(n)
            }
            if let ni = NSNumberFormatter().numberFromString(rwiidth!) {
                rwidth = CGFloat(ni)
            }
            
        }
        else
        {
            rheight = 2000
            
            rwidth = 2000
            
            
        }
        
        
        
        
        let postImageView = UIImageView()
        postImageView.backgroundColor = UIColor.lightGrayColor()

        if(rheight>0 && rwidth>0)
        {
            
            postImageView.frame.size.width = self.tableView.frame.width
            postImageView.frame.size.height = (rheight/rwidth)*self.tableView.frame.width
            
            postImageView.frame.origin.x = 0
            postImageView.frame.origin.y = viewTop
            
            ////println(postImageView.frame.size.width)
            ////println(postImageView.frame.size.height)
            ////println(postImageView.frame.origin.x)
            ////println(postImageView.frame.origin.y)
            
        }
        if(postType == "image")
        {
            
            let imageName = arrdata[row]["postData"] as! String
            
            let eventFolder = arrdata[row]["eventFolder"] as! String
            
            let eventImagePath = "\(documentDirectory)/\(imageName)"
            
            ////println(eventImagePath)
            
            cell!.contentView.addSubview(postImageView)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventImagePath))
            {
                var image = UIImage(named: eventImagePath)
                
                postImageView.image = image
                
                //cell!.contentView.addSubview(postImageView)
            }
            else
            {
                /*var isDownloading = mySharedEventData[row]["isDownloading"] as! Bool
                ////println("isDownloading for \(row) is \(isDownloading)")
                
                if mySharedEventData[row]["isDownloading"] as! Bool == true
                {*/
                //mySharedEventData[row]["isDownloading"] = false
                
                let s3BucketName = "eventnode1"
                let fileName = imageName
                
                let downloadFilePath = (documentDirectory as NSString).stringByAppendingPathComponent(fileName)
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType, imageURL: NSURL!) -> Void in
                    //			println(self)
                }
                
                
                
                
                let tempstr = String("http://d3a1uesrqnd2ko.cloudfront.net/\(eventFolder)\(fileName)")
                let tempurl = NSURL(string: tempstr)
                postImageView.sd_setImageWithURL(tempurl, completed: block)
                postImageView.backgroundColor = UIColor.lightGrayColor()
                
                //loaderCellView.hidden = true
                
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                ////println("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            ////println("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        ////println("downloading successfull")
                        
                        var image = UIImage(named: "\(documentDirectory)/\(imageName)")
                        
                        postImageView.image = image
                        
                    }
                    
                    return nil
                    
                })
                
                
                //}
            }
        }
        
        let postPlayButton = UIButton()
        
        if(postType == "video")
        {
            
            let videoName = arrdata[row]["postData"] as! String
            
            let eventFolder = arrdata[row]["eventFolder"] as! String
            
            let eventVideoPath = "\(documentDirectory)/\(videoName)"
            
            
            
            postPlayButton.enabled = false
            
            cell!.contentView.addSubview(postImageView)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventVideoPath)) {
                postPlayButton.enabled = true
                
                postPlayButton.frame = postImageView.frame
                
                postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
                
                postPlayButton.tag = indexPath.row
                postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
                
                
                let videoUrl = NSURL(fileURLWithPath: eventVideoPath)
                self.getFirstFrame(postImageView, videoURL: videoUrl, viewTop: viewTop)
                
                //cell!.contentView.addSubview(postPlayButton)
            }
            else
            {
                let s3BucketName = "eventnode1"
                let fileName = videoName
                
                let downloadFilePath = (documentDirectory as NSString).stringByAppendingPathComponent(fileName)
                let downloadingFileURL = NSURL.fileURLWithPath(downloadFilePath)
                
                let downloadRequest = AWSS3TransferManagerDownloadRequest()
                downloadRequest.bucket = s3BucketName
                downloadRequest.key  = "\(eventFolder)\(fileName)"
                downloadRequest.downloadingFileURL = downloadingFileURL
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                
                
                transferManager.download(downloadRequest).continueWithExecutor(AWSExecutor.mainThreadExecutor(), withBlock:{task in
                    
                    if (task.error != nil){
                        if (task.error.domain == AWSS3TransferManagerErrorDomain) {
                            switch (task.error.code) {
                            case AWSS3TransferManagerErrorType.Cancelled.rawValue:
                                break;
                            case AWSS3TransferManagerErrorType.Paused.rawValue:
                                break;
                                
                            default:
                                ////println("error downloading")
                                break;
                            }
                        } else {
                            // Unknown error.
                            ////println("error downloading")
                        }
                    }
                    
                    if (task.result != nil) {
                        
                        ////println("downloading successfull")
                        
                        postPlayButton.enabled = true
                        
                        postPlayButton.frame = postImageView.frame
                        
                        postPlayButton.setImage(UIImage(named: "play_icon.png"), forState: UIControlState.Normal)
                        postPlayButton.tag = indexPath.row
                        postPlayButton.addTarget(self, action:"playSelectedVideo:",forControlEvents: UIControlEvents.TouchUpInside)
                        
                        var videoUrl = NSURL(fileURLWithPath: eventVideoPath)
                        
                        self.getFirstFrame(postImageView, videoURL: videoUrl, viewTop: viewTop)
                        
                    }
                    
                    return nil
                    
                })
            }
            
            
        }
        
        
        let tempp = arrLikeDic.valueForKey("parentkey") as! NSArray
        
        let bag = NSCountedSet()
        bag.addObjectsFromArray(tempp as [AnyObject])
        //print (tempp)
        //print (bag.countForObject(arrdata[indexPath.row].valueForKey("keyvalue")!))
        let totalLikeCount = bag.countForObject(arrdata[indexPath.row].valueForKey("keyvalue")!)
        /*rahul  var resultSetTotalCount: FMResultSet! = ModelManager.instance.getTableData("PostLikes", selectColumns: ["count(*) as count"], whereString: "postObjectId=?", whereFields: [mySharedEventData[row].objectId!])
        
        resultSetTotalCount.next()
        
        var totalLikeCount = resultSetTotalCount.intForColumn("count")
        resultSetTotalCount.close()
        */
        
        let infoView = UIView()
        let likeButton = UIButton()
        
        let likeButtonWrapper = UIView()
        
        // Like Button Wrapper overlays postImageView entirely, so the click/tap is delivered to the wrapper instead of the image view.
        // Hence, adding gesture recognizer to wrapper.
        // Ideally the wrapper should contain the image, like button and play button making it a contentWrapper as opposed to a likeButtonWrapper.
        let imageTapRecognizer = UITapGestureRecognizer(target: self, action: "showImageInFullScreenView:")
        likeButtonWrapper.addGestureRecognizer(imageTapRecognizer)
        likeButtonWrapper.tag = row
        likeButtonWrapper.userInteractionEnabled = true
        
        var checkbool = Bool()
        for var i = 0; i < arrLikeDic.count; i++
        {
            let temp1 = arrLikeDic[i].valueForKey("parentkey") as! String
            if temp1 == arrdata[indexPath.row].valueForKey("keyvalue") as! String
            {
                if  arrLikeDic[i].valueForKey("userObjectId")as! String == currentUserId
                {
                    checkbool = true
                    break
                }
            }
            checkbool = false
        }
        
        if checkbool == true
        {
            likeButton.selected = true
            likeButton.setImage(UIImage(named:"heart-circle.png"), forState: UIControlState.Normal)
        }
        else
        {
            likeButton.selected = false
            
            likeButton.setImage(UIImage(named:"unlike.png"), forState: UIControlState.Normal)
        }
        
        
        likeButton.addTarget(self, action:"likeSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
        
        likeButton.tag = indexPath.row
        
        
        if(postType != "Note")
        {
            ////println(postImageView.frame)
            
            //postImageView.frame.height-heightDiff+((10/568)*self.view.frame.height)
            likeButtonWrapper.frame = CGRectMake(0, 0, self.view.frame.width, postImageView.frame.height)
            likeButtonWrapper.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha:0)
            likeButton.frame = CGRectMake((postImageView.frame.width)-((75/568)*self.view.frame.height), (postImageView.frame.height)-((75/568)*self.view.frame.height), (45/568)*self.view.frame.height, (45/568)*self.view.frame.height )
            likeButton.alpha = 0.7
            
            infoView.frame = CGRectMake(0, postImageView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
        }
        else
        {
            //
            //likeButtonWrapper.frame = CGRectMake(0, (cell!.contentView.frame.width-heightDiff+((33/568)*self.view.frame.height))-((45/568)*self.view.frame.height)-((5/568)*self.view.frame.height), self.view.frame.width, (45/568)*self.view.frame.height)
            
            likeButtonWrapper.frame = CGRectMake(0, textBackgroundView.frame.height+viewTop-65, self.view.frame.width, (55/568)*self.view.frame.height)
            // likeButtonWrapper.backgroundColor = UIColor(red: 211.0/255, green: 230.0/255, blue: 237.0/255, alpha: 1.0)
            
            likeButtonWrapper.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            likeButton.frame = CGRectMake(self.view.frame.width-((75/320)*self.view.frame.width), 0, (45/568)*self.view.frame.height, (45/568)*self.view.frame.height)
            
            infoView.frame = CGRectMake(0, textBackgroundView.frame.height + ((55/568)*self.view.frame.height) + viewTop, cell!.contentView.frame.width, (0.120625)*380)
        }
        
        if(postType == "video")
        {
            likeButtonWrapper.addSubview(postPlayButton)
        }
        
        likeButtonWrapper.addSubview(likeButton)
        
        ////println(likeButton.frame)
        ////println(infoView.frame)
        
        
        if(postType != "Note")
        {
            infoView.frame = CGRectMake(0, postImageView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
        }
        else
        {
            infoView.frame = CGRectMake(0, textBackgroundView.frame.height-heightDiff, cell!.contentView.frame.width, (0.120625)*380)
        }
        
        let deleteButton = UIButton()
        
        deleteButton.setTitle ("Delete", forState: UIControlState.Normal)
        deleteButton.titleLabel!.font =  UIFont(name:"AvenirNext-Medium" , size: 12)
        
        
        deleteButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        
        
        deleteButton.frame = CGRectMake((self.view.frame.width)*11/320, 0, self.view.frame.width/5,deleteButton.sizeThatFits(deleteButton.bounds.size).height)
        deleteButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        
        deleteButton.tag = indexPath.row
        
        deleteButton.addTarget(self, action:"deleteSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // Add More Button
        let moreImageView = UIImageView()
        
        moreImageView.frame = CGRectMake(0, 12, 30, 8)
        
        moreImageView.image = UIImage(named:"more.png")
        
        var moreButton = UIButton()
        //shareButton.backgroundColor = UIColor.redColor()
        
        moreButton.addSubview(moreImageView)
        
        //        moreButton.frame = CGRectMake(15, 0, 11, moreButton.sizeThatFits(moreButton.bounds.size).height)
        
        moreButton.frame = CGRectMake(self.view.frame.width - 15 - moreButton.sizeThatFits(moreButton.bounds.size).width, 0, 30,moreButton.sizeThatFits(moreButton.bounds.size).height)
        
        moreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
        
        moreButton.tag = indexPath.row
        
        moreButton.addTarget(self, action:"showMoreOptions:",forControlEvents: UIControlEvents.TouchUpInside)
        
        // Like Button
        let likeButtonSmall = UIButton()
        
        likeButtonSmall.setTitle ("", forState: UIControlState.Normal)
        
        likeButtonSmall.frame = CGRectMake(15, 0, (0.120625)*380,(0.120625)*380)
        
        likeButtonSmall.tag = indexPath.row
        
        //likeButton.addTarget(self, action:"likeSelectedPost:",forControlEvents: UIControlEvents.TouchUpInside)
        
        let postLikeText = UILabel()
        
        postLikeText.textAlignment = NSTextAlignment.Right
        postLikeText.text = "\(totalLikeCount) Loved it"
        
        postLikeText.textColor = UIColor.grayColor()
        postLikeText.backgroundColor = UIColor.clearColor()
        postLikeText.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        postLikeText.textAlignment = .Right
        
        postLikeText.frame = CGRectMake(30, (deleteButton.frame.height/2)-(postLikeText.sizeThatFits(postLikeText.bounds.size).height/2), postLikeText.sizeThatFits(postLikeText.bounds.size).width, postLikeText.sizeThatFits(postLikeText.bounds.size).height)
        
        
        
        ////println("likeheight: \(postLikeText.frame.height)")
        
        let likeImageView = UIImageView()
        
        likeImageView.image = UIImage(named:"heart.png")
        
        likeImageView.frame = CGRectMake(15,postLikeText.frame.origin.y+2, postLikeText.frame.height*3/4, postLikeText.frame.height*3/4)
        
        infoView.addSubview(likeImageView)
        infoView.addSubview(likeButtonSmall)
        infoView.addSubview(postLikeText)
        infoView.addSubview(moreButton)
        
        cell!.contentView.addSubview(likeButtonWrapper)
        cell!.contentView.addSubview(infoView)
        
        //var sepImageView = UIImageView()
        var sepImageView = UIView()
        
        //sepImageView.frame = CGRectMake((11/320)*self.view.frame.width, (infoView.frame.origin.y+infoView.frame.height-((2/568)*self.view.frame.height)), (298/320)*self.view.frame.width,(1/568)*self.view.frame.height)
        //sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height-((2/568)*self.view.frame.height)), self.view.frame.width,(1/568)*self.view.frame.height)
        
        //sepImageView.image = UIImage(named:"sep-line.png")
        
        
        var timeLabel = UILabel()
        
        
        print(postType)
        
        
        
        timeLabel.font = UIFont(name: "AvenirNext-Medium", size: 12.0)
        
        timeLabel.textColor = UIColor(red: 145.0/255, green: 145.0/255, blue: 145.0/255, alpha: 1)
        
        timeLabel.textAlignment = .Left
        
        
        
        
        if (arrdata[indexPath.row].valueForKey("time") != nil)
        {
            
            let strtemp = arrdata[indexPath.row].valueForKey("time") as! String
            
            let str = timedifference(strtemp)
            
            timeLabel.text = "  \(str) "
        }

        
        
        if(postType == "Note")
        {
            //sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height+((18/568)*self.view.frame.height)), self.view.frame.width,(1/568)*self.view.frame.height)
            sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height-((11/568)*self.view.frame.height)) , 0.4*self.view.frame.width,1)
            
            timeLabel.frame = CGRectMake(sepImageView.frame.size.width, (infoView.frame.origin.y+infoView.frame.height-(17/568)*self.view.frame.height), 0.5*self.view.frame.width, 15)
        }
        else
        {
            sepImageView.frame = CGRectMake(0, (infoView.frame.origin.y+infoView.frame.height-((7/568)*self.view.frame.height)), 0.4*self.view.frame.width,1)
            
            timeLabel.frame = CGRectMake(sepImageView.frame.size.width, (infoView.frame.origin.y+infoView.frame.height-(13/568)*self.view.frame.height), 0.5*self.view.frame.width, 15)
        }
        
        print(timeLabel.frame)
        
        sepImageView.backgroundColor = UIColor(red: 229.0/255, green: 229.0/255, blue: 229.0/255, alpha: 1)
        
        cell!.contentView.addSubview(sepImageView)
        cell!.contentView.addSubview(timeLabel)
        
        cell?.selectionStyle = .None
        
        if indexPath.row == arrdata.count-3
        {
            //print(arrdata)
            // *** STEP 4: RECEIVE MESSAGES FROM FIREBASE (limited to latest 25 messages)
        }
        
        if(indexPath.row == arrdata.count-1)
        {
            sepImageView.hidden = true
        }
        else
        {
            sepImageView.hidden = false
        }
        
        let m = arrdata.valueForKey("count").objectAtIndex(arrdata.count-1) as! Int
        if (indexPath.row == arrdata.count-1 && m > 1)
        {
            // updateFirebase()
            checkforreload()
            // view.userInteractionEnabled = false
        }
        return cell!
    }
    
    func checkforreload ()
    {
        if arrdata.count > 0
        {
            updateFirebase()
            activityIndicatorView.startAnimation()
        }
    }

    func showMoreOptions(sender: UIButton) {
        ////println("More button clicked")
        
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Share", "Flag")
        actionSheet.tag = sender.tag
        actionSheet.showInView(self.view)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        ////println("ActionSheet clickedButtonAtIndex \(buttonIndex)")
        switch (buttonIndex){
        case 0:
            print("Cancel")
            // Nothing to do. Popup menu closes.
        case 1:
            ////println("Share")
            shareSelectedPost(actionSheet.tag)
        case 2:
            ////println("Share")
            flagMessage(actionSheet.tag)
        default:
            print("Default")
            // Should not happen here.
        }
    }
    
    
    
    func flagMessage(indexId:Int)
    {
        let refreshAlert = UIAlertController(title: "Alert", message: "Are you sure you want to Flag this content?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            
        }))
        refreshAlert.addAction(UIAlertAction(title: "Flag", style: .Default, handler: { (action: UIAlertAction) in
            
            
            self.sendEmail(indexId)
            
        }))
        
        self.presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    
    
    func sendEmail(indexId:Int)
    {
        print(currentSharedEvent["eventCreatorObjectId"] as! String)
        
        let eventCreatorObjectId = currentSharedEvent["eventCreatorObjectId"] as! String
        
        
        let userQuery = PFUser.query()
        userQuery?.whereKey("objectId", equalTo:eventCreatorObjectId)
        ParseOperations.instance.fetchData(userQuery!, target: self, successSelector: "fetchedUserDataSuccess:", successSelectorParameters:indexId, errorSelector:"fetchedUserDataError:", errorSelectorParameters: nil)
        
    }
    
    
    
    func timedifference(dateString: String)->String
    {
        let date = NSDate()
        
        //let messageTimezoneOffset = resultSet.intForColumn("timezoneOffset")
        
        let currentTimeStamp = Int64(date.timeIntervalSince1970*1000)
        
        
        // let timezoneOffset = NSTimeZone.localTimeZone().secondsFromGMT*1000
        
        let timeDiff = Int64(currentTimeStamp - Int64(dateString)!)
        
        
        let nYears = timeDiff / (1000*60*60*24*365)
        
        
        let nMonths = (timeDiff % (1000*60*60*24*365)) / (1000*60*60*24*30)
        
        
        let nDays = ((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) / (1000*60*60*24)
        
        let nHours = (((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) / (1000*60*60)
        
        
        let nMinutes = ((((timeDiff % (1000*60*60*24*365)) % (1000*60*60*24*30)) % (1000*60*60*24)) % (1000*60*60)) / (1000*60)
        //println(nMinutes)
        
        var timeMsg = ""
        
        if nYears > 0
        {
            var yearWord = "years"
            if nYears == 1
            {
                yearWord = "year"
            }
            
            timeMsg = "about \(nYears) \(yearWord) ago"
        }
        else if nMonths > 0
        {
            var monthWord = "months"
            if nMonths == 1
            {
                monthWord = "month"
            }
            
            timeMsg = "about \(nMonths) \(monthWord) ago"
        }
        else if nDays > 0
        {
            var dayWord = "days"
            if nDays == 1
            {
                dayWord = "day"
            }
            
            timeMsg = "about \(nDays) \(dayWord) ago"
        }
        else if nHours > 0
        {
            var hourWord = "hours"
            if nHours == 1
            {
                hourWord = "hour"
            }
            
            timeMsg = "about \(nHours) \(hourWord) ago"
        }
        else if nMinutes > 0
        {
            var minuteWord = "minutes"
            if nMinutes == 1
            {
                minuteWord = "minute"
            }
            
            timeMsg = "about \(nMinutes) \(minuteWord) ago"
        }
        else
        {
            timeMsg = "just now"
        }
        
        return timeMsg
    }
    
    func fetchedUserDataSuccess(timer:NSTimer)
    {
        let objects = timer.userInfo?.valueForKey("internal") as? [PFObject]
        
        let indexId = timer.userInfo?.valueForKey("external") as! Int
        print(indexId)
        
        
        
        
        var eventCreatorObjectId = ""
        var eventCreatorEmailId = ""
        var eventCreatorName = ""
        
        
        
        print(currentSharedEvent)
        
        if let userData = objects
        {
            for hostData in userData
            {
                
                print(hostData)
                
                eventCreatorObjectId = hostData.objectId!
                eventCreatorName = hostData["fullUserName"] as! String
                eventCreatorEmailId = hostData["email"] as! String
                
            }
        }
        
        let eventId = currentSharedEvent.objectId!
        let reportedId = currentUserId
        let reportedName = fullUserName
        let reporterEmail = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
        
        
        var tempstr = arrdata[indexId]["postType"] as! String
        var tempstr1 = ""
        if tempstr == "Note"
        {
            tempstr1 = arrdata[indexId]["postData"] as! String
        }
        else
        {
            tempstr1 = "\(arrdata[indexId]["eventFolder"] as! String)\(arrdata[indexId]["postData"] as! String)"
        }
        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Flag/\(currentSharedEvent.objectId!)")
        
        messagesRef.childByAutoId().setValue([
            "content":tempstr1,
            "contentType":arrdata[indexId]["postType"] as! String,
            "ownerId":eventCreatorObjectId,
            "postId" : arrdata[indexId]["keyvalue"] as! String,
            "reporterId" : currentUserId
            
            ])
        
        
        var tempstr2 = ""
        if tempstr == "Note"
        {
            tempstr2 = arrdata[indexId]["postData"] as! String
        }
        else
        {
            tempstr2 = String("http://d3a1uesrqnd2ko.cloudfront.net/\(arrdata[indexId]["eventFolder"] as! String)\(arrdata[indexId]["postData"] as! String)")
        }
        print (tempstr2)
        let messageString = "Owner ID:\(eventCreatorObjectId) <br/> Owner Name: \(eventCreatorName)<br/> Owner Email: \(eventCreatorEmailId) <br/> Event ID: \(eventId) <br/>   Reported ID: \(reportedId) <br/> Reporter Name: \(reportedName) <br/> Reporter Email: \(reporterEmail)<br/><br/><br/> Content:\(tempstr2)"
        
        
        var sendReportEmail = AbusiveReportEmail()
        
        sendReportEmail.sendEmail("Content Abuse Report", message: messageString, emails: ["support@eventnode.co"])
        
    }
    func fetchedUserDataError(timer:NSTimer)
    {
        print("Error")
    }
    
    // TODO(geetikak): Refactor common code in EventPhotosVC and StreamVC based on role and permission model.
    // This is to support share content to various social networks, mail, messages, etc.
    func shareSelectedPost(row: Int) {
        let postType = arrdata[row]["postType"] as! String!
        
        ////println("Share Selected Post \(postType) \(row)")
        
        if(postType == "text") {
            let textPostToShare : String = arrdata[row]["postData"] as! String
            let objectsToShare : [String] = [textPostToShare]
            
            let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
            self.navigationController!.presentViewController(activityVC,
                animated: true,
                completion: nil)
        }
        
        if(postType == "image" || postType == "video") {
            let fileName = arrdata[row]["postData"] as! String
            
            let eventFolder = arrdata[row]["eventFolder"] as! String
            
            let eventMediaFilePath = "\(documentDirectory)/\(fileName)"
            
            ////println(eventMediaFilePath)
            
            let manager = NSFileManager.defaultManager()
            if (manager.fileExistsAtPath(eventMediaFilePath)) {
                
                if (postType == "image") {
                    let shareImage = UIImage(named: eventMediaFilePath)
                    
                    let objectsToShare : [UIImage] = [shareImage!]
                    
                    let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
                    self.navigationController!.presentViewController(activityVC, animated: true, completion: nil)
                    
                } else {
                    let videoUrl = NSURL(fileURLWithPath: eventMediaFilePath)
                    
                    let objectsToShare : [NSURL] = [videoUrl]
                    
                    let activityVC = UIActivityViewController(activityItems:objectsToShare , applicationActivities: nil)
                    self.navigationController!.presentViewController(activityVC, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    @IBAction func openmessagecount(sender: AnyObject) {
        
        messagePopupView.hidden = false
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        // do something specific to cell content clicked.
    }
    
    
    @IBAction func UpdateButton(sender: UIButton)
    {
        sender.hidden = true
        isEventStreamUpdated = false
        noOfStreamUpdates = 0
        downloadData()
        updateData()
    }
    @IBAction func newupdatebutton(sender: AnyObject) {
        tableView.setContentOffset(CGPoint.zero, animated:true)
        newUpdatesButton.hidden = true
        
    }
}
