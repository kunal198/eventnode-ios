//
//  CreateChannelViewController.swift
//  Eventnode
//
//  Created by mrinal khullar on 2/12/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

class CreateChannelViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var channelNameTextField: UITextField!
    @IBOutlet weak var channelimageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var loaderSubView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var basicView: UIView!
    var imageHeightDiff:CGFloat = 0
    var imageData: UIImage!
    var currentUserId: String!
    var fullUserName: String!
    var originalEventLogoFile: String!
    var eventLogoFile: String!
    var originalEventLogoFileUrl: NSURL!
     var originalImageData: UIImage!
    var eventLogoFileUrl: NSURL!
    var frameX: CGFloat!
    var frameY: CGFloat!
    var isAfterImage = false
    var messagesRef: Firebase!
    var linkForStream = ""
    var linkForInviteMembers = ""
       override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("imageTapped"))
        channelimageView.addGestureRecognizer(tapGestureRecognizer)

        
        
        if isAfterImage == false
        {
            newEvent = PFObject(className:"Events")
            newEvent["eventDescription"] = ""
            
            newEvent["eventTitle"] = ""
            
            eventTitle = ""
            
        }
    
        
        if imageData != nil
        {
            let originalHeight = channelimageView.frame.height
            channelimageView.frame.size.height = 3*(self.view.frame.width)/4
            imageHeightDiff = channelimageView.frame.height - originalHeight
            channelimageView.image = imageData
            scrollView.contentSize.height = scrollView.frame.height + imageHeightDiff
        }

        
        channelNameTextField.text = eventTitle
        
        basicView.frame.origin.y = basicView.frame.origin.y + imageHeightDiff
        
        currentUserId = NSUserDefaults.standardUserDefaults().objectForKey("currentUserId") as! String
        fullUserName = NSUserDefaults.standardUserDefaults().objectForKey("fullUserName") as! String
        
        self.channelNameTextField.delegate = self;
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
        loaderSubView.addSubview(indicator)
        
        indicator.frame = CGRectMake(((loaderSubView.frame.width/2)-(indicator.frame.width/2)) , ((loaderSubView.frame.height/2)-(indicator.frame.height)), indicator.frame.width, indicator.frame.height)
        
        //showLoader("Creating Event...");
        
        indicator.startAnimating()
        
        
    }
    
    func showLoader(message: String)
    {
        let loadingMessage = UILabel()
        loadingMessage.text = "\(message)"
        loadingMessage.textColor = UIColor.whiteColor()
        
        loadingMessage.lineBreakMode = NSLineBreakMode.ByWordWrapping
        loadingMessage.numberOfLines = 2
        loadingMessage.font = UIFont(name: "AvenirNext-Regular", size: 12.0)
        
        loadingMessage.textAlignment = .Center
        
        loaderSubView.addSubview(loadingMessage)
        
        loadingMessage.frame = CGRectMake(0 , ((loaderSubView.frame.height/2)), loaderSubView.frame.width, 20)
    }
    
    func imageTapped()
    {
        eventTitle = channelNameTextField.text!
        
       
        
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary){
            
            
            
            //showLoader("Loading")
            
            let imag = UIImagePickerController()
            imag.delegate = self
            imag.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imag.mediaTypes = [kUTTypeImage as String]
            imag.allowsEditing = false
            
            self.presentViewController(imag, animated: true, completion: nil)
        }
        else
        {
            NSLog("failed")
          //  loaderView.hidden=true
        }

    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!)
    {
        let data = UIImagePNGRepresentation(image)
        
        
        self.loaderView.hidden = true
        let adjustPhotoVC = self.storyboard!.instantiateViewControllerWithIdentifier("AdjustPhotoViewController") as! AdjustPhotoViewController
        adjustPhotoVC.imageData = image
        adjustPhotoVC.isForRSVP = false
        self.navigationController?.pushViewController(adjustPhotoVC, animated: false)
        
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        NSLog("picker cancel.")
        //self.loaderView.hidden = true
        picker .dismissViewControllerAnimated(true, completion: nil)
    }

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    

    @IBAction func channelDescription(sender: AnyObject)
    {
        
    }
    
    
    
    func correctlyOrientedImage(image: UIImage) -> UIImage {
        
        if image.imageOrientation == UIImageOrientation.Up
        {
            return image
        }
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        if image.size.width > 700 || image.size.height > 700
        {
            image.drawInRect(CGRectMake(0, 0, image.size.width/1.5, image.size.height/1.5))
        }
        else
        {
            image.drawInRect(CGRectMake(0, 0, image.size.width, image.size.height))
            
        }
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return normalizedImage;
    }
    
    @IBAction func createChannelButton(sender: AnyObject)
    {
        myEventData = [PFObject]()
        let replaced = channelNameTextField.text!.stringByReplacingOccurrencesOfString(" ", withString: "", options: [], range: nil)
        var haveError: Bool = false
        
        var formatter = NSDateFormatter();
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ";
        //let defaultTimeZoneStr = formatter.stringFromDate(date);
        // "2014-07-23 11:01:35 -0700" <-- same date, local, but with seconds
        //rahul formatter.timeZone = NSTimeZone.localTimeZone()
        formatter.timeZone = NSTimeZone(name: currentTimeZone)
        let timezoneOffset = Double(formatter.timeZone.secondsFromGMT)
        
        if(channelNameTextField.text == "" || replaced == "")
        {
            let refreshAlert = UIAlertController(title: "Error", message: "Please enter the name of the event.", preferredStyle: UIAlertControllerStyle.Alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction) in
                
            }))
            self.presentViewController(refreshAlert, animated: true, completion: nil)
        }
        else
        {
            showLoader("Creating Channel...")
            
            
            
            newEvent["isRSVP"] = false
           // newEvent["eventDescription"] = ""
            newEvent["eventLocation"] = ""
            
            newEvent["eventLatitude"] = 0
            newEvent["eventLongitude"] = 0
            newEvent["eventStartDateTime"] = NSDate()
            newEvent["eventEndDateTime"] = NSDate()
            newEvent["timezoneName"] = currentTimeZone
            newEvent["timezonedisplayname"] = currentTimeZoneName
        }
            newEvent["eventTimezoneOffset"] = timezoneOffset
        
        if !haveError
        {
            let date = NSDate()
            let currentTimeStamp = String(Int64(date.timeIntervalSince1970*1000))
            
            loaderView.hidden=false
            
            originalEventLogoFile = "\(currentTimeStamp)_\(currentUserId)_originaleventlogo.png"
            
            eventLogoFile = "\(currentTimeStamp)_\(currentUserId)_eventlogo.png"
            
            self.originalEventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(self.originalEventLogoFile))
            
            if imageData != nil
            {
                let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(self.originalImageData!), 0.5)
                
                originaldata!.writeToURL(self.originalEventLogoFileUrl!, atomically: true)
                
                let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                eventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(eventLogoFile))
                
                var cropdata = UIImageJPEGRepresentation(self.correctlyOrientedImage(imageData!), 0.5)
                var result = cropdata!.writeToURL(eventLogoFileUrl!, atomically: true)
                
                var tblFields: Dictionary! = [String: String]()
                
                tblFields["objectId"] = "hi"
                
                var myString = self.channelNameTextField.text
                myString = myString!.capitalizedString
               
                tblFields["eventTitle"] = myString
                
                tblFields["eventTitle"] = myString
                tblFields["eventImage"] = self.eventLogoFile
                tblFields["originalEventImage"] = self.originalEventLogoFile
                
                tblFields["eventDescription"] = newEvent["eventDescription"] as? String
                
                let frameX = self.frameX
                let frameY = self.frameY
                
                tblFields["frameX"] = "\(frameX)"
                tblFields["frameY"] = "\(frameY)"
                tblFields["eventCreatorObjectId"] = self.currentUserId
                tblFields["senderName"] = self.fullUserName
                
                tblFields["eventFolder"] = "\(self.currentUserId)/eventProfileImages/"
                tblFields["timezoneName"] = "\(currentTimeZone)"
                
             
                
                    tblFields["isRSVP"] = "0"
                
                
                var insertedId = ModelManager.instance.addTableData("Events", primaryKey: "eventId", tblFields: tblFields)
                
                if insertedId>0
                {
                    newEvent["eventId"] = insertedId
                    var timer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("uploadEvent:"), userInfo: insertedId, repeats: false)
                }
                else
                {
                    self.loaderView.hidden = true
                    Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
                }
            }
            else
            {
                Util.invokeAlertMethod("", strBody: "Please select event profile image first.", delegate: nil)
                loaderView.hidden = true
            }
            
        }
        
        
    }
    
    func uploadEvent(timer: NSTimer)
    {
        let insertedId = timer.userInfo as! Int
        
        if MyReachability.isConnectedToNetwork()
        {
            let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            uploadRequest.bucket = "eventnodepublicpics"
            uploadRequest.key =  "\(currentUserId)/eventProfileImages/\(eventLogoFile)"
            uploadRequest.body = eventLogoFileUrl
            
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
            
            upload(uploadRequest, isOriginal: false, insertedId: insertedId)
        }
        else
        {
            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelEventsViewController") as! ChannelEventsViewController
            self.navigationController?.pushViewController(homeVC, animated: false)
        }
    }
    
    func upload(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int)
    {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                            })
                            break;
                        default:
                            self.loaderView.hidden=true
                            self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                            
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                        
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                    
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetError(uploadRequest, isOriginal: isOriginal, insertedId: insertedId)
                
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    
                    
                    if(isOriginal == true){
                        
                        
                        let eventDetails = PFObject(className:"Events")
                        
                        var myString = self.channelNameTextField.text
                        myString = myString!.capitalizedString
                       
                        
                        
                        eventDetails["eventTitle"] = myString
                        
                        NSUserDefaults.standardUserDefaults().setValue(self.channelNameTextField.text, forKey: "EventTitle")
                        eventDetails["eventImage"] = self.eventLogoFile
                        eventDetails["originalEventImage"] = self.originalEventLogoFile
                        eventDetails["frameX"] = self.frameX
                        eventDetails["frameY"] = self.frameY
                        eventDetails["eventCreatorObjectId"] = self.currentUserId
                        eventDetails["senderName"] = self.fullUserName!
                        
                        eventDetails["eventFolder"] = "\(self.currentUserId)/eventProfileImages/"
                        
                       
                            eventDetails["isRSVP"] = false
                        
                            eventDetails["eventDescription"] = newEvent["eventDescription"] as! String
                        
                            eventDetails["eventLatitude"] = 0
                            eventDetails["eventLongitude"] = 0
              
                        
                        eventDetails["eventId"] = newEvent["eventId"]
                        eventDetails["eventTimezoneOffset"] = newEvent["eventTimezoneOffset"]
                        //                        eventDetails["timezonedisplayname"]
                        eventDetails["isUpdated"] = false
                        
                        self.createEvent(eventDetails, insertedId: insertedId)
                    }
                    else
                    {
                        
                        
                        
                        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
                        
                        let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
                        
                        uploadRequest.bucket = "eventnodepublicpics"
                        uploadRequest.key =  "\(self.currentUserId)/eventProfileImages/\(self.originalEventLogoFile)"
                        uploadRequest.body = self.originalEventLogoFileUrl
                        
                        uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead
                        
                        
                        self.upload(uploadRequest, isOriginal: true, insertedId: insertedId)
                    }
                    
                })
            }
            return nil
        }
    }
    
    
    func internetError(uploadRequest: AWSS3TransferManagerUploadRequest, isOriginal: Bool, insertedId: Int){
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    func createEvent(eventObject:PFObject, insertedId: Int)
    {
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createEventSuccess:", successSelectorParameters: insertedId, errorSelector: "createEventError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    func createEventSuccess(timer:NSTimer)
    {
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        
        // Log New Event metric.
        let creatorId = eventObject["eventCreatorObjectId"] as! String
        let isRsvp = eventObject["isRSVP"] as! Bool
        AnalyticsModel.instance.logNewEventEvent(creatorId, isRsvp: isRsvp)
        
        let eventId = timer.userInfo?.valueForKey("external") as! Int!
        //        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/messages/GroupChat/")
        //
        //        messagesRef.childByAppendingPath(eventObject.objectId!)
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId!
        tblFields["isPosted"] = "1"
        
        var date = ""
        var createdAt = ""
        var updatedAt = ""
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            
            tblFields["createdAt"] = date
            createdAt = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            
            tblFields["updatedAt"] = date
            updatedAt = date
        }
       
        
        currentEvent = eventObject
        
        
        let Device = UIDevice.currentDevice()
        
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let iOS9 = iosVersion >= 9
        
        if iOS9
        {
            let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            branchUniversalObject.title = "\(eventTitle)"
            branchUniversalObject.contentDescription = "My Content Description"
            // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
            branchUniversalObject.addMetadataKey("eventObjectId", value: eventObject.objectId!)
            branchUniversalObject.addMetadataKey("emailId", value: "noemail")
            branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
            branchUniversalObject.addMetadataKey("isRSVP", value:"false")
            branchUniversalObject.addMetadataKey("addToStream", value:"false")
            branchUniversalObject.addMetadataKey("inviteMembers", value:"false")
            branchUniversalObject.addMetadataKey("notifType", value:"createChannel")
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            linkProperties.channel = "email"
            linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/view_event/\(eventObject.objectId!)")
            linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/view_event/\(eventObject.objectId!)")
            linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/view_event/\(eventObject.objectId!)")
            
            branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        tblFields["socialSharingURL"] = url! as String
                        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [currentEvent["eventId"] as! Int])
                        
                        eventObject["socialSharingURL"] = url! as String
                        
                        eventObject.saveInBackground()
                        
                        
                    }
            })
            
        }
        else
        {
            var data = [
                "eventObjectId": eventObject.objectId!,
                "emailId": "noemail",
                "eventCreatorId": "\(currentUserId!)",
                "isRSVP": "false"
            ]
            
            Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                
                if error == nil
                {
                   
                    tblFields["socialSharingURL"] = url! as String
                    var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
                    
                    currentEvent["socialSharingURL"] = url! as String
                    eventObject["socialSharingURL"] = url! as String
                    
                    eventObject.saveInBackground()
                    
                }
                
            })
            
        }
        
        
        
        var isUpdated = ModelManager.instance.updateTableData("Events", tblFields: tblFields, whereString: "eventId=?", whereFields: [eventId])
        if isUpdated {
            
            let localNotification = UILocalNotification()
            
            localNotification.fireDate = NSDate()
            
            localNotification.alertBody = "Congrats you just created a Channel. Don’t forget to invite your friends"
            
            UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
            
            let email = NSUserDefaults.standardUserDefaults().objectForKey("email") as! String
            
            let eventCreatorId = eventObject["eventCreatorObjectId"] as! String
            
            
            let data = [
                "alert" : "Congrats you just created a Channel. Don’t forget to invite your friends",
                "notifType" :  "eventCreate",
                "eventObjectId": eventObject.objectId!,
                "objectId": "\(eventId)",
                "createdAt": "\(createdAt)",
                "updatedAt": "\(updatedAt)",
                "isUpdated": "\(isUpdated)",
                "emailId": "\(email)",
                "eventCreatorId": "\(eventCreatorId)",
                 "isRSVP": "false"
            ]
            
            var urlString = ""
            
            let Device = UIDevice.currentDevice()
            
            let iosVersion = NSString(string: Device.systemVersion).doubleValue
            
            let iOS9 = iosVersion >= 9
            
            
            
            self.linkForStream = ""
            self.linkForInviteMembers = ""
            
           
            
            let linkForInvite: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            linkForInvite.title = "\(eventTitle)"
            linkForInvite.contentDescription = "My Content Description"
            // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
            linkForInvite.addMetadataKey("eventObjectId", value: eventObject.objectId!)
            linkForInvite.addMetadataKey("emailId", value: "noemail")
            linkForInvite.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
            linkForInvite.addMetadataKey("isRSVP", value:"false")
            linkForInvite.addMetadataKey("notifType", value:"invitemembers")
            
            let linkProperties: BranchLinkProperties = BranchLinkProperties()
            linkProperties.feature = "sharing"
            //linkProperties.channel = "email"
            linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")
            linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")
            linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")

            linkForInvite.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        
                        self.linkForInviteMembers = url!
                        
                    }
                })
            
            
            let linkForAddToStream: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
            linkForAddToStream.title = "\(eventTitle)"
            linkForAddToStream.contentDescription = "My Content Description"
            // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
            linkForAddToStream.addMetadataKey("eventObjectId", value: eventObject.objectId!)
            linkForAddToStream.addMetadataKey("emailId", value: "noemail")
            linkForAddToStream.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
            linkForAddToStream.addMetadataKey("isRSVP", value:"false")
            linkForAddToStream.addMetadataKey("addToStream", value:"false")
            linkForAddToStream.addMetadataKey("inviteMembers", value:"false")
            linkForAddToStream.addMetadataKey("notifType", value:"createChannel")
            
            let linkForAddToStreamProperties: BranchLinkProperties = BranchLinkProperties()
            linkForAddToStreamProperties.feature = "sharing"
            //linkProperties.channel = "email"
            linkForAddToStreamProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")
            linkForAddToStreamProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")
            linkForAddToStreamProperties.addControlParam("$android_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)")
            
            linkForAddToStream.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                { (url: String?, error: NSError?) -> Void in
                    if error == nil
                    {
                        
                        self.linkForStream  = url!
                        
                    }
            })

            
            print(currentUserId)
            
            if iOS9
            {
                let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
                branchUniversalObject.title = "\(eventTitle)"
                branchUniversalObject.contentDescription = "My Content Description"
                // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
                branchUniversalObject.addMetadataKey("eventObjectId", value: eventObject.objectId!)
                branchUniversalObject.addMetadataKey("emailId", value: "noemail")
                branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
                branchUniversalObject.addMetadataKey("isRSVP", value:"false")
                branchUniversalObject.addMetadataKey("addToStream", value:"false")
                branchUniversalObject.addMetadataKey("inviteMembers", value:"false")
                branchUniversalObject.addMetadataKey("notifType", value:"createChannel")
                
                let linkProperties: BranchLinkProperties = BranchLinkProperties()
                linkProperties.feature = "sharing"
                //linkProperties.channel = "email"
                linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)#menu3")
                linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)#menu3")
                linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/manage_channel/\(eventObject.objectId!)#menu3")
                
                
                
                
                branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
                    { (url: String?, error: NSError?) -> Void in
                        if error == nil
                        {
                           
                            
                            
                            

                            
                            urlString = url!
                            self.sendEventCreationEmail(eventObject, urlString: urlString, email: email,addToStreamLink: self.linkForStream,inviteMembers: self.linkForInviteMembers)
                        }
                        else
                        {
                            self.sendEventCreationEmail(eventObject, urlString: urlString, email: email,addToStreamLink: self.linkForStream,inviteMembers: self.linkForInviteMembers)
                        }
                        
                })
            }
            else
            {
                Branch.getInstance().getShortURLWithParams(data, andChannel: "email", andFeature: BRANCH_FEATURE_TAG_SHARE, andCallback: {(url:String?, error:NSError?) -> Void in
                    
                    if error == nil
                    {
                        
                        urlString = url!
                        self.sendEventCreationEmail(eventObject, urlString: urlString, email: email,addToStreamLink: self.linkForStream,inviteMembers: self.linkForInviteMembers)
                    }
                    else
                    {
                        self.sendEventCreationEmail(eventObject, urlString: urlString, email: email,addToStreamLink: self.linkForStream,inviteMembers: self.linkForInviteMembers)
                    }
                    
                })
                
                
            }
            
            
            
            
            
        } else {
            
        }
        
        isUpdated = true
        
        
        var tblFieldsPost: Dictionary! = [String: String]()
        tblFieldsPost["objectId"] = ""
        tblFieldsPost["postData"] = "\(self.originalEventLogoFile)"
        tblFieldsPost["isApproved"] = "0"
        tblFieldsPost["postHeight"] = "\(self.originalImageData!.size.height)"
        tblFieldsPost["postWidth"] = "\(self.originalImageData!.size.width)"
        tblFieldsPost["postType"] = "image"
        tblFieldsPost["eventObjectId"] = "\(currentEvent.objectId!)"
        tblFieldsPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
        
        messagesRef = Firebase(url: "https://eventnode-rt.firebaseio.com/Stream/\(currentEvent.objectId!)")
        
        messagesRef.childByAutoId().setValue([
            "postData":"\(self.originalEventLogoFile)",
            "postHeight":String(format: "%.0f", Double((self.originalImageData!.size.height))),
            "postWidth":String(format: "%.0f", Double((self.originalImageData!.size.width))),
            "postType" :"image",
            "userObjectId" : "\(self.currentUserId)",
            "eventFolder" : "\(self.currentUserId)/\(currentEvent.objectId!)/",
            "count" : 1
            
            
            ])
        

        
        var insertedId = ModelManager.instance.addTableData("EventImages", primaryKey: "eventImageId", tblFields: tblFieldsPost)
        if insertedId>0
        {
            let transferManager = AWSS3TransferManager.defaultS3TransferManager()
            self.originalEventLogoFileUrl = NSURL(fileURLWithPath: (documentDirectory as NSString).stringByAppendingPathComponent(self.originalEventLogoFile))
            let uploadRequest : AWSS3TransferManagerUploadRequest = AWSS3TransferManagerUploadRequest()
            
            let originaldata = UIImageJPEGRepresentation(self.correctlyOrientedImage(self.originalImageData!), 0.5)
            originaldata!.writeToURL(self.originalEventLogoFileUrl!, atomically: true)
            uploadRequest.bucket = "eventnode1"
            uploadRequest.key =  "\(self.currentUserId)/\(currentEvent.objectId!)/\(self.originalEventLogoFile)"
            uploadRequest.body = self.originalEventLogoFileUrl
            uploadRequest.ACL = AWSS3ObjectCannedACL.PublicRead

            self.uploadFirstPost(uploadRequest, insertedId: insertedId)
        }
        else
        {
            self.loaderView.hidden = true
            Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
        }
    }
    
    
    func linkForInviteMembers (eventObjectId:String) -> String
    {
        
        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
        branchUniversalObject.title = "\(eventTitle)"
        branchUniversalObject.contentDescription = "My Content Description"
        // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
        branchUniversalObject.addMetadataKey("eventObjectId", value: eventObjectId)
        branchUniversalObject.addMetadataKey("emailId", value: "noemail")
        branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
        branchUniversalObject.addMetadataKey("isRSVP", value:"false")
        branchUniversalObject.addMetadataKey("notifType", value:"invitemembers")
        
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"
        //linkProperties.channel = "email"
        linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/manage_channel/\(eventObjectId)")
        linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/manage_channel/\(eventObjectId)")
        linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/manage_channel/\(eventObjectId)")
        
        var urlString = ""
        
        branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
            { (url: String?, error: NSError?) -> Void in
                if error == nil
                {
                   
                    
                    self.linkForInviteMembers = url!
                    
                }
                else
                {
                    self.linkForInviteMembers = url!
                }
                
        })
        
        return linkForInviteMembers
        

    }
    
    func linkForAddToStream(eventObjectId:String) -> String
     {
        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "web.eventnode.co")
        branchUniversalObject.title = "\(eventTitle)"
        branchUniversalObject.contentDescription = "My Content Description"
        // branchUniversalObject.imageUrl = "https://example.com/mycontent-12345.png"
        branchUniversalObject.addMetadataKey("eventObjectId", value: eventObjectId)
        branchUniversalObject.addMetadataKey("emailId", value: "noemail")
        branchUniversalObject.addMetadataKey("eventCreatorId", value:"\(currentUserId)")
        branchUniversalObject.addMetadataKey("isRSVP", value:"false")
        branchUniversalObject.addMetadataKey("notifType", value:"addtostream")
        
        let linkProperties: BranchLinkProperties = BranchLinkProperties()
        linkProperties.feature = "sharing"
        //linkProperties.channel = "email"
        linkProperties.addControlParam("$desktop_url", withValue:"http://eventnode.co/manage_channel/\(eventObjectId)")
        linkProperties.addControlParam("$ios_url", withValue:"http://eventnode.co/manage_channel/\(eventObjectId)")
        linkProperties.addControlParam("$android_url", withValue:"http://eventnode.co/manage_channel/\(eventObjectId)")
        
        var urlString = ""
        
        branchUniversalObject.getShortUrlWithLinkProperties(linkProperties,  andCallback:
            { (url: String?, error: NSError?) -> Void in
                if error == nil
                {
                    
                    
                    self.linkForStream = url!
                    
                }
                else
                {
                    self.linkForStream = url!
                }
                
        })
        
        return linkForStream
        
    }
    
    
    func uploadFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int) {
        let transferManager = AWSS3TransferManager.defaultS3TransferManager()
        
        transferManager.upload(uploadRequest).continueWithBlock { (task) -> AnyObject! in
            if let error = task.error {
                if error.domain == AWSS3TransferManagerErrorDomain as String {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error.code) {
                        switch (errorCode) {
                        case .Cancelled, .Paused:
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                self.loaderView.hidden=true
                                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            })
                            break;
                            
                        default:
                            self.loaderView.hidden=true
                            self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                            
                            break;
                        }
                    } else {
                        self.loaderView.hidden=true
                        self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                        
                    }
                } else {
                    self.loaderView.hidden=true
                    self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                    
                }
            }
            
            if let exception = task.exception {
                self.loaderView.hidden=true
                self.internetErrorForFirstPost(uploadRequest, insertedId: insertedId)
                
            }
            
            if task.result != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    
                    myNewPost = PFObject(className:"EventImages")
                    myNewPost["postData"] = self.originalEventLogoFile
                    myNewPost["postHeight"] = self.originalImageData!.size.height
                    myNewPost["postWidth"] = self.originalImageData!.size.width
                    myNewPost["postType"] = "image"
                    myNewPost["eventObjectId"] = currentEvent.objectId!
                    myNewPost["eventFolder"] = "\(self.currentUserId)/\(currentEvent.objectId!)/"
                    
                    self.createFirstPost(myNewPost, insertedId: insertedId)
                    
                })
            }
            return nil
        }
    }
    
    func internetErrorForFirstPost(uploadRequest: AWSS3TransferManagerUploadRequest, insertedId: Int){
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        channelNameTextField.resignFirstResponder()
        return true
    }
    
    func createFirstPost(eventObject:PFObject, insertedId: Int)
    {
        ParseOperations.instance.saveData(eventObject, target: self, successSelector: "createFirstPostSuccess:", successSelectorParameters: insertedId, errorSelector: "createFirstPostError:", errorSelectorParameters:[eventObject,insertedId])
    }
    
    func createFirstPostSuccess(timer:NSTimer)
    {
        self.loaderView.hidden=true
        isPostUpdated = true
        
        let eventObject = timer.userInfo?.valueForKey("internal") as! PFObject!
        let postId = timer.userInfo?.valueForKey("external") as! Int!
        
        var tblFields: Dictionary! = [String: String]()
        
        tblFields["objectId"] = eventObject.objectId
        tblFields["isPosted"] = "1"
        
        var date = ""
        
        if eventObject.createdAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.createdAt)!)
            
            tblFields["createdAt"] = date
        }
        
        if eventObject.updatedAt != nil {
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            date = dateFormatter.stringFromDate((eventObject.updatedAt)!)
            
            tblFields["updatedAt"] = date
        }
        
        
        
        let isUpdated = ModelManager.instance.updateTableData("EventImages", tblFields: tblFields, whereString: "eventImageId=?", whereFields: [postId])
        if isUpdated {
            
        } else {
            
        }
        
        let inviteFriendsVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelInviteFirstViewController") as! ChannelInviteFirstViewController
        
        inviteFriendsVC.isFromChannel = true
        
        self.navigationController?.pushViewController(inviteFriendsVC, animated: true)
    }
    
    func createFirstPostError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        isUpdated = false
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
    }
    
    
    func sendEventCreationEmail(eventObject: PFObject, urlString: String, email: String,addToStreamLink: String, inviteMembers: String)
    {
        
        let eventTitleText = eventObject["eventTitle"] as! String
        
        let eventFolder = eventObject["eventFolder"] as! String!
        let eventImage = eventObject["eventImage"] as! String!
        let isRsvp = eventObject["isRSVP"] as! Bool
        
        
        var emailMessage = ""
        
 
            let channelCreate = ChannelCreationSuccess()
        
        
        emailMessage = channelCreate.emailMessage(eventObject.objectId!, eventTitle: eventTitleText, hostName: fullUserName, imageUrl: "https://s3.amazonaws.com/eventnodepublicpics/\(eventFolder)\(eventImage)", type: "online", url: urlString, addToStreamLink: addToStreamLink, InviteMemberlink: inviteMembers)
   
        
        
        
        let sendEmail = SendEmail()
        
        sendEmail.sendEmail(" Success! You just created a Channel.", message: emailMessage, emails:[email])
        
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool{
        
        var _char = string.cStringUsingEncoding(NSUTF8StringEncoding)
        
        if(string=="")
        {
            
        }
        
        if(string != "")
        {
            if channelNameTextField.text!.characters.count < 30 {
                return true
            }
            else
            {
                return false
            }
        }
        else
        {
            return true
        }
    }
    
    func createEventError(timer:NSTimer)
    {
        self.loaderView.hidden = true
        
        let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
        self.navigationController?.pushViewController(homeVC, animated: false)
        
    }
    
    
    @IBAction func channelDescriptionView(sender: AnyObject)
    {
        let eventDescriptionVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventDescriptionViewController") as! EventDescriptionViewController
        
        eventDescriptionVC.hTitle = "Channel Description"
        
        self.navigationController?.pushViewController(eventDescriptionVC, animated: true)
    }
    
    @IBAction func viewTapped(sender: AnyObject)
    {
        channelNameTextField.resignFirstResponder()
    }
    
    @IBAction func closeButton(sender: AnyObject)
    {
        
        let refreshAlert = UIAlertController(title: "Discard Changes", message:" Are you sure you want to discard these changes?", preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction) in
            
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Discard", style: .Default, handler: { (action: UIAlertAction) in
            //            let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("EventViewController") as! EventViewController
            //            self.navigationController?.pushViewController(homeVC, animated: false)
            
            let myEventsResultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "eventCreatorObjectId='\(self.currentUserId)'", whereFields: [])
            
            myEventsResultSet.next()
            
            let myEventsCount = Int(myEventsResultSet.intForColumn("count"))
            
            myEventsResultSet.close()
            
            let mySharedEventsResultSet: FMResultSet! = ModelManager.instance.getTableData("Events", selectColumns: ["count(*) as count"], whereString: "isRSVP = 0 AND objectId IN (SELECT eventObjectId FROM Invitations WHERE userObjectId='\(self.currentUserId)')", whereFields: [])
            
            mySharedEventsResultSet.next()
            
            let mySharedEventsCount = Int(mySharedEventsResultSet.intForColumn("count"))
            
            mySharedEventsResultSet.close()
            
            if (myEventsCount>0)
            {
                let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelEventsViewController") as! ChannelEventsViewController
                self.navigationController?.pushViewController(homeVC, animated: false)
            }
            else if(mySharedEventsCount>0)
            {
                let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("ChannelInvitedViewController") as! ChannelInvitedViewController
                self.navigationController?.pushViewController(homeVC, animated: false)
            }
            else
            {
                let homeVC = self.storyboard!.instantiateViewControllerWithIdentifier("OnboardingViewController") as! OnboardingViewController
                self.navigationController?.pushViewController(homeVC, animated: false)
            }
            
            //self.navigationController?.popViewControllerAnimated(false)
            
        }))
        self.presentViewController(refreshAlert, animated: true, completion: nil)
        
    }
    
}
