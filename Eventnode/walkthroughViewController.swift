//
//  walkthroughViewController.swift
//  Eventnode
//
//  Created by brst on 4/12/16.
//  Copyright © 2016 Empro Systems LLC. All rights reserved.
//

import UIKit

class walkthroughViewController: UIViewController,UIScrollViewDelegate
{

    @IBOutlet weak var walkthroughScrollView: UIScrollView!
    
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var firstPage: UIImageView!
    @IBOutlet weak var secondPage: UIImageView!
    @IBOutlet weak var thirdPage: UIImageView!
    @IBOutlet weak var fouthPage: UIImageView!
    @IBOutlet weak var fifthPage: UIImageView!
    var pageIndex: Int = 0
    var strTitle: String!
    var strPhotoName: String!
    
    var arrPagePhoto: NSArray = NSArray()
    var isFrom = ""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        walkthroughScrollView.delegate = self
        
        UIApplication.sharedApplication().statusBarHidden = true
        
        walkthroughScrollView.contentSize = CGSizeMake(self.view.frame.width * 5 , 0)
        
        secondPage.frame.origin.x = firstPage.frame.origin.x + firstPage.frame.size.width
        thirdPage.frame.origin.x = secondPage.frame.origin.x + secondPage.frame.size.width
        fouthPage.frame.origin.x = thirdPage.frame.origin.x + thirdPage.frame.size.width
        fifthPage.frame.origin.x = fouthPage.frame.origin.x + fouthPage.frame.size.width
        
        firstPage.image = UIImage(named: "Page 1.png")
        secondPage.image = UIImage(named:"Page 2.png")
        thirdPage.image = UIImage(named: "Page 3.png")
        fouthPage.image = UIImage(named: "Page 4.png")
        fifthPage.image = UIImage(named: "Page 5.png")
        
       // firstPage.image = UIImage(named: strPhotoName)

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
        var pageWidth : CGFloat = walkthroughScrollView.frame.size.width
       
        
        var fractionalPage : CGFloat!
        
        fractionalPage = walkthroughScrollView.contentOffset.x / walkthroughScrollView.frame.size.width
        
        if fractionalPage == 4.0
        {
            skipBtn.setTitle("DONE", forState: UIControlState.Normal)
        }
        else
        {
            skipBtn.setTitle("SKIP", forState: UIControlState.Normal)
        }
        
        
        
    }
    

    @IBAction func skipButton(sender: AnyObject)
    {
       if isFrom == "appdelegate"
       {
            let myEventsVC = self.storyboard!.instantiateViewControllerWithIdentifier("ViewController") as!  ViewController
            self.navigationController?.pushViewController(myEventsVC, animated: false)
       }
       else
       {
            self.navigationController?.popViewControllerAnimated(true)
       }
    }


}
